# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains the TAGARELA code. 

* It's a tutoring system for Portuguese and Russian learners

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Either search for the project "tagarela-java" or
click on the link in the invitation. 

Click on Fork and press "create repository".
Now you have set up the repository.

* Download the source code:

```
#!bash

git clone https://USERNAME@bitbucket.org/USERNAME/PROJECTNAME
```


* Configure tagarela on your system


* * Go to /tagarela-java/doc/tagarela-settings and follow the instructions


### Contribution guidelines ###

* Follow and extend the guidelines in

* * /tagarela-java/doc/EXTENDING

* Do this whenever you add something new to the project
* Sync commits on BitBucket website, Overview, before pulling
* Apply recent changes first, if necessary

```
#!bash

git pull
```
* Now you can edit the source code, when you are finished you can do

```
#!bash

git status
```

* Add a new file with

```
#!bash

git add yourNewFile
```

* After you changed an existing file or added a new file do

```
#!bash

git commit -m "what you changed/added" yourChangedOrAddedFile
```

* To finally upload it onto the repository do (this may differ from user to user)

```
#!bash

git push -u origin master
```
* Create a pull request

On bitbucket press on create pull request and create it.

Now everyone is able to see your changes. The admin can 
merge your changes into the repo, others only accept your changes.

 ### Who do I talk to? ###

* Repo owner or admin
* *  Eddi89