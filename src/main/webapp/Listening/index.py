# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: index.py,v 1.33 2008-07-31 14:06:31 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

from mod_python import psp,apache
from os.path import basename,dirname

t        = apache.import_module("../code/tagarela_utils.py")
xml2dict = apache.import_module("../code/xml2dict.py")
ajax     = apache.import_module("../ajax.py")



####
# main index handler
####

def index(req,modNr='1',exNr='1',questNr='1',ajaxAct='',timeStamp='',debug=''):

    # ensure we've got an authorized session
    sess = t.ensure_authorized_session(req)

    # set local variable to activity type (= name of dir containing this file):
    actType=basename(dirname(__file__))

    # read in activity specification 
    actSpec = t.getActSpec(modNr,exNr,__file__)

    # save current state of affairs (includes actSpec!) in user model:
    t.usermodel_put(sess,'current',t.plainDict(locals()))

    # write all information stored in the user model for this session to log
    t.log(req,t.usermodel_getall(sess))

    # create top of page template:
    (topTmpl,lastExNr) = t.getTopTmpl(req,modNr,exNr,__file__,
				      'Compreensão Auditiva',
				      actSpec['presentation']['instructions'])

    # create question template 
    questionTmpl = t.getQuestTmpl(req,actSpec,actType,modNr,exNr,lastExNr,questNr,actSpec['lastQuestNr'],
				  __file__,inputfield=1)

    # create feedback template object
    feedbackTmpl = psp.PSP(req, filename='../include/feedback.tmpl', vars = {'feedbacktext' : ''}) 

    # get names for audio and image information from activity specification:
    AudioFileName = 'Module%s/%s/%s'%(modNr,exNr,actSpec['presentation']['audio']['filename'])
    ImageFileName = 'Module%s/%s/%s'%(modNr,exNr,actSpec['presentation']['image']['filename'])
    ImageAlt      = 'Image of %s'%actSpec['presentation']['image']['alt']

    # create main page template object
    mainPageTmpl = psp.PSP(req, filename='include/index.tmpl',
                           vars = {'Top' : topTmpl,
			           'AudioFileName': AudioFileName,
				   'ImageFileName': ImageFileName,
				   'ImageAlt': ImageAlt,
                                   'Question' : questionTmpl,
                                   'Feedback' : feedbackTmpl})                                   

    # set result of this to be an html page
    req.content_type ='text/html ; charset=utf-8'

    # save the session variable
    sess.save()			                      

    if ajaxAct == 'updateQuestion':                   # if doing a partial page update (ajax)
	return ajax.updateQuestion(req,questionTmpl)  # then return updated question info
    else:                                             
	mainPageTmpl.run()                            # else run the page to generate the html
