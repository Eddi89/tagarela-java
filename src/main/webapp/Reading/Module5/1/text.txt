<img src="Module<%=modNr%>/<%=exNr%>/fernando.jpg">
<center><big><b>O que você fez na semana passada?</b></big></center><p></p>

Sou estudante de medicina e estudo na Universidade de São Paulo. Os meus dias são parecidos todas as semanas. 
<p></p>
Semana passada eu acordei às 6:30 todos os dias com exceção da terça, quando tive uma aula às 7:00. Por isso acordei às 6:00.
Na quarta-feira eu cheguei na universidade às 9:00 e tive duas aulas: anatomia e citologia. 
Fui almoçar com uns amigos ao meio-dia e depois fui para a minha aula de inglês. 
Eu estudo inglês duas vezes por semana. Depois do curso eu voltei para a universidade para mais uma aula.
<p></p>
Na quinta à tarde eu fui ao cinema com a minha namorada. Fomos ver um filme americano
e depois eu levei ela em casa. 
À noite, quando cheguei em casa li uns capítulos do meu livro de anatomia para minha
prova no dia seguinte. 
