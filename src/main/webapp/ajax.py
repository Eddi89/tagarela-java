"""
---------------------------------------------------------------
$Id: ajax.py,v 1.5 2008-07-31 15:26:23 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 


AJAX handling on the server side
--------------------------------
Top level functions called directly by the xmlhttp request

Each top level function 
 a) calls AJAXread to obtain the data dictionary passed along,
 b) processes that data and 
 c) calls AJAXwrite to return a dictionary {'status':0/1,
                                            'contents': {Key1: Val1,
                                                         Key2: Val2...}
 with each:
 - key: the id of the element to be updated
 - value: the html to be inserted into the innerHTML of the element
 # Also calls req.session.save() to make sure the session information
 is preserved for ajax calls in the same way as for normal handlers
--------------------------------------------------------------- 
"""

from mod_python import apache

analysis = apache.import_module("./analysis/analysis.py")
t        = apache.import_module("./code/tagarela_utils.py")
json     = apache.import_module("./code/json.py")


def analyzeLearnerInput(req,actType,modNr,exNr,questNr,ajaxArg):

    sess = t.ensure_authorized_session(req)

    data = AJAXread(req,ajaxArg)

    # initialize dictionary for logging with the 'current' info 
    logOutput = current = t.usermodel_get(sess,'current',{})

    # store dictionary with learner input in user model:
    if (current):
      t.usermodel_put(sess,'%s-%s-%s-%s'%(current['actType'],current['modNr'],current['exNr'],current['questNr']),data)
    else:
      t.log('WARNING: "current" was not found in user model, which should never happen!!')

    # deal with different configurations of learner input:
    if ('learnerInput1' in data) and ('learnerInput2' in data) and ('learnerInput3' in data):
      inputList = [data['learnerInput1'], data['learnerInput2'],data['learnerInput3']]
    elif ('learnerInput1' in data) and ('learnerInput2' in data):
      inputList = [data['learnerInput1'], data['learnerInput2']]
    else:
      # for any other dictionary, we just get list of all values in the data dictionary:
      inputList = data.values()
      # if there are more than one elements, this is unexpected (since the order probably
      # matters but is lost in the dictionary), so write a warning to the log:
      if len(inputList) != 1: 
        t.log(req,'WARNING: PROCESSING PAGE WITH UNEXPECTED INPUT FIELDS %s in activity %s ',(inputList,current))

    # run analysis for inputList 
    fieldAnalysis = analysis.processInput(sess,inputList)
    feedback = "<div id='FeedbackTextUnit'> %s </div>"%fieldAnalysis

    # write out input and analysis to log file
    logOutput['learnerInput'] = inputList
    logOutput['feedback'] = fieldAnalysis
    
    resultDict = {'FeedbackText': feedback}
    
    # write the information we want to log output to the log file
    t.log(req,logOutput)

    sess.save()

    return AJAXwrite(req,resultDict)

# -----------------------------------------------

def showSolution(req,actType,modNr,exNr,questNr,ajaxArg):

    sess = t.ensure_authorized_session(req)

    data = AJAXread(req,ajaxArg)

    # initialize dictionary for logging with the 'current' info 
    logOutput = current = t.usermodel_get(sess,'current',{})

    feedback = '<p><i>One way to answer this is:</i></p><p><b>%s</b></p>'%data['showSolution']

    resultDict = {'FeedbackText': feedback}

    # log feedback provided under key showSolution:
    logOutput['showSolution'] = feedback

    # write the information we want to log output to the log file
    t.log(req,logOutput)
    
    sess.save()

    return AJAXwrite(req,resultDict)


# -----------------------------------------------

def updateQuestion(req,questionTmpl):
    sess = t.ensure_authorized_session(req)

    # initialize dictionary for logging with the 'current' info 
    logOutput = current = t.usermodel_get(sess,'current',{})

    # json uses utf-8 encoding, so arg2 of updateQuestion must be utf-8.
    # if it were iso-8859-1, we could instead do somethign like
    # questionTmplUnicode = questionTmpl.decode('iso-8859-1').encode('utf-8')

    # return updated Question element, and erase FeedbackText element contents:
    resultDict = {'Question': questionTmpl,
                  'FeedbackText': ''}

    logOutput['Question'] = questionTmpl

    # write the information we want to log output to the log file
    t.log(req,logOutput)

    sess.save()

    return AJAXwrite(req,resultDict)

# -----------------------------------------------
# auxiliary predicates doing the actual AJAX/JSON handling

import sys, os

def AJAXread(req,ajaxArg):
    try:
        data = json.read(ajaxArg)
    except:
        return AJAXwrite(req,"Error reading JSON string...",1)
    return data

def AJAXwrite(req,msg,status=0):
    # set a header so that we know the ajax request succeeded (as opposed
    # to e.g. an elapsed session cookie or a timeout having stopped the request
    # so that handlestatechange  needs to reload a full page instead of 
    # doing a partial update on the page)
    req.headers_out['X-AJAX-Update'] = "True"

    resultDict = {"status": status}
    if status:
        # Errors are displayed in element "FeedbackText":
        resultDict["contents"] = {"FeedbackText": 
                               "There was an error on the server: "\
                               + msg}
    else:
        # Non-error msg must be a dictionary specifying element(s) as well: 
        resultDict["contents"] = msg
        
    return json.write(resultDict)
