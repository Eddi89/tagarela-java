This directory contains the TAGARELA ICALL system, an intelligent
web-based workbook for the instruction of Portuguese.

Project information with papers and slides of talks about the
project can be found at http://purl.org/net/tagarela

Copyright 2005-2007 by Luiz Amaral and Detmar Meurers

Freely available under the Creative Commons
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/ 

Here's a short description of the contents:

index.py     main tagarela handler
ajax.py      ajax callback handler

include/     directory containing html templates, javascript, css
             (i.e. stuff that ultimately is sent to the client)

images/      images used by main tagarela page

code/        python support code for the web interface

             This code is imported by the various handlers. For
	     safety reasons, the directory includes an .htaccess
	     specification which disallows direct execution of any
	     file in this directory, i.e. their use as a handler.
	  
analysis/    Python code (and Brazilian spell checking dictionary) 
             for analysis.

databases/   General user database (ids, passwords) and individual
             learner model databases plus python support code

Description/ Activity specifications
FIB/            
Listening/
Reading/
Rephrasing/
Vocabulary/

Each activity specification contains:
index.py    activity handler
include/    directory with html templates
Module1     activity specification for Module 1
Module2     activity specification for Module 2

Each Module contains directories 1, 2, 3, ...
for activity models 1, 2, 3, ...

This version now includes activity.xml files.
