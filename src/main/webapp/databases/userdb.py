"""
---------------------------------------------------------------
$Id: userdb.py,v 1.34 2008-07-31 14:06:32 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

#### some utilities for the user database

try:
    from bsddb3 import btopen
except ImportError:
    from bsddb import btopen

from os.path import join,dirname

# file to use for the user database:
user_database = join(dirname(__file__),'tagarela_user_database.bsddb')


# currently just a function, accessible for anyone
def add_user(id,passwd):
    userdb = btopen(user_database)
    userdb[id] = passwd
    userdb.close()
    
def check_user(id):
    userdb = btopen(user_database)
    try:
        value = userdb[id]
    except:
        # need to catch KeyError when id doesn't exist
        value = ''
    userdb.close()
    return value

def clean():
    userdb = btopen(user_database,'n')
    userdb.close()
    

def default_users():
    # start with a clean database:
    clean()
    # define some basic users with "minimal" passwords while we're testing
    add_user('tristan', 'bloomington')
    add_user('marti', 'autolearn')
    add_user('rob', 'reynolds')
    add_user('glaucia.cedroni', 'Gullar')
    add_user('rziai', 'moo')
    add_user('meurers.1','Detmar')
    add_user('amaral.1','Alexandre')
    add_user('grinstead.11','mexico')
    add_user('adriane','amelia')
    add_user('shain.3','Adam')
    add_user('flavia','canada')
    add_user('glaucia','columbus')
    add_user('tom','bossanova')
    add_user('pilar','jazz')
    add_user('megan','sunshine')
    add_user('ione','expert')
    add_user('paulo','brazilian')
    add_user('long.25','mexico')
    add_user('rodgers','education')
    add_user('trude','tauchen')
    # testing accounts for student profiles
    add_user('student1', 'student1')
    add_user('student2', 'student2')
    add_user('student3', 'student3')
    add_user('student4', 'student4')
    # add real student users below, using add_user('ID','PASSWD')
    add_user('reynolds.237','teresaanne')
    add_user('reeder.63','port101')
    add_user('vanscoyoc.2','a121234')
    add_user('bonnie','portuguese')
    add_user('ericmehl','ceara')
    add_user('burcham.15','d836724b')
    add_user('figueroamelo.1','jennfig')
    add_user('mullin.28','mullin87')
    add_user('hughes.1030','buckeye06')
    add_user('toussant.5','alex')
    add_user('mcintire.261','bife')
    # Students from Flavia's 502
    add_user('lang.167','lang502w')
    add_user('ehni.2','ehni502z')
    add_user('hernandez.147','hernandez502x')
    add_user('donnellan.7','donnellan502v')
    add_user('roeth.6','roeth502u')
    add_user('dreler.9','dreler502t')
    add_user('baxter.105','baxter502s')
    add_user('annaojanen','annaojanen502r')
    add_user('mace.56','mace502q')
    add_user('insabella.1','insabella502p')
    add_user('cornett.32','cornett502o')
    add_user('herrera.32','herrera502n')
    add_user('johnson.2807','johnson502m')
    add_user('ashford.11','ashford502k')
    add_user('morrison.314','morrison502j')
    add_user('king.969','king502i')
    add_user('young.1189','young502h')
    add_user('alza.1','alza502g')
    add_user('davis.2133','davis502f')
    add_user('cox.507','cox502e')
    add_user('moya.4','moya502d')
    add_user('escalera.2','escalera502c')
    add_user('salcedo.7','salcedo502b')
    add_user('blanco.17','blanco502a')
    # Students from Flavia's 202
    add_user('gonzalez.204','reynoldsburg')
    add_user('rayburn.35','miami')
    add_user('boggs.986','oibrasil')
    add_user('lindquist.23','imcool')
    add_user('lord.36','portland')
    add_user('rosanky','banana')
    add_user('murphy.664','murphy')
    add_user('queener.3','capoeira')
    add_user('mclaughlin.175','boasorteluiz')
    add_user('simon.191','enon')
    add_user('teeter.13','zebra')
    add_user('dawes.8','hoje')
    add_user('smith.4989','troy')
    add_user('garrett.141','dia25')
    add_user('mcqueary.5','wowee')
    # Other interested researchers
    add_user('olga','brian')
    add_user('ayelet','haifa')
    add_user('rjblake','texas')
    add_user('markus','sunnyindiana')
    add_user('linda','chalmers')
    add_user('melissa','lorrain')
    add_user('olena','mir')
    add_user('anne','gerolsteiner')
    add_user('nilzimar','viera')
    add_user('chrishill','esl')
    add_user('kcorl','tommy')
    add_user('jason','kirkwood')
    # OSU research commercialization office:
    add_user('tony','hungary')
    add_user('ramon','boykott')

    # Barcelona CL group	
    add_user('silvia.necsulescu','glicom')
    add_user('maria.fuentes.fort','barcelona-upc')

    # Xiaofei's students
    add_user('psu1','psu1')
    add_user('psu2','psu2')
    add_user('psu3','psu3')
    add_user('psu4','psu4')
    add_user('psu5','psu5')
    add_user('psu6','psu6')
    add_user('psu7','psu7')
    add_user('psu8','psu8')
    add_user('psu9','psu9')
    add_user('psu10','psu10')
    add_user('psu11','psu11')
    add_user('psu12','psu12')
    add_user('psu13','psu13')
    add_user('psu14','psu14')
    add_user('psu15','psu15')

    # Cambridge/EF
    add_user('rachel.baker','sla.flt')
    add_user('yerrie.kim','ef.assess')
    add_user('dora','rceal.ef')
    add_user('hugo.bezerra','ama1zon')


    # Testing
    add_user('test','tue-bin-gen')

    
    # Detmars Bangor students
    add_user('learner1','1renrael')
    add_user('learner2','2renrael')
    add_user('learner3','3renrael')
    add_user('learner4','4renrael')
    add_user('learner5','5renrael')
    add_user('learner6','6renrael')
    add_user('learner7','7renrael')
    add_user('learner8','8renrael')
    add_user('learner9','9renrael')
    add_user('learner10','01renrael')
    add_user('learner11','11renrael')
    add_user('learner12','21renrael')
    add_user('learner13','31renrael')
    add_user('learner14','41renrael')
    add_user('learner15','51renrael')
    add_user('learner16','61renrael')
    add_user('learner17','71renrael')
    add_user('learner18','81renrael')
    add_user('learner19','91renrael')
    add_user('learner20','02renrael')
    add_user('learner21','12renrael')
    add_user('learner22','22renrael')
    add_user('learner23','32renrael')
    add_user('learner24','42renrael')
    add_user('learner25','52renrael')
    add_user('learner26','62renrael')
    add_user('learner27','72renrael')
    add_user('learner28','82renrael')
    add_user('learner29','92renrael')
    add_user('learner30','03renrael')

    # Brussels AI Lab
    add_user('katrien','neirtak')

    
# create the default ones:
default_users()
