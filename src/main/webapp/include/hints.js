$(document).ready(function(){
	$('#input').val('').focus().attr('autocomplete', 'off');
	$('.tooltip').remove();
}); 

var kbd;		
function showKeyboard() {
  kbd = new google.elements.keyboard.Keyboard(
  [google.elements.keyboard.LayoutCode.RUSSIAN],
  ['input']);
}

function correct(element) {
    $(element).next().css('color', 'green');
}

function incorrect(element) {
    $(element).next().css('color', 'red');
}

function showHint(element){
	$(element).toggle();
	$(element).next().toggle();
}
	
function showTooltip(element, contentLocation) {
	var $tooltip = $('.tooltip.'+contentLocation);
		
	// tooltip inspired by http://www.kriesi.at/archives/create-simple-tooltips-with-css-and-jquery
	
	// tooltip was already created
	if($tooltip.length){
		$(element).mouseover(function(){
			$tooltip.css({opacity:0.8, display:'none'}).fadeIn(400);
		}).mousemove(function(kmouse){
			$tooltip.css({left:kmouse.pageX+15, top:kmouse.pageY+15});
		}).mouseout(function(){
			$tooltip.fadeOut(400);
		});		
	}
	// create new tooltip
	else{
		var $tooltipContent = $($(element).attr(contentLocation));
		
		var stylesOuter = {
			    position:'absolute',
			    zIndex:'999',
			    left:'-9999px',
			    backgroundColor:'#dedede',
			    padding:'5px',
			    border:'1px solid #fff'
		};
		
		var stylesInner = {
			    margin:'0',
			    padding:'0',
			    color:'#fff',
			    backgroundColor:'#222',
			    padding:'2px 7px',
			    textAlign: 'left'
		};
		
		//$(element).each(function(i){ // just one element
		$tooltip = $('<div>');
		$tooltip.addClass('tooltip');
		$tooltip.addClass(contentLocation);
		var $content = $('<p>');
		$content.html($tooltipContent.prop("outerHTML"));
		$content.css(stylesInner);
		$tooltip.html($content);
		$tooltip.css(stylesOuter);
		$('body').append($tooltip);		
		
		//alert(my_tooltip.html());

		$(element).mouseover(function(){
			$tooltip.css({opacity:0.8, display:'none'}).fadeIn(400);
		}).mousemove(function(kmouse){
			$tooltip.css({left:kmouse.pageX+15, top:kmouse.pageY+15});
		}).mouseout(function(){
			$tooltip.fadeOut(400);
		});			
		//});
	}
}

function showFixedTooltip(element, contentLocation) {
	var tooltipClass = '.tooltip.'+contentLocation;
	var $tooltip = $(tooltipClass);
		
	// tooltip inspired by http://www.kriesi.at/archives/create-simple-tooltips-with-css-and-jquery
	
	// tooltip was already created
	if($tooltip.length){
	}
	// create new tooltip
	else{
		// create show sign (+)
		$showSign = $('<span>');
		$showSign.addClass('show');
		$showSign.css('cursor', 'pointer');
		$showSign.html('&#x2295;');
		$showSign.css('display','none');
		$(element).parent().after($showSign.prop('outerHTML'));
		
		// create hide sign (-)
		$hideSign = $('<span>');
		$hideSign.addClass('hide');
		$hideSign.css('cursor', 'pointer');
		$hideSign.html('&#x2296;');
		$(element).parent().after($hideSign.prop('outerHTML'));
		
		var $tooltipContent = $($(element).attr(contentLocation));
		
		var stylesOuter = {
			    position:'absolute',
			    zIndex:'999',
			    left:'-9999px',
			    backgroundColor:'#dedede',
			    padding:'5px',
			    border:'1px solid #fff'
		};
		
		var stylesInner = {
			    margin:'0',
			    padding:'0',
			    color:'#fff',
			    backgroundColor:'#222',
			    padding:'2px 7px',
			    textAlign: 'left'
		};
		
		//$(element).each(function(i){ // just one element
		$tooltip = $('<div>');
		$tooltip.addClass('tooltip');
		$tooltip.addClass(contentLocation);
		var $content = $('<p>');
		$content.html($tooltipContent.prop("outerHTML"));
		$content.css(stylesInner);
		$tooltip.html($content);
		$tooltip.css(stylesOuter);
		$('body').append($tooltip);		
		
		//alert(my_tooltip.html());

		$tooltip.css({opacity:0.8, display:'none'}).fadeIn(400);
		$tooltip.css({left:$(window).width()-$tooltip.width()-20, top:20});
		
		// enable click on the show sign
		$('body').on('click', '.show', function(e){
			$tooltip.css({opacity:0.8, display:'none'}).fadeIn(400);
			$tooltip.css({left:$(window).width()-$tooltip.width()-20, top:20});
			$('.show, .hide').toggle();
		});
		
		// enable click on the hide sign
		$('body').on('click', '.hide', function(e){
			$tooltip.fadeOut(400);
			$('.show, .hide').toggle();
		});
	}
}