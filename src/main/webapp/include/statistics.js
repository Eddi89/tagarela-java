function showStatisticsTooltip(element, contentLocation) {
	var tooltipClass = '.tooltip.'+contentLocation;
	var $tooltip = $(tooltipClass);
		
	// tooltip inspired by http://www.kriesi.at/archives/create-simple-tooltips-with-css-and-jquery
	
	// tooltip was already created
	if($tooltip.length){
	}
	// create new tooltip
	else{		
		var $tooltipContent = $('#'+contentLocation);
		var $subType = $tooltipContent.find('.subType');
		$subType.css('text-indent', '1em');
		
		var stylesOuter = {
			    position:'absolute',
			    zIndex:'999',
			    left:'-9999px',
			    backgroundColor:'#dedede',
			    padding:'5px',
			    border:'1px solid #fff'
		};
		
		var stylesInner = {
			    margin:'0',
			    padding:'0',
			    color:'#fff',
			    backgroundColor:'#222',
			    padding:'2px 7px',
			    textAlign: 'left'
		};
		
		//$(element).each(function(i){ // just one element
		$tooltip = $('<div>');
		$tooltip.addClass('tooltip');
		$tooltip.addClass(contentLocation);
		var $content = $('<p>');
		$content.html($tooltipContent.prop("outerHTML"));
		$content.css(stylesInner);
		$tooltip.html($content);
		$tooltip.css(stylesOuter);
		$tooltip.find('#' + contentLocation).toggle();
		$('body').append($tooltip);	
		
		var buttonID = $(element).attr('id');
		// enable click on the show button
		$('body').on('click', '#'+buttonID+'.show', function(e){
			$tooltip.css({opacity:0.8, display:'none'}).fadeIn(400);
			$tooltip.css({left:$(window).width()-$tooltip.width()-100, top:20});
			$(this).removeClass('show');
			$(this).text('Hide Statistics');
			$(this).addClass('hide');
		});
		
		// enable click on the hide button
		$('body').on('click', '#'+buttonID+'.hide', function(e){
			$tooltip.fadeOut(400);
			$(this).removeClass('hide');
			$(this).text('Show Statistics');
			$(this).addClass('show');
		});
	}	
}

function showStatistics(element) {
	var buttonID = $(element).attr('id');
	// enable click on the show button
	$('body').on('click', '#'+buttonID+'.show', function(e){
		$(this).removeClass('show');
		var prevText = $(this).text();
		var newText = prevText.replace('Show', 'Hide');
		$(this).text(newText);
		$(this).addClass('hide');
		$(this).next().next().show();
	});
	
	// enable click on the hide button
	$('body').on('click', '#'+buttonID+'.hide', function(e){
		$(this).removeClass('hide');
		var prevText = $(this).text();
		var newText = prevText.replace('Hide', 'Show');
		$(this).text(newText);
		$(this).addClass('show');
		$(this).next().next().hide();
	});
}