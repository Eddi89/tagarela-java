// ---------------------------------------------------------------
// $Id: ajax.js,v 1.12 2007-11-24 06:22:28 dm Exp $
// ---------------------------------------------------------------
// Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
// Project information: http://purl.org/net/tagarela
// Licensed under the Creative Commons 
// Attribution-Noncommercial-Share Alike 3.0 License
// http://creativecommons.org/licenses/by-nc-sa/3.0/us/
// --------------------------------------------------------------- 

var xmlHttp;
function createXMLHttpRequest() {
    if (window.ActiveXObject) {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    } else if (window.XMLHttpRequest) {
        xmlHttp = new XMLHttpRequest();
    }
}

// only works with url argument ending in at least one argument (added with ?)
function postJSON(url,ajaxArg) {
    var url = url + "&timeStamp=" + new Date().getTime();
    var queryString = "ajaxArg=" + JSON.stringify(ajaxArg);
    createXMLHttpRequest();
    xmlHttp.open("POST", url, true);
    xmlHttp.onreadystatechange = handleStateChange;
    xmlHttp.setRequestHeader("Content-Type",
			     "application/x-www-form-urlencoded; charset=UTF-8");
    xmlHttp.send(queryString);
}

//------------------------------
// new inputFieldValues() creates an object storing
// all text input fields in an associative array (= Python dictionary)

function inputFieldValues() {
    var x = document.forms[0].elements;
    for (var i=0;i<x.length;i++)
	{
            var f = x[i];
	    if (f.type == 'text') {
		this[f.id] = f.value;
	    }
	}
}

//---------------------------------------------------------------------
// Update the page when we get the reply from the ajax request:

function handleStateChange() {
    if (xmlHttp.readyState == 4) {
        if (xmlHttp.status == 200) {
		    if (xmlHttp.getResponseHeader("X-AJAX-Update")) {
		        updatePage();
		        // and now call initializePage() for this partially updated page
	 	        // to ensure the new input fields are recorded for input etc.
		        initializePage();
		    }
		    else {
			// our partial update attempt wasn't successful (e.g.
			// because session cookie timeout or other problem):
			location.reload();
		    }
			$('#input').val('').focus().attr('autocomplete', 'off');
			$('.tooltip').remove();
			if($('#statistics').length){
				var $button = $('button.hide');
				$button.removeClass('hide');
				$button.text('Show Statistics');
				$button.addClass('show');
				$('button.show').show();
			}
			else{
				$('button.show').hide();
				$('button.hide').hide();
			}
        }
	// if not status 200, i.e., eg., 500 (internal server errror)
	// show error message on entire page:
	else {
	    document.body.style.setProperty("text-align",'left',null);
	    document.body.style.setProperty("background-color",'#FFF',null);
	    document.body.innerHTML = xmlHttp.responseText;
	}
    }
}

// updatePage expect the responseText of the xmlHttp request to 
// be a dictionary of the form  { elementID1 : newHTMLforElement2,
//                                elementID2 : newHTMLforElement2,
//                                 ...                             }
// and replaces the innerHTML of each element with the value provided. 
function updatePage() {
    var response = JSON.parse(xmlHttp.responseText);
    
    for (var propName in response.contents) {
	var value = response.contents[propName];
	var responseDiv = document.getElementById(propName);
	// error:
	if (response.status) {
	    //  report error
	    var errorContainer = document.createElement('pre');
	    errorContainer.className = 'serverError';
	    errorContainer.appendChild(document.createTextNode(value));
	    responseDiv.appendChild(errorContainer);
	}
	// good response:
	else {
	    responseDiv.innerHTML = value;
	}
    }
}
