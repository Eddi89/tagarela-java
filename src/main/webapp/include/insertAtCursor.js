// ---------------------------------------------------------------
// $Id: insertAtCursor.js,v 1.12 2007-11-24 06:22:28 dm Exp $
// ---------------------------------------------------------------
// Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
// Project information: http://purl.org/net/tagarela
// Licensed under the Creative Commons 
// Attribution-Noncommercial-Share Alike 3.0 License
// http://creativecommons.org/licenses/by-nc-sa/3.0/us/
// --------------------------------------------------------------- 

var focusedInput = '';

window.onload = initializeNewPage;

// called only for new pages:
function initializeNewPage() {
    return initializePage();
}

// called both for new pages and partial AJAX updates:
function initializePage() {
    //showStoredValues();
    return handleInputFields();
}

// attach the relevant handlers to the new or newly updated input fields
function handleInputFields() {
    var x = document.forms[0].elements;
    focusedInput = '';
    for (var i=0;i<x.length;i++)
	{
	    if (x[i].type == 'text') {
		// add on focus handler and save first field in this loop as default:
		x[i].onfocus = function (event) {     
		    // provides event under variable e for IE
		    if (!event) {// IE 
			focusedInput = event.srcElement;
		    }
		    else {
			focusedInput = event.target;
		    }
		}
		if (focusedInput == '') { focusedInput = x[i];}
		// add enter key detection handler (needed to submit using 
		// enter even when multiple text input fields exist):
		x[i].onkeyup = function (event) { return interpretKey(event) ;}
	    }
	}
}

// for new or newly updated pages, for all inputs which are blank/empty
// get the values the user last submitted for those inputs 
function showStoredValues() {
    var x = document.forms[0].elements;
    focusedInput = '';
    for (var i=0;i<x.length;i++)
	{
	    if (x[i].type == 'text') {
		element = document.getElementById("stored"+ x[i].id)
		    if (element  && (!x[i].value)) x[i].value = element.innerHTML;
	    }
	}
}


function insertAtCursor(myValue) {
    //based on code from http://alexking.org/blog/2004/06/03/js-quicktags-under-lgpl/
    myField = focusedInput;
    //IE support
    if (document.selection) {
	myField.focus();
	sel = document.selection.createRange();
	sel.text = myValue;
	myField.focus();
    }
    //MOZILLA/NETSCAPE support
    else if (myField.selectionStart || myField.selectionStart == '0') {
	var startPos = myField.selectionStart;
	var endPos = myField.selectionEnd;
	var scrollTop = myField.scrollTop;
	myField.value = myField.value.substring(0, startPos)
	    + myValue 
	    + myField.value.substring(endPos, myField.value.length);
	myField.focus();
	myField.selectionStart = startPos + myValue.length;
	myField.selectionEnd = startPos + myValue.length;
	myField.scrollTop = scrollTop;
    } else {
	myField.value += myValue;
	myField.focus();
    }
}


// finding the keypress for the different browsers is a bit of a pain 
// cf. e.g. http://www.quirksmode.org/js/events_access.html
// and http://www.quirksmode.org/js/events_properties.html#key
// and http://jennifermadden.com/javascript/stringEnterKeyDetector.html

function interpretKey(e){ // provides event as argument for Mozilla family

    var keycode;
    if (!e) var e = window.event; // provides event under variable e for IE

    if (e.keyCode) keycode = e.keyCode; // Mozilla family
    else { 
	if (e.which) keycode = e.which; // IE family
	else return true; // shouldn't happen, if it does, for some odd 
	                  // browser, just pass the key
    }

    // special case for forward and backward arrow, which have
    // the same keycodes as real characters:
    // left arrow matchies %, both with keycode 37
    // and right arrow matches ' both with keycode 39
    // but the arrow keys have charCode 0, so we check that here and pass them
    if(e.charCode == 0 && (keycode == 37 || keycode == 39)) return true;

    if(keycode == 13){ // enter key was pressed
	document.forms[0].submit(); //submit the form
	return false;
    }else{
	var keychar = String.fromCharCode(keycode);
    	// define permited characters (by specifing negated character set)
	okChars= /[^~"'`@#$%^&*()_+=\[\]{}\|;<>\/\\]/;
        // return true if the key matches the okChars
        return okChars.test(keychar);
    }
}

