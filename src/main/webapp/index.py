# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: index.py,v 1.49 2007-11-24 06:25:37 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

from mod_python import psp, util, apache
from os.path import join,dirname

try:
    from bsddb3 import btopen
except ImportError:
    from bsddb import btopen

import re

t        = apache.import_module("./code/tagarela_utils.py")

####
# default index handler

def index(req):
    """Define an index handler to catch and redirect shorthand 
       requests (those not providing index.py or the function 
       to be called)
    """
    util.redirect(req,'index.py/main')


####
# show main TAGARELA page

def main(req,what='',msg='',modNr='',exNr='1'):
    # create a session for the req object:
    sess = t.get_session(req)

    # write log entry:
    t.log(req,'main(', t.plainDict(locals()))
    # define what to use as main content of page:
    if what in ["about", "acknowledge", "people", "publications"]: 
	# testing against an explicit list above is only done to catch hackers
	mainContent = psp.PSP(req, filename='include/%s.tmpl'%what)
    else:
	mainContent = '' 

    # if user has authenticated but is not yet authorized 
    # (i.e. selected menu instead of clicking on 'Accept')
    # then remove the authenticated_id
    if sess.has_key('authenticated_id') and not sess.has_key('authorized_id'):
	del sess['authenticated_id']
	sess.save()

    # set access control to allow cross-site stuff (not sure whether this should be done here)
    #req.headers_out.add('Access-Control', 'allow <*>')
    
    # show the page
    _showpage(req,mainContent=mainContent,
              msg=msg,modNr=modNr,exNr=exNr)

####
# authorize a session if consent was given

def authorize(req,consent):
    # get session for the req object:
    sess = t.get_session(req)

    if consent=='Accept':
	sess['authorized_id'] = sess['authenticated_id']
	sess.save()
	# store in user model
	t.usermodel_put(sess,'consent_given',1)
	# update log:
	t.log(req,'authorized from consent form')
	mainContent = psp.PSP(req,filename='include/howto.tmpl')
	_showpage(req,mainContent=mainContent)
    else:
	finishLogout(req)

####
# Log in user and show main page

def login(req, id='', passwd='',mainContent='',msg='',modNr='',exNr='1'):
    # access server variables:
    req.add_common_vars()

    # create a session for the req object:
    sess = t.get_session(req)
    # store the tagarela home directory in session:
    script_name = req.subprocess_env.get('SCRIPT_NAME')
    host = req.subprocess_env.get('SERVER_NAME')
    sess['tagarela_index_url'] = "http://"+host+script_name
    sess['tagarela_dir_url'] = sess['tagarela_index_url'].replace("index.py","")
    
    _browser_check(req,req.subprocess_env.get('HTTP_USER_AGENT',''),sess['tagarela_index_url'])

    # do login process: 
    if sess.has_key('authorized_id'):
        msg='You are already logged in!'
    elif not(id): 
        msg='Username required!'
    elif not(passwd): 
        msg='Password required!'
    else:
        try:
	    # Define the user database to use, relative to this file's location
	    user_database_file = join(dirname(__file__),
                                      'databases/tagarela_user_database.bsddb')
	    # open user database in read mode
	    userdb = btopen(user_database_file,'r')
            stored_passwd = userdb[id]
            userdb.close()
        except:
            # catches KeyError when id doesn't exist as key
            stored_passwd = ''
        if not stored_passwd == passwd:
            msg='Login incorrect!'
        else: # matching password found
	    msg=''
            # mark session as authenticated
	    sess['authenticated_id'] = id
            # init indiv. user's usermodel database and store filename
            sess['usermodel_filename'] = t.init_usermodel_db(__file__,id)
	    # update log:
	    t.log(req,'authenticated')
            # Check whether user has accepted consent form, in which 
	    # case we can authorize the session:
	    if t.usermodel_get(sess,'consent_given'):
		sess['authorized_id'] = id
		# update log:
		t.log(req,'authorized from usermodel')
		mainContent = psp.PSP(req,filename='include/howto.tmpl')
	    else:
		mainContent = psp.PSP(req,filename='include/consent.tmpl')
            # save the session so that we keep the authorized_id set
            sess.save()

    # show the page
    _showpage(req, sess=sess, msg=msg,mainContent=mainContent,modNr=modNr,exNr=exNr)


def _browser_check(req,user_agent,tagarela_index_url):
    # Some basic browser checking:
    # if it's MSIE, only accept version 7 and above,
    # if it's Safari, only accept version 3 and above,
    # if it's Firefox, only accept version 2 and above
    # but note that we let any other browser proceed, which seems better 
    # than ruling anything else out categorically - but who knows what they'll see...
    if (("MSIE" in user_agent and re.search("MSIE [456]",user_agent) != None) or
	("Safari" in user_agent and re.search("Version/[12]",user_agent) != None) or
	("Firefox" in user_agent and re.search("Firefox/1\.",user_agent) != None)): 
	util.redirect(req,tagarela_index_url+'/main?msg=Please upgrade to<br>IE 7, Firefox 2, Safari 3 or newer.')
	
    # List of USER-AGENT strings for various browsers:
    # http://www.zytrax.com/tech/web/browser_ids.htm


####
# Log out user and show main page

# handler for logout requests (includes showing questionnaire):
def logout(req):
    # create a session for the req object:
    sess = t.get_session(req)
    # update log:
    t.log(req,'logout questionnaire')
    # get questionnaire
    if 'authorized_id' in sess:
	who = sess['authorized_id'] 
    else:
	who =  ''
    mainContent = psp.PSP(req, filename='include/questionnaire.tmpl',
			  vars = {'authorized_id': who})
    # show the page
    _showpage(req,mainContent=mainContent)


# simple, internal logout handler:
def finishLogout(req,mainContent=''):
    # create a session for the req object:
    sess = t.get_session(req)
    # update log:
    t.log(req,'finish logout')
    # invalidate this session (release storage, cookie)
    sess.invalidate()
    # show the page
    _showpage(req,mainContent=mainContent)

####

# import the smtp library to support sending emails
import smtplib

smtp_server = "smtpserv.uni-tuebingen.de"    # Use Tübingen mail server since TAGARELA runs here now

from_address = "TAGARELA <dm@sfs.uni-tuebingen.de>"

# define the function called in form.html
def submitQuestionnaire(req, **input):

    subject = 'TAGARELA Exit Questionnaire'

    # create the message text (using """ to create a multi line string)
    msg = """\
From: %s
Subject: %s
To: dm@sfs.uni-tuebingen.de, amaral@spanport.umass.edu, rziai@sfs.uni-tuebingen.de

Id: %s

1) It was easy to navigate within the web-pages, 
   and to find the exercises I wanted
   %s
   %s

2) Instructions were clear, and I knew what to do 
   for each exercise.
   %s
   %s

3) I understood the functions of the buttons.
   %s
   %s

4) Exercises were useful and related to the topic of the unit.
   %s
   %s

5) Exercises helped me review and practice the topics I needed.
   %s
   %s

6) The feedback provided was useful.
   %s
   %s

7) The feedback provided helped me think about my errors.
   %s
   %s

8) I feel I am more prepared to answer questions in my exams
   after I used the system.
   %s
   %s

9) I prefer using an electronic workbook like TAGARELA to a 
   regular paper-based one. 
   %s
   %s

10) My favorite type of exercise is.
    %s
    %s

11) General comments:
    %s
""" % (from_address,subject,
       t.getval(input,'authorized_id'),
       t.getval(input,'navigate'), t.getval(input,'commentnavigate'),
       t.getval(input,'instructions'), t.getval(input,'commentinstructions'),
       t.getval(input,'buttons'), t.getval(input,'commentbuttons'),
       t.getval(input,'exercises'), t.getval(input,'commentexercises'),
       t.getval(input,'review'), t.getval(input,'commentreview'),
       t.getval(input,'feedback'), t.getval(input,'commentfeedback'),
       t.getval(input,'thinkerrors'), t.getval(input,'commentthinkerrors'),
       t.getval(input,'readyforexam'), t.getval(input,'commentreadyforexam'),
       t.getval(input,'paperworkbook'), t.getval(input,'commentpaperworkbook'),
       t.getval(input,'exercisetypes'), t.getval(input,'commentexercisetypes'),
       t.getval(input,'generalcomments'))

    # send the email
    conn = smtplib.SMTP(smtp_server)
    conn.sendmail(from_address, ["dm@sfs.uni-tuebingen.de","amaral@spanport.umass.edu","rziai@sfs.uni-tuebingen.de"], msg)
    conn.quit()

    # provide feedback to the user
    finishLogout(req,mainContent = "<h2>Thank you for your comments!</h2>")

# define the function called in form.html
def handleSystemFeedback(req, feedbackProvided, systemDevelopFeedback):

    # create a session for the req object:
    sess = t.get_session(req)
    
    authorized_id = sess['authorized_id'] 

    # get the current state of affairs (i.e., activity type, number)
    current = t.usermodel_get(sess,'current')
    actType = t.getval(current,'actType')
    modNr = t.getval(current,'modNr')
    exNr = t.getval(current,'exNr')
    questNr = t.getval(current,'questNr')

    if feedbackProvided == "Cancel":
	_showpage(req)
    else:
	subject = 'TAGARELA System Development Feedback'

	# create the message text (using """ to create a multi line string)
	msg = """\
From: %s
Subject: %s
To: dm@sfs.uni-tuebingen.de, amaral@spanport.umass.edu, rziai@sfs.uni-tuebingen.de

Comment by %s on %s-%s-%s-%s: 
%s

""" % (from_address,subject,authorized_id,actType,modNr,exNr,questNr,systemDevelopFeedback)

	# send the email
	conn = smtplib.SMTP(smtp_server)
	conn.sendmail(from_address, ["dm@sfs.uni-tuebingen.de","amaral@spanport.umass.edu","rziai@sfs.uni-tuebingen.de"], msg)
	conn.quit()

	# provide feedback to the user
	_showpage(req,mainContent = "<h2>Thank you for your comments!</h2>")


# handler for logout requests (includes showing questionnaire):
def systemDevelopFeedback(req):
    # update log:
    t.log(req,'showing system feedback form')
    # show the page
    mainContent = psp.PSP(req, filename='include/systemDevelopFeedback.tmpl')
    _showpage(req,mainContent=mainContent)

# handler for logout requests (includes showing questionnaire):
def showAdminInterface(req):
    # update log:
    t.log(req,'showing admin interface')
    # show the page
    mainContent = psp.PSP(req, filename='include/admin.tmpl')
    _showpage(req,mainContent=mainContent)


admin  = apache.import_module("./code/admin.py")

# handler for logout requests (includes showing questionnaire):
def handleAdminAct(req,showLog='',studentIdForLog=''):
    sess = t.get_session(req)
    if showLog == 'Cancel':
	mainContent = ''
    else:
	mainContent = admin.prettyLog(studentIdForLog)
    _showpage(req,mainContent=mainContent)

####
# Auxiliary Function

def _showpage(req, sess='', msg='', mainContent = '',modNr ='',exNr='1'):
    """_showpage is the internal function creating the actual page.

       It is called by the main showpage handler, the login and logout 
       handlers, and the indexMod handler.
    """
    # result of this is an html page:
    req.content_type ='text/html'

    sess = t.get_session(req)

    if not(modNr):
	current = t.usermodel_get(sess,'current')
	if 'modNr' in current:
	    modNr = current['modNr']
	else:
	    modNr = '1'

    if not(mainContent):
	# if no mainContent is provided, load the image as main content
	mainContent = psp.PSP(req, filename='include/homeImage.tmpl')

    topSidebar = _createTopSidebar(req,sess,modNr)

    # for administrators also display administrator menu
    # (HACK: should make use of groups instead of user ids)
    if 'authorized_id' in sess and sess['authorized_id'] in ['amaral.1','meurers.1']:
	adminMenu = psp.PSP(req, filename='include/adminMenu.tmpl')
    else:
	adminMenu = ''

    # create main page template object
    mainPageTmpl = psp.PSP(req, filename='include/index.tmpl',
                           vars = {'msg' : msg,
			           'TagarelaVersion' : t.tagarelaVersion,
				   'TopSidebar' : topSidebar,
				   'AdminMenu' : adminMenu,
				   'MainContent' : mainContent})
    # publish the template 
    mainPageTmpl.run()

    # for debugging use:
    # topSidebar.display_code()



def _createTopSidebar(req,sess,modNr):
    if sess.has_key('authenticated_id'):

	if sess.has_key('authorized_id'):

	    modListTmpl = [0,1,2,3,4]
	    for i in modListTmpl:
		if str(i+1) == modNr:
		    status='Active'
		else:
		    status='Inactive'
		modListTmpl[i] = psp.PSP(req,
					 filename='include/modMenu%s.tmpl'%status,
					 vars = {'modNr' : i+1 })
            
	    # logout version of sidebar
	    navTabs = psp.PSP(req, filename='include/navTabs.tmpl',
			      vars = {'mod1List': modListTmpl[0],
				      'mod2List': modListTmpl[1],
				      'mod3List': modListTmpl[2],
				      'mod4List': modListTmpl[3],
				      'mod5List': modListTmpl[4]
				      })

	else:
	    navTabs = ''
        return psp.PSP(req, filename='include/logoutSidebar.tmpl',
                       vars = {'Username' : sess['authenticated_id'],
                               'NavTabs' : navTabs})
    else:
        # login version of sidebar
        return psp.PSP(req, filename='include/loginSidebar.tmpl')
