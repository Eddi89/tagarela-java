# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: matchStr.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

import tokenizer

### String matching module
## It basically compares both strings

### INPUT: string of student's answer and list of possible answers from ActModel
### OUTPUT: A dictionary = {'string-match':[ANALYSIS,STRING]}
###                        ANALYSIS can be "ok", "ok-noP", or "no-match"

def mainstrMatching(stString,listofAnswers):
  stLow = stString.lower()
  tokenizedST = tokenizer.mainToknz(stLow)
  for item in listofAnswers:
    lowitem = item.lower()
    tokenizedAN = tokenizer.mainToknz(lowitem)
    itemNpunc = item[:-1]
    itemNlow = lowitem[:-1]
    if item == stString:
      return ['ok',stString]
    elif lowitem == stLow:
      return ['ok-low',stString]
    elif tokenizedAN[0] == tokenizedST[0]:
      return ['ok-space',stString]
    elif itemNpunc == stString:
      return ['ok-noP',stString]
    elif itemNlow == stLow:
      return ['ok-low-noP',stString]
  return ['no-match',stString]


#### Testing #####

# st = "O nome dele é Paulo"
# tAnss = ["Ele se chama Paulo.", "O nome dele é Paulo."]
# 
# 
# 
# def agr_test():
#     print mainstrMatching(st,tAnss)
# 
# 
# if __name__ == "__main__":
#   agr_test()
