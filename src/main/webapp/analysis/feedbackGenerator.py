# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: feedbackGenerator.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

# OuTPUT: List = ['feedback Message #',[necessary words in order]]

#### These functions decide on feedback strategies for each type of errors

######################
## SPELLING ERRORS ###
######################

def spellingerrors(errors):
  ## there are several types of errors
  ## 1) One word is wrong and system does not have suggestions
  if len(errors) == 1:
    if errors[0][1] == 'no suggestion':
      feedbackInfo = ['spell1',[errors[0][0]]]
      return feedbackInfo
  ## 2) One word wrong and system has one suggestion
    else:
      if len(errors[0][1].split(',')) == 1:
        feedbackInfo = ['spell2',[errors[0][0],errors[0][1]]]
        return feedbackInfo
  ## 3) One wrong word and system has multiple suggestions
      else:
        feedbackInfo = ['spell3',[errors[0][0],errors[0][1]]]
        return feedbackInfo
  ## 4) More than one wrong word
  else:
    suggest = ""
    for x in errors:
      suggest = suggest + "<p><b><i>" + x[0] + "</b></i>: " + x[1] + "</p>"
    feedbackInfo = ['spell4',[suggest]]
    return feedbackInfo


######################
## String Matching ###
######################
# 'ok', 'ok-low', 'ok-noP', 'ok-low-noP'

def stringMatch(smOutput,exercise):
  if smOutput[0] == 'ok':
    if exercise == '1':
      feedbackInfo = ['strM1',[]]
    elif exercise == '2':
      feedbackInfo = ['strM5',[]]
    elif exercise == '3':
      feedbackInfo = ['strM6',[]]
    elif exercise == '4':
      feedbackInfo = ['strM7',[]]
    elif exercise == '5':
      feedbackInfo = ['strM8',[]]
    elif exercise == '6':
      feedbackInfo = ['strM9',[]]
    elif exercise ==  '7':
      feedbackInfo = ['strM10',[]]
    elif exercise == '8':
      feedbackInfo = ['strM11',[]]
    else:
      feedbackInfo = ['strM12',[]]
  elif smOutput[0] == 'ok-low':
    feedbackInfo = ['strM2',[]]
  elif smOutput[0] == 'ok-space':
    feedbackInfo = ['strM13',[]]
  elif smOutput[0] == 'ok-noP':
    feedbackInfo = ['strM3',[]]
  elif smOutput[0] == 'ok-noPS':
    feedbackInfo = ['strM14',[]]
  elif smOutput[0] == 'ok-low-noP':
    feedbackInfo = ['strM4',[]]
  return feedbackInfo


###########################
## Support to Generation ##
###########################


# datastructure = {'learnerInput':learnerInput,'tokens':tokenization,'lexInfo':lexicalanalysis,
#                    'spellAnalysis':spellanalysis, 'disambiguator':disambanalysis, 'parser':parsetree[0],
#                    'agreement':AgreementCheck, 'bestAnswer':bestAnswer, 'requiredWords':requiredWs,
#                    'tokensAnswer':tokensAN, 'lexiconAN':lexiconAN, 'disambiguatedAN':disambiguatedAN,
#                    'reqWordsAna': reqWordAna, 'stringmatching':stringMatching, 'tokenMatching':TokenMatching, 
#                    'canonicMatching':CanonicMatching, 'POSmatching': POSMatching, 'parserAN':parsetreeAN[0],
#                    'error':{'type':'unidentified'},'semanticCheck':meaningInfo}

def agreementFeedback(ai):
  if len(ai['agreement'][1]) == 0:
    return {'error':'no-analysis'}
  elif len(ai['agreement'][1]) == 1:
    return {'error':'agr1'}
  elif len(ai['agreement'][1]) > 2:
    return {'error':'agr2'}
  else:
    return {'error':'no-analysis'}




#semanticCheck = {'tokenNumbers': {'matches': 2, 'noM-An': 3, 'noM-St': 0},
#                 'canonicNumbers': {'matches': 0, 'noM-An': 3, 'noM-St': 0},
#                 'posNumbers': {'matches': 0, 'noM-An': 3, 'noM-St': 0},
#                 'total#tokens': {'StAn': 2, 'TaAn': 5}}


# Function that evaluates the type of error to report feedback on
def semanticfeedback(ai):

  smChk = ai['semanticCheck']
  


  # if all tokens match after string matching did not succeed
  if (smChk['tokenNumbers']['noM-An'] == 0) and (smChk['tokenNumbers']['noM-St'] == 0):
    if smChk['total#tokens']['StAn'] == smChk['total#tokens']['TaAn']:
      return {'error':'word-order'}
    elif smChk['total#tokens']['StAn'] < smChk['total#tokens']['TaAn']:
      return {'error':'word-order'}
      #return {'error':'missingPunct'}
    elif smChk['total#tokens']['StAn'] > smChk['total#tokens']['TaAn']:
      return {'error':'extraPunct'}
    else:
      return {'error':'no-analysis'}


  # IF all canonic forms are matched
  elif(smChk['canonicNumbers']['noM-An'] == 0) and (smChk['canonicNumbers']['noM-St'] == 0):
    # check to see if there is an agreement error
    checkAgreement = agreementFeedback(ai)
    if checkAgreement['error'] == 'agr1':
      return checkAgreement
    elif checkAgreement['error'] == 'agr2':
      return checkAgreement
    # report on words that are matched by canonic form
    elif checkAgreement['error'] == 'no-analysis':
      if len(ai['canonicMatching']['canonic-matches']) == 1:
        return {'error':'CanoCompl1'}
      elif len(ai['canonicMatching']['canonic-matches']) > 1:
        return {'error':'CanoCompl2'}
      else:
        return {'error':'no-analysis'}
    else:
      return {'error':'no-analysis'}
  

  # IF all POS forms are matched
  elif(smChk['posNumbers']['noM-An'] == 0) and (smChk['posNumbers']['noM-St'] == 0):
    #POSfeedback = checkPOSfeedback(ai)
    posM = ai['POSmatching']
    for item in posM['POS-matches']:
      if (item[2] == 'verb') or (item[2] == 'noun'):
        return {'error':'pos1','pos':item[2],'canonics':[ai['disambiguator'][item[1][1]][1][0]['canonic'],ai['disambiguatedAN'][item[1][1]][1][0]['canonic']]}
      elif (item[2] == 'prep'):
        return {'error':'pos2','pos':'preposition','canonics':[ai['disambiguatedAN'][item[1][1]][1][0]['canonic'],ai['disambiguator'][item[1][1]][1][0]['canonic']]}
      elif (item[2] == 'propNoun'):
        return {'error':'pos3','pos':'proper noun','canonics':[ai['disambiguator'][item[1][1]][0],ai['disambiguatedAN'][item[1][1]][0]]}
      elif (item[2] == 'adj'):
        return {'error':'pos1','pos':'adjective','canonics':[ai['disambiguator'][item[1][1]][1][0]['canonic'],ai['disambiguatedAN'][item[1][1]][1][0]['canonic']]}
      elif (item[2] == 'adv'):
        return {'error':'pos1','pos':'adverb','canonics':[ai['disambiguator'][item[1][1]][1][0]['canonic'],ai['disambiguatedAN'][item[1][1]][1][0]['canonic']]}
      elif (item[2] == 'pronoun'):
        return {'error':'pos4','pos':'pronoun','canonics':[ai['disambiguator'][item[1][1]][0],ai['disambiguatedAN'][item[1][1]][0]]}
      elif (item[2] == 'det'):
        return {'error':'pos1','pos':'determiner','canonics':[ai['disambiguator'][item[1][1]][0],ai['disambiguatedAN'][item[1][1]][0]]}
      else:
        return {'error':'pos5','canonics':[ai['disambiguator'][item[1][1]][0],ai['disambiguatedAN'][item[1][1]][0]]}
  
  # If several concepts are missing
  elif smChk['tokenNumbers']['noM-An'] > 3:
    return {'error':'missing+4'}


  #Special cases of extra concepts
  elif smChk['tokenNumbers']['noM-An'] == 0:
    if smChk['tokenNumbers']['noM-St'] > 0:
      for item in ai['tokenMatching']['token-ST-no-match']:
        num = item[1]
        posNoM = ai['disambiguator'][num][1][0]['pos']
        try:
          numN = num+1
          posNoMnext = ai['disambiguator'][numN][1][0]['pos']
        except:
          posNoMnext = 'noItem'
        if (posNoM == 'det') and (posNoMnext == 'propNoun'):
          return {'error':'det-propNoun','det':item[0]}
    if smChk['tokenNumbers']['noM-St'] == 1:
      return {'error':'exta-element1','element':ai['tokenMatching']['token-ST-no-match'][0][0]}
    elif smChk['tokenNumbers']['noM-St'] > 1:
      elements = ''
      for x in ai['tokenMatching']['token-ST-no-match']:
        elements = elements + '<b>' + x[0] + '<b>, '
        answer = elements[:-2]+'.'
        return {'error':'exta-element2','elements':answer}
    else:
          return {'error':'no-analysis'}

  ######### no extra but some missing
  elif smChk['tokenNumbers']['noM-St'] == 0:
    if smChk['tokenNumbers']['noM-An'] > 0:
      if smChk['tokenNumbers']['noM-An'] == 1:

        if ai['tokenMatching']['token-AN-no-match'][0][0] == 'de':
          #return {'error':'test','argument':ai['disambiguatedAN'][ai['tokenMatching']['token-AN-no-match'][0][1]-1][0]}
          if ai['disambiguatedAN'][ai['tokenMatching']['token-AN-no-match'][0][1]-1][1][0]['canonic'] == 'gostar':
            #return {'error':'test','argument':[ai['tokenMatching']['token-AN-no-match'][0][1]][1][0]['canonic']}
            return {'error':'gostar-de'}
          else:
            return {'error':'no-de'}
        else:

          word = ai['disambiguatedAN'][ai['tokenMatching']['token-AN-no-match'][0][1]]
          if word[1][0]['pos'] == 'verb':
            return {'error':'missing1','pos':word[1][0]['pos']}#,'element':ai['tokenMatching']['token-ST-no-match'][0][0]}
          elif word[1][0]['pos'] == 'noun':
            return {'error':'missing1','pos':word[1][0]['pos']}#,'element':ai['tokenMatching']['token-ST-no-match'][0][0]}
          else:
            return {'error':'missing5','word':word[0]}

      elif smChk['tokenNumbers']['noM-An'] == 2:
        words = [ai['tokenMatching']['token-AN-no-match'][0][0],ai['tokenMatching']['token-AN-no-match'][1][0]]
        return {'error':'missing-2-words','words':words}
      else:
        #return {'error':'test','argument': ai['disambiguatedAN'][ai['tokenMatching']['token-AN-no-match'][0][1]]}
        return {'error':'missing2'}

  ## Missing and extra
  elif smChk['tokenNumbers']['noM-An'] > 0:

      if (smChk['tokenNumbers']['noM-An'] == 1) and (smChk['tokenNumbers']['noM-St'] >0):
        word = ai['disambiguatedAN'][ai['tokenMatching']['token-AN-no-match'][0][1]]
        if word[1][0]['pos'] == 'verb':
          #return {'error':'test','argument': ai['disambiguatedAN'][ai['tokenMatching']['token-AN-no-match'][0][1]]}
          return {'error':'missing6','pos':word[1][0]['pos']}#,'element':ai['tokenMatching']['token-ST-no-match'][0][0]}
        elif word[1][0]['pos'] == 'noun':
          return {'error':'missing6','pos':word[1][0]['pos']}#,'element':ai['tokenMatching']['token-ST-no-match'][0][0]}
        else:
          return {'error':'missing7','word':word[0]}
      elif smChk['tokenNumbers']['noM-An'] == 2:
        words = [ai['tokenMatching']['token-AN-no-match'][0][0],ai['tokenMatching']['token-AN-no-match'][1][0]]
        return {'error':'missing-2-words2','words':words}
      else:
        return {'error':'missing4'}
   
  #  elif smChk['tokenNumbers']['noM-St'] > 0:
#       if smChk['tokenNumbers']['noM-An'] == 1:
#         return {'error':'extra1'}
#       else:
#         return {'error':'extra2'}

  else:
    return {'error':'no-analysis'}



##########################
## GENERATION FUNCTIONS ##
##########################

##
## Description, Reading, Listening, Rephrasing, Vocabulary
##
def generation1(ai,current):
  # if len(ai['reqWordsAna']) > 0:
#     if len(ai['reqWordsAna']) == 1:
#       feedbackInfo = ['reqW1D',[ai['reqWordsAna'][0]]]
#       return feedbackInfo
#     else:
#       feedbackInfo = ['reqW2D',[]]
#       return feedbackInfo

  mfeedback = semanticfeedback(ai)
  if mfeedback['error'] == 'word-order':
    feedbackInfo = ['descWO1',[]]
  elif mfeedback['error'] == 'missingPunct':
    feedbackInfo = ['missingPunct',[]]
  elif mfeedback['error'] == 'extraPunct':
    feedbackInfo = ['extraPunct',[]]
  elif mfeedback['error'] == 'agr1':
    feedbackInfo = ['agr1',[ai['agreement'][1][0][0],ai['agreement'][1][0][1][0],ai['agreement'][1][0][1][1],ai['agreement'][1][0][2]]]
  elif mfeedback['error'] == 'agr2':
    errors = ai['agreement'][1]
    message = ''
    number = 1
    for item in errors:
      message = message + '<p>' + str(number) +') agreement error in <i><b>' + item[0] + '</b></i> between the <i><b>' + item[1][0] + '</b></i> and the <b><i>' + item[1][1] + '</b></i> in the sequence <b><i>' + item[2] + '.</b></i></p>'
      number = number +1
    feedbackInfo = ['agr2',[message]]
  elif mfeedback['error'] == 'CanoCompl1':
    feedbackInfo = ['CanoCompl1',[ai['canonicMatching']['canonic-matches'][0][0][0]]]
  elif mfeedback['error'] == 'CanoCompl2':
    words = ''
    for x in ai['canonicMatching']['canonic-matches']:
      words = words + '<b><i>"' + x[0][0] + '"</b></i>, '
    answer = words[:-2]+'.'
    feedbackInfo = ['CanoCompl2',[answer]]
  elif mfeedback['error'] == 'pos1':
    feedbackInfo = ['pos1',[mfeedback['pos'],mfeedback['canonics'][0],mfeedback['canonics'][1]]]
  elif mfeedback['error'] == 'pos2':
    feedbackInfo = ['pos2',[mfeedback['pos'],mfeedback['canonics'][0],mfeedback['canonics'][1]]]
  elif mfeedback['error'] == 'pos3':
    feedbackInfo = ['pos3',[mfeedback['pos'],mfeedback['canonics'][0].capitalize(),mfeedback['canonics'][1].capitalize()]]
  elif mfeedback['error'] == 'pos4':
    feedbackInfo = ['pos4',[mfeedback['pos'],mfeedback['canonics'][0],mfeedback['canonics'][1]]]
  elif mfeedback['error'] == 'pos5':
    feedbackInfo = ['pos5',[mfeedback['canonics'][0],mfeedback['canonics'][1]]]
  elif mfeedback['error'] == 'missing+4':
    if current['actType'] == 'Reading':
      feedbackInfo = ['missingR',[]]
    elif current['actType'] == 'Listening':
      feedbackInfo = ['missingL',[]]
    elif current['actType'] == 'Description':
      feedbackInfo = ['missingD',[]]
    elif current['actType'] == 'Rephrasing':
      feedbackInfo = ['missingRP',[]]
    elif current['actType'] == 'Vocabulary':
      feedbackInfo = ['missingV',[]]
    else:
      feedbackInfo = ['noMessage',[]]
  elif mfeedback['error'] == 'det-propNoun':
      feedbackInfo = ['det-propNoun',[mfeedback['det']]]
  elif mfeedback['error'] == 'exta-element1':
      feedbackInfo = ['exta-element1',[mfeedback['element']]]
  elif mfeedback['error'] == 'exta-element2':
      feedbackInfo = ['exta-element2',[mfeedback['elements']]]
  elif mfeedback['error'] == 'missing1':
      feedbackInfo = ['missing1',[mfeedback['pos']]]
  elif mfeedback['error'] == 'missing2':
      feedbackInfo = ['missing2',[]]
  elif mfeedback['error'] == 'missing3':
      feedbackInfo = ['missing3',[]]
  elif mfeedback['error'] == 'missing5':
      feedbackInfo = ['missing5',[mfeedback['word']]]
  elif mfeedback['error'] == 'missing4':
      feedbackInfo = ['missing4',[]]
  elif mfeedback['error'] == 'extra1':
      feedbackInfo = ['extra1',[]]
  elif mfeedback['error'] == 'extra2':
      feedbackInfo = ['extra2',[]]
  elif mfeedback['error'] == 'test':
      feedbackInfo = ['test',[mfeedback['argument']]]
  elif mfeedback['error'] == 'gostar-de':
      feedbackInfo = ['gostar-de',[]]
  elif mfeedback['error'] == 'no-de':
      feedbackInfo = ['no-de',[]]
  elif mfeedback['error'] == 'missing-2-words':
      feedbackInfo = ['missing-2-words',mfeedback['words']]
  elif mfeedback['error'] == 'missing-2-words2':
      feedbackInfo = ['missing-2-words2',mfeedback['words']]
  elif mfeedback['error'] == 'missing6':
      feedbackInfo = ['missing6',[mfeedback['pos']]]
  elif mfeedback['error'] == 'missing7':
      feedbackInfo = ['missing7',[mfeedback['word']]]

  else:
    feedbackInfo = ['noMessage',[]]
    #feedbackInfo = ['check',[len(ai['reqWordsAna']),len(ai['reqWordsAna'])]]
  return feedbackInfo

## FIBs
def generation2(fibsAnalysis,current):
  feedbackmessages = []
  for item in fibsAnalysis['Analysis']:
    if item['error'] == 'ok':
      feedbackmessages = feedbackmessages + ['<b>Correct!</b>']
    elif item['error'] == 'multiple-tokens':
      feedbackmessages = feedbackmessages + ['Incorrect. More than one word in blank.']
    elif item['error'] == 'same-canonic':
      feedbackmessages = feedbackmessages + ['Correct word but wrong form.']
    elif item['error'] == 'different-canonic':
      feedbackmessages = feedbackmessages + ['Incorrect.']
    else:
      feedbackmessages = feedbackmessages + ['Wrong']
  return ['fibsFeedback'+str(len(fibsAnalysis['Analysis'])),feedbackmessages]


##########################################################################
## DECIDES which generation function to call based on the type of activity
##########################################################################

#Decides the procedure based on activity type
def decision(annotatedInput,current):
  if (current['actType'] == 'Description') or (current['actType'] == 'Vocabulary') or  (current['actType'] == 'Reading') or (current['actType'] == 'Listening') or (current['actType'] == 'Rephrasing'):
    feedbackInfo = generation1(annotatedInput,current)
  elif current['actType'] == 'FIB':
    feedbackInfo =  generation2(annotatedInput,current) #['test',[annotatedInput]]
  else:
   feedbackInfo = ['noMessage',[]]
    #feedbackInfo = ['check',['current',current['actType']]]
  return feedbackInfo


#####################
### MAIN FUNCTION ###
#####################

from mod_python import apache
t = apache.import_module("../code/tagarela_utils.py")

def mainGenerator(sess,annotatedInput):
  current = t.usermodel_get(sess,'current')
  #{'questNr': '1', 'exNr': '1', 'actType': 'Description', 'modNr': '1'}
  if annotatedInput['error']['type'] == 'noActModel':
    feedback = ['noActModel',[]]
  elif annotatedInput['error']['type'] == 'unidentified':
    feedback = decision(annotatedInput,current)
  elif annotatedInput['error']['type'] == 'spelling':
    feedback = spellingerrors(annotatedInput['error']['analysis'])
  elif annotatedInput['stringmatching'][0][0:2] == 'ok':
    feedback = stringMatch(annotatedInput['stringmatching'],current['questNr'])
  else:
    feedback = ['noMessage',[]]
  
  return feedback



## OLD CODE

# def checkPOSfeedback(ai):
#   posM = ai['POSmatching']
#   for item in posM['POS-matches']:
#     if item[2] == 'verb':
#       return {'error':'pos1','pos':'verb','canonics':[ai['disambiguator'][item[1][1]][1][0]['canonic'],ai['disambiguatedAN'][item[1][1]][1][0]['canonic']]}
#
