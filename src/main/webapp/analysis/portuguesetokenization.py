# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: portuguesetokenization.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

#List of pronouns after verbs
pron = {'-a':['a',[{'source':'tokn','pos':'pronoun','case':'acc','gen':'f','num':'si','per':'3s','canonic':'o'}]],
                  '-la':['a',[{'source':'tokn','pos':'pronoun','case':'acc','gen':'f','num':'si','per':'3s','canonic':'o'}]],
                  '-na':['a',[{'source':'tokn','pos':'pronoun','case':'acc','gen':'f','num':'si','per':'3s','canonic':'o'}]],
                  '-as':['as',[{'source':'tokn','pos':'pronoun','case':'acc','gen':'f','num':'pl','per':'3p','canonic':'o'}]],
                  '-las':['as',[{'source':'tokn','pos':'pronoun','case':'acc','gen':'f','num':'pl','per':'3p','canonic':'o'}]],
                  '-nas':['as',[{'source':'tokn','pos':'pronoun','case':'acc','gen':'f','num':'pl','per':'3p','canonic':'o'}]],
                  '-o':['o',[{'source':'tokn','pos':'pronoun','case':'acc','gen':'m','num':'si','per':'3s','canonic':'o'}]],
                  '-lo':['o',[{'source':'tokn','pos':'pronoun','case':'acc','gen':'m','num':'si','per':'3s','canonic':'o'}]],
                  '-no':['o',[{'source':'tokn','pos':'pronoun','case':'acc','gen':'m','num':'si','per':'3s','canonic':'o'}]],
                  '-os':['os',[{'source':'tokn','pos':'pronoun','case':'acc','gen':'m','num':'pl','per':'3p','canonic':'o'}]],
                  '-los':['os',[{'source':'tokn','pos':'pronoun','case':'acc','gen':'m','num':'pl','per':'3p','canonic':'o'}]],
                  '-lhe':['lhe',[{'source':'tokn','pos':'pronoun','case':'dat','num':'si','per':'3s','canonic':'ele'}]],
                  '-lhes':['lhes',[{'source':'tokn','pos':'pronoun','case':'dat','num':'pl','per':'3p','canonic':'ele'}]],
                  #'m-nos':['os',[{'source':'tokn','pos':'pronoun','case':'acc','gen':'m','num':'pl','per':3}],
                  '-me':['me',[{'source':'tokn','pos':'pronoun','case':'acc','num':'si','per':'1s','canonic':'eu'},{'source':'tokn','pos':'pronoun','case':'dat','num':'si','per':1,'canonic':'eu'}]],
                  '-te':['te',[{'source':'tokn','pos':'pronoun','case':'acc','num':'si','per':'2s','canonic':'tu'},{'source':'tokn','pos':'pronoun','case':'dat','num':'si','per':2,'canonic':'tu'}]],
                  '-se':['se',[{'source':'tokn','pos':'pronoun','case':'dat','canonic':'ele'}]],
                  '-nos':['nos',[{'source':'tokn','pos':'pronoun','case':'acc','num':'pl','per':'1p','canonic':'nós'},{'source':'tokn','pos':'pronoun','case':'dat','num':'pl','per':'1p','canonic':'nós'}]],
                  '-vos':['vos',[{'source':'tokn','pos':'pronoun','case':'acc','num':'pl','per':'2p','canonic':'vós'},{'source':'tokn','pos':'pronoun','case':'dat','num':'pl','per':'2p','canonic':'vós'}]]}



########
########  Look at the tokens and see if there are different types of contractions
  

#List of Contractions
prepositions = {'ao':[['a',[{'source':'tokn','pos':'prep'}]],
                     ['o',[{'source':'tokn','pos':'det','spos':'de','gen':'m','num':'si','canonic':'o'}]]],
  'à':[['a',[{'source':'tokn','pos':'prep'}]],
      ['a',[{'source':'tokn','pos':'det','spos':'de','gen':'f','num':'si','canonic':'o'}]]],
  'aonde':[['a',[{'source':'tokn','pos':'prep'}]],
           ['onde',[{'source':'tokn','pos':'adv','spos':'int-lug'}]]],
  'às':[['a',[{'source':'tokn','pos':'prep'}]],
       ['as',[{'source':'tokn','pos':'det','spos':'de','gen':'f','num':'pl','canonic':'o'}]]],
  'aos':[['a',[{'source':'tokn','pos':'prep'}]],
        ['os',[{'source':'tokn','pos':'det','spos':'de','gen':'m','num':'pl','canonic':'o'}]]],
  'àquela':[['a',[{'source':'tokn','pos':'prep'}]],
           ['aquela',[
{
'source':'tokn',
'gender':'f',
'spos':'dem;',
'number':'si',
'pos':'pronoun',
'person':'3s',
'canonic':'aquele',
'frequency':'31',
'number rule':'3',
'gender rule':'2',
'contraction':'',
},
{
'degree':'n',
'gender':'f',
'number':'si',
'pos':'noun',
'regency':'',
'source':'tokn',
'canonic':'aquele',
'frequency':'2',
'number rule':'1',
'gender rule':'2',
}]]],
  'àquelas':[['a',[{'source':'tokn','pos':'prep'}]],
  ['aquelas',[
{
'degree':'n',
'gender':'f',
'number':'pl',
'pos':'noun',
'regency':'',
'source':'lexicon',
'canonic':'aquele',
'frequency':'2',
'number rule':'1',
'gender rule':'2',
},
{
'source':'lexicon',
'gender':'f',
'spos':'dem;',
'number':'pl',
'pos':'pronoun',
'person':'3s',
'canonic':'aquele',
'frequency':'29',
'number rule':'3',
'gender rule':'2',
'contraction':'',
},
]]],
  'àquele':[['a',[{'source':'tokn','pos':'prep'}]],
  ['aquele',[
{
'source':'lexicon',
'gender':'m',
'spos':'dem;',
'number':'si',
'pos':'pronoun',
'person':'3s',
'canonic':'aquele',
'frequency':'31',
'number rule':'3',
'gender rule':'2',
'contraction':'',
},
{
'degree':'n',
'gender':'m',
'number':'si',
'pos':'noun',
'regency':'',
'source':'lexicon',
'canonic':'aquele',
'frequency':'2',
'number rule':'1',
'gender rule':'2',
},
]]],
  'àqueles':[['a',[{'source':'tokn','pos':'prep'}]],['aqueles',[
{
'source':'lexicon',
'gender':'m',
'spos':'dem;',
'number':'pl',
'pos':'pronoun',
'person':'3s',
'canonic':'aquele',
'frequency':'31',
'number rule':'3',
'gender rule':'2',
'contraction':'',
},
{
'degree':'n',
'gender':'m',
'number':'pl',
'pos':'noun',
'regency':'',
'source':'lexicon',
'canonic':'aquele',
'frequency':'2',
'number rule':'1',
'gender rule':'?',
},
]]],
  'àquilo':[['a',[{'source':'tokn','pos':'prep'}]],['aquilo',[
{
'source':'lexicon',
'gender':'m',
'spos':'dem;',
'number':'si',
'pos':'pronoun',
'person':'3s',
'canonic':'aquilo',
'frequency':'30',
'number rule':'?',
'gender rule':'?',
'contraction':'',
},
]]],
  'da':[['de',[{'source':'tokn','pos':'prep'}]],
       ['a',[{'source':'tokn','pos':'det','spos':'de','gen':'f','num':'si','canonic':'o'}]]],
  'das':[['de',[{'source':'tokn','pos':'prep'}]],
        ['as',[{'source':'tokn','pos':'det','spos':'de','gen':'f','num':'pl','canonic':'o'}]]],
  'daí':[['de',[{'source':'tokn','pos':'prep'}]],
         ['aí',[{'source':'tokn','pos':'adv','spos':'lug'}]]],
  'dali':[['de',[{'source':'tokn','pos':'prep'}]],
          ['ali',[{'source':'tokn','pos':'adv','spos':'lug'}]]],
  'daquela':[['de',[{'source':'tokn','pos':'prep'}]],['aquela',[
{
'source':'lexicon',
'gender':'f',
'spos':'dem;',
'number':'si',
'pos':'pronoun',
'person':'3s',
'canonic':'aquele',
'frequency':'31',
'number rule':'3',
'gender rule':'2',
'contraction':'',
},
{
'degree':'n',
'gender':'f',
'number':'si',
'pos':'noun',
'regency':'',
'source':'lexicon',
'canonic':'aquele',
'frequency':'2',
'number rule':'1',
'gender rule':'2',
},
]]],
  'daquelas':[['de',[{'source':'tokn','pos':'prep'}]],['aquelas',[
{
'degree':'n',
'gender':'f',
'number':'pl',
'pos':'noun',
'regency':'',
'source':'lexicon',
'canonic':'aquele',
'frequency':'2',
'number rule':'1',
'gender rule':'2',
},
{
'source':'lexicon',
'gender':'f',
'spos':'dem;',
'number':'pl',
'pos':'pronoun',
'person':'3s',
'canonic':'aquele',
'frequency':'29',
'number rule':'3',
'gender rule':'2',
'contraction':'',
},
]]],
  'daquele':[['de',[{'source':'tokn','pos':'prep'}]],['aquele',[
{
'source':'lexicon',
'gender':'m',
'spos':'dem;',
'number':'si',
'pos':'pronoun',
'person':'3s',
'canonic':'aquele',
'frequency':'31',
'number rule':'3',
'gender rule':'2',
'contraction':'',
},
{
'degree':'n',
'gender':'m',
'number':'si',
'pos':'noun',
'regency':'',
'source':'lexicon',
'canonic':'aquele',
'frequency':'2',
'number rule':'1',
'gender rule':'2',
},
]]],
  'daqueles':[['de',[{'source':'tokn','pos':'prep'}]],['aqueles',[
{
'source':'lexicon',
'gender':'m',
'spos':'dem;',
'number':'pl',
'pos':'pronoun',
'person':'3s',
'canonic':'aquele',
'frequency':'31',
'number rule':'3',
'gender rule':'2',
'contraction':'',
},
{
'degree':'n',
'gender':'m',
'number':'pl',
'pos':'noun',
'regency':'',
'source':'lexicon',
'canonic':'aquele',
'frequency':'2',
'number rule':'1',
'gender rule':'?',
},
]]],
  'daqui':[['de',[{'source':'tokn','pos':'prep'}]],['aqui',[{'source':'tokn','pos':'adv','spos':'lug'}]]],
  'daquilo':[['de',[{'source':'tokn','pos':'prep'}]],['aquilo',[{'source':'tokn','pos':'pronoun','gen':'m','num':'si','spos':'dem','canonic':'aquilo'}]]],
  'dela':[['de',[{'source':'tokn','pos':'prep'}]],['ela',[{'source':'tokn','pos':'pronoun','spos':'obl','gen':'f','num':'si','canonic':'ele'}]]],
  'dele':[['de',[{'source':'tokn','pos':'prep'}]],['ele',[{'source':'tokn','pos':'pronoun','spos':'obl','gen':'m','num':'si','canonic':'ele'}]]],
  'delas':[['de',[{'source':'tokn','pos':'prep'}]],
           ['elas',[{'source':'tokn','pos':'pronoun','spos':'obl','gen':'f','num':'pl','canonic':'ele'}]]],
  'deles':[['de',[{'source':'tokn','pos':'prep'}]],
           ['eles',[{'source':'tokn','pos':'pronoun','spos':'obl','gen':'m','num':'pl','canonic':'ele'}]]],
  'dentre':[['de',[{'source':'tokn','pos':'prep'}]],
            ['entre',[{'source':'tokn','pos':'adv','spos':'lug'}]]],
  'dessa':[['de',[{'source':'tokn','pos':'prep'}]],
           ['essa',[{'source':'tokn','pos':'0'}]]],
  'dessas':[['de',[{'source':'tokn','pos':'prep'}]],
           ['essas',[{'source':'tokn','pos':'0'}]]],
  'desse':[['de',[{'source':'tokn','pos':'prep'}]],
           ['esse',[{'source':'tokn','pos':'0'}]]],
  'desses':[['de',[{'source':'tokn','pos':'prep'}]],
            ['esses',[{'source':'tokn','pos':'0'}]]],
  'desta':[['de',[{'source':'tokn','pos':'prep'}]],
           ['esta',[{'source':'tokn','pos':'0'}]]],
  'destas':[['de',[{'source':'tokn','pos':'prep'}]],
            ['estas',[{'source':'tokn','pos':'0'}]]],
  'deste':[['de',[{'source':'tokn','pos':'prep'}]],
           ['este',[{'source':'tokn','pos':'0'}]]],
  'destes':[['de',[{'source':'tokn','pos':'prep'}]],
            ['estes',[{'source':'tokn','pos':'0'}]]],
  'disso':[['de',[{'source':'tokn','pos':'prep'}]],
           ['isso',[
{
'source':'lexicon',
'gender':'mf',
'spos':'dem;',
'number':'si',
'pos':'pronoun',
'person':'2s',
'canonic':'isso',
'frequency':'34',
'number rule':'?',
'gender rule':'?',
'contraction':'',
},
]]],
  'disto':[['de',[{'source':'tokn','pos':'prep'}]],['isto',[
{
'source':'lexicon',
'gender':'mf',
'spos':'dem;',
'number':'si',
'pos':'pronoun',
'person':'1s',
'canonic':'isto',
'frequency':'32',
'number rule':'?',
'gender rule':'?',
'contraction':'',
},
]]],
  'do':[['de',[{'source':'tokn','pos':'prep'}]],['o',[{'source':'tokn','pos':'det','spos':'de','gen':'m','num':'si','canonic':'o'}]]],
  'dos':[['de',[{'source':'tokn','pos':'prep'}]],['os',[{'source':'tokn','pos':'det','spos':'de','gen':'m','num':'pl','canonic':'o'}]]],
  'dum':[['de',[{'source':'tokn','pos':'prep'}]],['um',[{'source':'tokn','pos':'det','spos':'i','gen':'m','num':'si','canonic':'um'}]]],
  'duns':[['de',[{'source':'tokn','pos':'prep'}]],['uns',[{'source':'tokn','pos':'det','spos':'i','gen':'m','num':'pl','canonic':'um'}]]],
  'duma':[['de',[{'source':'tokn','pos':'prep'}]],['uma',[{'source':'tokn','pos':'det','spos':'i','gen':'f','num':'si','canonic':'um'}]]],
  'dumas':[['de',[{'source':'tokn','pos':'prep'}]],['umas',[{'source':'tokn','pos':'det','spos':'i','gen':'f','num':'pl','canonic':'um'}]]],
  'donde':[['de',[{'source':'tokn','pos':'prep'}]],
             ['onde',[{'source':'tokn','pos':'adv','spos':'int-lug'}]]],
  'na':[['em',[{'source':'tokn','pos':'prep'}]],['a',[{'source':'tokn','pos':'det','spos':'de','gen':'f','num':'si','canonic':'o'}]]],
  'no':[['em',[{'source':'tokn','pos':'prep'}]],['o',[{'source':'tokn','pos':'det','spos':'de','gen':'m','num':'si','canonic':'o'}]]],
  'nas':[['em',[{'source':'tokn','pos':'prep'}]],['as',[{'source':'tokn','pos':'det','spos':'de','gen':'f','num':'pl','canonic':'o'}]]],
  #'nos':[['em',[{'source':'tokn','pos':'prep'}]],['os',[{'source':'tokn','pos':'det','spos':'de','gen':'m','num':'pl','canonic':'o'}]]],
  'numa':[['em',[{'source':'tokn','pos':'prep'}]],['uma',[{'source':'tokn','pos':'det','spos':'i','gen':'f','num':'si','canonic':'um'}]]],
  'num':[['em',[{'source':'tokn','pos':'prep'}]],['uns',[{'source':'tokn','pos':'det','spos':'i','gen':'m','num':'si','canonic':'um'}]]],
  'numas':[['em',[{'source':'tokn','pos':'prep'}]],['umas',[{'source':'tokn','pos':'det','spos':'i','gen':'f','num':'pl','canonic':'um'}]]],
  'nuns':[['em',[{'source':'tokn','pos':'prep'}]],['uns',[{'source':'tokn','pos':'det','spos':'i','gen':'m','num':'pl','canonic':'um'}]]],
  'nalgum':[['em',[{'source':'tokn','pos':'prep'}]],['algum',[{'source':'tokn','pos':'0'}]]],
  'nalguns':[['em',[{'source':'tokn','pos':'prep'}]],['alguns',[{'source':'tokn','pos':'0'}]]],
  'nalguma':[['em',[{'source':'tokn','pos':'prep'}]],['alguma',[{'source':'tokn','pos':'0'}]]],
  'nalgumas':[['em',[{'source':'tokn','pos':'prep'}]],['algumas',[{'source':'tokn','pos':'0'}]]],
  'naquela':[['em',[{'source':'tokn','pos':'prep'}]],['aquela',[{'source':'tokn','pos':'0'}]]],
  'naquelas':[['em',[{'source':'tokn','pos':'prep'}]],['aquelas',[{'source':'tokn','pos':'0'}]]],
  'naquele':[['em',[{'source':'tokn','pos':'prep'}]],['aquele',[{'source':'tokn','pos':'0'}]]],
  'naqueles':[['em',[{'source':'tokn','pos':'prep'}]],['aqueles',[{'source':'tokn','pos':'0'}]]],
  'naquilo':[['em',[{'source':'tokn','pos':'prep'}]],['aquilo',[{'source':'tokn','pos':'pronoun','gen':'m','num':'si','spos':'dem','canonic':'aquilo'}]]],
  'nela':[['em',[{'source':'tokn','pos':'prep'}]],['ela',[{'source':'tokn','pos':'pronoun','spos':'obl','gen':'f','num':'si','canonic':'ele'}]]],
  'nele':[['em',[{'source':'tokn','pos':'prep'}]],['ele',[{'source':'tokn','pos':'pronoun','spos':'obl','gen':'m','num':'si','canonic':'ele'}]]],
  'nelas':[['em',[{'source':'tokn','pos':'prep'}]],['elas',[{'source':'tokn','pos':'pronoun','spos':'obl','gen':'f','num':'pl','canonic':'ele'}]]],
  'neles':[['em',[{'source':'tokn','pos':'prep'}]],['eles',[{'source':'tokn','pos':'pronoun','spos':'obl','gen':'m','num':'pl','canonic':'ele'}]]],
  'nessa':[['em',[{'source':'tokn','pos':'prep'}]],['essa',[{'source':'tokn','pos':'0'}]]],
  'nessas':[['em',[{'source':'tokn','pos':'prep'}]],['essas',[{'source':'tokn','pos':'0'}]]],
  'nesse':[['em',[{'source':'tokn','pos':'prep'}]],['esse',[{'source':'tokn','pos':'0'}]]],
  'nesses':[['em',[{'source':'tokn','pos':'prep'}]],['esses',[{'source':'tokn','pos':'0'}]]],
  'nesta':[['em',[{'source':'tokn','pos':'prep'}]],['esta',[{'source':'tokn','pos':'0'}]]],
  'nestas':[['em',[{'source':'tokn','pos':'prep'}]],['estas',[{'source':'tokn','pos':'0'}]]],
  'neste':[['em',[{'source':'tokn','pos':'prep'}]],['este',[{'source':'tokn','pos':'0'}]]],
  'nestes':[['em',[{'source':'tokn','pos':'prep'}]],['estes',[{'source':'tokn','pos':'0'}]]],
  'nisso':[['em',[{'source':'tokn','pos':'prep'}]],['isso',[{'source':'tokn','pos':'pronoun','gen':'m','num':'si','spos':'dem','canonic':'isso'}]]],
  'nisto':[['em',[{'source':'tokn','pos':'prep'}]],['isto',[{'source':'tokn','pos':'pronoun','gen':'m','num':'si','spos':'dem','canonic':'isso'}]]],
  'noutra':[['em',[{'source':'tokn','pos':'prep'}]],['outra',[{'source':'tokn','pos':'0'}]]],
  'noutro':[['em',[{'source':'tokn','pos':'prep'}]],['outro',[{'source':'tokn','pos':'0'}]]],
  'noutras':[['em',[{'source':'tokn','pos':'prep'}]],['outras',[{'source':'tokn','pos':'0'}]]],
  'noutros':[['em',[{'source':'tokn','pos':'prep'}]],['outros',[{'source':'tokn','pos':'0'}]]],
  'pela':[['por',[{'source':'tokn','pos':'prep'}]],['a',[{'source':'tokn','pos':'det','spos':'de','gen':'f','num':'si','canonic':'o'}]]],
  'pelo':[['por',[{'source':'tokn','pos':'prep'}]],['o',[{'source':'tokn','pos':'det','spos':'de','gen':'m','num':'si','canonic':'o'}]]],
  'pelas':[['por',[{'source':'tokn','pos':'prep'}]],['as',[{'source':'tokn','pos':'det','spos':'de','gen':'f','num':'pl','canonic':'o'}]]],
  'pelos':[['por',[{'source':'tokn','pos':'prep'}]],['os',[{'source':'tokn','pos':'det','spos':'de','gen':'m','num':'pl','canonic':'o'}]]],
  'pra':[['para',[{'source':'tokn','pos':'prep'}]],['a',[{'source':'tokn','pos':'det','spos':'de','gen':'f','num':'si','canonic':'o'}]]],
  'pro':[['para',[{'source':'tokn','pos':'prep'}]],['o',[{'source':'tokn','pos':'det','spos':'de','gen':'m','num':'si','canonic':'o'}]]],
  'pras':[['para',[{'source':'tokn','pos':'prep'}]],['as',[{'source':'tokn','pos':'det','spos':'de','gen':'f','num':'pl','canonic':'o'}]]],
  'pros':[['para',[{'source':'tokn','pos':'prep'}]],['os',[{'source':'tokn','pos':'det','spos':'de','gen':'m','num':'pl','canonic':'o'}]]],
  'praquela':[['para',[{'source':'tokn','pos':'prep'}]],['aquela',[{'source':'tokn','pos':'0'}]]],
  'praquelas':[['para',[{'source':'tokn','pos':'prep'}]],['aquelas',[{'source':'tokn','pos':'0'}]]],
  'praquele':[['para',[{'source':'tokn','pos':'prep'}]],['aquele',[{'source':'tokn','pos':'0'}]]],
  'praqueles':[['para',[{'source':'tokn','pos':'prep'}]],['aqueles',[{'source':'tokn','pos':'0'}]]],
  'praquilo':[['para',[{'source':'tokn','pos':'prep'}]],['aquilo',[{'source':'tokn','pos':'pronoun','gen':'m','num':'si','spos':'dem','canonic':'aquele'}]]]}





#List of abreviations
#abrev = ("Sr.","Sra.","prof.","profa.")

abreviations = ['a.', 'a.c.', 'a.d.', 'a.p.', 'aa.', 'aa', 'ab.', 'abr.', 'abr', 'abrev.',
'abs.', 'ac.', 'acad.', 'ac\xfast.', 'adapt.', 'adit.', 'adj.', 'adv.', 'ago.', 'ago',
'agr.', 'agrim.', 'al.', 'alat.', 'alfaiat.', '\xe1lg.', 'alg.', 'alm.', 'alq.', 'alt.', 
'altit.', 'alv.', 'amer.', 'anal.', 'anat.', 'angl.', 'ant.', 'antr.', 'antrop.', 'ap.', 
'apart.', 'aportug.', 'aprox.', 'arc.', 'arceb.', 'arit.', 'arqueol.', 'arquit.', 'art.', 
'artilh.', 'ass.', 'astron.', 'at.', 'aum.', 'av.', 'b.', 'b\xedb.', 'bibl.', 'b\xedbl.', 
'bibliog.', 'bibliogr.', 'bibliot.', 'bim.', 'bimem.', 'biof\xeds.', 'biogr.', 'biol.', 
'bioq.', 'biotip.', 'bisp.', 'bitrans.', 'bol.', 'bot.', 'bpo.', 'br.', 'bras.', 'brasil.', 
'brig.', 'brig.\xba', 'brig\xba', 'bur.', 'burl.', 'c.', 'c.-alm.', 'c.c.', 'c.g.s.', 'c.i.f.',
'c', 'cad.', 'cal.', 'cal\xe7.', 'c\xe1lc.', 'cap.', 'caps.', 'car.', 'card.', 'carp.', 'cart.', 
'cartogr.', 'cat.', 'categ.', 'cat\xf3l.', 'caus.', 'cav.', 'cav.\xba', 'cav\xba', 'cb.', 'cc.', 
'cel.', 'cenog.', 'cent.', 'centr.', 'cer.', 'cer\xe2m.', 'cerv.', 'cf.', 'cfr.', 'cg', 'cgf', 
'cgr', 'chapel.', 'chil.', 'chin.', 'cia.', 'cia', 'ciber.', 'cibern.', 'cicl.', 'cid.', 'ci\xeanc.', 
'cient.', 'cin.', 'cineg.', 'cinem.', 'cing.', 'cir.', 'circ.', 'c\xedrc.', 'circunscr.', 'cirurg.', 
'cit.', 'citol.', 'citt.', 'cl', 'cl\xe1s.', 'cl\xe1ss.', 'cl\xedn.', 'cm', 'c\xf3d.', 'col.', 
'col.\xba', 'colet.', 'col\xba', 'colomb.', 'cols.', 'com.', 'comb.', 'combust.', 'comdor.', 'comend.', 
'comerc.', 'comp.', 'compar.', 'compl.', 'compr.', 'compt.', 'comte.', 'comte', 'comunic.', 'c\xf4n.', 
'conc.', 'concess.', 'concret.', 'cond.', 'conf.', 'confed.', 'confl.', 'confls.', 'conhec.', 'conj.', 
'conjug.', 'cons.', 'conse.\xba', 'consel.', 'conselh.', 'conseq.', 'const.', 'constel.', 'constr.', 
'cont.', 'contab.', 'contemp.', 'contr.', 'coord.', 'coreog.', 'coreogr.', 'corr.', 'cosm.', 'cp.', 
'cr.', 'cr.\xaa', 'cr\xe9d.', 'crim.', 'criptog.', 'crist.', 'cron.', 'cr\xf4n.', 'cronol.', 'cruz.', 
'cul.', 'culin.', 'cult.', 'cx.', 'cx', 'd.', 'd.\xaa', 'd.c.', 'd', 'da.', 'd\xaa', 'dd.', 'd\xe9b.', 
'dec.', 'decr.', 'ded.\xba', 'def.', 'defin.', 'definit.', 'dem.', 'democ.', 'democr.', 'demog.', 
'demogr.', 'dens.', 'dep.', 'depr.', 'deps.', 'depto', 'deptos', 'der.', 'deriv.', 'derm.', 'des.', 
'desc.', 'desemb.', 'desen.', 'desp.', 'despor.', 'det.', 'determ.', 'dev.\xba', 'dez.', 'dez.\xba', 
'dez', 'dg', 'dgf', 'dgr', 'dial.', 'dic.', 'did.', 'did\xe1t.', 'diet.', 'dif.', 'dim.', 'dimin.', 
'din.', 'din\xe2m.', 'dipl.', 'diplom.', 'dir.', 'disc.', 'diss.', 'docs.', 'docum.', 'd\xf3l.', 
'dom.', 'dr.', 'dr.\xaa', 'dr.as', 'dr.\xaas', 'dra.', 'dra', 'dram.', 'drs.', 'dur.', 'dz.', 'e.g.', 
'ecol.', 'econ.', 'ed.', 'edif.', 'educ.', 'elem.', 'eletr.', 'eletr\xf4n.', 'elipt.', 'em.\xaa', 
'em\xaa', 'emb.', 'embr.', 'emp.', 'emp\xedr.', 'empr.', 'empr\xe9s.', 'enc.', 'encicl.', 'end.', 
'energ.', 'energ\xe9t.', 'enf.', 'eng.', 'eng.\xba', 'enol.', 'ens.', 'ent.', 'entom.', 'entomol.', 
'enx.', 'epig.', 'epigr.', 'eq.', 'equat.', 'equit.', 'equiv.', 'erud.', 'esc.', 'escand.', 'escoc.', 
'escol.', 'escr.', 'escul.', 'escult.', 'esgr.', 'esl.', 'eslav.', 'esot.', 'esp.', 'espec.', 'especialm.', 
'especif.', 'especnte', 'espir.', 'espirt.', 'esport.', 'esq.', 'est.', 'estad.', 'estat.', 'estat\xedst.', 
'est\xe9t.', 'estil.', 'estr.', 'estrang.', 'estrangeir.', 'estrat.', 'estrat\xe9g.', 'estrut.', 'estud.', 
'et.', 'etc.', 'etim.', 'etimol.', 'eur.', 'ex.', 'ex.\xaa', 'ex.ma', 'ex.mo', 'ex\xaa', 'excdo', 'excl.', 
'exclam.', 'exe.', 'ex\xe9rc.', 'exmo', 'exp.', 'exper.', 'experim.', 'expl.', 'explor.', 'explos.', 'export.', 
'expr.', 'express.', 'ext.', 'extens.', 'extr.', 'extrat.', 'f.', 'f.\xba', 'f', 'f\xe1b.', 'fac.', 'fam.',
'farm.', 'fasc.', 'fascc.', 'fascs.', 'fed.', 'feder.', 'fem.', 'fen\xf4m.', 'fer.', 'ferrov.', 'feud.', 'fev.', 
'fev.\xba', 'fev', 'ff.', 'fg.', 'fig.', 'fil.', 'filos.', 'fin.', 'finl.', 'f\xeds.', 'fisc.', 'fisiol.', 'fl.', 
'flex.', 'flor.', 'floric.', 'fls.', 'flum.', 'flumin.', 'fluv.', 'fm.', 'fo.', 'f\xba', 'fol.', 'folc.', 
'folcl.', 'folh.', 'fols.', 'fon.', 'fon\xe9t.', 'fonol.', 'form.', 'f\xf3rm.', 'formul.', 'fort.', 'f\xf3s.', 
'fot.', 'fotogr.', 'fotom.', 'fr.', 'franc.', 'freg.', 'freq.', 'frig.', 'frut.', 'fs.', 'fss.', 'ft.', 'fund.', 
'fut.', 'futb.', 'futeb.', 'futur.', 'g.', 'g.m.', 'g', 'ga.', 'gal.', 'gar.', 'gav.', 'g\xean.', 'gen.', 'gen\xe9t.', 
'geo.', 'geof.', 'geof\xeds.', 'geog.', 'geogr.', 'geol.', 'geom.', 'ger.', 'germ.', 'gf', 'gin.', 'gin\xe1st.', 
'ginec.', 'ginecol.', 'g\xedr.', 'gloss.', 'gov.', 'gr.', 'graf.', 'gr\xe1f.', 'grafol.', 'gram.', 'grav.', 'grd', 
'grs.', 'grz', 'gs', 'gt.', 'guar.', 'h.', 'h.c.', 'h.p.', 'h', 'ha', 'hab.', 'hag.', 'hebr.', 'herb.', 
'herd.\xba', 'het.', 'hg', 'h\xedb.', 'h\xedbr.', 'hidr.', 'hidr\xe1ul.', 'hidrod.', 'hidrog.', 'hidrogr.', 
'hidrom.', 'hidrost.', 'hidrot.', 'hig.', 'hind.', 'hip.', 'h\xedp.', 'hipnot.', 'hipnoter.', 'hip\xf3t.', 
'hipot.', 'hisp.', 'hist.', 'histol.', 'hl', 'hm', 'hol.', 'hom.', 'homeo.', 'homeop.', 'homof.', 'homog.', 
'homogr.', 'homon.', 'hom\xf4n.', 'hon.', 'hond.', 'hort.', 'hortic.', 'hpz', 'humor.', 'h\xfang.', 'hw', 
'i.e.', 'iat.', 'ib.', 'ib\xe9r.', 'ibid.', 'iconog.', 'iconogr.', 'ict.', 'ictiol.', 'igr.', 'il.', 
'ilm\xaa.', 'ilma', 'ilm\xba.', 'ilmo', 'ilum.', 'ilus.', 'ilustr.', 'imigr.', 'imit.', 'imp.', 'imper.', 
'imperat.', 'imperf.', 'impes.', 'impess.', 'import.', 'impr.', 'inc.', 'incog.', 'incs.', 'ind.', 
'indef.', 'indet.', 'indiv.', 'ind\xfas.', 'ind\xfast.', 'inf.', 'infan.', 'infer.', 'infin.', 'infinit.', 
'infl.', 'inform.', 'ing.', 'ingl.', 'inscr.', 'insep.', 'inst.', 'instit.', 'int.', 'intens.', 'interj.', 
'intern.', 'internac.', 'interr.', 'interrog.', 'interrogat.', 'intr.', 'intrans.', 'inus.', 'inv.', 'invar.', 
'inven\xe7.', 'invest.', 'investig.', 'iran.', 'iraq.', 'irl.', 'irr.', 'irreg.', 'isl.', 'islam.', 'isr.', 
'israel.', 'it.', '\xedt.', 'ital.', 'italian.', 'iug.', 'jan.', 'jan.\xba', 'jan', 'jap.', 'jard.', 'jardin.', 
'jav.', 'jaz.', 'joalh.', 'jog.', 'jorn.', 'jr.', 'jr', 'jud.', 'judsmo', 'jul.', 'jul', 'jun.', 'jun', 
'jur.', 'juris.', 'jurisp.', 'jurispr.', 'k', 'kcal', 'kg.', 'kg', 'kgf.', 'kgm', 'kj', 'kl', 'km', 'l.', 
'l', 'lab.', 'labor.', 'laborat.', 'labort.', 'lact.', 'l\xe2m.', 'lan\xe7.', 'larg.', 'lat.', 'latit.', 
'lb.', 'leg.', 'l\xe9g.', 'legisl.', 'l\xe9gs.', 'leit.', 'lex.', 'lgo.', 'lib.', 'lig.', 'ling.', 'l\xedng.', 
'lingu\xedst.', 'l\xedq.', 'lit.', 'liter.', 'literat.', 'litn.', 'lituan.', 'litur.', 'liturg.', 'liv.', 
'livr.', 'loc.', 'l\xf3g.', 'log.', 'logar.', 'log\xedst.', 'long.', 'lr.', 'ltd.', 'ltda.', 'lug.', 'lus.', 
'luso-afr.', 'lut.', 'm.', 'm.d.c.', 'm', 'm\xaa', 'ma\xe7.', 'ma\xe7on.', 'mag.', 'mag\xaa.', 'mag\xaa', 
'magn.', 'magnet.', 'magnit.', 'mai.', 'mai', 'mai\xfasc.', 'maj.', 'mal.', 'malh.', 'manuf.', 'm\xe1q.', 
'maq.', 'maranh.', 'marg.', 'marin.', 'marinh.', 'marr.', 'marroq.', 'marx.', 'masc.', 'mat.',
'matad.', 'matem.', 'mater.', 'matogros.', 'mato-gros.', 'm\xe1x.', 'mcal', 'mec.', 'mec\xe2n.', 'med.', 
'm\xe9d.', 'mediev.', 'medv.', 'mem.', 'memo.', 'memor.', 'mens.', 'mer.', 'merc.', 'mercad.', 'merid.', 
'met.', 'metaf.', 'met\xe1f.', 'metaf\xeds.', 'metaf\xf3r.', 'metalur.', 'meteor.', 'meteorol.', 'm\xe9tr.', 
'mex.', 'mexic.', 'mgf', 'mgr', 'microb.', 'microbiol.', 'microsc.', 'microsc\xf3p.', 'mil.', 'min.', 
'm\xedn.', 'miner.', 'ming.', 'min\xfas.', 'm\xedst.', 'mit.', 'mitol.', 'ml', 'mm', 'mob.', 'mod.', 
'moed.', 'mon.', 'monog.', 'monogr.', 'mons.', 'mont.', 'mor.', 'moralid.', 'morf.', 'morfol.', 'mov.', 
'mr.', 'mrs.', 'mun.', 'munic.', 'mus.', 'm\xfas.', 'museol.', 'n.', 'n.b.', 'n.\xba', 'n', 'nac.', 
'nat.', 'natur.', 'n\xe1ut.', 'nav.', 'naz.', 'neerl.', 'neg.', 'neol.', 'neolog.', 'neozel.', 'neur.', 
'nicar.', 'nicarg.', 'nig.', 'niger.', 'n\xba', 'nom.', 'nor.', 'n\xf3rd.', 'norm.', 'normat.', 
'norueg.', 'notic.', 'nov.', 'nov.\xba', 'nov', 'num.', 'n\xfam.', 'nutr.', 'o.d.c.', 'o.k.', 'ob.', 
'obed.', 'obr.\xba', 'obs.', 'obsol.', 'obss.', 'obst.', 'obstet.', 'oc.', 'ocean.', 'oceangr.', 
'ocid.', 'ocul.', 'ocult.', 'odont.', 'odontol.', 'of.', 'oft.', 'oftalm.', 'oftalmol.', 'ok', 'olig.', 
'on\xe7.', 'op.', '\xf3pt.', 'optat.', 'or.', 'orat.', 'ord.', 'ordin.', 'org.', 'organiz.', 'orig.', 
'origin.', 'ort.', 'ortog.', 'ortogr.', 'ortogr\xe1f.', 'ortop.', 'out.', 'out.\xba', 'out', 'oz.', 
'p.', 'p.ex.', 'pl', 'pag.', 'p\xe1g.', 'p\xe1gg.', 'pags.', 'pal.', 'paleont.', 'paleontol.', 'pals.',
'pan.', 'panam.', 'pap.', 'paq.', 'par.', 'paraens.', 'parag.', 'paraib.', 'paral.', 'paran.', 'parl.', 
'par\xf4n.', 'part.', 'partic.', 'pass.', 'passt.', 'past.', 'patol.', 'patr.', 'p\xe1tr.', 'patron.', 
'patr\xf4n.', 'paul.', 'paulist.', 'p\xe7.', 'pe', 'pec.', 'ped.', 'pedag.', 'pediat.', 'pedr.', 'pej.', 
'peq.', 'per.', 'perf.', 'perfum.', 'pern.', 'pernamb.', 'pers.', 'p\xe9rs.', 'perspect.', 'pesc.', 
'pesq.', 'pess.', 'petr.', 'petroq.', 'pg.', 'p\xedl.', 'pint.', 'pirot.', 
'pirotec.', 'pirot\xe9c.', 'pisc.', 'piscic.', 'pl.', 'planej.', 'pleb.', 'pneu.', 
'poes.', 'po\xe9t.', 'pol.', 'pol\xedc.', 'pol\xedt.', 'pop.', 'popul.', 
'port.', 'poss.', 'possess.', 'pov.', 'pp.', 'pr.', 'pr\xe1t.', 'pref.', 'pr\xe9-hist.', 
'prep.', 'prepos.', 'pres.', 'presc.', 'presid.', 'pret.', 'prev.', 
'prim.', 'primit.', 'princ.', 'priv.', 'probl.', 'proc.', 'prod.', 'prof.', 
'prof.\xaa', 'prof.\xaas', 'profis.', 'profiss.', 'profs.', 'pron.', 'pronon.', 
'pron\xfan.', 'propag.', 'propos.', 'pr\xf3t.', 'prot.', 'protest.', 'protoc.',
'protoz.', 'prov.', 'provb.', 'provc.', 'prox.', 'ps.', 'pseud.', 'psic.', 'psiq.', 
'psiquiat.', 'pto.', 'pub.', 'public.', 'pug.', 'q.', 'q.b.', 'q.e.d.', 'q.v.', 
'ql.', 'qt.', 'qtd', 'qual.', 'qualif.', 'quant.', 'quantit.', 'quart.', 
'quest.', 'qu\xedm.', 'quinz.', 'quir.', 'quirom.', 'r.', 'r.\xaa', 'r.p.m.', 'r.p.s.', 
'r', 'rac.', 'rad.', 'r\xe1d.', 'radioat.', 'radiod.', 'radiodif.', 'radiogr.', 
'radiol.', 'radiot.', 'radiotec.', 'radiot\xe9c.', 'radiot\xe9cn.', 
'radioter.', 'rb.', 'rd.', 'realid.', 'rec.', 'rec.\xba', 'recip.', 'rec\xedp.',
'rec\xedpr.', 'red.', 'ref.', 'refl.', 'reg.', 'reg.\xba', 'regim.', 'region.',
'regress.', 'rel.', 'rela\xe7.', 'relat.', 'relativ.', 'relig.', 'reloj.', 'rem.te', 
'rep.', 'repart.', 'repert.', 'report.', 'rep\xfab.', 'res.', 'rest.', 
'restr.', 'result.', 'ret.', 'ret\xf3r.', 'retrosp.', 'rev.', 'rev.\xaa', 
'rev.do', 'rev.\xba', 'revers.', 'rev\xba', 'rg.', 'rib.', 'rio-gr.', 'rod.', 'rom.',
'romn.', 'rot.', 'rs.', 'rub.', 'rur.', 'rus.', 'russ.', 'r\xfast.', 's.', 's.a.', 
's.a.s.', 's.d.', 's.n.', 's.\xba', 's.r.', 's.rev.a', 's.rev.\xaa', 's.rev.ma', 
's.rev.mas', 's.s.', 's.s.a', 's.s.\xaa', 's.s.as', 'si', 'sab.', 'sac.',
'sal.', 'sap.', 'sapat.', 'sarg.', 's\xe1t.', 'sb.', 'sc.', 'sec.', 's\xe9c.', 
'secr.', 's\xe9cs.', 'seg.', 'segg.', 'segs.', 'sel.', 'sem.', 'sem\xe2nt.', 
'semin.', 'sen.', 'sent.', 'seq.', 'seqq.', 's\xe9r.', 'serg.', 'serr.', 
'serralh.', 'serv.', 's\xe9rv.', 'set.', 'set.\xba', 'set', 'setent.', 'sf.', 
'sg.', 'sib.', 'sid.', 'sider.', 'sign.', 'signif.', 's\xedl.', 'silog.', 'silv.'
, 'silvic.', 'simb.', 's\xedmb.', 'simbol.', 'simb\xf3l.', 'simpl.', 'sin.', 
'sing.', 'sinon.', 'sin\xf4n.', 's\xednt.', 'sint.', 's\xedr.', 'sist.', 'sit.',
'snr.', 'snr.\xaa', 'soc.', 'sociol.', 's\xf3l.', 'sov.', 'sovi\xe9t.', 'sr.', 
'sr.\xaa', 'sr.\xaas', 'sra.', 'sras.', 'sres.', 'srs.', 'srta', 'srt\xaa', 
'srtas', 'srt\xaas', 'ss.', 'ss.rev.as', 'ss.rev.mas', 'sta.', 'sto.', 'sub.', 
'subj.', 'subjunt.', 'subord.', 'subst.', 'subvar.', 'suc.', 'sue.', 'suec.', 
'suf.', 'suj.', 'sul-af.', 'sul-afr.', 'sul-amer.', 'sup.', 'superf.', 'superl.',
'supl.', 'suprf.', 'suprl.', 'suprs.', 'surr.', 'surreal.', 't.', 't.v.', 't', 
'tab.', 't\xe1b.', 'tail.', 'tang.', 'taquigr.', 't\xe1t.', 'taxid.', 'tb.', 
'tchec.', 'teat.', 'teatr.', 'tec.', 't\xe9c.', 'tecel.', 'tecn.', 't\xe9cn.', 
't\xe9cnol.', 'tect.', 'tel.', 'tele.', 'telec.', 'telecom.', 'telef.', 'teleg.',
'telegr.', 'tel\xe9gr.', 'telem.', 'telev.', 'temp.', 'temper.', 'ten.', 'teol.'
, 'teor.', 'terap.', 'terap\xeaut.', 'term.', 't\xe9rm.', 'termin.', 'terminol.'
, 'termod.', 'termodin\xe2m.', 'terr.', 'territ.', 'tes.', 'test.', 'test.\xba',
't\xeaxt.', 'tf', 'tib.', 'tibet.', 'tint.', 'tip.', 'tipogr.', 'tipol.', 't\
xedt.', 'ton.', 't\xf4n.', 'topog.', 'topogr.', 'topol.', 'torp.', 'tosc.', 
'tox.', 'toxiol.', 'tr.', 'trab.', 'trad.', 'tradic.', 'tr\xe1f.', 'trans.', 
'transit.', 'transp.', 'trat.', 'trav.', 'trib.', 'trig.', 'trigon.', 'trim.', 
'trimest.', 'triv.', 'trop.', 'tunis.', 'tup.', 'tupi-guar.', 'tur.', 'turc.', 
'turism.', 'tv.', 'tv', 'tvs', 'u.e.', 'ucran.', 'ult.', 'umb.', 'un.', 'unif.',
'univ.', 'univers.', 'urb.', 'urban.', 'urol.', 'urug.', 'us.', 'util.', 'utilid.', 
'utilit.', 'utop.', 'ut\xf3pi.', 'v.', 'v.\xaa', 'v.\xba', 'v.p.', 'v.rev.a',
'v.rev.\xaa', 'v.rev.as', 'v.rev.ma', 'v.rev.mas', 'v.s.', 'v.s.a', 'v.s.\xaa',
'v.s.as', 'v', 'v\xaa', 'var.', 'vat.', 'vb.', 'veg.', 'vel.', 'venez.'
, 'venezuel.', 'verb.', 'vern.', 'vers.', 'versif.', 'vet.', 'veter.', 'vidr.',
'vig.', 'vig.\xba', 'vig\xba', 'vin.', 'vinic.', 'viol.', 'vitr.', 'v\xba', 
'voc.', 'vog.', 'vol.', 'voll.', 'vols.', 'vs.', 'vulc.', 'vulg.', 'vv.', 'w', 
'wh', 'ws', 'xenof.', 'xerog.', 'xerogr.', 'xilog.', 'xin.', 'yd', 'zo.', 'zool.'
, 'zoot.', 'zootec.', 'zoot\xe9c.']

if __name__ == "__main__":
	import sys
	sys.path.append('../code')
	import json as json
	print json.write(pron) + "\n\n"
	print json.write(prepositions) + "\n\n"
	print json.write(abreviations)


