# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: reqConcept.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""


def makelist(listTokenizer):
  listtokens = []
  for element in listTokenizer:
    listtokens = listtokens + [element[0]]
  return listtokens
  

  
def mainReqConc(tokensSt,listofReqwords):
  listoftokens = makelist(tokensSt)
  required = []
  present = []
  for item in listofReqwords:
    if item in listoftokens:
      present = present + [item]
    else:
      required = required + [item]
  return required


# sTok = [['se', [{'pos': '0'}]], ['chama', [{'pos': '0'}]], ['paulo', [{'pos': '0'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# reqWs = ['paulo','ele']
# 
# 
# def agr_test():
#     print mainReqConc(sTok,reqWs)
# 
# 
# if __name__ == "__main__":
#   agr_test()
