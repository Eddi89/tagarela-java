"""
---------------------------------------------------------------
$Id: feedbackManager.py,v 1.5 2008-07-31 14:06:32 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

from mod_python import apache
t = apache.import_module("../code/tagarela_utils.py")

# load the various feedback modules
import feedbackGenerator
import messageGenerator

def feedback(sess,annotatedInput):
   feedbackAnalysis = feedbackGenerator.mainGenerator(sess,annotatedInput)
   feedbackMessage = messageGenerator.mainMessage(feedbackAnalysis)

   # define link to solution:
   current = t.usermodel_get(sess,'current')
   if ('bestAnswer' in annotatedInput) and ('stringmatching' in annotatedInput) and (annotatedInput['stringmatching'][0] != 'ok'):
      showAnswerLink = """<p><i>To see a possible answer, click <a href="#" onclick="postJSON('../ajax.py/showSolution?actType=%s&amp;modNr=%s&amp;exNr=%s&amp;questNr=%s', {'showSolution':'%s'});return false;">here</a>.</i></p>"""%(current['actType'],current['modNr'],current['exNr'],current['questNr'],annotatedInput['bestAnswer'])
   else:
      showAnswerLink = ''
      apache.log_error('### TAGARELA ERROR: CANNOT FIND BEST ANSWER in %s, exercise %s, question %s'%(current['modNr'],current['exNr'],current['questNr']),apache.APLOG_CRIT)
   #
   return """
         <i>Input:</i> <b>%s</b>
         <p>%s</p>
         %s
         """%(annotatedInput['learnerInput'],feedbackMessage,showAnswerLink)




def debugLongfeedback(sess,annotatedInput):

  return """
          <p>Input: %s</p>
          <p>Tokens: %s</p>
          <p>Spelling: %s</p>
          <p>Lexicon: %s</p>
          <p>Disambiguator: %s</p>
          <p>Parser: %s</p>
          <p>Agreement Analysis: %s</p>
          <p>Best Answer: %s</p>
          <p>Tokens Answer: %s</p>
          <p>Lexicon Answer: %s</p>
          <p>Disambiguated Answer: %s</p>
          <p>Required Words: %s</p>
          <p>String Matching: %s</p>
          <p>Token Matching: %s</p>
          <p>Canonic Matching: %s</p>
          <p>POS Matching: %s</p>
          <p>Semantic analysis: %s</p>
          <p>Parser Answer: %s</p>
          <p>Current: %s</p>
         """%(annotatedInput['learnerInput'],annotatedInput['tokens'],
              annotatedInput['spellAnalysis'],annotatedInput['lexInfo'],
              annotatedInput['disambiguator'],annotatedInput['parser'],
              annotatedInput['agreement'],annotatedInput['bestAnswer'], 
              annotatedInput['tokensAnswer'], annotatedInput['lexiconAN'],
              annotatedInput['disambiguatedAN'], annotatedInput['reqWordsAna'],
              annotatedInput['stringmatching'],annotatedInput['tokenMatching'],
              annotatedInput['canonicMatching'],annotatedInput['POSmatching'],annotatedInput['semanticCheck'],
              annotatedInput['parserAN'],t.usermodel_get(sess,'current'))



def debugShortfeedback(sess,annotatedInput):

  return """
          <p>Input: %s</p>
          <p>Tokens: %s</p>
          <p>Spelling: %s</p>
          <p>Parser: %s</p>
          <p>Agreement Analysis: %s</p>
          <p>Best Answer: %s</p>
          <p>Required Words: %s</p>
          <p>String Matching: %s</p>
          <p>Token Matching: %s</p>
          <p>Canonic Matching: %s</p>
          <p>POS Matching: %s</p>
          <p>Parser Answer: %s</p>
          <p>Current: %s</p>
         """%(annotatedInput['learnerInput'],annotatedInput['tokens'],
              annotatedInput['spellAnalysis'], annotatedInput['parser'],
              annotatedInput['agreement'],annotatedInput['bestAnswer'], 
              annotatedInput['reqWordsAna'], annotatedInput['stringmatching'],
              annotatedInput['tokenMatching'], annotatedInput['canonicMatching'],
              annotatedInput['POSmatching'], annotatedInput['parserAN'],
              t.usermodel_get(sess,'current'))

# datastructure = {'learnerInput':learnerInput,'tokens':tokenization[1],'lexInfo':lexicalanalysis,
#                    'spellAnalysis':spellanalysis, 'disambiguator':disambanalysis, 'parser':parsetree[0],
#                    'agreement':AgreementCheck, 'bestAnswer':bestAnswer, 'requiredWords':requiredWs,
#                    'tokensAnswer':tokensAN, 'lexiconAN':lexiconAN, 'disambiguatedAN'disambiguatedAN,
#                    'reqWordsAna': reqWordAna, 'stringmatching':stringMatching, 'tokenMatching':TokenMatching,
#                    'canonicMatching':CanonicMatching, 'POSmatching': POSMatching, 'parserAN':parsetreeAN[0]}


def generalfeedback(sess,annotatedInput):
  return """
         <p>%s</p>
         """%annotatedInput

def main(sess,annotatedInput):
   return feedback(sess,annotatedInput)
