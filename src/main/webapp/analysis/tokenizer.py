# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: tokenizer.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

import string
import re
import portuguesetokenization

### Things to fix
# Capital letters for contractions with propositions, e.g., 'Ao' 'Na', etc
# What to do with 'word' (word in quotations)?

###########
# Load all information about tokenization of portuguese 
# from portuguesetokenization module

abrev = portuguesetokenization.abreviations
pronAfterVerbs = portuguesetokenization.pron
prepcont = portuguesetokenization.prepositions

###########

### Punctuation Function ###
## Separates punctuation to facilitate spliting

def punctuation(nopunc):
    # separating punctuation for tokenization
    #all puntuation except '.'
    #nopunc = re.sub(r'([,:;!\?\{\}\[\]\(\)])',r' \1 ',nopunc)
    nopunc = nopunc.replace('!','')
    nopunc = nopunc.replace(',',' ,')
    nopunc = nopunc.replace('{','')
    nopunc = nopunc.replace('}','')
    nopunc = nopunc.replace('[','')
    nopunc = nopunc.replace(']','')
    nopunc = nopunc.replace('(','')
    nopunc = nopunc.replace(')','')
    nopunc = nopunc.replace(':',' :')
    nopunc = nopunc.replace(';','')
    nopunc = nopunc.replace('?',' ?')


    return nopunc


###########


### abreviation function ###
# It should take care of tokens ending with "." (a dot)
# It cheks to see if the token is in the abreviation list
# if not it splits "token", "dot"

def abreviation(splitString):
  abrevOutPut = []
  for x in splitString:
    #Check is it could be an abreviation with punctuation
    if x[-1] == ".":
      #The abreviation list is not capitalized
      #we have to put the abreviation in lower case
      y = x.lower()
      #check if y is in abrev list
      if y in abrev:
        abrevOutPut = abrevOutPut + [[x,
                                      [{'pos':'abrev',
                                      'source':'tokn'}]]]
        #abrevList = abrevList + [x]
      else:
      #if y is not in abrev list then the "." is final dot

        item = y.split('.')
        abrevOutPut = abrevOutPut + [[item[0],[{'pos':'0'}]],
                                     [x[-1],[{'pos':'punct','source':'tokn'}]]]
    else:
      abrevOutPut = abrevOutPut + [[x,[{'pos':'0'}]]]
  return abrevOutPut



########  From here on the data structure is already created
########  Look at the tokens and see if there are different types of contractions


### Contraction Functions ###

### First functions deal with pronouns ####
## Tricky case: -nos and *m-nos=-os   => Problem not solved

def makeverb(token):
  if token[-4:-2] == 'á-':
    verb = token[:-4]+'ar'
  elif token[-4:-2] == 'ê-':
    verb = token[:-4]+'er'
  elif token[-4:-2] == 'í-':
    verb = token[:-4]+'ir'
  else:
    lista = token.split('-')
    verb = lista[0]
  return verb

def endings(lista):
  endings = pronAfterVerbs.keys()
  for x in endings:
    # if ending = last segment of token that has the lengh of ending
    # This is necessary to guarantee that -a and -as will be distinguished
    if x == lista[0][-len(x):]:
      verb = [makeverb(lista[0]),[{'pos':'verb','source':'tokn'}]]
      return verb, pronAfterVerbs[x]
  return lista


def pronouns(abrevOutPut):
  contrList = []
  for tokendef in abrevOutPut:
    reply = endings(tokendef)
    if str(type(reply)) == "<type 'tuple'>":
      contrList = contrList + [reply[0]] + [reply[1]]
    else:
      contrList = contrList + [endings(tokendef)]
  return contrList



###################
## These functions deal with contractions with prepositions

def prepositions(lista):
  #list to be built
  prepList = []

  # Make a list with all contracted prepositions 
  # before the for loop begins
  keysprep = prepcont.keys()
  #for each token in the list ['token':{definition1},{definition N}]
  for token in lista:
    # token[1] = definition 1 of token (dictionary)
    if token[1][0]['pos'] == '0':
      #token[0] = token (string)
      if token[0] in keysprep:
        prepList = prepList + [prepcont[token[0]][0]] + [prepcont[token[0]][1]]
      else:
        prepList = prepList + [token]
    else:
      prepList = prepList + [token]
  return prepList


#### List of Tokens ###
# It creates a list of tokens without any other info

def listoftok(listaitens):
  toknzd = []
  for x in listaitens:
    toknzd = toknzd + [x[0]]
  return toknzd



### Main Function ###
# For the moment it receives a string #


def mainToknz(st):
    lowst = st.lower()
    #Separating ponctuation to faciliate spliting
    sepPunct = punctuation(lowst)
    #Spliting string using spaces
    splitString = sepPunct.split()
    #calling abreviation
    #The output of abreviation is a list of dictionaries aready with
    # POS for elements that are abreviated
    # AbrevOuPut is what will become a list of annotated tokens
    abrevOutPut = abreviation(splitString)
    #Calling contraction
    pronomes = pronouns(abrevOutPut)
    #Calling contracted prepostions
    contractionprep = prepositions(pronomes)
    listoftokens = listoftok(contractionprep)


    return (listoftokens,contractionprep)

#def toknz_test1():
#    print "Sr. e.. Sra.: quero ver!  ", mainToknz('Sr. e.. Sra.: quero... ver.!')
    #print "Sr. comeu na minha casa urban.!  ", main_toknz('Sr. comeu na minha casa urban.!')
     #print "Sr. vai comê-la, e contá-la. \n \n", main_toknz('Sr. vai comê-la e contá-la.')
     #print "Sr. pegou-me aqui. \n \n", main_toknz('Sr. pegou-me aqui.')
     #print "Srs. revisaram-nos e vestiram-se aqui. \n \n", main_toknz('Srs. revisaram-nos e vestiram-se aqui.')
     #print "Srs. revisaram-na ao pai. \n \n", main_toknz('Srs. revisaram-na ao pai.')
      #print "ao Srs. revisaram-na ao pai. \n \n", main_toknz('ao Srs. revisaram-na ao pai.')
#      print "aos Srs. queiram-nas aonde ousá-los \n \n", mainToknz('daí praquilo falam-me')
    #['Sr','.'] ==  main_toknz('Sr.')

#if __name__ == "__main__":
#   toknz_test1()
