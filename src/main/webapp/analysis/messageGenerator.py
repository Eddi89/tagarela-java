# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: messageGenerator.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""


messages = {'spell1':"I don't know the word <b><i>%s</i></b>.<p>Please replace it before I continue my analysis.</p>",
            'spell2':"I cannot find the word <b><i>%s</i></b> in my dictionary.<p>Are you sure you don't mean <b><i>%s</i></b>?</p>",
            'spell3':"I think there is a problem with the spelling of <b><i>%s</i></b>.<p>See if you can replace it with one of the following words: %s.</p>",
            'spell4':"I found some spelling errors in your input. Look at the suggestions I have for them.%s",
            'strM1':"Very Good! Keep going!",
            'strM2':"Your answer is correct, but review it for capitalization errors.",
            'strM3':"Good! But you forgot the final punctuation mark.",
            'strM4':"Your answer is fine, but you did not put the final puctuation mark and you should check it for capitalization errors.",
            'strM5':"Excellent!",
            'strM6':"You got it!",
            'strM7':"Very Good!",
            'strM8':"That's it!",
            'strM9':"Good Job!",
            'strM10':"That's right!",
            'strM11':"Perfect!",
            'strM12':"All right! Keep going!",
            'strM13':"Your answer is correct, but review it for unnecessary spaces or punctuation.",
            'strM14':"There seems to be an issue with the final punctuation in your sentence.",
            'reqW1D':"You have not included this key word in your answer: %s.<p>Please use all the words required to describe the picture.</p>",
            'reqW2D':"There are some key words missing in your answer.<p>Look at the list of required words and make sure you use all of them.</p>",
            'descWO1':"I know your answer is complete, but I can't match it to any possible answers in my database.<p>Try changing the position of the words in your sentence, or checking for wrong punctuation.</p>",
            'missingPunct':"You have provided all the necessary words but review your answer for missing or unnecessary punctuation. <p>See if you are not missing the final dot.</p>",
            'extraPunct':"I think there are some punctuation marks in your answer that are not necessary.",
            'agr1':"There is an agreement error in <b>%s</b> between the <b>%s</b> and the <b>%s</b> in the sequence <b><i>%s</i></b> from your answer.",
            'agr2':"There are some agreement errors in your sentence. Look at the explanations below to fix them. %s",
            'CanoCompl1':'You are almost there. Change the form of the word <b>"%s"</b>, and try again.',
            'CanoCompl2':'You are using different forms of some of the words in the answer. Change the forms of the words below so that they can match the ones I am expecting.<p>%s</p>',
            'pos1':"I am not expecting the %s <b>%s</b> for this answer. Try using <b>%s</b> instead.",
            'pos2':"There is a problem with the %s you have chosen. Try using <b>%s</b> instead of <b>%s</b>.",
            'pos3':"I think there is a problem with the %s you have chosen. <p>Are you sure you want to use <b>%s</b> instead of <b>%s</b>?</p>",
            'pos4':"Please check if the %s <b>%s</b> is appropriate for this answer.<p> You may try using <b>%s</b> instead. </p>",
            'pos5':"Is the word <b>%s</b> appropriate for this answer?<p> Try using <b>%s</b> instead. </p>",
            'missingR':"There are too many elements missing in your answer for me to evaluate it. <p>Please go back to the text and try to find the necessary information to answer the question.</p>",
            'missingL':"There are too many elements missing in your answer for me to evaluate it. <p>Read the question carefully and listen to the passage one more time.</p>",
            'missingD':"It is impossible for me to evaluate your answer. There are too many necessary words missing. <p>Make sure you use all the required words to describe the picture.</p>",
            'missingRP':"I cannot evaluate your answer because it lacks several necessary words required to complete the task.<p>Read the instructions again and use the words presented by the exercise.</p>",
            'missingV':"I cannot evaluate your answer because there are too many required words missing. Read the instructions, and use all the necessary vocabulary items required by the exercise.",
            'check':"Checking: %s, %s",
            'det-propNoun':'Proper nouns usually do not have to be preceded by determiners.<p>Try re-writing your sentence without <b>"%s"</b>.</p>',
            'exta-element1':'You have included all the necessary elements to answer the question, but I found one extra word. <p>Re-write your sentence without <b>"%s"</b>.</p>',
            'exta-element2':'Although all necessary words to answer the question are present in your sentence, I believe there are also some unnecessary ones. Re-write your sentence without the following words:<p>%s</p>',
            'missing1':"Your answer is close, but there is a <b>%s</b> missing in your sentence.",
            'missing2':"You are going in the right direction, but there are still three words missing in your answer.",
            'missing3':"There is one important word missing in your answer.",
            'missing4':"There are three necessary words missing in your answer. <p>You may also be using some words that are not necessary to answer the question.</p>",
            'missing5':"You have written all the words necessary to answer the question, except for <b>%s</b>.",
            'extra1':"Your answer has one extra word.",
            'extra2':"Your answer has several extra words.",
            'gostar-de':"REMEMBER! <p>You have to use the preposition <b>de</b> after the verb gostar.</p>",
            'no-de':"The preposition <b>de</b> is missing.",
            'missing-2-words':"You have to include the words <b>%s</b> and <b>%s</b> in your answer.",
            'missing-2-words2':"The words <b>%s</b> and <b>%s</b> are missing in your answer. <p>Also review it for unnecessary words.</p>",
            'missing6':"There is an important <b>%s</b> missing in your sentence. <p>Also review it for unnecessary words.</p>",
            'missing7':"There may be unnecessary words in your answer, but begin by adding the word <b>%s</b>.",
            'fibsFeedback1':"Blank 1: %s",
            'fibsFeedback2':"Blank 1: %s<p></p>Blank 2: %s",
            'fibsFeedback3':"Blank 1: %s<p></p>Blank 2: %s<p></p>Blank 3:: %s",
            'test':"testing messages: %s",
            'noActModel':"There is a problem with this activity. <p>Please click on the 'Report Errors' button and send this error message.</p> ",
            'noMessage':"I don't know what to say to you. <p>Please click on the 'Report Errors' button, and send the sentence you typed in.</p>"}



def mainMessage(feedGenOutput):
   arguments = len(feedGenOutput[1])
   if arguments == 0:
     message = messages[feedGenOutput[0]]
   elif arguments == 1:
     message = messages[feedGenOutput[0]]%(feedGenOutput[1][0])
   elif arguments == 2:
     message = messages[feedGenOutput[0]]%(feedGenOutput[1][0],feedGenOutput[1][1])
   elif arguments == 3:
     message = messages[feedGenOutput[0]]%(feedGenOutput[1][0],feedGenOutput[1][1],feedGenOutput[1][2])
   elif arguments == 4:
     message = messages[feedGenOutput[0]]%(feedGenOutput[1][0],feedGenOutput[1][1],feedGenOutput[1][2],feedGenOutput[1][3])
   return message




