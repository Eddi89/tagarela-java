# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: agreement.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

## OBS: 'sp' = singular or plural
#       'mf' = masculine or feminine


######################
## Functions for NP agreement
######################

## Function 1
## Agr between two elements
def simpleNPs(np,items):
  NPagreement = {}
  for element in items:
      if element[1][0]['number']=='sp':
        element[1][0]['number'] = 'sipl'
    # Check for number agreement
  if (items[0][1][0]['number'] in items[1][1][0]['number']) or (items[1][1][0]['number'] in items[0][1][0]['number']):
      if len(items[0][1][0]['number']) == 2:
        number = items[0][1][0]['number']
      elif len(items[1][1][0]['number']) == 2:
        number = items[1][1][0]['number']
      else:
        number = items[0][1][0]['number']
      NPagreement['number'] = ['ok',np[2][0][0] + '-' + np[2][1][0],number]
  else:
      if items[0][1][0]['pos'] == 'noun':
        number = items[0][1][0]['number']
      elif items[1][1][0]['pos'] == 'noun':
        number = items[1][1][0]['number']
      else:
        number = items[0][1][0]['number']
      NPagreement['number'] = ['error',np[2][0][0] + '-' + np[2][1][0],number,[[items[0][0],items[0][1][0]['number']],[items[1][0],items[1][1][0]['number']]]]
    #Check for gender agreement
  if (items[0][1][0]['gender'] in items[1][1][0]['gender']) or (items[1][1][0]['gender'] in items[0][1][0]['gender']):
      if len(items[0][1][0]['gender']) == 1:
        gender = items[0][1][0]['gender']
      elif len(items[1][1][0]['gender']) == 1:
        gender = items[1][1][0]['gender']
      else:
        gender = items[0][1][0]['gender']
      NPagreement['gender'] = ['ok',np[2][0][0] + '-' + np[2][1][0],gender]
  else:
      if items[0][1][0]['pos'] == 'noun':
        gender = items[0][1][0]['gender']
      elif items[1][1][0]['pos'] == 'noun':
        gender = items[1][1][0]['gender']
      else:
        gender = items[0][1][0]['gender']
      NPagreement['gender'] = ['error',np[2][0][0] + '-' + np[2][1][0],gender,[[items[0][0],items[0][1][0]['gender']],[items[1][0],items[1][1][0]['gender']]]]

  output = [np[0],np[1],{'gender':NPagreement['gender'][2],'number':NPagreement['number'][2]},[NPagreement]]
  return output

###Function 2
## Agr among three elements

def complexNPs(np,items):
  subNP = np[2][1]
  specNP = np[2][0]
  itemsSubNP = items[1:len(items)]
  itemSpecNP = items[0]
  if len(itemsSubNP) == 2:
    agrSubNP = simpleNPs(subNP,itemsSubNP)
  elif len(itemsSubNP) > 2:
    agrSubNP = complexNPs(subNP,itemsSubNP)
  agrSubNP[2]['pos']='noun'
  NP2bar = [np[0],np[1],[specNP,[agrSubNP[0],agrSubNP[1],agrSubNP[3][0]['gender'][1]]]]
  NP2baritems = [itemSpecNP,[agrSubNP[0],[agrSubNP[2]]]]
  agrNP2bar = simpleNPs(NP2bar,NP2baritems)
  final = [agrNP2bar[0], agrNP2bar[1], agrNP2bar[2], agrNP2bar[3]+agrSubNP[3]]
  return final


## Single item NP
def singleN(np,items):
  description = {'gender':items[0][1][0]['gender'],'number':items[0][1][0]['number'],'pos':items[0][1][0]['pos']}
  if 'person' in items[0][1][0]:
    description['person'] = items[0][1][0]['person']
  else:
    description['person'] = '3'
  if 'spos' in items[0][1][0]:
    description['case'] = items[0][1][0]['spos']
  output = [np[0],np[1],description,[{'one element':['ok']}]]
  return output


## NP with NPcoord
def npCoord(nps,items):
  try:
    agrNP1 = agrNPs(nps[2][0],items[nps[2][0][1][0]:nps[2][0][1][1]])
    agrNP2 = agrNPs(nps[2][1][2][1],items[nps[2][1][2][1][1][0]:nps[2][1][2][1][1][1]])
    ## Gender combinations in coordinated NPs
    if agrNP1[2]['gender'] == 'mf':
      if agrNP2[2]['gender'] == 'm':
        gender = 'm'
      else:
        gender = 'mf'
    elif agrNP2[2]['gender'] == 'mf':
      if agrNP1[2]['gender'] == 'm':
        gender = 'm'
      else:
        gender = 'mf'
    elif agrNP1[2]['gender'] == 'm':
      gender = 'm'
    elif agrNP2[2]['gender'] == 'm':
      gender = 'm'
    else:
      gender = 'f'
    ## Number is much simpler :-)
    number = 'pl'
    ## person depends on the elements
    #if 'person' not in agrNP1:
    if 'person' in agrNP1[2]:
      partialPer = [int(agrNP1[2]['person'][0])]
    else:
      partialPer = [3]
    if 'person' in agrNP2[2]:
      partialPer = partialPer + [int(agrNP2[2]['person'][0])]
    else:
      partialPer = partialPer + [3]
    if (partialPer[0] == partialPer[1]) or (partialPer[0] > partialPer[1]):
      person = str(partialPer[1])+'p'
    elif partialPer[1] > partialPer[0]:
      person = str(partialPer[0])+'p'
    else:
      person = '3p'
    npCoordAna = ['NP', nps[1],{'person':person,'gender':gender,'number':number},agrNP1[3]+agrNP2[3]]
    return npCoordAna
  except:
    return ['NP', nps[1],{'person':'3p','gender':'mf','number':'pl'},['no-analysis']]



### Main function for NP AGR
def agrNPs(np,items):
  if np[1][1]-np[1][0] == 1:
    NPagrAnalysis = singleN(np,items)
  elif np[1][1]-np[1][0] == 2:
    NPagrAnalysis = simpleNPs(np,items)
    #print NPagrAnalysis
  elif np[1][1]-np[1][0] > 2:
    if np[2][1][0] == 'PP':
      itemsNP = items[np[2][0][1][0]:np[2][0][1][1]]
      if len(itemsNP) == 2:
        NPagrAnalysis = simpleNPs(np[2][0],itemsNP)
      elif len(itemsNP) == 1:
        NPagrAnalysis = singleN(np[2][0],itemsNP)
      elif len(itemsNP) > 2:
        NPagrAnalysis = complexNPs(np[2][0],itemsNP)
      else:
        NPagrAnalysis = 'no-analysis'
    elif np[2][1][0] == 'NPcoord':
      NPagrAnalysis = npCoord(np,items)
    else:
      NPagrAnalysis = complexNPs(np,items)
  else:
     NPagrAnalysis = 'no-analysis'
  return NPagrAnalysis


###########################
### Subject Verb Agreement
##########################


def findverb(VPList):
  for element in VPList:
    if element[0] == 'verb':
      return element
    elif element[0] == 'VP' and (type(element[2]) == str):
      return element
  return 'v-not-found'


def findelements(vp,elements):
  for item in vp[2]:
    if item[1][1]-item[1][0] == 1:
      if item in elements:
        elements = elements
      else:
        elements = elements + [item]
    elif item[1][1]-item[1][0] > 1:
      itemss = findelements(item,elements)
      for x in itemss:
       if x in elements:
         elements = elements
       else:
         elements = elements + [x]
  return elements



def agrSubjVerb(np,vp,items):
  # Find a verb in a VP
  #VP with one element does not need to find elements
  if vp[1][1]-vp[1][0] == 1:
    v = vp[:]
    Ventry = items[v[1][0]:v[1][1]]
  ## VP of several elements = look for v
  else:
    v = findverb(findelements(vp,[]))
    Ventry = items[v[1][0]:v[1][1]]
  #Add person to np if it does not have it
  if 'person' not in np[2]:
    np[2]['person']='3'+np[2]['number'][0]
  #Check for agreement
  if np[2]['person'] in Ventry[0][1][0]['form']:
    analysis = ['NP-VP',[np[1][0],v[1][1]],{'person':np[2]['person'],'canonic':Ventry[0][1][0]['canonic']},[{'person':['ok','subj-verb',np[2]['person']]}]]
  elif np[2]['person'] not in Ventry[0][1][0]['form']:
    analysis = ['NP-VP',[np[1][0],v[1][1]],{'form':Ventry[0][1][0]['form'],'canonic':Ventry[0][1][0]['canonic']},[{'person':['error','subj-verb',Ventry[0][1][0]['form'],[['subj',np[2]['person']],['verb',Ventry[0][1][0]['form']]]]}]]
  else:
    analysis = 'no-analysis'
  return analysis


###########################
### Predicative Agreement
##########################

def findAdj(APList):
  for element in APList:
    if element[0] == 'adj':
      return element
    elif element[0] == 'adj' and (type(element[2]) == str):
      return element
  return 'v-not-found'

def predicativeAgrAP(allitems,APparse,NPagr):
  if APparse[1][1]-APparse[1][0] == 1:
    adj = APparse[:]
    adjEntry = allitems[adj[1][0]:adj[1][1]]
  else:
    adj = findAdj(findelements(APparse,[]))
    adjEntry = allitems[adj[1][0]:adj[1][1]]
  #Check for number:
  if (NPagr[2]['number'] in adjEntry[0][1][0]['number']) or (adjEntry[0][1][0]['number'] in NPagr[2]['number']):
     analysis = ['subj-pred',[NPagr[1],adj[1]],{'number':NPagr[2]['number']},[{'number':['ok','subj-pred',NPagr[2]['number']]}]]
  elif NPagr[2]['number'] != adjEntry[0][1][0]['number']:
     analysis = ['subj-pred',[NPagr[1],adj[1]],{'number':NPagr[2]['number']},[{'number':['error','subj-pred',NPagr[2]['number'],[['subj',NPagr[2]['number']],['adj',adjEntry[0][1][0]['number']]]]}]]
  else:
     analysis = 'no-analysis'
  #Check for gender
  if (NPagr[2]['gender'] in adjEntry[0][1][0]['gender']) or (adjEntry[0][1][0]['gender'] in NPagr[2]['gender']) :
     analysis[2]['gender'] = NPagr[2]['gender']
     analysis[3][0]['gender'] = ['ok','subj-pred',NPagr[2]['gender']]
     analysis = ['subj-pred',[NPagr[1],adj[1]],analysis[2],[analysis[3][0]]]
  elif NPagr[2]['gender'] != adjEntry[0][1][0]['gender']:
     analysis[2]['gender'] = NPagr[2]['gender']
     analysis[3][0]['gender'] =  ['error','subj-pred',NPagr[2]['gender'],[['subj',NPagr[2]['gender']],['adj',adjEntry[0][1][0]['gender']]]]
     analysis = ['subj-pred',[NPagr[1],adj[1]],analysis[2],[analysis[3][0]]]
  else:
     analysis = analysis
  return analysis


# NP - NP predicative agreement
def agreNpNpSubjPred(NPpred,NPsubj):
  #Check for number:
  if (NPsubj[2]['number'] in NPpred[2]['number']) or (NPpred[2]['number'] in NPsubj[2]['number']):
     analysis = ['subj-pred',[NPsubj[1],NPpred[1]],{'number':NPsubj[2]['number']},[{'number':['ok','subj-pred',NPsubj[2]['number']]}]]
  elif NPsubj[2]['number'] != NPpred[2]['number']:
     analysis = ['subj-pred',[NPsubj[1],NPpred[1]],{'number':NPsubj[2]['number']},[{'number':['error','subj-pred',NPsubj[2]['number'],[['subj',NPsubj[2]['number']],['NP-pred',NPpred[2]['number']]]]}]]
  else:
     analysis = 'no-analysis'
  #Check for gender
  if (NPsubj[2]['gender'] in NPpred[2]['gender']) or (NPpred[2]['gender'] in NPsubj[2]['gender']):
     analysis[2]['gender'] = NPsubj[2]['gender']
     analysis[3][0]['gender'] = ['ok','subj-pred',NPsubj[2]['gender']]
     analysis = ['subj-pred',[NPsubj[1],NPpred[1]],analysis[2],[analysis[3][0]]]
  elif NPsubj[2]['gender'] != NPpred[2]['gender']:
     analysis[2]['gender'] = NPsubj[2]['gender']
     analysis[3][0]['gender'] =  ['error','subj-pred',NPsubj[2]['gender'],[['subj',NPsubj[2]['gender']],['NP-pred',NPpred[2]['gender']]]]      
     analysis = ['subj-pred',[NPsubj[1],NPpred[1]],analysis[2],[analysis[3][0]]]
  else:
     analysis = analysis
  #print analysis, '\n'

  return analysis



###########################
### Sentence Agreement
##########################

def agreesentence(parseroutput,disambiguator):
  agranalysisS = {}
  # First check for agreement in NP subject
  if parseroutput[2][0][0]=='NP' or 'Nbar':
    np = parseroutput[2][0]
    items = disambiguator[parseroutput[2][0][1][0]:parseroutput[2][0][1][1]]
    agranalysisS = {'np-subj':agrNPs(np,items)}
  else:
    agranalysisS = {'np-subj':'no-analysis'}

  # Now check for Subject-verb agreement
  if agranalysisS['np-subj'] == 'no-analysis':
    agranalysisS['subj-verb'] = 'no-analysis'
  else:
    agranalysisS['subj-verb'] = agrSubjVerb(agranalysisS['np-subj'],parseroutput[2][1],disambiguator)

  # Now check for predicative agreement
  if agranalysisS['subj-verb'][2]['canonic'] == ('ser' or 'estar'):
    if parseroutput[2][1][2][1][0]=='NP':
      agranalysisS['predicative-NP'] = agrNPs(parseroutput[2][1][2][1],disambiguator[parseroutput[2][1][2][1][1][0]:parseroutput[2][1][2][1][1][1]])
      agranalysisS['subj-pred'] = agreNpNpSubjPred(agranalysisS['predicative-NP'],agranalysisS['np-subj'])
    if parseroutput[2][1][2][1][0]=='AP':
      agranalysisS['subj-pred'] = predicativeAgrAP(disambiguator,parseroutput[2][1][2][1],agranalysisS['np-subj'])
  # And for other NPs in VPs that are not predicative
  else:
   try:
    if parseroutput[2][1][2][1][0]=='NP':
      agranalysisS['NP-in-VP'] = agrNPs(parseroutput[2][1][2][1],disambiguator[parseroutput[2][1][2][1][1][0]:parseroutput[2][1][2][1][1][1]])
    elif parseroutput[2][1][2][1][2][1][0]=='NP':
      agranalysisS['NP-in-VP'] = agrNPs(parseroutput[2][1][2][1][2][1],disambiguator[parseroutput[2][1][2][1][2][1][1][0]:parseroutput[2][1][2][1][2][1][1][1]])
   except:
    agranalysisS = agranalysisS
  return agranalysisS


####
#INTERPRETER
####


def agreementinterpreter(agreement,disambiguator):
 agreementCodes = {'det-Nbar':['determiner','noun'],'det-noun':['determiner','noun'],
                            'det-propNoun':['determiner','noun'],'numeral-noun':['numeral','noun'],
                            'poss-noun':['possessive','noun'],'poss-Nbar':['possessive','noun'],
                            'noun-AP':['noun','adjective'],'AP-noun':['adjective','noun'],
                            'Nbar-AP':['noun','adjective'],'AP-Nbar':['adjective','noun'],
                            'AP-noun':['adjective','noun'],'AP-noun':['adjective','noun'],
                            'subj-verb':['subject','verb'],'subj-pred':['subject','predicative']}
 try:
  errors = []
  for element in agreement:
    analysis = agreement[element][3]
    for item in analysis:
      for topic in item:
        if item[topic][0] == 'error':
         try:
          if type(agreement[element][1][0]) == int:
            string = ''
            sequence = disambiguator[agreement[element][1][0]:agreement[element][1][1]]
            for x in sequence:
              string = string + x[0] + ' '
            errors = errors + [[topic,agreementCodes[item[topic][1]],string[:-1]]]
          # IN case of subj-pred agreement
          elif type(agreement[element][1][0]) == list:
            string = ''
            sequence = disambiguator[agreement[element][1][0][0]:agreement[element][1][1][1]]
            for x in sequence:
              string = string + x[0] + ' '
            errors = errors + [[topic,agreementCodes[item[topic][1]],string[:-1]]]
         except:
          errors = errors + [[topic,item[topic][1],agreement[element][1]]]
  return errors
 except:
  errors = []
  return errors




## MAIN FUNCTION
def mainagr(parseroutput,disambiguator):
 try:
  if parseroutput[0] == 'S' and (parseroutput[2][0][0] =='NP' and parseroutput[2][1][0] =='VP'):
    agreement = agreesentence(parseroutput,disambiguator)
  elif parseroutput[0] == 'VP':
    agreement = {'subj-verb':['no-subj',parseroutput[1],'no-analysis']}
    try:
      if parseroutput[2][1][2][1][0]=='NP':
        agreement['NP-in-VP'] = agrNPs(parseroutput[2][1][2][1],disambiguator[parseroutput[2][1][2][1][1][0]:parseroutput[2][1][2][1][1][1]])
      elif parseroutput[2][1][2][1][2][1][0]=='NP':
        agreement['NP-in-VP'] = agrNPs(parseroutput[2][1][2][1][2][1],disambiguator[parseroutput[2][1][2][1][2][1][1][0]:parseroutput[2][1][2][1][2][1][1][1]])
    except:
      agreement = agreement
  elif parseroutput[0] == 'NP':
    agreement = {'only-NP': agrNPs(parseroutput,disambiguator)}

  else:
    agreement = {'agreement':'no-analysis'}
 except:
   agreement = {'agreement':'no-analysis'}
 
 agrAnalysis = agreementinterpreter(agreement,disambiguator)
 #print output,'\n'

 return (agreement,agrAnalysis)










### TESTING
# O menino come
# test1p = ['S', [0, 3], [['NP', [0, 2], [['det', [0, 1], 'o'], ['noun', [1, 2], 'menino']]], ['VP', [2, 3], 'come']]]
# test1d = [['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 'gender rule': '1'}]], ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '29', 'number rule': '?', 'gender rule': '?'}]], ['come', [{'form': 'imper-afirm;2s;pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
#Os menino comeu
#test2p = ['S', [0, 3], [['NP', [0, 2], [['det', [0, 1], 'os'], ['noun', [1, 2], 'menino']]], ['VP', [2, 3], 'comeu']]]
#test2d = [['os', [{'gender': 'm', 'spos': 'de', 'number': 'pl', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '1', 'gender rule': '1'}]], ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '29', 'number rule': '?', 'gender rule': '?'}]], ['comeu', [{'form': 'pret-perf;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]
# lindas menino comeu
# test3p = ['S', [0, 3], [['NP', [0, 2], [['adj', [0, 1], 'lindas'], ['noun', [1, 2], 'menino']]], ['VP', [2, 3], 'comeu']]]
# test3d = [['lindas', [{'degree': 'n', 'gender': 'f', 'number': 'pl', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'lindo', 'frequency': '4', 'number rule': '1', 'gender rule': '1'}]], ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '29', 'number rule': '?', 'gender rule': '?'}]], ['comeu', [{'form': 'pret-perf;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]
# Bonitos ônibus comem
# test4p = ['S', [0, 3], [['NP', [0, 2], [['adj', [0, 1], 'bonitos'], ['noun', [1, 2], '\xc3\xb4nibus']]], ['VP', [2, 3], 'comem']]]
# test4d = [['bonitos', [{'degree': 'n', 'gender': 'm', 'number': 'pl', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'bonito', 'frequency': '15', 'number rule': '1', 'gender rule': '1'}]], ['\xc3\xb4nibus', [{'degree': 'n', 'gender': 'm', 'number': 'sp', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': '\xf4nibus', 'frequency': '32', 'number rule': '?', 'gender rule': '?'}]], ['comem', [{'form': 'pres;3p;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]
# Os bonitos ônibus comem
# test5p = ['S', [0, 4], [['NP', [0, 3], [['det', [0, 1], 'os'], ['Nbar', [1, 3], [['adj', [1, 2], 'bonitos'], ['noun', [2, 3], '\xc3\xb4nibus']]]]], ['VP', [3, 4], 'comem']]]
# test5d = [['os', [{'gender': 'm', 'spos': 'de', 'number': 'pl', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '1', 'gender rule': '1'}]], ['bonitos', [{'degree': 'n', 'gender': 'm', 'number': 'pl', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'bonito', 'frequency': '15', 'number rule': '1', 'gender rule': '1'}]], ['\xc3\xb4nibus', [{'degree': 'n', 'gender': 'm', 'number': 'sp', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': '\xf4nibus', 'frequency': '32', 'number rule': '?', 'gender rule': '?'}]], ['comem', [{'form': 'pres;3p;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]
# Os menino bonito comeu
# test6p = ['S', [0, 4], [['NP', [0, 3], [['det', [0, 1], 'os'], ['Nbar', [1, 3], [['noun', [1, 2], 'menino'], ['adj', [2, 3], 'bonito']]]]], ['VP', [3, 4], 'comeu']]]
# test6d = [['os', [{'gender': 'm', 'spos': 'de', 'number': 'pl', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '1', 'gender rule': '1'}]], ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '29', 'number rule': '?', 'gender rule': '?'}]], ['bonito', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'bonito', 'frequency': '28', 'number rule': '1', 'gender rule': '1'}]], ['comeu', [{'form': 'pret-perf;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]
# Os bonito menino comeu
# test7p = ['S', [0, 4], [['NP', [0, 3], [['det', [0, 1], 'os'], ['Nbar', [1, 3], [['adj', [1, 2], 'bonito'], ['noun', [2, 3], 'menino']]]]], ['VP', [3, 4], 'comeu']]]
# test7d = [['os', [{'gender': 'm', 'spos': 'de', 'number': 'pl', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '1', 'gender rule': '1'}]], ['bonito', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'bonito', 'frequency': '28', 'number rule': '1', 'gender rule': '1'}]], ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '29', 'number rule': '?', 'gender rule': '?'}]], ['comeu', [{'form': 'pret-perf;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]
# Os meninos bonito comeu
# test8p = ['S', [0, 4], [['NP', [0, 3], [['det', [0, 1], 'os'], ['Nbar', [1, 3], [['noun', [1, 2], 'meninos'], ['adj', [2, 3], 'bonito']]]]], ['VP', [3, 4], 'comeu']]]
# test8d = [['os', [{'gender': 'm', 'spos': 'de', 'number': 'pl', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '1', 'gender rule': '1'}]], ['meninos', [{'degree': 'n', 'gender': 'm', 'number': 'pl', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '30', 'number rule': '?', 'gender rule': '?'}]], ['bonito', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'bonito', 'frequency': '28', 'number rule': '1', 'gender rule': '1'}]], ['comeu', [{'form': 'pret-perf;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]
# O bonito menino comeu
# test9p = ['S', [0, 4], [['NP', [0, 3], [['det', [0, 1], 'o'], ['Nbar', [1, 3], [['adj', [1, 2], 'bonito'], ['noun', [2, 3], 'menino']]]]], ['VP', [3, 4], 'comeu']]]
# test9d = [['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 'gender rule': '1'}]], ['bonito', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'bonito', 'frequency': '28', 'number rule': '1', 'gender rule': '1'}]], ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '29', 'number rule': '?', 'gender rule': '?'}]], ['comeu', [{'form': 'pret-perf;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]
# A minha filha bonita comeu
# test10p = ['S', [0, 5], [['NP', [0, 4], [['det', [0, 1], 'a'], ['Nbar', [1, 4], [['poss', [1, 2], 'minha'], ['Nbar', [2, 4], [['noun', [2, 3], 'filha'], ['adj', [3, 4], 'bonita']]]]]]], ['VP', [4, 5], 'comeu']]]
# test10d = [['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}]], ['minha', [{'source': 'lexicon', 'gender': 'f', 'spos': 'poss;', 'number': 'si', 'pos': 'poss', 'person': '1s', 'canonic': 'meu', 'frequency': '33', 'number rule': '3', 'gender rule': '?', 'contraction': ''}]], ['filha', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'filho', 'frequency': '31', 'number rule': '?', 'gender rule': '?'}]], ['bonita', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'bonito', 'frequency': '27', 'number rule': '1', 'gender rule': '1'}]], ['comeu', [{'form': 'pret-perf;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]
# A menina de botas comeu
# test11p = ['S', [0, 5], [['NP', [0, 4], [['NP', [0, 2], [['det', [0, 1], 'a'], ['noun', [1, 2], 'menina']]], ['PP', [2, 4], [['prep', [2, 3], 'de'], ['NP', [3, 4], 'botas']]]]], ['VP', [4, 5], 'comeu']]]
# test11d = [['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}]], ['menina', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '30', 'number rule': '?', 'gender rule': '?'}]], ['de', [{'source': 'lexicon', 'canonic': 'de', 'frequency': '35', 'pos': 'prep'}]], ['botas', [{'degree': 'n', 'gender': 'f', 'number': 'pl', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'bota', 'frequency': '22', 'number rule': '?', 'gender rule': '?'}]], ['comeu', [{'form': 'pret-perf;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]
# A minhas meninos bonita comeu
#test12p = ['S', [0, 5], [['NP', [0, 4], [['det', [0, 1], 'a'], ['Nbar', [1, 4], [['poss', [1, 2], 'minhas'], ['Nbar', [2, 4], [['noun', [2, 3], 'meninos'], ['AP', [3, 4], 'bonita']]]]]]], ['VP', [4, 5], 'comeu']]]
#test12d = [['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}]], ['minhas', [{'source': 'lexicon', 'gender': 'f', 'spos': 'poss;', 'number': 'pl', 'pos': 'poss', 'person': '1s', 'canonic': 'meu', 'frequency': '29', 'number rule': '3', 'gender rule': '?', 'contraction': ''}]], ['meninos', [{'degree': 'n', 'gender': 'm', 'number': 'pl', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '30', 'number rule': '?', 'gender rule': '?'}]], ['bonita', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'bonito', 'frequency': '27', 'number rule': '1', 'gender rule': '1'}]], ['comeu', [{'form': 'pret-perf;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]
# Ele come carne
# test13p = ['S', [0, 3], [['NP', [0, 1], 'ele'], ['VP', [1, 3], [['VP', [1, 2], 'come'], ['NP', [2, 3], 'carne']]]]]
# test13d = [['ele', [{'source': 'lexicon', 'gender': 'm', 'spos': 'obl-to;ret;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'ele', 'frequency': '35', 'number rule': '3', 'gender rule': '2', 'contraction': ''}]], ['come', [{'form': 'imper-afirm;2s;pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]], ['carne', [{'degree': 'n', 'gender': 'mf', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'carne', 'frequency': '31', 'number rule': '1', 'gender rule': '?'}]]]
# A menina bonita de sapato comeu
# test14p = ['S', [0, 6], [['NP', [0, 5], [['NP', [0, 3], [['det', [0, 1], 'a'], ['Nbar', [1, 3], [['noun', [1, 2], 'menina'], ['AP', [2, 3], 'bonita']]]]], ['PP', [3, 5], [['prep', [3, 4], 'de'], ['NP', [4, 5], 'sapato']]]]], ['VP', [5, 6], 'comeu']]]
# test14d = [['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}]], ['menina', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '30', 'number rule': '?', 'gender rule': '?'}]], ['bonita', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'bonito', 'frequency': '27', 'number rule': '1', 'gender rule': '1'}]], ['de', [{'source': 'lexicon', 'canonic': 'de', 'frequency': '35', 'pos': 'prep'}]], ['sapato', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'sapato', 'frequency': '27', 'number rule': '1', 'gender rule': '?'}]], ['comeu', [{'form': 'pret-perf;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]
# A minha lindo filhas inteligente de sapato comeu
# test15p = ['S', [0, 8], [['NP', [0, 7], [['NP', [0, 5], [['det', [0, 1], 'a'], ['Nbar', [1, 5], [['poss', [1, 2], 'minha'], ['Nbar', [2, 5], [['AP', [2, 3], 'lindo'], ['Nbar', [3, 5], [['noun', [3, 4], 'filhas'], ['AP', [4, 5], 'inteligente']]]]]]]]], ['PP', [5, 7], [['prep', [5, 6], 'de'], ['NP', [6, 7], 'sapato']]]]], ['VP', [7, 8], 'comeu']]]
# test15d = [['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}]], ['minha', [{'source': 'lexicon', 'gender': 'f', 'spos': 'poss;', 'number': 'si', 'pos': 'poss', 'person': '1s', 'canonic': 'meu', 'frequency': '33', 'number rule': '3', 'gender rule': '?', 'contraction': ''}]], ['lindo', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'lindo', 'frequency': '20', 'number rule': '1', 'gender rule': '1'}]], ['filhas', [{'degree': 'n', 'gender': 'f', 'number': 'pl', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'filho', 'frequency': '29', 'number rule': '?', 'gender rule': '?'}]], ['inteligente', [{'degree': 'n', 'gender': 'mf', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'inteligente', 'frequency': '28', 'number rule': '1', 'gender rule': '?'}]], ['de', [{'source': 'lexicon', 'canonic': 'de', 'frequency': '35', 'pos': 'prep'}]], ['sapato', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'sapato', 'frequency': '27', 'number rule': '1', 'gender rule': '?'}]], ['comeu', [{'form': 'pret-perf;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]
# Ele muito amanhã come muita carne
# test16p = ['S', [0, 6], [['NP', [0, 1], 'ele'], ['VP', [1, 6], [['AdvP', [1, 3], [['adv', [1, 2], 'muito'], ['adv', [2, 3], 'amanh\xc3\xa3']]], ['VP', [3, 6], [['verb', [3, 4], 'come'], ['NP', [4, 6], [['adj', [4, 5], 'muita'], ['noun', [5, 6], 'carne']]]]]]]]]
# test16d = [['ele', [{'source': 'lexicon', 'gender': 'm', 'spos': 'obl-to;ret;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'ele', 'frequency': '35', 'number rule': '3', 'gender rule': '2', 'contraction': ''}]], ['muito', [{'degree': 'n', 'spos': 'int;', 'pos': 'adv', 'source': 'lexicon', 'canonic': 'muito', 'frequency': '34'}]], ['amanh\xc3\xa3', [{'degree': 'n', 'spos': 'cir-temp;', 'pos': 'adv', 'source': 'lexicon', 'canonic': 'amanh\xe3', 'frequency': '32'}]], ['come', [{'form': 'imper-afirm;2s;pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]], ['muita', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'muito', 'frequency': '31', 'number rule': '1', 'gender rule': '1'}]], ['carne', [{'degree': 'n', 'gender': 'mf', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'carne', 'frequency': '31', 'number rule': '1', 'gender rule': '?'}]]]
# A minhas filho bonita comeram uma carro
# test17p = ['S', [0, 7], [['NP', [0, 4], [['det', [0, 1], 'a'], ['Nbar', [1, 4], [['poss', [1, 2], 'minhas'], ['Nbar', [2, 4], [['noun', [2, 3], 'filho'], ['adj', [3, 4], 'bonita']]]]]]], ['VP', [4, 7], [['verb', [4, 5], 'comeram'], ['NP', [5, 7], [['numeral', [5, 6], 'uma'], ['noun', [6, 7], 'carro']]]]]]]
# test17d = [['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}]], ['minhas', [{'source': 'lexicon', 'gender': 'f', 'spos': 'poss;', 'number': 'pl', 'pos': 'poss', 'person': '1s', 'canonic': 'meu', 'frequency': '29', 'number rule': '3', 'gender rule': '?', 'contraction': ''}]], ['filho', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'filho', 'frequency': '34', 'number rule': '?', 'gender rule': '?'}]], ['bonita', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'bonito', 'frequency': '27', 'number rule': '1', 'gender rule': '1'}]], ['comeram', [{'form': 'pret-m-q-p;3p;pret-perf;3p;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]], ['uma', [{'gender': 'f', 'spos': 'car', 'number': 'si', 'pos': 'numeral', 'source': 'lexicon', 'canonic': 'um', 'frequency': '31', 'number rule': '?', 'gender rule': '3'}]], ['carro', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'carro', 'frequency': '32', 'number rule': '2', 'gender rule': '?'}]]]
# A minha filha é uma menina bonita
# test18p = ['S', [0, 7], [['NP', [0, 3], [['det', [0, 1], 'a'], ['Nbar', [1, 3], [['poss', [1, 2], 'minha'], ['noun', [2, 3], 'filha']]]]], ['VP', [3, 7], [['verb', [3, 4], '\xc3\xa9'], ['NP', [4, 7], [['numeral', [4, 5], 'uma'], ['Nbar', [5, 7], [['noun', [5, 6], 'menina'], ['adj', [6, 7], 'bonita']]]]]]]]]
# test18d = [['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}]], ['minha', [{'source': 'lexicon', 'gender': 'f', 'spos': 'poss;', 'number': 'si', 'pos': 'poss', 'person': '1s', 'canonic': 'meu', 'frequency': '33', 'number rule': '3', 'gender rule': '?', 'contraction': ''}]], ['filha', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'filho', 'frequency': '31', 'number rule': '?', 'gender rule': '?'}]], ['\xc3\xa9', [{'form': 'pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'ser', 'frequency': '35'}]], ['uma', [{'gender': 'f', 'spos': 'car', 'number': 'si', 'pos': 'numeral', 'source': 'lexicon', 'canonic': 'um', 'frequency': '31', 'number rule': '?', 'gender rule': '3'}]], ['menina', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '30', 'number rule': '?', 'gender rule': '?'}]], ['bonita', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'bonito', 'frequency': '27', 'number rule': '1', 'gender rule': '1'}]]]
# A minha filha é muito bonita e cansada
# test19p = ['S', [0, 8], [['NP', [0, 3], [['det', [0, 1], 'a'], ['Nbar', [1, 3], [['poss', [1, 2], 'minha'], ['noun', [2, 3], 'filha']]]]], ['VP', [3, 8], [['verb', [3, 4], '\xc3\xa9'], ['AP', [4, 8], [['AP', [4, 6], [['adv', [4, 5], 'muito'], ['adj', [5, 6], 'bonita']]], ['APcoord', [6, 8], [['conj', [6, 7], 'e'], ['AP', [7, 8], 'cansada']]]]]]]]]
# test19d = [['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}]], ['minha', [{'source': 'lexicon', 'gender': 'f', 'spos': 'poss;', 'number': 'si', 'pos': 'poss', 'person': '1s', 'canonic': 'meu', 'frequency': '33', 'number rule': '3', 'gender rule': '?', 'contraction': ''}]], ['filha', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'filho', 'frequency': '31', 'number rule': '?', 'gender rule': '?'}]], ['\xc3\xa9', [{'form': 'pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'ser', 'frequency': '35'}]], ['muito', [{'degree': 'n', 'spos': 'int;', 'pos': 'adv', 'source': 'lexicon', 'canonic': 'muito', 'frequency': '34'}]], ['bonita', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'bonito', 'frequency': '27', 'number rule': '1', 'gender rule': '1'}]], ['e', [{'pos': 'conj', 'sub-type': 'adit;adve;', 'source': 'lexicon', 'canonic': 'e', 'frequency': '35', 'type': 'coord;'}]], ['cansada', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'cansado', 'frequency': '15', 'number rule': '?', 'gender rule': '?'}]]]
# A minha filha é bonito
#test20p = ['S', [0, 5], [['NP', [0, 3], [['det', [0, 1], 'a'], ['Nbar', [1, 3], [['poss', [1, 2], 'minha'], ['noun', [2, 3], 'filha']]]]], ['VP', [3, 5], [['verb', [3, 4], '\xc3\xa9'], ['AP', [4, 5], 'bonito']]]]]
#test20d = [['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}]], ['minha', [{'source': 'lexicon', 'gender': 'f', 'spos': 'poss;', 'number': 'si', 'pos': 'poss', 'person': '1s', 'canonic': 'meu', 'frequency': '33', 'number rule': '3', 'gender rule': '?', 'contraction': ''}]], ['filha', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'filho', 'frequency': '31', 'number rule': '?', 'gender rule': '?'}]], ['\xc3\xa9', [{'form': 'pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'ser', 'frequency': '35'}]], ['bonito', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'bonito', 'frequency': '28', 'number rule': '1', 'gender rule': '1'}]]]
# A minha filha é uns meninos bonitos
# test21p = ['S', [0, 7], [['NP', [0, 3], [['det', [0, 1], 'a'], ['Nbar', [1, 3], [['poss', [1, 2], 'minha'], ['noun', [2, 3], 'filha']]]]], ['VP', [3, 7], [['verb', [3, 4], '\xc3\xa9'], ['NP', [4, 7], [['det', [4, 5], 'uns'], ['Nbar', [5, 7], [['noun', [5, 6], 'meninos'], ['adj', [6, 7], 'bonitos']]]]]]]]]
# test21d = [['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}]], ['minha', [{'source': 'lexicon', 'gender': 'f', 'spos': 'poss;', 'number': 'si', 'pos': 'poss', 'person': '1s', 'canonic': 'meu', 'frequency': '33', 'number rule': '3', 'gender rule': '?', 'contraction': ''}]], ['filha', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'filho', 'frequency': '31', 'number rule': '?', 'gender rule': '?'}]], ['\xc3\xa9', [{'form': 'pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'ser', 'frequency': '35'}]], ['uns', [{'gender': 'm', 'spos': 'i', 'number': 'pl', 'pos': 'det', 'source': 'lexicon', 'canonic': 'um', 'frequency': '30', 'number rule': '1', 'gender rule': '3'}]], ['meninos', [{'degree': 'n', 'gender': 'm', 'number': 'pl', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '30', 'number rule': '?', 'gender rule': '?'}]], ['bonitos', [{'degree': 'n', 'gender': 'm', 'number': 'pl', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'bonito', 'frequency': '15', 'number rule': '1', 'gender rule': '1'}]]]
# Comeu a carne em minha mão
# test22p = ['VP', [0, 6], [['verb', [0, 1], 'comeu'], ['NP', [1, 6], [['NP', [1, 3], [['det', [1, 2], 'a'], ['noun', [2, 3], 'carne']]], ['PP', [3, 6], [['prep', [3, 4], 'em'], ['NP', [4, 6], [['poss', [4, 5], 'minha'], ['noun', [5, 6], 'm\xc3\xa3o']]]]]]]]]
# test22d = [['comeu', [{'form': 'pret-perf;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]], ['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}]], ['carne', [{'degree': 'n', 'gender': 'mf', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'carne', 'frequency': '31', 'number rule': '1', 'gender rule': '?'}]], ['em', [{'source': 'lexicon', 'canonic': 'em', 'frequency': '35', 'pos': 'prep'}]], ['minha', [{'source': 'lexicon', 'gender': 'f', 'spos': 'poss;', 'number': 'si', 'pos': 'poss', 'person': '1s', 'canonic': 'meu', 'frequency': '33', 'number rule': '3', 'gender rule': '?', 'contraction': ''}]], ['m\xc3\xa3o', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'm\xe3o', 'frequency': '32', 'number rule': '1', 'gender rule': '?'}]]]
# 
# test23p = [['pronoun', [0, 1], 'ela'], ['poss', [1, 2], 'minha'], ['adv', [2, 3], 'n\xc3\xa3o'], ['verb', [3, 4], 'comeu'], ['pronoun', [4, 5], 'nada'], ['prep', [5, 6], 'para'], ['det', [6, 7], 'a'], ['noun', [7, 8], 'menina'], ['adj', [8, 9], 'lindo'], ['NP', [0, 1], 'ela'], ['AdvP', [2, 3], 'n\xc3\xa3o'], ['VP', [3, 4], 'comeu'], ['NP', [4, 5], 'nada'], ['NP', [7, 8], 'menina'], ['AP', [8, 9], 'lindo'], ['VP', [2, 4], [['adv', [2, 3], 'n\xc3\xa3o'], ['verb', [3, 4], 'comeu']]], ['VP', [3, 5], [['verb', [3, 4], 'comeu'], ['NP', [4, 5], 'nada']]], ['NP', [6, 8], [['det', [6, 7], 'a'], ['noun', [7, 8], 'menina']]], ['NP', [7, 9], [['noun', [7, 8], 'menina'], ['AP', [8, 9], 'lindo']]], ['Nbar', [7, 9], [['noun', [7, 8], 'menina'], ['AP', [8, 9], 'lindo']]], ['PP', [5, 8], [['prep', [5, 6], 'para'], ['NP', [6, 8], [['det', [6, 7], 'a'], ['noun', [7, 8], 'menina']]]]], ['NP', [6, 9], [['det', [6, 7], 'a'], ['Nbar', [7, 9], [['noun', [7, 8], 'menina'], ['AP', [8, 9], 'lindo']]]]], ['PP', [5, 9], [['prep', [5, 6], 'para'], ['NP', [6, 9], [['det', [6, 7], 'a'], ['Nbar', [7, 9], [['noun', [7, 8], 'menina'], ['AP', [8, 9], 'lindo']]]]]]], ['NP', [4, 8], [['NP', [4, 5], 'nada'], ['PP', [5, 8], [['prep', [5, 6], 'para'], ['NP', [6, 8], [['det', [6, 7], 'a'], ['noun', [7, 8], 'menina']]]]]]], ['VP', [3, 8], [['verb', [3, 4], 'comeu'], ['NP', [4, 8], [['NP', [4, 5], 'nada'], ['PP', [5, 8], [['prep', [5, 6], 'para'], ['NP', [6, 8], [['det', [6, 7], 'a'], ['noun', [7, 8], 'menina']]]]]]]]], ['NP', [4, 9], [['NP', [4, 5], 'nada'], ['PP', [5, 9], [['prep', [5, 6], 'para'], ['NP', [6, 9], [['det', [6, 7], 'a'], ['Nbar', [7, 9], [['noun', [7, 8], 'menina'], ['AP', [8, 9], 'lindo']]]]]]]]], ['VP', [3, 9], [['verb', [3, 4], 'comeu'], ['NP', [4, 9], [['NP', [4, 5], 'nada'], ['PP', [5, 9], [['prep', [5, 6], 'para'], ['NP', [6, 9], [['det', [6, 7], 'a'], ['Nbar', [7, 9], [['noun', [7, 8], 'menina'], ['AP', [8, 9], 'lindo']]]]]]]]]]]]
# test23d = [['ela', [{'source': 'lexicon', 'gender': 'f', 'spos': 'obl-to;ret;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'ele', 'frequency': '34', 'number rule': '3', 'gender rule': '2', 'contraction': ''}]], ['minha', [{'source': 'lexicon', 'gender': 'f', 'spos': 'poss;', 'number': 'si', 'pos': 'poss', 'person': '1s', 'canonic': 'meu', 'frequency': '33', 'number rule': '3', 'gender rule': '?', 'contraction': ''}]], ['n\xc3\xa3o', [{'degree': 'n', 'spos': 'neg;', 'pos': 'adv', 'source': 'lexicon', 'canonic': 'n\xe3o', 'frequency': '35'}]], ['comeu', [{'form': 'pret-perf;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]], ['nada', [{'source': 'lexicon', 'gender': 'm', 'spos': 'inde;', 'pos': 'pronoun', 'number': 'si', 'person': 'n', 'canonic': 'nada', 'frequency': '33', 'number rule': '?', 'gender rule': '?', 'contraction': ''}]], ['para', [{'source': 'lexicon', 'canonic': 'para', 'frequency': '35', 'pos': 'prep'}]], ['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}]], ['menina', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '30', 'number rule': '?', 'gender rule': '?'}]], ['lindo', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'lindo', 'frequency': '20', 'number rule': '1', 'gender rule': '1'}]]]
#

# test24p = ['S', [0, 5], [['NP', [0, 4], [['NP', [0, 2], [['det', [0, 1], 'a'], ['noun', [1, 2], 'menina']]], ['NPcoord', [2, 4], [['conj', [2, 3], 'e'], ['NP', [3, 4], 'maria']]]]], ['VP', [4, 5], 'comem']]]
# test24d = [['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}]], ['menina', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '30', 'number rule': '?', 'gender rule': '?'}]], ['e', [{'pos': 'conj', 'sub-type': 'adit;adve;', 'source': 'lexicon', 'canonic': 'e', 'frequency': '35', 'type': 'coord;'}]], ['maria', [{'gender': 'f', 'number': 'si', 'pos': 'propNoun', 'source': 'lexicon', 'canonic': 'maria', 'frequency': '32', 'capitalize': 'li'}]], ['comem', [{'form': 'pres;3p;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]

# A menina e eu comemos
# test25p = ['S', [0, 5], [['NP', [0, 4], [['NP', [0, 2], [['det', [0, 1], 'a'], ['noun', [1, 2], 'menina']]], ['NPcoord', [2, 4], [['conj', [2, 3], 'e'], ['NP', [3, 4], 'eu']]]]], ['VP', [4, 5], 'comemos']]]
# test25d = [['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}]], ['menina', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '30', 'number rule': '?', 'gender rule': '?'}]], ['e', [{'pos': 'conj', 'sub-type': 'adit;adve;', 'source': 'lexicon', 'canonic': 'e', 'frequency': '35', 'type': 'coord;'}]], ['eu', [{'source': 'lexicon', 'gender': 'mf', 'spos': 'ret;', 'pos': 'pronoun', 'number': 'si', 'person': '1s', 'canonic': 'eu', 'frequency': '34', 'number rule': '?', 'gender rule': '?', 'contraction': ''}]], ['comemos', [{'form': 'pres;1p;pret-perf;1p;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]

# Eu sou linda
# test26p = ['S', [0, 3], [['NP', [0, 1], 'eu'], ['VP', [1, 3], [['verb', [1, 2], 'sou'], ['AP', [2, 3], 'linda']]]]]
# test26d = [['eu', [{'source': 'lexicon', 'gender': 'mf', 'spos': 'ret;', 'pos': 'pronoun', 'number': 'si', 'person': '1s', 'canonic': 'eu', 'frequency': '34', 'number rule': '?', 'gender rule': '?', 'contraction': ''}]], ['sou', [{'form': 'pres;1s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'ser', 'frequency': '32'}]], ['linda', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'lindo', 'frequency': '20', 'number rule': '1', 'gender rule': '1'}]]]

# def agr_test():
#     print mainagr(test2p,test2d)
#     # Try # Ele come carne
# 
# if __name__ == "__main__":
#   agr_test()



#Agreement Analysis: {'NP-in-VP': ['NP', [3, 5], {'gender': 'm', 'number': 'pl'},
#                                   [{'gender': ['error', 'det-noun', 'm', [['a', 'f'], ['biscoitos', 'm']]],
#                                     'number': ['error', 'det-noun', 'pl', [['a', 'si'], ['biscoitos', 'pl']]]}]],
# #                     'subj-verb': ['NP-VP', [0, 3], {'canonic': 'comer', 'form': 'pret-m-q-p;3p;pret-perf;3p;'},
#                                   [{'person': ['error', 'subj-verb', 'pret-m-q-p;3p;pret-perf;3p;',
#                                               [['subj', '3s'], ['verb', 'pret-m-q-p;3p;pret-perf;3p;']]]}]],
# #                     'np-subj': ['NP', [0, 2], {'gender': 'f', 'number': 'si', 'person': '3s'},
#                                   [{'gender': ['error', 'det-noun', 'f', [['os', 'm'], ['menina', 'f']]],
#                                     'number': ['error', 'det-noun', 'si', [['os', 'pl'], ['menina', 'si']]]}]]}
# 
# #              {'NP-in-VP': ['NP', [2, 4], {'gender': 'm', 'number': 'si'}, 
#                               [{'gender': ['ok', 'det-noun', 'm'], 'number': ['ok', 'det-noun', 'si']}]],
# #               'subj-verb': ['NP-VP', [0, 2], {'person': '3s', 'canonic': 'comer'}, 
#                              [{'person': ['ok', 'subj-verb', '3s']}]],
# #               'np-subj': ['NP', [0, 1], {'case': 'obl-to;ret;', 'gender': 'm', 'person': '3s', 'number': 'si', 'pos': 'pronoun'}, 
            #                 [{'one element': ['ok']}]]}






### OLD CODE

   # try statament is to look for noun:
    # try:
#       for element in items:
#         if element[1][0]['pos']=='noun':
#           position = items.index(element)
#           other = items[:]
#           del other[position]
#           #print other,'\n'
#           #print items,'\n'
#           noun = (element,position)

  #check for number agreement
     #  if noun[0][1][0]['number']=='sp':
#         NPagreement['number'] = ['ok',np[2][0][0] + '-' + np[2][1][0],other[0][1][0]['number']]
#       else:
#         #print items[0][1][0]['number'], items[1][1][0]['number']
#         #print np[2][0][0] + '-' + np[2][1][0]
#         #print items[0][0],items[0][1][0]['number']
#         if items[0][1][0]['number'] == items[1][1][0]['number']:
#           NPagreement['number'] = ['ok',np[2][0][0] + '-' + np[2][1][0],items[0][1][0]['number']]
#         else:
#           NPagreement['number'] = ['error',np[2][0][0] + '-' + np[2][1][0],[items[0][0],items[0][1][0]['number']],[items[1][0],items[1][1][0]['number']]]








    # except:
#       if items[0][1][0]['number'] == items[1][1][0]['number']:
#           NPagreement['number'] = ['ok',np[2][0][0] + '-' + np[2][1][0],items[0][1][0]['number']]
#       else:
#           NPagreement['number'] = ['error',np[2][0][0] + '-' + np[2][1][0],[items[0][0],items[0][1][0]['number']],[items[1][0],items[1][1][0]['number']]]
# 
#       if items[0][1][0]['gender'] == items[1][1][0]['gender']:
#           NPagreement['gender'] = ['ok',np[2][0][0] + '-' + np[2][1][0],items[0][1][0]['gender']]
# 
#       else:
#           NPagreement['gender'] = ['error',np[2][0][0] + '-' + np[2][1][0],[items[0][0],items[0][1][0]['gender']],[items[1][0],items[1][1][0]['gender']]]


#  if np[2][0][1][1]-np[2][0][1][0]== 2:
#       subNP = np[2][0]
#       specNP = np[2][1]
#       #it means that the first element has arity 2
#       order = 21
#     # elif coverage of item2 = 2
#     elif np[2][1][1][1]-np[2][1][1][0] == 2:
#       subNP = np[2][1]
#       specNP = np[2][0]
#       order = 12
#     subitems = items[subNP[1][0]:subNP[1][1]]
#     agrSubNP = agreeNPs(subNP,subitems)
#     #print agrSubNP
#     if order == 12:
#       NP2bar = [np[0],np[1],[specNP,[agrSubNP[0],agrSubNP[1],agrSubNP[3][0]['gender'][1]]]]
#       print NP2bar
#       agrSubNP[2]['pos']='noun'
#       NP2baritems = [items[0],[agrSubNP[0],[agrSubNP[2]]]]
#       #print NP2baritems
#       agrNP2bar = agreeNPs(NP2bar,NP2baritems)
#       #print final
#     elif order == 21:
#       NP2bar = [np[0],np[1],[[agrSubNP[0],agrSubNP[1],agrSubNP[3][0]['gender'][1]],specNP]]


## OLD agr function

# def agreeNPs(np,items):
#   NPagreement = {}
#   # if len(items) == 4:
#   # More complex because it can have a PP (O menino de mochila)
#   # Or it could be 3bars (A minha linda filha)
#   if len(items)== 3:
#     subNP = np[2][1]
#     specNP = np[2][0]
#     subitems = items[subNP[1][0]:subNP[1][1]]
#     agrSubNP = agreeNPs(subNP,subitems)
#     NP2bar = [np[0],np[1],[specNP,[agrSubNP[0],agrSubNP[1],agrSubNP[3][0]['gender'][1]]]]
#     agrSubNP[2]['pos']='noun'
#     NP2baritems = [items[0],[agrSubNP[0],[agrSubNP[2]]]]
#     agrNP2bar = agreeNPs(NP2bar,NP2baritems)
#     final = [agrNP2bar[0], agrNP2bar[1], agrNP2bar[2], agrNP2bar[3]+agrSubNP[3]]
#     return final
# 
#   #test to see how many items there are in the NP
#   elif len(items) == 2:
#   # possible NP configurations for 2 items = 'det,noun', 'det,propNoun', 'numeral,noun',
#   #                                          'poss,noun', 'noun,AP', 'AP,noun'
#     for element in items:
#       if element[1][0]['number']=='sp':
#         element[1][0]['number'] = 'sipl'
#     # Check for number agreement
#     if (items[0][1][0]['number'] in items[1][1][0]['number']) or (items[1][1][0]['number'] in items[0][1][0]['number']):
#         if len(items[0][1][0]['number']) == 2:
#           gender = items[0][1][0]['number']
#         elif len(items[1][1][0]['number']) == 2:
#           gender = items[1][1][0]['number']
#         else:
#           gender = items[0][1][0]['number']
#         NPagreement['number'] = ['ok',np[2][0][0] + '-' + np[2][1][0],gender]
#     else:
#         if items[0][1][0]['pos'] == 'noun':
#           gender = items[0][1][0]['number']
#         elif items[1][1][0]['pos'] == 'noun':
#           gender = items[1][1][0]['number']
#         else:
#           gender = items[0][1][0]['number']
#         NPagreement['number'] = ['error',np[2][0][0] + '-' + np[2][1][0],gender,[[items[0][0],items[0][1][0]['number']],[items[1][0],items[1][1][0]['number']]]]
# 
#       #Check for gender agreement
# 
#     if (items[0][1][0]['gender'] in items[1][1][0]['gender']) or (items[1][1][0]['gender'] in items[0][1][0]['gender']):
#         if len(items[0][1][0]['gender']) == 1:
#           gender = items[0][1][0]['gender']
#         elif len(items[1][1][0]['gender']) == 1:
#           gender = items[1][1][0]['gender']
#         else:
#           gender = items[0][1][0]['gender']
#         NPagreement['gender'] = ['ok',np[2][0][0] + '-' + np[2][1][0],gender]
#     else:
#         if items[0][1][0]['pos'] == 'noun':
#           gender = items[0][1][0]['gender']
#         elif items[1][1][0]['pos'] == 'noun':
#           gender = items[1][1][0]['gender']
#         else:
#           gender = items[0][1][0]['gender']
#         NPagreement['gender'] = ['error',np[2][0][0] + '-' + np[2][1][0],gender,[[items[0][0],items[0][1][0]['gender']],[items[1][0],items[1][1][0]['gender']]]]
# 
#   output = [np[0],np[1],{'gender':NPagreement['gender'][2],'number':NPagreement['number'][2]},[NPagreement]]
# 
#   return output


# def agr4elements(np,items):
#   subNP = np[2][1]
#   print 'SUB NP', subNP
#   specNP = np[2][0]
#   print 'SPEC NP', specNP
#   subitems = items[subNP[1][0]:subNP[1][1]]
#   print 'SUB ITENS', subitems
#   return 'x'

#     agrSubNP[2]['pos']='noun'
#     #print agrSubNP
#     NP2bar = [np[0],np[1],[specNP,[agrSubNP[0],agrSubNP[1],agrSubNP[3][0]['gender'][1]]]]
#     #print NP2bar
#     NP2baritems = [itemSpecNP,[agrSubNP[0],[agrSubNP[2]]]]
#     print NP2baritems
#     agrNP2bar = agr2elements(NP2bar,NP2baritems)
#     final = [agrNP2bar[0], agrNP2bar[1], agrNP2bar[2], agrNP2bar[3]+agrSubNP[3]]
#     return final
#   elif len(itemsSubNP) > 2:
#     agrNP4bar = agr3elements(subNP,itemsSubNP)
#     agrNP4bar[2]['pos']='noun'
#     NP3bar = [np[0],np[1],[specNP,[agrNP4bar[0],agrNP4bar[1],agrNP4bar[3][0]['gender'][1]]]]
#     NP3baritems = [itemSpecNP,[agrNP4bar[0],[agrNP4bar[2]]]]
#     agr4bar = agr2elements(NP3bar,NP3baritems)
#     print agr4bar
# 
# #     agrSubNP[2]['pos']='noun'
# #     NP2baritems = [items[0],[agrSubNP[0],[agrSubNP[2]]]]
#     #print agrNP4bar
#   return 'x'


# def loopfind(element1,element2):
#   if element1[1][1]-element1[1][0] == 1:
#     output = findverbInVP(element1)
#     if type(output[2]) == str:
#       return output
#   if element2[1][1]-element1[1][0] == 1:
#     output = findverbInVP(element1)
#     if type(output[2]) == str:
#       return output
#   return 'not-found'
#   
# 
# def largefind(
# 
# def findverb(vp):
#   place = vp[2]
#   element1 = place[0]
#   element2 = place[1]
#   if loopfind(element1,element2) == 'not-found':
#     if element1[1][1]-element1[1][0] > 1:
#       element = loopfind
# 
#       output = findverb(element1)
#       if type(output[2]) == str:
#         return output
#     if element2[1][1]-element1[1][0] > 1:
#       output = findverb(element1)
#       if type(output[2]) == str:
#         return output
# 

#   else:
#     if element1[1][1]-element1[1][0] > 1:
#       element = findverb(element1)
#     if element == 'not-found':
#       element = findverb(element2)
#     if element == 'not-found':
#       return 'not-found'
#     else:
#       return element
# 
#   return 'not-found'



 #   else:
#      if element1[0] == 'verb':
#        return element
#      elif element1[0] == 'VP' and (type(element1[2]) == str):
#        return element
#      element = findverb(element)
#    for element in place:
#     if element[1][1]-element[1][0] > 1:
#       output = findverb(element)



#    ## LOOP not working
#     if element[1][1]-element[1][0] > 1:
#       output = findverb(element)
#       print output
#       # if type(output[2]) == str:
# #         return output
# #       else:
# #         output = findverb(element)
#  return 'x'

# Find NPS
# def findNPs(ParserOutputList):
#   NPs = []
#   for element in ParserOutputList:
#     if element[1][1]-element[1][0] == 1:
#       NPs = NPs
#     else:
#       if element in NPs:
#         NPs = NPs
#       else:
#         if element[0] == 'NP':
#           print element, '\n'
#           NPs = NPs + element
#   return NPs


