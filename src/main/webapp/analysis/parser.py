"""
---------------------------------------------------------------
$Id: parser.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

#### This Module checks local agreement features

# EXEMPLE

# [['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source':
# 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 'gender rule':
# '1'}]], ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun'
# , 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '29', 
# 'number rule': '?', 'gender rule': '?'}]]]


# rules = [['det','noun'],
#          ['det','adj','noun']]

rules = [['NP,VP','S'],
         ['det,noun','NP'],
         ['det,propNoun','NP'],
         ['numeral,noun','NP'],
         ['poss,noun','NP'],
         ['poss,noun','Nbar'],
         ['poss,Nbar','NP'],
         ['poss,Nbar','Nbar'],
         ['noun,AP','NP'],
         ['AP,noun','NP'],
         ['noun,AP','Nbar'],
         ['AP,noun','Nbar'],
         ['Nbar,AP','Nbar'],
         ['AP,Nbar','Nbar'],
         ['Nbar,AP','NP'],
         ['AP,Nbar','NP'],
         ['det,Nbar','NP'],
         #['adj,noun','Nbar'],
         #['noun,adj','Nbar'],
         #['noun,adj','NP'],
         #['adj,noun','NP'],
         #['noun,poss','Nbar'],
         #['noun,poss','NP'],
         #['NP,verb','Vbar'],
         ['adv,adj','AP'],
         ['adv,adv','AdvP'],
         ['adv,PP','AdvP'],
         ['verb,VP','VP'],
         ['verb,PP','VP'],
         ['NP,PP','NP'],
         ['VP,AdvP','VP'],
         ['VP,AdvP','Vbar'],
         ['adv,verb','VP'],
         ['verb,AP','VP'],
         ['Vbar,AP','VP'],
         ['Vbar,PP','VP'],
         ['Vbar,NP','VP'],
         ['verb,NP','VP'],
         ['prep,NP','PP'],
         ['NP,Vbar','S'],
         ['NP,S','NP'],
         ['conj,S','S'],
         ['adv,S','S'],
         ['conj,NP','NPcoord'],
         ['NP,NPcoord','NP'],
         ['conj,VP','VPcoord'],
         ['VP,VPcoord','VP'],
         ['conj,AP','APcoord'],
         ['AP,APcoord','AP'],
         ['conj,AdvP','AdvPcoord'],
         ['AdvP,Advcoord','AdvP'],
         ['pronoun,verb','Vbar']]



def conc(x,y):
  return (x,y)
term = []

def reducing(terminalsym,lengh):
  #print terminalsym, '\n'
  reduced = terminalsym[:]
  contribution = 0
  for item1 in terminalsym:
    removablelist = terminalsym[:]
    removablelist.remove(item1)
    #print removablelist
    redge = item1[1][1]
    #print redge
    for item2 in removablelist:
      ledge = item2[1][0]
      #print ledge
      if ledge == redge:
        rule = item1[0]+','+item2[0]
        for regra in rules:
          if rule == regra[0]:
            subtree = [item1,item2]
            option = [regra[1],[item1[1][0],item2[1][1]],subtree]
            if option in reduced:
              reduced = reduced
            else:
              reduced = reduced + [option]
              contribution = contribution + 1

  for x in reduced:
    if x[0] == 'S' and (x[1] == [0,lengh]):
      return x
  if contribution == 0:
    for x in reduced:
      if x[0] == 'VP' and (x[1] == [0,lengh]):
        return x
      elif x[0] == 'NP' and (x[1] == [0,lengh]):
        return x
    return reduced
  
  term = reducing(reduced,lengh)
  return term

## First look-up at terminal nodes
def posShifting(poslist):
  n = 1
  poscomplete = []
  lexicalitems = []
  for y in poslist:
    poscomplete = poscomplete + [y[0]]
  partial = poscomplete[:]
  for element in poslist:
    lexicalitems = lexicalitems + [[element[0],[partial.index(element[0]),partial.index(element[0])+1],element[1]]]
    partial = ['dummy']*n + poscomplete[n:]
    n = n+1
  augmentedlist = lexicalitems[:]
  for definition in lexicalitems:
    if definition[0] == 'verb':
      augmentedlist = augmentedlist + [['VP',definition[1],definition[2]]]
    elif definition[0] == 'noun':
      augmentedlist = augmentedlist + [['NP',definition[1],definition[2]]]
    elif definition[0] == 'pronoun':
      augmentedlist = augmentedlist + [['NP',definition[1],definition[2]]]
    elif definition[0] == 'propNoun':
      augmentedlist = augmentedlist + [['NP',definition[1],definition[2]]]
    elif definition[0] == 'adv':
      augmentedlist = augmentedlist + [['AdvP',definition[1],definition[2]]]
    elif definition[0] == 'adj':
      augmentedlist = augmentedlist + [['AP',definition[1],definition[2]]]
    else:
      augmentedlist = augmentedlist

  return (augmentedlist,lexicalitems)


### Main Function
def mainparse(disambanalysis):
  #Parselist = disambanalysis - punctuation
  parselist = []
  for entry in disambanalysis:
    if entry[1][0]['pos'] == 'punct':
      parselist = parselist
    else:
      parselist = parselist + [entry]
  poslist = []
  for item in parselist:
    poslist = poslist + [[item[1][0]['pos'],item[0]]]
   # posseq = posseq + ',' + item[1][0]['pos']
    #poslist = posseq[1:]
  terminalsym = posShifting(poslist)
  lengh = len(terminalsym[1])
  parsed = reducing(terminalsym[0],lengh)
  return (parsed,parselist)



##TESTING

# test1 = [['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source':
# 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 'gender rule':
# '1'}]], ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun'
# , 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '29',
# 'number rule': '?', 'gender rule': '?'}]]]
# 
# test2 = [['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'det',
# 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 
# 'gender rule': '1'}]], ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si',
# 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino',
# 'frequency': '29', 'number rule': '?', 'gender rule': '?'}]], ['comeu', [{'form':
# 'pret-perf;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 
# 'lexicon', 'canonic': 'comer', 'frequency': '0'}]], ['a', [{'gender': 'f', 'spos'
# : 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 
# 'frequency': '35', 'number rule': '?', 'gender rule': '?'}]], ['comida', 
# [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 
# 'source': 'lexicon', 'canonic': 'comida', 'frequency': '29', 'number rule': '?',
# 'gender rule': '?'}]]]
# 
# test3 = [['em', [{'source': 'tokn', 'canonic': 'em', 'frequency': '35', 'pos': 
# 'prep'}]], ['o', [{'spos': 'de', 'pos': 'det', 'source': 'tokn', 'canonic': 'o',
# 'num': 'si', 'gen': 'm'}]], ['carro', [{'degree': 'n', 'gender': 'm', 'number':
# 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'carro',
# 'frequency': '32', 'number rule': '2', 'gender rule': '?'}]], ['viram', [{'form':
# 'pres;3p;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 
# 'canonic': 'virar', 'frequency': '30'}]], ['ele', [{'source': 'lexicon', 'gender'
# : 'm', 'spos': 'obl-to;ret;', 'pos': 'pronoun', 'number': 'si', 'person': '3s',
# 'canonic': 'ele', 'frequency': '35', 'number rule': '3', 'gender rule': '2', 
# 'contraction': ''}]]]
# 
# test4 = [['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'det',
# 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 
# 'gender rule': '1'}]], ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si',
# 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 
# 'frequency': '29', 'number rule': '?', 'gender rule': '?'}]], ['bonito', 
# [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'adj', 'regency': '', 
# 'source': 'lexicon', 'canonic': 'bonito', 'frequency': '28', 'number rule': '1',
# 'gender rule': '1'}]], ['come', [{'form': 'imper-afirm;2s;pres;3s;', 'valency':
# '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 
# 'frequency': '0'}]], ['um', [{'gender': 'm', 'spos': 'i', 'number': 'si', 'pos':
# 'det', 'source': 'lexicon', 'canonic': 'um', 'frequency': '35', 'number rule': 
# '1', 'gender rule': '3'}]], ['grande', [{'degree': 'n', 'gender': 'mf', 'number':
# 'si', 'pos': 'adj', 'regency': 'de;em;por;', 'source': 'lexicon', 'canonic': 
# 'grande', 'frequency': '33', 'number rule': '3', 'gender rule': '?'}]], ['prato',
# [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 
# 'source': 'lexicon', 'canonic': 'prato', 'frequency': '29', 'number rule': '1', 
# 'gender rule': '?'}]]]
# 
# test5 = [['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'det', 
# 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 
# 'gender rule': '1'}]], ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si',
# 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 
# 'frequency': '29', 'number rule': '?', 'gender rule': '?'}]], ['come', [{'form':
# 'imper-afirm;2s;pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source'
# : 'lexicon', 'canonic': 'comer', 'frequency': '0'}]], ['com', [{'source': 
# 'lexicon', 'canonic': 'com', 'frequency': '35', 'pos': 'prep'}]], ['um', 
# [{'gender': 'm', 'spos': 'i', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 
# 'canonic': 'um', 'frequency': '35', 'number rule': '1', 'gender rule': '3'}]], 
# ['garfo', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency'
# : '', 'source': 'lexicon', 'canonic': 'garfo', 'frequency': '20', 'number rule':
# '1', 'gender rule': '?'}]]]
# 
# test6 = [['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'det', 
# 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 
# 'gender rule': '1'}]], ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si',
# 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 
# 'frequency': '29', 'number rule': '?', 'gender rule': '?'}]], ['viu', [{'form': 
# 'pret-perf;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon'
# , 'canonic': 'ver', 'frequency': '31'}]], ['o', [{'gender': 'm', 'spos': 'de', 
# 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': 
# '35', 'number rule': '11', 'gender rule': '1'}]], ['homem', [{'degree': 'n', 
# 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon',
# 'canonic': 'homem', 'frequency': '32', 'number rule': '2', 'gender rule': '?'}]]
# , ['de', [{'source': 'lexicon', 'canonic': 'de', 'frequency': '35', 'pos': 
# 'prep'}]], ['bin\xc3\xb3culos', [{'degree': 'n', 'gender': 'm', 'number': 'pl', 
# 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'bin\xf3culo',
# 'frequency': '16', 'number rule': '1', 'gender rule': '?'}]]]
# 
# test7 = [['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'det', 
# 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 
# 'gender rule': '1'}]], ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si',
# 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 
# 'frequency': '29', 'number rule': '?', 'gender rule': '?'}]], ['lentamente', [{
#   'degree': 'n', 'spos': 'cir-mod;', 'pos': 'adv', 'source': 'lexicon', 'canonic':
# 'lentamente', 'frequency': '2'}]], ['abriu', [{'form': 'pret-perf;3s;', 
# 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 
# 'abrir', 'frequency': '31'}]], ['a', [{'gender': 'f', 'spos': 'de', 'number': 
# 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 
# 'number rule': '?', 'gender rule': '?'}]], ['porta', [{'degree': 'n', 'gender': 
# 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic'
# : 'porta', 'frequency': '31', 'number rule': '1', 'gender rule': '?'}]]]
# 
# test8 = [['lentamente', [{'degree': 'n', 'spos': 'cir-mod;', 'pos': 'adv','source': 'lexicon', 'canonic': 'lentamente', 'frequency': '2'}]], ['o', [{
# 'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon',
# 'canonic': 'o', 'frequency': '35', 'number rule': '11', 'gender rule': '1'}]], 
# ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 
# 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '29', 
# 'number rule': '?', 'gender rule': '?'}]], ['abriu', [{'form': 'pret-perf;3s;',
# 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic':
# 'abrir', 'frequency': '31'}]], ['a', [{'gender': 'f', 'spos': 'de', 'number': 
# 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35',
# 'number rule': '?', 'gender rule': '?'}]], ['porta', [{'degree': 'n', 'gender': 
# 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic'
# : 'porta', 'frequency': '31', 'number rule': '1', 'gender rule': '?'}]]]
# 
#
#test9 = [['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 'gender rule': '1'}]], ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '29', 'number rule': '?', 'gender rule': '?'}]], ['come', [{'form': 'imper-afirm;2s;pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]
# # test10 = [['lentamente', [{'degree': 'n', 'spos': 'cir-mod;', 'pos': 'adv', 'source': 'lexicon', 'canonic': 'lentamente', 'frequency': '2'}]], ['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 'gender rule': '1'}]], ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '29', 'number rule': '?', 'gender rule': '?'}]], ['come', [{'form': 'imper-afirm;2s;pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]
# 
# test11 = [['sua', [{'source': 'lexicon', 'gender': 'f', 'spos': 'poss;', 'number': 'si', 'pos': 'poss', 'person': '3s', 'canonic': 'seu', 'frequency': '35', 'number rule': '3', 'gender rule': '?', 'contraction': ''}]], ['m\xc3\xa3e', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'm\xe3e', 'frequency': '31', 'number rule': '1', 'gender rule': '?'}]], ['come', [{'form': 'imper-afirm;2s;pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]],['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# # test12 = [['que', [{'pos': 'conj', 'sub-type': 'caus;conc;cons;fin;temp;inte;comp;', 'source': 'lexicon', 'canonic': 'que', 'frequency': '34', 'type': 'subord;'}]], ['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 'gender rule': '1'}]], ['menino', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'menino', 'frequency': '29', 'number rule': '?', 'gender rule': '?'}]], ['come', [{'form': 'imper-afirm;2s;pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comer', 'frequency': '0'}]]]
# 
# 
# def agr_test():
#     print mainparse(test9)
# 
# if __name__ == "__main__":
#   agr_test()



#### OLD CODE ####

#ruleseq = map(conc,range(len(x.split(','))),x.split(','))
    #coverage = coverage + [[x,

  #sequence = map(conc,range(len(poslist.split(','))),poslist.split(','))


 #print partial2
  # for x in allrules.keys():
#     ruleitems = x.split(',')
#     lenghrule = len(ruleitems)
#     pospartial = poscomplete[:]
#     for item in pospartial:
#       if item == ruleitems[0]:
#         position = pospartial.index(item)
#         listtomatch = poscomplete[position:position+lenghrule]
#         if listtomatch == ruleitems:
#           matched = [allrules[x],listtomatch,[position,position+lenghrule]]
#           possiblecover = possiblecover + [matched]
#       pospartial = ['dummy']+pospartial[1:]

 # allrules = {}
#   for rule in rules.keys():
#       if rule in poslist:
#         allrules[rule] = trules[rule]
