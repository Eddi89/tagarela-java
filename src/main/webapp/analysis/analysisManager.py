# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: analysisManager.py,v 1.4 2008-07-31 14:06:32 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

from mod_python import apache
t        = apache.import_module("../code/tagarela_utils.py")

############################
# load the various analysis modules:
############################

import tokenizer
import spellChecker
import lexLookup
import disambiguator
import parser
import agreement
import matchStr
import reqConcept
import difflib
import matchToken
import matchCanonic
import matchPOS
import semanticsCheck

########################################
#CALL Functions
## when necessary for the modules that produce analysis
########################################

def callSpelling(tokenizationOut):
  spellinglist = []
  #A tuple comes in (listoftokens,tokenswithinfo)
  spellana = spellChecker.mainSpell(tokenizationOut[1])
  for x, z in zip(tokenizationOut[0],spellana):
    spellinglist = spellinglist + [(x,z)]
  return spellinglist


#################
# Functions to fetch information about the activity
#################
  #{u'procMode': u'1', u'targetList': {u'target': [{u'reqWords': {u'word': u'paulo'},
  #                                                u'string': u'Ele se chama Paulo.'}, 
  #                                                {u'reqWords': {u'word': u'paulo'}, 
  #                                                u'string': u'O nome dele \xe9 Paulo.'}]}}
  
  #{u'procMode': u'1', u'targetList': {u'target': {u'reqWords': {u'word': [u'paulo', u'm\xe9dico']}, 
  #                                                u'string': u'Paulo \xe9 m\xe9dico.'}}}


def activityInfo(session):
  #target = [[string,[list of words]],[string,[list of words]]]
  if type(session['targetList']) == dict:
    #return 'OOKK'
    if type(session['targetList']['target']) == dict:
      if type(session['targetList']['target']['reqWords']['word']) == str: 
        return  [[session['targetList']['target']['string'],[session['targetList']['target']['reqWords']['word']]]]
      if type(session['targetList']['target']['reqWords']['word']) == list:        
        return  [[session['targetList']['target']['string'],session['targetList']['target']['reqWords']['word']]]        
      else:
        return [['no-act-info']]
    elif type(session['targetList']['target']) == list:
      target = []
      for item in session['targetList']['target']:
        if type(item['reqWords']['word']) == str:
          target = target + [[item['string'],[item['reqWords']['word']]]]
        elif type(item['reqWords']['word']) == list:
          target = target + [[item['string'],item['reqWords']['word']]]
        else:
          target = target
      return target
    else:
        target = [['no-act-info']]
  else:
        target = [['no-act-info']]
  return target

# Make list of possible answers in strings
def stringsAn(allAnswers):
  stringAns = []
  if allAnswers[0] == ['no-act-info']:
    return allAnswers
  else:
    for item in allAnswers:
      item8 = item[0]
      stringAns = stringAns + [item8]
  return stringAns

#Get required words for answer
def required(allAnswers,bestanswer):
  reqWords = []
  for answer in allAnswers:
    if answer[0] == bestanswer:
      chosenAnswer = answer
  for x in chosenAnswer[1]:
    reqWords = reqWords + [x]
  return reqWords


# choose the best answer to work with using difflib module
def chooseAnswer(learnerInput,answerstrings):
  listofRatios =[]
  listofoptions = []
  for answer in answerstrings:
    calculate =  difflib.SequenceMatcher(lambda x: x == " ",
                        answer,
                        learnerInput)
    ratio = calculate.ratio()
    listofRatios = listofRatios + [ratio]
    listofoptions = listofoptions + [[ratio,answer]]
  betterRatio = max(listofRatios)
  for option in listofoptions:
    if option[0] == betterRatio:
      return option[1]
  return answerstrings[0]


##############################
## Decision Functions
##############################

def spellDecision(spellanalysis,lexicalanalysis):
 #[['en', [{'pos': '0'}]], ['en', [{'pos': '0'}]]]
 # [('en', 'e, em, eu, ex, zen'), ('en', 'e, em, eu, ex, zen')]
  spelllist=[]
  try:
    number = 0
    for item in spellanalysis:
      if item[1] == 'ok':
        spelllist = spelllist
      else:
        element = lexicalanalysis[number]
        #for element in lexicalanalysis:
        if element[0] == item[0]:
          for definition in element[1]:
            if definition['pos'] == '0':
              spelllist = spelllist + [item]
            else:
              spelllist = spelllist
        else:
              spelllist = spelllist
      number = number+1
  except:
    spelllist = spelllist
  return spelllist


#####################################
## MAIN FUNCTION
### The main function returns the analysis of all sub modules
## in a dictionary plus the 'error':[{list of dictionaries}]
#####################################

def allothers(sess,learnerInput):
  AnalysisDict = {'learnerInput': learnerInput}

  ## Fetch basic info about the exercise
  current = t.usermodel_get(sess,'current')
  actModel = current['actSpec']['question'][int(current['questNr'])-1]
  
  #apache.log_error('### FOO BAR: ACTIVITY SPEC FOUND: %s'%(actModel),apache.APLOG_CRIT)

  if actModel['targetList'] == []:
    AnalysisDict['error'] = {'type':'noActModel'}
    return AnalysisDict
  allAnswers = activityInfo(actModel) # list (str) of the type [[string1,[req words1]],[string2,[req words2]]]

  #apache.log_error('### FOO BAR: ACTIVITY SPEC FOUND: %s'%(allAnswers),apache.APLOG_CRIT)

  answerstrings = stringsAn(allAnswers) #list of strings (in utf-8) that answer the question

  bestAnswer = chooseAnswer(learnerInput,answerstrings)
  AnalysisDict['bestAnswer'] = bestAnswer

  ## First check if strings match ##
  stringMatching = matchStr.mainstrMatching(learnerInput,answerstrings)
    ## STRING MATCHING DECISION
  #decide to continue or not
  # ['ok', 'Ele se chama Paulo.']
  if stringMatching[0][0:2] == 'ok':
    AnalysisDict['stringmatching'] = stringMatching
    AnalysisDict['error'] = {'type':'none'} 
    return AnalysisDict

  ##########################################################
  ### Pre-processing Student Input ####
  #Tokenization, Spelling, and Lexical look-up for student input
  tokenization = tokenizer.mainToknz(learnerInput) # It returns a tuple (listoftokens,tokenswithinfo)
  spellanalysis = callSpelling(tokenization)
  lexicalanalysis = lexLookup.lexmain(tokenization[1])
  disambanalysis = disambiguator.mainDisamb(lexicalanalysis)
  
    ##SPELLING DECISION###
  ## It stops processing if there is a spelling error ##
  spellingdecision = spellDecision(spellanalysis,lexicalanalysis)
  if len(spellingdecision) > 0:
    AnalysisDict['error'] = {'type':'spelling','analysis':spellingdecision,'spellanalysis':spellanalysis,'lexical':lexicalanalysis}
    return AnalysisDict
  
  ### IT KEEPS PROCESSING OTHERWISE ##

  ### Pre-processing Target Answer ####
  # choose the closest target answer to work with (using difflib module)
  requiredWs = required(allAnswers,bestAnswer) # required words in utf-8 from the best answer
  tokensAN = tokenizer.mainToknz(bestAnswer) #tokenizes the best answer
  lexiconAN = lexLookup.lexmain(tokensAN[1])
  disambiguatedAN = disambiguator.mainDisamb(lexiconAN)
  
  #### MEANING BASED CHECKS #####

  #Check if required words are present
  # returns a list of required words that are not found in ST input
  reqWordAna = reqConcept.mainReqConc(tokenization[1],requiredWs)
  
  ##Sequences of matchings
  TokenMatching = matchToken.mainTokenMatch(tokenization[0],tokensAN[0])
  CanonicMatching = matchCanonic.mainCanonicMatch(disambanalysis,disambiguatedAN,TokenMatching)
  POSMatching = matchPOS.mainPOSmatch(disambanalysis,disambiguatedAN,CanonicMatching)

  #Parser will return a tuple with the elements it used to parse, i.e. no puctuation
  parsetree = parser.mainparse(disambanalysis)
  AgreementCheck = agreement.mainagr(parsetree[0],parsetree[1])

  ## Parser for Answer to see if it is an S.
  parsetreeAN = parser.mainparse(disambiguatedAN)

  AnalysisDict['tokens'] = tokenization
  AnalysisDict['lexInfo'] = lexicalanalysis
  AnalysisDict['spellAnalysis'] = spellanalysis
  AnalysisDict['disambiguator'] = disambanalysis
  AnalysisDict['parser']= parsetree[0]
  AnalysisDict['agreement'] = AgreementCheck
  AnalysisDict['requiredWords'] = requiredWs
  AnalysisDict['tokensAnswer'] = tokensAN
  AnalysisDict['lexiconAN'] = lexiconAN
  AnalysisDict['disambiguatedAN'] = disambiguatedAN
  AnalysisDict['reqWordsAna'] = reqWordAna
  AnalysisDict['stringmatching'] = stringMatching
  AnalysisDict['tokenMatching'] = TokenMatching
  AnalysisDict['canonicMatching'] = CanonicMatching
  AnalysisDict['POSmatching'] = POSMatching
  AnalysisDict['parserAN'] = parsetreeAN[0]
  AnalysisDict['error'] = {'type':'unidentified'}
  
  meaningInfo = semanticsCheck.mainSemantics(AnalysisDict)
  
  AnalysisDict['semanticCheck'] = meaningInfo

  return AnalysisDict


###############################################
########### FIB PROCESSING ####################
###############################################

## auxiliary function
def analyzeFIB(pairs):
  analysis = []
  for item in pairs:
    if item[0] == item[1]:
      analysis = analysis + [{'error':'ok'}]
    elif item[0] != item[1]:
      tokenizeAn = item[1].split()
      if len(tokenizeAn) > 1:
        analysis = analysis + [{'error':'multiple-tokens'}]
      else:
        stword = lexLookup.lexmain([[item[1],[{'pos': '0'}]]])
        anword = lexLookup.lexmain([[item[0],[{'pos': '0'}]]])
        try:
          canonicSt = stword[0][1][0]['canonic']
          canonicAn = anword[0][1][0]['canonic']
        except:
          canonicSt = item[1]
          canonicAn = item[0]
        if canonicSt == canonicAn:
          analysis = analysis + [{'error':'same-canonic', 'tokens':[stword,anword]}]
        else:
          analysis = analysis + [{'error':'different-canonic','tokens':[stword,anword]}]
        #analysis = analysis + [{'error':'error'}]
    else:
      analysis = analysis + [{'error':'no evaluation'}]
  return analysis


#main FIB function
def fibProcessing(sess,learnerInput):
  DisplayableLernerInput = ''
  number = 1
  for x in learnerInput:
    DisplayableLernerInput = DisplayableLernerInput + str(number) + ') ' + x + '; '
    number = number +1
  AnalysisDict = {'learnerInput': DisplayableLernerInput[:-2]}
  AnalysisDict['error'] = {'type':'unidentified'}
  session = t.usermodel_get(sess,'actModel')
  #AnalysisDict['Act Info'] = session
  answers = []
  for x in session['targetList']:
    answers = answers + [x['target']['string']]
  AnalysisDict['answers'] = answers
  pairs = []
  for (x,y) in zip(answers,learnerInput):
    pairs = pairs + [[x,y.lstrip().rstrip()]]
  AnalysisDict['Pairs'] = pairs
  analysis = analyzeFIB(pairs)
  AnalysisDict['Analysis'] = analysis


#   current = t.usermodel_get(sess,'current')
#   answers = []
#   for x in session['targetList']:
#     answers = answers + [x['target']['string']]
#   #current = t.usermodel_get(sess,'current')
#   AnalysisDict = {'learnerInput':learnerInput, 'info':answers, 'current':current}
  return AnalysisDict


def main(sess,learnerInput):
  current = t.usermodel_get(sess,'current')
  if current['actType'] == 'FIB':
    output = fibProcessing(sess,learnerInput)
  else:
    output = allothers(sess,learnerInput[0])
  return output



## Old Code

 # AnalysisDict = {'learnerInput': learnerInput}
#    AnalysisDict['error'] = {'type':'noActModel'}
#    return AnalysisDict

# get tokenization for answers
# def tokensAnswers(answerstrings):
#   tokenizedAns = []
#   for item in answerstrings:
#     tokenizedAns = tokenizedAns + [tokenizer.mainToknz(item)]
#   return tokenizedAns

