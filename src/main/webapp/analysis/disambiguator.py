# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: disambiguator.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

### A syntactic desambiguator ###

### input = lexLookup output

### output = desambiguated lexLookup output

#map(lambda y: map(lambda x: x['pos'], y), lexUtils.getEntries([á','maria','fala','portuguesa']))


def rules(item,sequence,finallist):
  #item = (#,['POS1','POSn'])
  try:
    next = sequence[item[0]+1][1]
  except: 
    next = []
  try:
    previous = finallist[-1:]
  except:
    previous = []
  if 'prep' in item[1]:
    if next == ['pronoun']:
      return ['prep']
    elif 'det' in next:
      return ['prep']
    # next rule would be more accurate if we could rely on subcategorization of verb
    elif previous == [['verb']] and 'poss' in  next:
      return ['prep']
    else:
      nooption = item[1]
  if 'verb' in item[1]:
    if previous == [['pronoun']]:
      return ['verb']
    elif previous == [['propNoun']]:
      return ['verb']
    elif previous == [['noun']]:
      return ['verb']
    elif next == ['noun'] and ('adj' not in item[1]) and ('poss' not in item[1]):
      return ['verb']
    else:
      nooption = item[1]
  if 'adj' in item[1]:
    if previous == [['noun']]:
      return ['adj']
    elif next == ['noun']:
      return ['adj']
    else:
      nooption = item[1]
  
  if 'numeral' in item[1]:
    if next == ['numeral','noun']:
      return ['numeral']
    elif next == ['noun']:
      return ['numeral']
    elif previous == [['numeral']]:
       return ['noun']
    else:
      nooption = item[1]

  #to be more precize we would need the subcategorization of the verb (reflexive)
  if 'conj' in item[1] and 'adv' not in item[1]:
    if ('verb' not in next) and ('verb' not in item[1]):
      return ['conj']
    else:
      nooption = item[1]

  else: return item[1]
  return nooption

###############################
############## END RULES ######
###############################

def conc(x,y):
  return (x,y)

def disambiguator(lista):
  sequence = map(conc,range(len(lista)),lista)
  finallist = []
  for x in sequence:
    # Comment next line out for final version
    #finallist = finallist + [x[1]]
    if len(x[1]) == 1:
      finallist = finallist + [x[1]]
    else:
      item = rules(x,sequence,finallist)
      finallist = finallist + [item]
  return finallist


## This function fetches the POS for all definitions of an item + frequency

def POSfetcher(listaDict):
  poslist = []
  for x in listaDict:
   poslist = poslist + [x['pos']]
  return poslist

## This function fetches the POS with frequency

def POSwFreq(listaDict):
 posWfreqlist = []
 for x in listaDict:
    try:
      posWfreqlist = posWfreqlist + [(x['pos'],int(x['frequency']))]
    except:
      posWfreqlist = posWfreqlist + [(x['pos'],'0')]
 return posWfreqlist


### If the rules cannot disambiguate the sentence 
### this function uses the frequency in the lexical 
### entry to do the job


def frequency(disambiguated,POSwFList):
  dis = map(conc,range(len(disambiguated)),disambiguated)
  poswf = map(conc,range(len(POSwFList)),POSwFList)
  finaldis = []
  for item in dis:
    if len(item[1]) == 1:
      finaldis = finaldis + [item[1]]
    else:
     try:
      element = poswf[item[0]]
      # the stop sign allows the disambiguator to select only the first element with the 
      # highest frequency. The second one is discharged.
      stop = 0
      #This separates the frequencies in a separate list.
      separatenumbers = zip(*element[1])
      # It finds the higher number
      higher = max(separatenumbers[1])
      for x in element[1]:
        if higher == x[1]:
          if stop == 0:
            solution = x[0] 
            finaldis = finaldis + [[solution]]
            stop = stop+1
        else:
          finaldis = finaldis
     except: finaldis = finaldis + [item[1]]
  return finaldis


########### Building DATA #####

def definitionfunc(pos,allposs):
  for x in allposs:
    if x['pos'] == pos[0]:
      return x
  return 'nao'

def deslist(freqdisamb,lista):
  disEntries = []
  #create numbers for elements
  freqnum = map(conc,range(len(freqdisamb)),freqdisamb)
  listanum = map(conc,range(len(lista)),lista)
  for element in listanum:
    #find definition by sending the selected POS from freqnum and the list of definitions form element
    definition = definitionfunc(freqnum[element[0]][1],element[1][1])

    disEntries = disEntries + [[element[1][0],[definition]]]

  return disEntries







#Main function

def mainDisamb(lista):
 POSList = []
 POSwFList = []
 for token in lista:
   POSs = POSfetcher(token[1])
   POSList = POSList + [POSs]
   POSwF = POSwFreq(token[1])
   POSwFList = POSwFList + [POSwF]
 disambiguated = disambiguator(POSList)
 freqdisamb = frequency(disambiguated,POSwFList)
 disambiguatedlist = deslist(freqdisamb,lista)

 return disambiguatedlist


################### TEST SUITE ##############

# lexico1 = [['de', [{'source': 'lexicon', 'canonic': 'de', 'frequency': '35', 'pos': 'prep'}]], ['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'art', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}, {'source': 'lexicon', 'canonic': 'a', 'frequency': '35', 'pos': 'prep'}, {'source': 'lexicon', 'gender': 'f', 'spos': 'dem;obl-at;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'o', 'frequency': '32', 'number rule': '?', 'gender rule': '?', 'contraction': ''}, {'gender': 'm', 'number': 'si', 'pos': 'acronym', 'source': 'lexicon', 'canonic': 'a', 'frequency': '2', 'capitalize': 'ln'}, {'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'a', 'frequency': '32', 'number rule': '?', 'gender rule': '?'}]], ['carro', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'carro', 'frequency': '32', 'number rule': '2', 'gender rule': '?'}]]]
#lexico2 = [['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'art', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 'gender rule': '1'}, {'source': 'lexicon', 'gender': 'm', 'spos': 'dem;obl-at;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'o', 'frequency': '34', 'number rule': '3', 'gender rule': '1', 'contraction': ''}, {'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'o', 'frequency': '31', 'number rule': '1', 'gender rule': '?'}]], ['jo\xc3\xa3o', [{'gender': 'm', 'number': 'si', 'pos': 'propNoun', 'source': 'lexicon', 'canonic': 'jo\xe3o', 'frequency': '33', 'capitalize': 'li'}]], ['mora', [{'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'mora', 'frequency': '28', 'number rule': '1', 'gender rule': '?'}, {'form': 'pres;3s;imper-afirm;2s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'morar', 'frequency': '30'}]], ['em', [{'source': 'tokn', 'canonic': 'em', 'frequency': '35', 'pos': 'prep'}]], ['o', [{'gender': 'm', 'spos': 'de', 'pos': 'art', 'number': 'si', 'source': 'tokn', 'canonic': 'o', 'num': 'si', 'frequency': '35', 'gender rule': '1', 'number rule': '11', 'gen': 'm'}]], ['rio', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'rio', 'frequency': '35', 'number rule': '1', 'gender rule': '?'}, {'form': 'pres;1s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'rir', 'frequency': '2'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# lexico3 = [['ele', [{'source': 'lexicon', 'gender': 'm', 'spos': 'obl-to;ret;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'ele', 'frequency': '35', 'number rule': '3', 'gender rule': '2', 'contraction': ''}]], ['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'art', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 'gender rule': '1'}, {'source': 'lexicon', 'gender': 'm', 'spos': 'dem;obl-at;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'o', 'frequency': '34', 'number rule': '3', 'gender rule': '1', 'contraction': ''}, {'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'o', 'frequency': '31', 'number rule': '1', 'gender rule': '?'}]], ['viu', [{'form': 'pret-perf;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'ver', 'frequency': '31'}]]]
# lexico4 = [['ele', [{'source': 'lexicon', 'gender': 'm', 'spos': 'obl-to;ret;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'ele', 'frequency': '35', 'number rule': '3', 'gender rule': '2', 'contraction': ''}]], ['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'art', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 'gender rule': '1'}, {'source': 'lexicon', 'gender': 'm', 'spos': 'dem;obl-at;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'o', 'frequency': '34', 'number rule': '3', 'gender rule': '1', 'contraction': ''}, {'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'o', 'frequency': '31', 'number rule': '1', 'gender rule': '?'}]], ['compra', [{'form': 'pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comprar', 'frequency': '30'}, {'degree': 'n', 'gender': 'f', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'compra', 'frequency': '32', 'number rule': '2', 'gender rule': '?'}, {'form': 'imper-afirm;2s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'comprar', 'frequency': '30'}]]]
# lexico5 = [['de', [{'source': 'lexicon', 'canonic': 'de', 'frequency': '35', 'pos': 'prep'}]], ['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'art', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}, {'source': 'lexicon', 'canonic': 'a', 'frequency': '35', 'pos': 'prep'}, {'source': 'lexicon', 'gender': 'f', 'spos': 'dem;obl-at;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'o', 'frequency': '32', 'number rule': '?', 'gender rule': '?', 'contraction': ''}, {'gender': 'm', 'number': 'si', 'pos': 'acronym', 'source': 'lexicon', 'canonic': 'a', 'frequency': '2', 'capitalize': 'ln'}, {'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'a', 'frequency': '32', 'number rule': '?', 'gender rule': '?'}]], ['\xc3\xa9', [{'form': 'pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'ser', 'frequency': '35'}]]]
# lexico6 = [['de', [{'source': 'lexicon', 'canonic': 'de', 'frequency': '35', 'pos': 'prep'}]], ['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'art', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}, {'source': 'lexicon', 'canonic': 'a', 'frequency': '35', 'pos': 'prep'}, {'source': 'lexicon', 'gender': 'f', 'spos': 'dem;obl-at;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'o', 'frequency': '32', 'number rule': '?', 'gender rule': '?', 'contraction': ''}, {'gender': 'm', 'number': 'si', 'pos': 'acronym', 'source': 'lexicon', 'canonic': 'a', 'frequency': '2', 'capitalize': 'ln'}, {'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'a', 'frequency': '32', 'number rule': '?', 'gender rule': '?'}]], ['carro', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'carro', 'frequency': '32', 'number rule': '2', 'gender rule': '?'}]], ['para', [{'source': 'lexicon', 'canonic': 'para', 'frequency': '35', 'pos': 'prep'}, {'source': 'lexicon', 'canonic': 'para', 'frequency': '2', 'pos': 'prefix'}]]]
# lexico7 = [['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'art', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}, {'source': 'lexicon', 'canonic': 'a', 'frequency': '35', 'pos': 'prep'}, {'source': 'lexicon', 'gender': 'f', 'spos': 'dem;obl-at;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'o', 'frequency': '32', 'number rule': '?', 'gender rule': '?', 'contraction': ''}, {'gender': 'm', 'number': 'si', 'pos': 'acronym', 'source': 'lexicon', 'canonic': 'a', 'frequency': '2', 'capitalize': 'ln'}, {'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'a', 'frequency': '32', 'number rule': '?', 'gender rule': '?'}]], ['carro', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'carro', 'frequency': '32', 'number rule': '2', 'gender rule': '?'}]]]
# lexico8 = [['de', [{'source': 'lexicon', 'canonic': 'de', 'frequency': '35', 'pos': 'prep'}]], ['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'art', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}, {'source': 'lexicon', 'canonic': 'a', 'frequency': '35', 'pos': 'prep'}, {'source': 'lexicon', 'gender': 'f', 'spos': 'dem;obl-at;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'o', 'frequency': '32', 'number rule': '?', 'gender rule': '?', 'contraction': ''}, {'gender': 'm', 'number': 'si', 'pos': 'acronym', 'source': 'lexicon', 'canonic': 'a', 'frequency': '2', 'capitalize': 'ln'}, {'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'a', 'frequency': '32', 'number rule': '?', 'gender rule': '?'}]], ['lindo', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'lindo', 'frequency': '20', 'number rule': '1', 'gender rule': '1'}, {'form': 'pres;1s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'lindar', 'frequency': '2'}]], ['carro', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'carro', 'frequency': '32', 'number rule': '2', 'gender rule': '?'}]]]
# lexico9 = [['para', [{'source': 'lexicon', 'canonic': 'para', 'frequency': '35', 'pos': 'prep'}, {'source': 'lexicon', 'canonic': 'para', 'frequency': '2', 'pos': 'prefix'}]], ['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'art', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 'gender rule': '1'}, {'source': 'lexicon', 'gender': 'm', 'spos': 'dem;obl-at;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'o', 'frequency': '34', 'number rule': '3', 'gender rule': '1', 'contraction': ''}, {'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'o', 'frequency': '31', 'number rule': '1', 'gender rule': '?'}]], ['comprar', [{'form': 'fut-subj;3s;fut-subj;1s;inf-pess;3s;inf-pess;1s;', 'valency': 'bi;td;', 'pos': 'verb', 'regency': 'a;com;de;para;por;', 'source': 'lexicon', 'canonic': 'comprar', 'frequency': '32'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# lexico10= [['pode', [{'form': 'pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'poder', 'frequency': '35'}, {'form': 'imper-afirm;3s;pres-subj;3s;pres-subj;1s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'podar', 'frequency': '35'}]],['pode', [{'form': 'pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'poder', 'frequency': '35'}, {'form': 'imper-afirm;3s;pres-subj;3s;pres-subj;1s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'podar', 'frequency': '35'}]]]
# 
# lexico11 = [['cinco', [{'gender': 'mf', 'spos': 'car', 'number': 'pl', 'pos': 'numeral', 'source': 'lexicon', 'canonic': 'cinco', 'frequency': '33', 'number rule': '?', 'gender rule': '?'}, {'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'cinco', 'frequency': '28', 'number rule': '1', 'gender rule': '?'}]], ['quartos', [{'gender': 'm', 'spos': 'fra-ord', 'number': 'pl', 'pos': 'numeral', 'source': 'lexicon', 'canonic': 'quarto', 'frequency': '2', 'number rule': '1', 'gender rule': '1'}, {'degree': 'n', 'gender': 'm', 'number': 'pl', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'quarto', 'frequency': '2', 'number rule': '1', 'gender rule': '1'}]]]
#lexico12 = [['grande', [{'degree': 'n', 'gender': 'mf', 'number': 'si', 'pos': 'adj', 'regency': 'de;em;por;', 'source': 'lexicon', 'canonic': 'grande', 'frequency': '2', 'number rule': '3', 'gender rule': '?'}, {'degree': 'n', 'gender': 'mf', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'grande', 'frequency': '32', 'number rule': '?', 'gender rule': '?'}]],['homem', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'homem', 'frequency': '32', 'number rule': '2', 'gender rule': '?'}]]]


# def lex_test():
#     print mainDisamb(lexico2)
# #
# # 
# if __name__ == "__main__":
#   lex_test()
# #

