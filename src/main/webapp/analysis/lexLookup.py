# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: lexLookup.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

### PROBLEMS

# It is not recognizing tokens with accents


import commands
import re
import string
import lexicon
import lexUtils


## FUNCTION 1
## Look-up still Unknown item

def unknownPOS(listItem):
  #['naa', [{'pos': 'UNKNOWN'}]]
  try:
    lexico = [listItem[0],lexUtils.getEntry(listItem[0])]  #[listItem[0],tLexicon[listItem[0]]]
  except:
    lexico = listItem
  return lexico



#### Funtctions for Known POS
def correctdef(pos,listofdef):

  for definition in listofdef:
    if pos == definition['pos']:
      correct = definition
      return correct
    else:
      nocorrect = 'nodef'
  return nocorrect



def knownPOS(listItem):
  #e.g., listItem = ['comeram', [{'source': 'tokn', 'pos': 'verb'}]]

  try:
    # fetch lexical definitions
    definitions = lexUtils.getEntry(listItem[0])
    #for x in definitions:
     #print x, '\n'
    #return definitions
    # select definition for POS
    #print listItem[1]['pos']
    correctDef = correctdef(listItem[1][0]['pos'],definitions)
    #if there are no definitions for POS return original definitions
    if correctDef == 'nodef':
      return listItem
    else:
      itemsdeflex =  correctDef.keys()
      itemsoriginaldef = listItem[1][0].keys()
      finaldefinition = listItem[1][0]
     #return itemsdeflex, itemsoriginaldef
      for item in itemsdeflex:
  #        print item
          if item in itemsoriginaldef:
            finaldefinition = finaldefinition
          else:
           finaldefinition[item] = correctDef[item]
      return [listItem[0],[finaldefinition]]

  except:
    finaldefinition = listItem
    return finaldefinition

# 1) Buscar a definiçÂão da palavra lexico
# 2) Selecionar a definiçÂão para a POS
# 3) Fazer duas listas [definiçÂão lexico] e [definiçÂão dada]
# 4) se item [definiçÂão lexico] NOT IN [definiçÂão dada]
#      entrada[item] = 'def lex'
#    else:
#      entrada[item] = 'def dada'
#   return entrada


#[['Eles', {'pos': 'UNKNOWN'}], ['comeram', {'source': 'tokn', 'pos': 'verb'}], 
#['o', {'case': 'acc', 'pos': 'pron', 'per': 3, 'source': 'tokn', 'canonic': 'o', 'num': 'si', 'gen': 'm'}]]


## Main Function ##

def lexmain(toknOutPutList):
  #return toknOutPutList
  lexlookup = []
    #['ele', [{'pos': '0'}]]
#
  for x in toknOutPutList:
   # lexlookup = [x]
    if x[1][0]['pos'] == '0':
      #return "ok"
      #return lexUtils.getEntry('a')
      lexlookup = lexlookup + [unknownPOS(x)]     
    else:
      lexlookup = lexlookup + [knownPOS(x)]
  return lexlookup
#
# def outra(x):
#  return "ok aqui"


# def lex_test():
#    print lexmain([['comeram', [{'source': 'tokn', 'pos': '0'}]]])
# if __name__ == "__main__":
#  lex_test()
