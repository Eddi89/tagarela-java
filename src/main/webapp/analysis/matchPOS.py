# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: matchPOS.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

## Input:  the output of the disambiguator for both ST and AN inputs
## Output: ?

def select(matched,cleanMatches):
  # find target elements in clean matches
  TaCm = []
  for item in cleanMatches:
    TaCm = TaCm + [item[1][1]]
  if len(matched) == 2:
    dif1 = abs(matched[0][1][0] - matched[0][1][1])
    dif2 = abs(matched[1][1][0] - matched[1][1][1])
    if dif1 == dif2:
      chosenByDis = matched[0]
    elif dif1 < dif2:
      chosenByDis =  matched[0]
    elif dif1 > dif2:
      chosenByDis = matched[1]
    if matched[0][1][1] in TaCm:
      chosenByRedun = matched[1]
    elif matched[1][1][1] in TaCm:
      chosenByRedun = matched[0]
    else:
      chosenByRedun = matched[0]
    if chosenByRedun == chosenByDis:
      return chosenByDis
    elif chosenByRedun != chosenByDis:
      return chosenByRedun
    else:
      return chosenByDis
  elif len(matched) == 3:
    return select([matched[0],select([matched[1],matched[2]],cleanMatches)],cleanMatches)
  elif len(matched) == 4:
    return select([matched[0],select([matched[1],select([matched[2],matched[3]],cleanMatches)],cleanMatches)],cleanMatches)
  elif len(matched) == 5:
    return select([matched[0],select([matched[1],select([matched[2],
                  select([matched[3],matched[4]],cleanMatches)],cleanMatches)],cleanMatches)],cleanMatches)
  else:
    return matched


def matching(listTst,listTan):
  matches = []
  cleanMatches = []
  matchedST = []
  matchedAN = []
  notmatchedST = []
  notmatchedAN = []
  #matches
  for x in listTst:
    matched = []
    for y in listTan:
      if x[2] == y[2]:
        match = [(x[0],y[0]),[x[1],y[1]],x[2]]
        matched = matched + [match]
 # selectedMatch = select(matches)
    if len(matched) == 1:
      cleanMatches = cleanMatches + matched
      matchedST = matchedST + [matched[0][1][0]]
      matchedAN = matchedAN + [matched[0][1][1]]
    elif len(matched) > 1:
      chosen = select(matched,cleanMatches)
      cleanMatches = cleanMatches + [chosen]
      matchedST = matchedST + [chosen[1][0]]
      matchedAN = matchedAN + [chosen[1][1]]
    else:
      cleanMatches = cleanMatches
  #print matchedST, matchedAN

  for item in listTst:
    if item[1] not in matchedST:
      notmatchedST = notmatchedST + [item]
    else:
      notmatchedST = notmatchedST
  for item in listTan:
    if item[1] not in matchedAN:
      notmatchedAN = notmatchedAN + [item]
    else:
      notmatchedAN = notmatchedAN
  return {'POS-matches':cleanMatches, 'POS-ST-no-match':notmatchedST, 'POS-AN-no-match':notmatchedAN}



def mainPOSmatch(disambSt,disambAn,canonicMatched):
  listTst = []
  listTan = []
  for y in canonicMatched['canonic-ST-no-match']:
    try:
      listTst = listTst + [[y[0],y[1], disambSt[y[1]][1][0]['pos']]]
    except:
      listTst = listTst + [[y[0],y[1],y[0]]]
  for x in canonicMatched['canonic-AN-no-match']:
    try:
      listTan = listTan + [[x[0],x[1], disambAn[x[1]][1][0]['pos']]]
    except:
      listTan = listTan + [[x[0],x[1],x[0]]]
  POSmatch = matching(listTst,listTan)
  return POSmatch




#### Testing #####

# tokPOSst = [['ele', [{'source': 'lexicon', 'gender': 'm', 'spos': 'obl-to;ret;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'ele', 'frequency': '35', 'number rule': '3', 'gender rule': '2', 'contraction': ''}]], ['chama', [{'form': 'imper-afirm;2s;pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'chamar', 'frequency': '31'}]], ['paulo', [{'gender': 'm', 'number': 'si', 'pos': 'propNoun', 'source': 'lexicon', 'canonic': 'paulo', 'frequency': '35', 'capitalize': 'li'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# tokPOSan = [['ele', [{'source': 'lexicon', 'gender': 'm', 'spos': 'obl-to;ret;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'ele', 'frequency': '35', 'number rule': '3', 'gender rule': '2', 'contraction': ''}]], ['se', [{'source': 'lexicon', 'gender': 'mf', 'spos': 'obl-at;refl;', 'number': 'sp', 'pos': 'pronoun', 'person': '3s', 'canonic': 'se', 'frequency': '35', 'number rule': '?', 'gender rule': '?', 'contraction': ''}]], ['chama', [{'form': 'imper-afirm;2s;pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'chamar', 'frequency': '31'}]], ['paulo', [{'gender': 'm', 'number': 'si', 'pos': 'propNoun', 'source': 'lexicon', 'canonic': 'paulo', 'frequency': '35', 'capitalize': 'li'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# 
#
# 
# def agr_test():
#     print mainPOSmatch(tokPOSst,tokPOSan)
# 
# 
# if __name__ == "__main__":
#   agr_test()
