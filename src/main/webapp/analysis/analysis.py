"""
---------------------------------------------------------------
$Id: analysis.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

import analysisManager
import feedbackManager

def processInput(sess,learnerInput):
  annotatedInput = analysisManager.main(sess,learnerInput)
  return feedbackManager.main(sess,annotatedInput)
