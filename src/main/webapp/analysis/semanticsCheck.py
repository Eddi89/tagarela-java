# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: semanticsCheck.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

########################
## RESULT OF SEMANTICS##
########################

#Token Matching: {'token-matches': [[('ele', 'ele'), [0, 0], 'ele'], [('chama', 'chama'), [1, 2], 'chama']],
#                  'token-AN-no-match': [['se', 1, 'se'], ['paulo', 3, 'paulo'], ['.', 4, '.']],
#                  'token-ST-no-match': []}
# 
# Canonic Matching: {'canonic-matches': [], 
#                    'canonic-AN-no-match': [['se', 1, 'se'], ['paulo', 3, 'paulo'], ['.', 4, '.']], 
#                    'canonic-ST-no-match': []}
# 
# POS Matching: {'POS-matches': [],
#                'POS-ST-no-match': [], 
#                'POS-AN-no-match': [['se', 1, 'pronoun'], ['paulo', 3, 'propNoun'], ['.', 4, 'punct']]}
# 
# Tokens: (['ele', 'chama'], [['ele', [{'pos': '0'}]], ['chama', [{'pos': '0'}]]])
# Tokens Answer: (['ele', 'se', 'chama', 'paulo', '.'], [['ele', [{'pos': '0'}]], ['se', [{'pos': '0'}]], ['chama', [{'pos': '0'}]], ['paulo', [{'pos': '0'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]])

def checknumbers (tokM,canM,posM,tokens,tokensAnswer):
  numberOfMatches ={'total#tokens':{'StAn':len(tokens[0]),'TaAn':len(tokensAnswer[0])},
                     'tokenNumbers': {'matches':len(tokM['token-matches']),'noM-St':len(tokM['token-ST-no-match']),
                                     'noM-An':len(tokM['token-AN-no-match'])},
                     'canonicNumbers':{'matches':len(canM['canonic-matches']),#{'matches':len(canM['canonic-matches']),
                                       'noM-St':len(canM['canonic-ST-no-match']),
                                       'noM-An':len(canM['canonic-AN-no-match'])},
                     'posNumbers':  {'matches':len(posM['POS-matches']),'noM-St':len(posM['POS-ST-no-match']),
                                     'noM-An':len(posM['POS-AN-no-match'])}}
  return numberOfMatches



#Token Matching: {'token-matches': [[('paulo', 'paulo'), [0, 3], 'paulo'], [('ele', 'ele'), [1, 0], 'ele'], 
#                                  [('se', 'se'), [2, 1], 'se'], [('chama', 'chama'), [3, 2], 'chama']], 
#                 'token-AN-no-match': [['.', 4, '.']], 'token-ST-no-match': []}

# Disambiguator = [['ele', [{'source': 'lexicon', 'gender': 'm', 'spos': 'obl-to;ret;', 
#                             'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'ele', 
#                             'frequency': '35', 'number rule': '3', 'gender rule': '2', 'contraction': ''}]], 
#                  ['se', [{'source': 'lexicon', 'gender': 'mf', 'spos': 'obl-at;refl;', 'number': 'sp', 
#                            'pos': 'pronoun', 'person': '3s', 'canonic': 'se', 'frequency': '35', 'number rule': '?', 
#                            'gender rule': '?', 'contraction': ''}]], 
#                  ['chama', [{'form': 'imper-afirm;2s;pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 
#                             'source': 'lexicon', 'canonic': 'chamar', 'frequency': '31'}]], 
#                  ['paulo', [{'gender': 'm', 'number': 'si', 'pos': 'propNoun', 'source': 'lexicon', 
#                             'canonic': 'paulo', 'frequency': '35', 'capitalize': 'li'}]], 
#                  ['.', [{'source': 'tokn', 'pos': 'punct'}]]]


def mainSemantics(datastructure):
 try:
  newTokenMatching = datastructure['tokenMatching'].copy()
  #list of tokens that have not matched minus punctuation
  tokensNoMAnNoP = []
  tokensNoMStNoP = []

  for x in datastructure['tokenMatching']['token-AN-no-match']:
    if datastructure['disambiguatedAN'][x[1]][1][0]['pos'] == 'punct':
      tokensNoMAnNoP = tokensNoMAnNoP
    else:
      tokensNoMAnNoP = tokensNoMAnNoP + [x]
  newTokenMatching['token-AN-no-match'] = tokensNoMAnNoP
  
  for y in datastructure['tokenMatching']['token-ST-no-match']:
    if datastructure['disambiguator'][y[1]][1][0]['pos'] == 'punct':
      tokensNoMStNoP = tokensNoMStNoP
    else:
      tokensNoMStNoP = tokensNoMStNoP + [y]
  newTokenMatching['token-ST-no-match'] = tokensNoMStNoP
  
  newCanonicMatching = datastructure['canonicMatching'].copy()
  #list of tokens that have not matched minus punctuation
  canonicNoMAnNoP = []
  canonicNoMStNoP = []
  for w in datastructure['canonicMatching']['canonic-AN-no-match']:
    if datastructure['disambiguatedAN'][w[1]][1][0]['pos'] == 'punct':
      canonicNoMAnNoP = canonicNoMAnNoP
    else:
      canonicNoMAnNoP = canonicNoMAnNoP + [w]
  newCanonicMatching['canonic-AN-no-match'] = canonicNoMAnNoP
  
  for v in datastructure['canonicMatching']['canonic-ST-no-match']:
    if datastructure['disambiguator'][v[1]][1][0]['pos'] == 'punct':
      canonicNoMStNoP = canonicNoMStNoP
    else:
      canonicNoMStNoP = canonicNoMStNoP + [v]
  newCanonicMatching['canonic-ST-no-match'] = canonicNoMStNoP
  
  newPOSMatching = datastructure['POSmatching'].copy()
  #list of tokens that have not matched minus punctuation
  posNoMAnNoP = []
  posNoMStNoP = []
  for k in datastructure['POSmatching']['POS-AN-no-match']:
    if datastructure['disambiguatedAN'][k[1]][1][0]['pos'] == 'punct':
      posNoMAnNoP = posNoMAnNoP
    else:
      posNoMAnNoP = posNoMAnNoP + [k]
  newPOSMatching['POS-AN-no-match'] = posNoMAnNoP
  
  for a in datastructure['POSmatching']['POS-ST-no-match']:
    if datastructure['disambiguator'][a[1]][1][0]['pos'] == 'punct':
      posNoMStNoP = posNoMStNoP
    else:
      posNoMStNoP = posNoMStNoP + [a]
  newPOSMatching['POS-ST-no-match'] = posNoMStNoP

  numbers = checknumbers(newTokenMatching,newCanonicMatching,
                         newPOSMatching,datastructure['tokens'],datastructure['tokensAnswer'])

 except:
   numbers = checknumbers(datastructure['tokenMatching'],datastructure['canonicMatching'],
                          datastructure['POSmatching'],datastructure['tokens'],datastructure['tokensAnswer'])


 return numbers

# datastructure = {'learnerInput':learnerInput,'tokens':tokenization,'lexInfo':lexicalanalysis,
#                    'spellAnalysis':spellanalysis, 'disambiguator':disambanalysis, 'parser':parsetree[0],
#                    'agreement':AgreementCheck, 'bestAnswer':bestAnswer, 'requiredWords':requiredWs, 
#                    'tokensAnswer':tokensAN, 'lexiconAN':lexiconAN, 'disambiguatedAN':disambiguatedAN,
#                    'reqWordsAna': reqWordAna, 'stringmatching':stringMatching, 'tokenMatching':TokenMatching, 
#                    'canonicMatching':CanonicMatching, 'POSmatching': POSMatching, 'parserAN':parsetreeAN[0],
#                    'error':{'type':'unidentified'}}
