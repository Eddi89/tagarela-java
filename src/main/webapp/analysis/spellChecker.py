# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: spellChecker.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

import commands
import re
import string

from os.path import join,dirname,abspath

# configurations for ispell
ispell = commands.getoutput("/usr/bin/which ispell")

# brazilian hash is assumed in directory at same level as this file:
br_hash = abspath(join(dirname(__file__),'br.ispell-2.4/br.hash'))


#####################################################
# This function cleans the output from Ispell.
# It leaves the suggested words for non-words in the input
# and it replaces the * with 'ok'


def cleaning(resp):
    error = []
    for x in resp:
        #clean the wrong answer (Ispell output)
        #by deleting the first four elements
        if '&' in x:
           #& naa 10 0: Ana, na, nada, naja, nana, nas, nata, na a, na-a, nua\n
           suggestions = x.split(':')[1][1:]
           error = error + [suggestions]
        elif '#' in x:
           error = error + ['no suggestion']
        else:
           error = error + ['ok']
    return error



#####################################################

####################################################
#This function consults ispell and outputs a list with words and suggestions

def checkspelling(listoftokens):
    analysis = []
    # separate the words in the sentence
    # Once the tokenizer is working, this line won't be necessary
    #spl = frase.split()

    # Calls Ispell
    for word in listoftokens:
        # ispell is too old for unicode, convert input to latin1
        word_iso = word.decode("utf-8").encode("iso-8859-1")
        
        checked = commands.getoutput("echo %s | %s -a -d %s"%(word_iso,ispell,br_hash))

        # returns for

        #Naa =  '@(#) International Ispell Version 3.3.02 12 Jun 2005\n&
        # naa 10 0: Ana, na, nada, naja, nana, nas, nata, na a, na-a, nua\n'
        
        #Na = '@(#) International Ispell Version 3.3.02 12 Jun 2005\n*\n'
        
        #naadaa = '@(#) International Ispell Version 3.3.02 12 Jun 2005\n# naadaa 0\n'
        
        #pros = '@(#) International Ispell Version 3.3.02 12 Jun 2005\n+ O\n'

        # clean lines
        # 1) creates a list with the new line as a parameter 
        # convert back to utf8
        linha = checked.decode("iso-8859-1").encode("utf-8").split('\n')
        #linha = checked.split('\n')
        
        # 2) deletes the first and the last item of the list
        #    to keep the word and suggestions only
        analysis = analysis + [linha[1]]

    #returns ['*', '& commemos 1 0: comemos']
    #for "eu commemos"
    #return analysis
    return analysis


####################################################
#This function extracts the words to be checked from 
# the output of the tokenizer.
#tokenslist = []

def maketokenslist(toknOutPutList):
  tokenlist = []
  for token in toknOutPutList:
    tokenlist = tokenlist + [token[0]]
  return tokenlist


################# CONTROL FUNCTION ####################

#The input for spell module is data2
#The output is data3

#{'user_input': 'resp',
#'tokenized_input': ['Eu', 'ele', 'e', 'ela', 'comemos']
#'spell_analysis':{'word1':{'word':'ok'},
#                  'word2':{'word':[list of suggestions]}


def mainSpell(toknOutPutList):
  # Make a list of tokens for spell checking
  tokens = maketokenslist(toknOutPutList)
  # Calls spell checking function
  spellchecking = checkspelling(tokens)
  # Calls cleaning function
  cleaned = cleaning(spellchecking)
  # The out put is a list with suggestions for each token
  return cleaned




############ OLD CODE #########

#output of checkspelling function for "eu commo"
#['*', '& commo 8 0: coamo, coemo, comamo, comemo, como, com mo, com-mo, cosmo']

# Creates a list of the same size as the input_list
  #  numeros = [y+1 for y in range(len(resp))]
# Calls cleaning function   
  #  cleaned = cleaning(spellchecking)
# Calls dictionary creation function
  #  dictionary = dic_creation(numeros,cleaned,data2['tokenized_input'])
# Creates data3 and adds the spell_analysis to it
  #  data3 = data2
  #  data3['spell_analysis'] = dictionary

    #return data3


######################################################




###################################################

# This function creates the dictionary
# that encapsulates the results from spell check 

#def dic_creation(numeros,checked_list,data2):
#    dic = {}
    #This combines two lists into one dictionary
#    for (x,y,z) in zip(numeros,data2,checked_list):
 #       dic[x] = {y:z}
  #  return dic
    
