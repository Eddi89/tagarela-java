"""
---------------------------------------------------------------
$Id: lexUtils.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

import dbm
import cPickle

from os.path import join,dirname
lexfilename = join(dirname(__file__),'lexiconNilc')

#This is done in file corpus.py
#def createNewLex():
#    # initialize database
#    lexicon = dbm.open(lexfilename,'n')
#
#    # add one of those for each entry you are writing to the lexicon:
#    assignValue(lexicon,
#		'entry1', (1,'foo', {'a':2,'b':1}))
#
#    # close the cdatabase file:
#    lexicon.close()


def getEntry(key):
    lexicon = dbm.open(lexfilename)
    entry = getValue(lexicon,key)
    lexicon.close()
    return entry

def getEntries(keyList):
    lexicon = dbm.open(lexfilename)
    entryList = map(lambda key: getValue(lexicon,key), keyList)
    lexicon.close()
    return entryList

def putEntry(key,value):
    lexicon = dbm.open(lexfilename,'c')
    assignValue(lexicon,key,value)
    lexicon.close()

def putEntries(keyValList):
    lexicon = dbm.open(lexfilename,'c')
    map(lambda keyVal: assignValue(lexicon,keyVal[0],keyVal[1]), keyValList)
    lexicon.close()

# basic operations on lexicon (note: key and value must be strings for dbm):
def assignValue(lexicon,key,value):
    lexicon[key] = cPickle.dumps(value)

def getValue(lexicon,key):
    value = cPickle.loads(lexicon[key])
    return value
