# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: matchToken.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

def select(matched,cleanMatches):
  # find target elements in clean matches
  TaCm = []
  for item in cleanMatches:
    TaCm = TaCm + [item[1][1]]
  if len(matched) == 2:
    dif1 = abs(matched[0][1][0] - matched[0][1][1])
    dif2 = abs(matched[1][1][0] - matched[1][1][1])
    if dif1 == dif2:
      chosenByDis = matched[0]
    elif dif1 < dif2:
      chosenByDis =  matched[0]
    elif dif1 > dif2:
      chosenByDis = matched[1]
    if matched[0][1][1] in TaCm:
      chosenByRedun = matched[1]
    elif matched[1][1][1] in TaCm:
      chosenByRedun = matched[0]
    else:
      chosenByRedun = matched[0]
    if chosenByRedun == chosenByDis:
      return chosenByDis
    elif chosenByRedun != chosenByDis:
      return chosenByRedun
    else:
      return chosenByDis
  elif len(matched) == 3:
    return select([matched[0],select([matched[1],matched[2]],cleanMatches)],cleanMatches)
  elif len(matched) == 4:
    return select([matched[0],select([matched[1],select([matched[2],matched[3]],cleanMatches)],cleanMatches)],cleanMatches)
  elif len(matched) == 5:
    return select([matched[0],select([matched[1],select([matched[2],
                  select([matched[3],matched[4]],cleanMatches)],cleanMatches)],cleanMatches)],cleanMatches)
  else:
    return matched



def matching(listTst,listTan):
  matches = []
  cleanMatchesA = []
  cleanMatchesB = []
  matchedSTA = []
  matchedANA = []
  matchedSTB = []
  matchedANB = []
  notmatchedST = []
  notmatchedAN = []
  #matches
  for x in listTst:
    matchedA = []
    for y in listTan:
      if x[2] == y[2]:
        match = [(x[0],y[0]),[x[1],y[1]],x[2]]
        matchedA = matchedA + [match]
    if len(matchedA) == 1:
      cleanMatchesA = cleanMatchesA + matchedA
      matchedSTA = matchedSTA + [matchedA[0][1][0]]
      matchedANA = matchedANA + [matchedA[0][1][1]]
    elif len(matchedA) > 1:

      chosen = select(matchedA,cleanMatchesA)
      cleanMatchesA = cleanMatchesA + [chosen]
      matchedSTA = matchedSTA + [chosen[1][0]]
      matchedANA = matchedANA + [chosen[1][1]]
    else:
      cleanMatchesA = cleanMatchesA

  for w in listTan:
    matchedB = []
    for v in listTst:
      if w[2] == v[2]:
        match = [(v[0],w[0]),[v[1],w[1]],v[2]]
        matchedB = matchedB + [match]
    if len(matchedB) == 1:
      cleanMatchesB = cleanMatchesB + matchedB
      matchedSTB = matchedSTB + [matchedB[0][1][0]]
      matchedANB = matchedANB + [matchedB[0][1][1]]
    elif len(matchedB) > 1:

      chosen = select(matchedB,cleanMatchesB)
      cleanMatchesB = cleanMatchesB + [chosen]
      matchedSTB = matchedSTB + [chosen[1][0]]
      matchedANB = matchedANB + [chosen[1][1]]
    else:
      cleanMatchesB = cleanMatchesB

  if len(cleanMatchesB) < len(cleanMatchesA):
    for item in listTst:
      if item[1] not in matchedSTB:
        notmatchedST = notmatchedST + [item]
      else:
        notmatchedST = notmatchedST
    for item in listTan:
      if item[1] not in matchedANB:
        notmatchedAN = notmatchedAN + [item]
      else:
        notmatchedAN = notmatchedAN
    return {'token-matches':cleanMatchesB, 'token-ST-no-match':notmatchedST, 'token-AN-no-match':notmatchedAN}
  else:
    for item in listTst:
      if item[1] not in matchedSTA:
        notmatchedST = notmatchedST + [item]
      else:
        notmatchedST = notmatchedST
    for item in listTan:
      if item[1] not in matchedANA:
        notmatchedAN = notmatchedAN + [item]
      else:
        notmatchedAN = notmatchedAN
    return {'token-matches':cleanMatchesA, 'token-ST-no-match':notmatchedST, 'token-AN-no-match':notmatchedAN}



def makelist(listTokenizer):
  listtokens = []
  n = 0
  for element in listTokenizer:
    listtokens = listtokens + [[element,n,element]]
    n = n+1
  return listtokens
  


# def cleanMatches(tmatch):
#   originalList = tmatch['token-matches']
#   changeblelist = originalList[:]
#   repetitions = []
#   for element in changeblelist:
#     copylist = originalList[:]
#     copylist.remove(element)
#     for item in copylist:
#       if item[2] == element[2]:
#        changeblelist.remove(element)
#        repetitions = repetitions + [element]
#   #for x in repetition:
# 
#   print changeblelist
#   print repetitions
#     #print element
#   return 'ok'

def mainTokenMatch(tokensSt,tokensAn):
  listTst = makelist(tokensSt)
  listTan = makelist(tokensAn)
  tmatch = matching(listTst,listTan)
  #cleaned = cleanMatches(tmatch)
  return tmatch






#### Testing #####

# tokensSt = [['ele', [{'pos': '0'}]], ['chama', [{'pos': '0'}]], ['paulo', [{'pos': '0'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# tokensAn = [['ele', [{'pos': '0'}]], ['se', [{'pos': '0'}]], ['chama', [{'pos': '0'}]], ['paulo', [{'pos': '0'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# 
# #O menino gosta do sorvete (2x)
# tokensSt1 = [['o', [{'pos': '0'}]], ['menino', [{'pos': '0'}]], ['gosta', [{'pos': '0'}]], ['de', [{'source': 'tokn', 'canonic': 'de', 'frequency': '35', 'pos': 'prep'}]], ['o', [{'gender': 'm', 'spos': 'de', 'pos': 'det', 'number': 'si', 'source': 'tokn', 'canonic': 'o', 'num': 'si', 'frequency': '35', 'gender rule': '1', 'number rule': '11', 'gen': 'm'}]], ['sorvete', [{'pos': '0'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# tokensAn1 = [['o', [{'pos': '0'}]], ['menino', [{'pos': '0'}]], ['gosta', [{'pos': '0'}]], ['de', [{'source': 'tokn', 'canonic': 'de', 'frequency': '35', 'pos': 'prep'}]], ['o', [{'gender': 'm', 'spos': 'de', 'pos': 'det', 'number': 'si', 'source': 'tokn', 'canonic': 'o', 'num': 'si', 'frequency': '35', 'gender rule': '1', 'number rule': '11', 'gen': 'm'}]], ['sorvete', [{'pos': '0'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# 
# # Ele vê ele dentro dele.
# tokensSt2 = [['ele', [{'pos': '0'}]], ['quase', [{'pos': '0'}]], ['nunca', [{'pos': '0'}]], ['v\xc3\xaa', [{'pos': '0'}]], ['ele', [{'pos': '0'}]], ['dentro', [{'pos': '0'}]], ['de', [{'source': 'tokn', 'canonic': 'de', 'frequency': '35', 'pos': 'prep'}]], ['ele', [{'person': '3s', 'gender': 'm', 'spos': 'obl', 'pos': 'pronoun', 'number': 'si', 'source': 'tokn', 'canonic': 'ele', 'num': 'si', 'frequency': '35', 'gender rule': '2', 'number rule': '3', 'gen': 'm', 'contraction': ''}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# tokensAn2 = [['ele', [{'pos': '0'}]], ['v\xc3\xaa', [{'pos': '0'}]], ['ele', [{'pos': '0'}]], ['dentro', [{'pos': '0'}]], ['de', [{'source': 'tokn', 'canonic': 'de', 'frequency': '35', 'pos': 'prep'}]], ['ele', [{'person': '3s', 'gender': 'm', 'spos': 'obl', 'pos': 'pronoun', 'number': 'si', 'source': 'tokn', 'canonic': 'ele', 'num': 'si', 'frequency': '35', 'gender rule': '2', 'number rule': '3', 'gen': 'm', 'contraction': ''}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# 
# tokensSt3 = [['o', [{'pos': '0'}]], ['menino', [{'pos': '0'}]], ['o', [{'pos': '0'}]], ['pai', [{'pos': '0'}]], ['o', [{'pos': '0'}]], ['carro', [{'pos': '0'}]], ['o', [{'pos': '0'}]], ['cara', [{'pos': '0'}]], ['o', [{'pos': '0'}]], ['homem', [{'pos': '0'}]]]
# tokensAn3 = [['o', [{'pos': '0'}]], ['o', [{'pos': '0'}]], ['cara', [{'pos': '0'}]], ['pai', [{'pos': '0'}]], ['o', [{'pos': '0'}]], ['homem', [{'pos': '0'}]], ['foi', [{'pos': '0'}]], ['o', [{'pos': '0'}]]]
# 

tokensSt4 = ['o', 'jo\xc3\xa3o','a', 'mora','a', 'em', 'o', 'rio', 'de', 'janeiro', '.']

tokensAn4 = ['jo\xc3\xa3o','a', 'mora', 'em', 'o', 'rio', 'de', 'a', 'janeiro', '.']

def agr_test():
    print mainTokenMatch(tokensSt4,tokensAn4)


if __name__ == "__main__":
  agr_test()


##### OLD COLD ####

#    for item in matches:
#           print item[1][0]
#           if x[1] == item[1][0]:
#             selected = select(match,item)
#           elif y[1] == item[1][1]:
#             selected = select(match,item)
#           else:
#             selected = match
#         #cleanMatches = cleanMatches + [selected]
#       matches = matches + [match]
#       matchesST = matchesST + [x[1]]
#       matchesAN = matchesAN + [y[1]]
#   #cleanMatches =
#   #print cleanMatches
#   noMatchST = []
#   noMatchAN = []
#   for x in listTst:
#     if x[1] not in matchesST:
#       noMatchST = noMatchST + [x]
#   for y in listTan:
#     if y[1] not in matchesAN:
#       noMatchAN = noMatchAN + [y]
#   # print 'ST: ',noMatchST
# #   print 'AN: ',noMatchAN




