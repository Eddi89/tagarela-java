# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: matchCanonic.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

## Input:  the output of the disambiguator for both ST and AN inputs
## Output: ?


def select(matched,cleanMatches):
  # find target elements in clean matches
  TaCm = []
  for item in cleanMatches:
    TaCm = TaCm + [item[1][1]]
  if len(matched) == 2:
    dif1 = abs(matched[0][1][0] - matched[0][1][1])
    dif2 = abs(matched[1][1][0] - matched[1][1][1])
    if dif1 == dif2:
      chosenByDis = matched[0]
    elif dif1 < dif2:
      chosenByDis =  matched[0]
    elif dif1 > dif2:
      chosenByDis = matched[1]
    if matched[0][1][1] in TaCm:
      chosenByRedun = matched[1]
    elif matched[1][1][1] in TaCm:
      chosenByRedun = matched[0]
    else:
      chosenByRedun = matched[0]
    if chosenByRedun == chosenByDis:
      return chosenByDis
    elif chosenByRedun != chosenByDis:
      return chosenByRedun
    else:
      return chosenByDis
  elif len(matched) == 3:
    return select([matched[0],select([matched[1],matched[2]],cleanMatches)],cleanMatches)
  elif len(matched) == 4:
    return select([matched[0],select([matched[1],select([matched[2],matched[3]],cleanMatches)],cleanMatches)],cleanMatches)
  elif len(matched) == 5:
    return select([matched[0],select([matched[1],select([matched[2],
                  select([matched[3],matched[4]],cleanMatches)],cleanMatches)],cleanMatches)],cleanMatches)
  else:
    return matched


def matching(listTst,listTan):
  matches = []
  cleanMatches = []
  matchedST = []
  matchedAN = []
  notmatchedST = []
  notmatchedAN = []
  #matches
  for x in listTst:
    matched = []
    for y in listTan:
      if x[2] == y[2]:
        match = [(x[0],y[0]),[x[1],y[1]],x[2]]
        matched = matched + [match]
 # selectedMatch = select(matches)
    if len(matched) == 1:
      cleanMatches = cleanMatches + matched
      matchedST = matchedST + [matched[0][1][0]]
      matchedAN = matchedAN + [matched[0][1][1]]
    elif len(matched) > 1:
      chosen = select(matched,cleanMatches)
      cleanMatches = cleanMatches + [chosen]
      matchedST = matchedST + [chosen[1][0]]
      matchedAN = matchedAN + [chosen[1][1]]
    else:
      cleanMatches = cleanMatches
  #print matchedST, matchedAN

  for item in listTst:
    if item[1] not in matchedST:
      notmatchedST = notmatchedST + [item]
    else:
      notmatchedST = notmatchedST
  for item in listTan:
    if item[1] not in matchedAN:
      notmatchedAN = notmatchedAN + [item]
    else:
      notmatchedAN = notmatchedAN
  return {'canonic-matches':cleanMatches, 'canonic-ST-no-match':notmatchedST, 'canonic-AN-no-match':notmatchedAN}


def mainCanonicMatch(disambSt,disambAn,tokenMatched):
  listTst = []
  listTan = []
  for y in tokenMatched['token-ST-no-match']:
    try:
      listTst = listTst + [[y[0],y[1], disambSt[y[1]][1][0]['canonic']]]
    except:
      listTst = listTst + [[y[0],y[1],y[0]]]
  for x in tokenMatched['token-AN-no-match']:
    try:
      listTan = listTan + [[x[0],x[1], disambAn[x[1]][1][0]['canonic']]]
    except:
      listTan = listTan + [[x[0],x[1],x[0]]]
  Cmatch = matching(listTst,listTan)
  return Cmatch


#### Testing #####
#
# tokPOSst = [['ele', [{'source': 'lexicon', 'gender': 'm', 'spos': 'obl-to;ret;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'ele', 'frequency': '35', 'number rule': '3', 'gender rule': '2', 'contraction': ''}]], ['chama', [{'form': 'imper-afirm;2s;pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'chamar', 'frequency': '31'}]], ['paulo', [{'gender': 'm', 'number': 'si', 'pos': 'propNoun', 'source': 'lexicon', 'canonic': 'paulo', 'frequency': '35', 'capitalize': 'li'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# tokPOSan = [['ele', [{'source': 'lexicon', 'gender': 'm', 'spos': 'obl-to;ret;', 'pos': 'pronoun', 'number': 'si', 'person': '3s', 'canonic': 'ele', 'frequency': '35', 'number rule': '3', 'gender rule': '2', 'contraction': ''}]], ['se', [{'source': 'lexicon', 'gender': 'mf', 'spos': 'obl-at;refl;', 'number': 'sp', 'pos': 'pronoun', 'person': '3s', 'canonic': 'se', 'frequency': '35', 'number rule': '?', 'gender rule': '?', 'contraction': ''}]], ['chama', [{'form': 'imper-afirm;2s;pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'chamar', 'frequency': '31'}]], ['paulo', [{'gender': 'm', 'number': 'si', 'pos': 'propNoun', 'source': 'lexicon', 'canonic': 'paulo', 'frequency': '35', 'capitalize': 'li'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# 
# tokPOSst1 = [['eles', [{'source': 'lexicon', 'gender': 'm', 'spos': 'obl-to;ret;', 'pos': 'pronoun', 'number': 'pl', 'person': '3p', 'canonic': 'ele', 'frequency': '34', 'number rule': '3', 'gender rule': '2', 'contraction': ''}]], ['chamam', [{'form': 'pres;3p;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'chamar', 'frequency': '29'}]], ['brasileiros', [{'degree': 'n', 'gender': 'm', 'number': 'pl', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'brasileiro', 'frequency': '32', 'number rule': '1', 'gender rule': '?'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# tokPOSan = [['eles', [{'source': 'lexicon', 'gender': 'm', 'spos': 'obl-to;ret;', 'pos': 'pronoun', 'number': 'pl', 'person': '3p', 'canonic': 'ele', 'frequency': '34', 'number rule': '3', 'gender rule': '2', 'contraction': ''}]], ['se', [{'source': 'lexicon', 'gender': 'mf', 'spos': 'obl-at;refl;', 'number': 'sp', 'pos': 'pronoun', 'person': '3s', 'canonic': 'se', 'frequency': '35', 'number rule': '?', 'gender rule': '?', 'contraction': ''}]], ['chamam', [{'form': 'pres;3p;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'chamar', 'frequency': '29'}]], ['brasileiros', [{'degree': 'n', 'gender': 'm', 'number': 'pl', 'pos': 'adj', 'regency': '', 'source': 'lexicon', 'canonic': 'brasileiro', 'frequency': '32', 'number rule': '1', 'gender rule': '?'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]

# tokPOSst2 = [['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 'gender rule': '1'}]], ['nome', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'nome', 'frequency': '34', 'number rule': '2', 'gender rule': '?'}]]]
# tokPOSan2 = [['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 'gender rule': '1'}]], ['nome', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'nome', 'frequency': '34', 'number rule': '2', 'gender rule': '?'}]], ['de', [{'source': 'tokn', 'canonic': 'de', 'frequency': '35', 'pos': 'prep'}]], ['ele', [{'person': '3s', 'gender': 'm', 'spos': 'obl', 'pos': 'pronoun', 'number': 'si', 'source': 'tokn', 'canonic': 'ele', 'num': 'si', 'frequency': '35', 'gender rule': '2', 'number rule': '3', 'gen': 'm', 'contraction': ''}]], ['\xc3\xa9', [{'form': 'pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'ser', 'frequency': '35'}]], ['paulo', [{'gender': 'm', 'number': 'si', 'pos': 'propNoun', 'source': 'lexicon', 'canonic': 'paulo', 'frequency': '35', 'capitalize': 'li'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# tokenMatched = {'token-matches': [[('o', 'o'), [0, 0], 'o'], [('nome', 'nome'), [1, 1], 'nome']], 
#                 'token-AN-no-match': [['de', 2, 'de'], ['ele', 3, 'ele'], ['\xc3\xa9', 4, '\xc3\xa9'], ['paulo', 5, 'paulo'], ['.', 6, '.']], 
#                 'token-ST-no-match': []}
# 
# tokPOSst3 = [['a', [{'gender': 'f', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '?', 'gender rule': '?'}]], ['seu', [{'source': 'lexicon', 'gender': 'm', 'spos': 'poss;', 'number': 'si', 'pos': 'poss', 'person': '3s', 'canonic': 'seu', 'frequency': '35', 'number rule': '3', 'gender rule': '?', 'contraction': ''}]], ['nome', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'nome', 'frequency': '34', 'number rule': '2', 'gender rule': '?'}]]]
# tokPOSan3 = [['o', [{'gender': 'm', 'spos': 'de', 'number': 'si', 'pos': 'det', 'source': 'lexicon', 'canonic': 'o', 'frequency': '35', 'number rule': '11', 'gender rule': '1'}]], ['nome', [{'degree': 'n', 'gender': 'm', 'number': 'si', 'pos': 'noun', 'regency': '', 'source': 'lexicon', 'canonic': 'nome', 'frequency': '34', 'number rule': '2', 'gender rule': '?'}]], ['de', [{'source': 'tokn', 'canonic': 'de', 'frequency': '35', 'pos': 'prep'}]], ['ele', [{'person': '3s', 'gender': 'm', 'spos': 'obl', 'pos': 'pronoun', 'number': 'si', 'source': 'tokn', 'canonic': 'ele', 'num': 'si', 'frequency': '35', 'gender rule': '2', 'number rule': '3', 'gen': 'm', 'contraction': ''}]], ['\xc3\xa9', [{'form': 'pres;3s;', 'valency': '', 'pos': 'verb', 'regency': '', 'source': 'lexicon', 'canonic': 'ser', 'frequency': '35'}]], ['paulo', [{'gender': 'm', 'number': 'si', 'pos': 'propNoun', 'source': 'lexicon', 'canonic': 'paulo', 'frequency': '35', 'capitalize': 'li'}]], ['.', [{'source': 'tokn', 'pos': 'punct'}]]]
# tokenMatched1 = {'token-matches': [[('nome', 'nome'), [2, 1], 'nome']], 'token-AN-no-match': [['o', 0, 'o'], ['de', 2, 'de'], ['ele', 3, 'ele'], ['\xc3\xa9', 4, '\xc3\xa9'], ['paulo', 5, 'paulo'], ['.', 6, '.']], 'token-ST-no-match': [['a', 0, 'a'], ['seu', 1, 'seu']]}
# 
# #
# def agr_test():
#     print mainCanonicMatch(tokPOSst3,tokPOSan3,tokenMatched1)
# 
# 
# if __name__ == "__main__":
#   agr_test()

### OLD CODE

# def makelist(disambOutput):
#  listtokens = []
#  n = 0
#  for item in disambOutput:
#    if 'canonic' in item[1][0]:
#      listtokens = listtokens + [[item[0],n,item[1][0]['canonic']]]
#    else:
#      listtokens = listtokens + [[item[0],n,item[0]]]
#    n = n+1
#  return listtokens
