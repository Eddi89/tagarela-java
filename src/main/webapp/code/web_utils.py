"""
---------------------------------------------------------------
$Id: web_utils.py,v 1.5 2008-07-31 14:06:32 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

from os.path import join,dirname,abspath

#############################################################
#  SESSION HANDLING
#############################################################

from mod_python import Session
from mod_python import util # for util.redirect

def get_session(req):
    """Get a session only if we didn't get one yet for this req object.
       This avoids locking, which otherwise would happen if we (accidentally )
       tried to create a second session for the same req object.
       cf. http://www.modpython.org/pipermail/mod_python/2006-March/020560.html
    """
    if not hasattr(req,"session"):
        req.session = Session.Session(req)
	#if req.session.is_new():
	#    log(req,'NEWNEWNEW session ID: %s ',req.session.id())
	#else:
	#    log(req,'OLD session ID: %s, CONTAINS:%s'%(req.session.id(),req.session))
	#    log(req,'user model: %s'%usermodel_get(req.session,'current'))
    return req.session


def ensure_authorized_session(req):
    """Gets a session and checks if it's been authorized by login.
       If not, redirects to main tagarela page"""
    
    sess = get_session(req)
    if sess.is_new() or not(sess.has_key('authorized_id')):
        if sess.has_key('tagarela_index_url'):
            tagarela_index_url = sess['tagarela_index_url']+"/"
        else:
            # IMPORTANT: This fallback only works if called from
            # a subdirectory of the main index handler directory.
            tagarela_index_url = '../index.py/'

        util.redirect(req,join(tagarela_index_url,
                               'main?msg=Please log in (again)!'))
    return sess



#############################################################
#  USER LOGIN AND USERMODEL
#############################################################

try:
    from bsddb3 import btopen
except ImportError:
    from bsddb import btopen

import cPickle as pickle # for serializing non-strings for the user_model
#import json
import sys
path = dirname(__file__)

# need to adjust module path for simplejson
if not path in sys.path:
    sys.path.append(dirname(__file__))
    
# total madness: due to unicode issues, we have to use one json
# implementation for reading and one for writing, ARGH!
import simplejson as writejson
import json as readjson

# the individual user databases:
def init_usermodel_db(localFile,id):
    # create/access the individual user's record database:
    usermodel_filename = join(dirname(localFile),
                               'databases/%s.bsddb'%id)

    #apache.log_error('XXX Filename to be opened is %s'%usermodel_filename)
    usermodel_db = btopen(usermodel_filename)

    # store the id in the database, using pickle as in general usermodel_put
    usermodel_db['id'] = writejson.dumps(id) 
    usermodel_db.close()
    return usermodel_filename

# syntactic sugar: put in session AND usermodel:
#def sess_usermodel_put(sess,key,value):
#    sess[key] = value
#   sess.save()
#    return usermodel_put(sess,key,value)

def usermodel_put(sess,key,value):
    if sess.has_key('usermodel_filename'):
        user_model_db = btopen(sess['usermodel_filename'])
        user_model_db[key] = writejson.dumps(value)
        user_model_db.close()
    else:
        value = ''
    return value

def usermodel_get(sess,key,default=''):
    if sess.has_key('usermodel_filename'):
        try:
	    user_model_db = btopen(sess['usermodel_filename'])
            value = readjson.read(user_model_db[key])
	    user_model_db.close()
        except:
            value = default
    else:
        value = default
    return value

# get the entire user model
def usermodel_getall(sess,default=''):
    um = {}
    if sess.has_key('usermodel_filename'):
        try:
	    user_model_db = btopen(sess['usermodel_filename'])
	    for (key,pickledVal) in user_model_db.iteritems():
		um[key] =  readjson.read(pickledVal)
	    user_model_db.close()
        except:
            user_model_db = {}
    else:
        value = default
    return um


#############################################################
# LOGGING
#############################################################

from mod_python import apache # for APLOG_INFO

    # IMPORTANT: We write our information at the APLOG_INFO level, 
    # so LogLevel in the httpd.conf must be set to debug or info 
    # to see the output in the log file!

from time import localtime, strftime


# Note:
# In cases where you don't have access to the req object, you can use
# apache.log_error('MESSAGE TO REPORT')

# Logging to the apache error log at level [info]:
def log(req,*argsToLog):
    argsAsStr = ''
    for arg in argsToLog:
	argsAsStr += _arg2str(arg)
    req.log_error('%s#%s'%(_log_prefix(req),
			   argsAsStr),
		  apache.APLOG_INFO)

# define the prefix we want to use for all 
def _log_prefix(req):
    prefix = ['#TAGARELA#']
    # store the current, local time:
    prefix.append({'now':strftime("%Y/%m/%d-%H:%M:%S",localtime())})
    # store the remote name of the machine making the request:
    prefix.append({'machine':req.get_remote_host()})
    # store which installation of tagarela we are using:
    prefix.append({'version':abspath(join(dirname(__file__),'..'))})
    # store authorized_id if exists, else authenticated_id if exists:
    if hasattr(req,"session") and req.session.has_key('authorized_id'):
        prefix.append({'authorized_id': req.session['authorized_id']})
    elif hasattr(req,"session") and req.session.has_key('authenticated_id'):
        prefix.append({'authenticated_id': req.session['authenticated_id']})
    return _arg2str(prefix)

# TODO: FOR DEBUGGING ONLY, REPLACED BY SIMPLE MATCHING TO STRINGS:
def _arg2str(arg):
    return str(arg)

def _realarg2str(arg):
    if isinstance(arg,list):	# turn list into string
	mystr = ''
	for element in arg:
	    try:
		(key,value) = element
		mystr += "%s:%s#"%(key,value)
	    except:
		mystr += str(element)
    elif isinstance(arg,dict):	# turn dictionary into string
	mystr = ''

	# treat some values separately:
	# 'actType' should appear first and be typeset specially:
	if 'actType' in arg: mystr += arg.pop('actType')+"("
	# the next three should be next, in this order:
	if 'modNr' in arg: mystr += "%s:%s#"%('modNr',arg.pop('modNr'))
	if 'exNr' in arg: mystr += "%s:%s#"%('exNr',arg.pop('exNr'))
	if 'questNr' in arg: mystr += "%s:%s#"%('questNr',arg.pop('questNr'))
	# handle other, i.e., normal key:value pairs:
	for key,value in arg.iteritems():
	    mystr += "%s:%s#"%(key,value)
    else:			# turn other data into string
	mystr = str(arg)
    return mystr

# For logging and specifying the 'current' affairs in the user model, it's 
# convenient to pass locals(), but we don't want 'req' or 'sess', and ajaxstuff
# so this little function can be used as wrapper around locals() to remove them.
#
# The function also removes keys with null values (not entirely clear this
# is the best thing to do since it loses the distinction between 
# key = '' and key undefined, but it results in more compact representations)

def plainDict(big):
    small = {}
    for key,value in big.iteritems():
	if value and (key not in ['sess','req','ajaxArg','ajaxAct','timeStamp']):
	    small[key] = value
    return small


#############################################################

# Code for turning psp template into html string, based on
# http://www.modpython.org/pipermail/mod_python/2006-June/021292.html

from mod_python import _psp

import os

# like lowercase version but resolves relative filename just like PSP in psp.py
def PSP2HTML(req,filename=None,vars={}):
    # if filename is not absolute, default to our guess
    # of current directory
    if not os.path.isabs(filename):
	base = os.path.split(req.filename)[0]
	filename = os.path.join(base, filename)
    return psp2html(filename,vars)


def psp2html(filename=None,vars={},string=None):
    if (string and filename):
        raise ValueError, "Must specify either filename or string"

    req = FakeRequestObject()
    vars['req'] = req

    if filename:
        source = _psp.parse(filename)
        code = compile(source, filename, 'exec')
    else:
        source = _psp.parsestring(string)
        code = compile(source, '<string>', 'exec')
    global_scope = globals().copy()
    global_scope.update(vars)
    exec code in global_scope
    return req.html()

class FakeRequestObject(object):
     def __init__(self):
          self.htmlList = []

     def write(self, value, length):
         self.htmlList.append(value)

     def html(self):
	 return "".join(self.htmlList)

