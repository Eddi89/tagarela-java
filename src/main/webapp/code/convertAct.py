# -*- coding: utf-8 -*-

import xml.dom.minidom
import os
import glob
import re

def includeAV(dom):
    if os.path.isfile("audio.mp3"):
        text = dom.createTextNode("audio.mp3")
        audio = dom.createElement("audio")
        filename = dom.createElement("filename")
        filename.appendChild(text)
        audio.appendChild(filename)
        dom.childNodes[0].childNodes[0].appendChild(audio)
    if os.path.isfile("image.tmpl"):
        p = re.compile("src=\"(.+?)\" alt=\"(.+?)\"", re.UNICODE)
        m = p.search("".join(open("image.tmpl").readlines()))
        filename = dom.createElement("filename")
        alt = dom.createElement("alt")
        filename.appendChild(dom.createTextNode("../../" + m.group(1)))
        alt.appendChild(dom.createTextNode(m.group(2)))
        image = dom.createElement("image")
        image.appendChild(filename)
        image.appendChild(alt)
        dom.childNodes[0].childNodes[0].appendChild(image)
    else:
        jpglist = glob.glob("*.jpg")
        for jpg in jpglist:
            text = dom.createTextNode(jpg)
            filename = dom.createElement("filename")
            alt = dom.createElement("alt")
            image = dom.createElement("image")
            filename.appendChild(text)
            image.appendChild(filename)
            image.appendChild(alt)
            dom.childNodes[0].childNodes[0].appendChild(image)
        
def includeInstr(dom):
    en = open("instr-en.txt")
    pt = open("instr-pt.txt")
    enstr = "".join(en.readlines()).decode("iso-8859-1")
    ptstr = "".join(pt.readlines()).decode("iso-8859-1")
    instructions = dom.createElement("instructions")
    l1 = dom.createElement("L1")
    l1text = dom.createTextNode(enstr)
    l1.appendChild(l1text)
    l2 = dom.createElement("L2")
    l2text = dom.createTextNode(ptstr)
    l2.appendChild(l2text)
    instructions.appendChild(l1)
    instructions.appendChild(l2)
    dom.childNodes[0].childNodes[0].appendChild(instructions)

def includeProcMode(dom):
    act1 = xml.dom.minidom.parse("questions/1act.xml")
    procMode = act1.getElementsByTagName("procMode")[0]
    imported = dom.importNode(procMode,True)
    dom.childNodes[0].appendChild(imported)

def includeQuestions(dom):
    howmany = len(glob.glob("questions/*.txt"))
    for x in range(howmany):
        question = dom.createElement("question")
        ptstr = "".join(open("questions/" + str(x+1) + ".txt").readlines()).decode("iso-8859-1")
        string = dom.createElement("string")
        string.appendChild(dom.createTextNode(ptstr))
        question.appendChild(string)
        actx = xml.dom.minidom.parse("questions/" + str(x+1) + "act.xml")
        targetlists = actx.getElementsByTagName("targetList")
        if len(targetlists) > 1:
            targetlist = dom.createElement("targetList")
            finalstring = ""
            for tl in targetlists:
                finalstring += tl.getElementsByTagName("string")[0].childNodes[0].nodeValue + " "
            finalstring = finalstring.strip()
            target = dom.createElement("target")
            tstring = dom.createElement("string")
            ttext = dom.createTextNode(finalstring)
            tstring.appendChild(ttext)
            reqWords = dom.createElement("reqWords")
            target.appendChild(tstring)
            target.appendChild(reqWords)
            targetlist.appendChild(target)
        else:
            targetlist = targetlists[0]
        imported = dom.importNode(targetlist,True)
        question.appendChild(imported)
        dom.childNodes[0].appendChild(question)
    
def main():
    document = xml.dom.minidom.Document()
    activity = document.createElement("activity")
    activity.appendChild(document.createElement("presentation"))
    document.appendChild(activity)
    includeAV(document)
    includeInstr(document)
    includeProcMode(document)
    includeQuestions(document)
    print document.toxml(encoding = "UTF-8")
        
if __name__ == "__main__":
    main()