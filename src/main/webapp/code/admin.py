#!/usr/bin/env python

"""
---------------------------------------------------------------
$Id: admin.py,v 1.2 2007-11-24 06:22:28 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

# name of log to be used
logName = "/opt/local/apache2/logs/error_log"

# the installed version of tagarela we want to report information about:
installedVer = "/opt/local/apache2/htdocs/tagarela"

import re

def prettyLog(user=''):
    result = '<h3>Tagarela log output'
    if user: result +=  "for user %s"%user
    result +=  "</h3><ol>"

    lineStart = re.compile(r"^.*#TAGARELA#',")
    lineEnd = re.compile(r", referer:.*$")
    feedbackExpr = re.compile(r"'feedback': '(.*)', 'questNr'")

    logLines = ''  
    filehandle = open(logName,'r')
    line = filehandle.readline()
    while line:
	if line.find("#TAGARELA#") != -1 and line.find(installedVer) != -1:
	    line = line.replace(r'\\','\\')
	    line = lineStart.sub("[",line)
	    line = lineEnd.sub("",line)
	    [headerStr,infoStr] = line.split('#',1)
	    # turn header list with dictionary elements into single dictionary
	    h = reduce(updateReturn,eval(headerStr))
	    
	    if not(user) or h.get('authorized_id','').find(user) != -1:
		userinfo = h.get('authorized_id','NOT LOGGED IN')
		headOut = "(%s, %s, %s)"%(userinfo,
							 h.get('now'),
							 shortMachine(h))
		if infoStr.find("'feedback':") != -1:
		    # triple quotes around feedback to avoid eval problems
	    	    infoStr = feedbackExpr.sub(r'"feedback":"""\1""", "questNr"',infoStr)
		    infoStr = infoStr.replace(r'\\n','').replace('<p>','<br>').replace('</p>','')
		    info = eval(infoStr)
		    actInfo = "%s %s %s %s"%(info.get('actType'),
					     info.get('modNr'),
					     info.get('exNr'),
					     info.get('questNr'))
		    logLines +=  "<li><big><code>%s %s</code></big><p> %s </li>"%(actInfo,headOut,info.get('feedback'))
	line = filehandle.readline()

    if not logLines:
	logLines = "No log entries found"
    result +=  logLines
    result +=  "</ol></body></html>"
    filehandle.close()

    return result


def shortMachine(h):
    return h.get('machine').replace('.ling.ohio-state.edu','').replace('ohio-state.edu','')

# like update but returns the updated dictionary
def updateReturn(x,y):
    x.update(y)
    return x


if __name__ == '__main__':
    import sys
    if len(sys.argv)==2:
	print prettyLog(sys.argv[1])
    else:
	print prettyLog()
