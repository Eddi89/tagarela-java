# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: tagarela_utils.py,v 1.5 2008-07-31 14:06:32 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

# define utility functions used throughout the tagarela site

from mod_python import psp
from os.path import join,dirname,abspath

#############################################################

from version import tagarelaVersion

#############################################################
# Include the general utilities for handling sessions, logins,
# and logging to the server log that we're defining in 
# web_utils.py into this module:

from web_utils import *

#############################################################
# SPECIFICATION OF ACTIVITY PAGES
#############################################################

import os # for os.listdir
import re # for regular expression substitution
import sys # for sys.stderr

def getval(dict,key,default=''):
    if key in dict:
	return dict[key]
    else:
	return default

def file2string(file):
    """Turn a (multi-line) file into a string.  Very similar
       to readlines() method, but we return one long string
       instead of a list of strings, one per line"""
    try:
       f = open(file)
       line = ''
       for newline in f:
           line = line + newline
    except:
       line = ''
    return line


# create index of modules based on what Module directories exist
def get_mod_index(localfile,currentDir,exNr):
    motherDir = dirname(localfile)
    dirString = ''
    for dir in sorted(os.listdir(motherDir)):
        if dir.startswith('Module'):	    # use only dirs starting with Module
            dir = re.sub('Module', '', dir) # strip off Module prefix
            if dir == currentDir:
                dirString = dirString + "\n" +  dir
            else:
                dirString = dirString + "\n" + '<a href="?modNr=%s&amp;exNr=1">%s</a> '%(dir,dir)
    return 'Módulos: ' + dirString


# create index of exercises based on what exercise directories exist
def get_ex_index(localfile,currentDir,modNr):
    motherDir = join(dirname(localfile), 'Module%s'%modNr)
    dirString = ''
    for dir in sorted(os.listdir(motherDir)):
	if not dir in ["CVS",".svn"]:	# use all directories except for the CVS dir
	    lastExNr = dir
	    if dir == currentDir:
		dirString = dirString + "\n" +  dir
	    else:
		dirString = dirString + "\n" + '<a href="?modNr=%s&amp;exNr=%s">%s</a> '%(modNr,dir,dir)
    return ('Atividades: ' + dirString,lastExNr)
        

# create index of questions based on lastQuestNr (passed in from activity model)
def get_question_index(localfile,questNr,lastQuestNr,modNr,exNr):
    qStr = ""
    for q in range(1,lastQuestNr+1):
	if q != int(questNr): # if not current directory, make it clickable
	    qStr += "\n" + '<a href="#" onclick=\'postJSON(\"?ajaxAct=updateQuestion&amp;modNr=%s&amp;exNr=%s&amp;questNr=%s\",\"\"); return false;\'>%s</a> '%(modNr,exNr,q,q)
	else:
	    qStr += "\n" + str(q)
    return "Questões: "+ qStr


# create the "next" button (either next question or next activity, depending on what's currently being shown)
def  nextQuestButton(req,modNr,exNr,questNr,lastQuestNr,lastExNr):
    if int(questNr) == lastQuestNr:
	if exNr == lastExNr:
	    nextQuestBtnTmpl = '' 
	else:
	    exNr = int(exNr) + 1
	    nextQuestNr = 1
	    nextQuestBtnTmpl = PSP2HTML(req, filename='../include/nextActButton.tmpl',
	        vars = {'modNr': modNr,
		        'exNr': exNr,
                        'questNr': questNr,
                        'nextActNr': exNr,
                        'nextQuestNr': nextQuestNr})
    else:
        nextQuestNr = int(questNr)+1
	nextQuestBtnTmpl = PSP2HTML(req, filename='../include/nextQuestButton.tmpl',
                          vars = {'modNr': modNr,
                                  'exNr': exNr,
                                  'questNr': questNr,
                                  'nextQuestNr': nextQuestNr})
    return nextQuestBtnTmpl


# get template for the top of the page
def getTopTmpl(req,modNr,exNr,localfile,exType,instr):
    # create horizontal navigation bar object
    horizNavBarTmpl = psp.PSP(req, filename='../include/horizNavBar.tmpl',
                              vars = {'modNr': modNr})

    instructionsTmpl = PSP2HTML(req, filename='../include/instructions.tmpl',
			       vars = {'InstructionsEngl' : instr['L1'],
                           'InstructionsPort' : instr['L2']
                             })

    (exIndex,lastExNr) = get_ex_index(localfile,exNr,modNr)

    topTmpl = psp.PSP(req, filename='../include/top.tmpl',
                   vars = {'HorizNavBar' : horizNavBarTmpl,
                           'ModuleIndex' : get_mod_index(localfile,modNr,exNr),
                            'ExerciseIndex' : exIndex,
                           'ExerciseTypeName': exType,
                           'Instructions' : instructionsTmpl,
			   'TagarelaVersion' : tagarelaVersion
                           })
    return (topTmpl,lastExNr)


# get template for part of the page showing the questions
def getQuestTmpl(req,actSpec,actType,modNr,exNr,lastExNr,questNr,lastQuestNr,localfile,inputfield=''):
    sess = req.session

    # create exercise index 
    questionIndex = get_question_index(localfile,questNr,lastQuestNr,modNr,exNr)

    # if not last question, create next question button
    nextQuestBtnTmpl = nextQuestButton(req,modNr,exNr,questNr,lastQuestNr,lastExNr)

    # get the question
    questionText = '<div id="questionText">'\
        + actSpec['question'][int(questNr)-1]['string']\
        + '</div>'

    # if inputfield was specified, include an input field with id "learnerInput"
    if inputfield:
	questionInput = '<input id="learnerInput" type="text"/>'
    else:
	questionInput = ''

    # encode on web page what activity, module, exercise and question we are at
    actKey = '%s-%s-%s-%s'%(actType,modNr,exNr,questNr)
    actVal = usermodel_get(sess,actKey,{})
    # retrieve the user id from the model
    userID = usermodel_get(sess,'id',)
    storedValues = ""
    for (key,value) in actVal.iteritems():
	#log(req,"here:%s is:%s"%(key,value))
	storedValues += '<div id="stored%s" style="position:absolute; left:-1000px;">%s</div>'%(key,value)

    # create accented characters template
    specialCharsTmpl = PSP2HTML(req, filename='../include/specialChars.tmpl')

    #req.log_error('activity model for module %s, exercise %s, question %s from %s is %s'%(modNr,exNr,questNr,localfile,actModel),apache.APLOG_ERR)
    #storedActModel = usermodel_get(req.session,'actModel')
    #req.log_error('Retrieved activity model for module %s, exercise %s, question %s from %s is %s'%(modNr,exNr,questNr,localfile,storedActModel),apache.APLOG_ERR)

    # create question template object
    questionTmpl = PSP2HTML(req, filename='../include/question.tmpl',
                          vars = {'storedValues':storedValues,
				  'actType' : actType,
				  'modNr':modNr,
				  'exNr':exNr,
				  'questNr': questNr,
                  'userID': userID,
				  'questionText': questionText,
				  'questionInput' : questionInput,
				  'questionIndex': questionIndex,
				  'specialChars' : specialCharsTmpl,
				  'nextQuestBtn' : nextQuestBtnTmpl} )

    return questionTmpl



import xml2dict

# read in activity specification
def getActSpec(modNr,exNr,localfile):
    actModelFilename = join(dirname(localfile),
                            'Module%s/%s/act.xml'%(modNr,exNr))
    try: 
        actSpec = xml2dict.readConfig(actModelFilename,{'modNr':modNr,'exNr':exNr})['activity']

	   # minimal well-formedness checking (more should be added here):
	   # a) if no question list was provided, set it to empty list here:
        if not isinstance(actSpec['question'],list):
            actSpec['question'] = []

	# explicitly store total number of questions for transparent access later
        actSpec['lastQuestNr'] = len(actSpec['question'])
    except:
        apache.log_error('### TAGARELA ERROR: Could not load ActModel for module %s, exercise %s from file %s'%(modNr,exNr,actModelFilename),apache.APLOG_CRIT)
        actSpec = {}
    return actSpec

