#!/usr/bin/env python

import bsddb

lexfile = open('../analysis/lexiconNilc.json')

db = bsddb.btopen('lexiconNilc.bsddb')

for line in lexfile.readlines():
	data = line.split("\t")
	db[data[0]] = data[1]

db.close()

