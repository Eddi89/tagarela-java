#!/usr/bin/env python

import cjson
import bsddb
import sys

lexFile = sys.argv[1]
featName = sys.argv[2]

db = bsddb.btopen(lexFile)

for k in db.keys():
	arr = cjson.decode(db[k])
	if type(arr) == str:
		sys.stderr.write("WARNING: String value '" + arr + "' instead of array for key: " + k + "\n")
		continue
	for item in arr:
		if type(item) == str:
			sys.stderr.write("WARNING: String value '" + item + "' instead of dict, key is: " + k + "\n")
			continue
		if item.has_key(featName):
			print item[featName]

db.close()

