#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xml.dom.minidom
import os
import glob
import re
import sys

actXML = sys.argv[1]
procModeStr = sys.argv[2]

sys.stderr.write("Parsing " + actXML + "...\n")
actModel = xml.dom.minidom.parse(actXML)
procModeElement = actModel.getElementsByTagName("procMode")[0]
newProcModeText = actModel.createTextNode(procModeStr)
procModeElement.replaceChild(newProcModeText, procModeElement.childNodes[0])
#outfile = open(act + "/act.xml", 'w')
#actModel.writexml(outfile, encoding = "UTF-8")
#outfile.close()
print actModel.toxml(encoding = "UTF-8")
actModel.unlink()

