# -*- coding: utf-8 -*-
"""
---------------------------------------------------------------
$Id: index.py,v 1.28 2008-07-31 14:06:32 dm Exp $
---------------------------------------------------------------
Copyright 2005-2007 by Luiz Amaral and Detmar Meurers
Project information: http://purl.org/net/tagarela
Licensed under the Creative Commons 
Attribution-Noncommercial-Share Alike 3.0 License
http://creativecommons.org/licenses/by-nc-sa/3.0/us/
--------------------------------------------------------------- 
"""

from mod_python import psp,apache
from os.path import basename,dirname

t        = apache.import_module("../code/tagarela_utils.py")
ajax     = apache.import_module("../ajax.py")

####
# main index handler
####

def index(req,modNr='1',exNr='1',questNr='1',ajaxAct='',ajaxArg='',timeStamp='',debug=''):

    sess = t.ensure_authorized_session(req)

    if debug: return t.log(req,t.usermodel_getall(sess))

    # Set local variable to activity type (= name of dir containing this file):
    # variable is called 'actType', which is formatted specially by t.log
    actType=basename(dirname(__file__))
    
    # read in activity specification 
    actSpec = t.getActSpec(modNr,exNr,__file__)
    
    # store current state of affairs in user model:
    current = t.usermodel_put(sess,'current',t.plainDict(locals()))

    t.log(req,t.usermodel_getall(sess))

    # create top of page template:
    (topTmpl,lastExNr) = t.getTopTmpl(req,modNr,exNr,__file__,
                      'Vocabulário',
                      actSpec['presentation']['instructions'])

    # hack: don't show the general input field for exercise 3 since
    # that exercise includes its own fields as part of a FIB-like text.
    if modNr == '3':
       showInput = 0
    else:
       showInput = 1

    # create question template 
    #questionTmpl = t.getQuestTmpl(req,actType,modNr,exNr,lastExNr,questNr,__file__,inputfield=showInput)
    questionTmpl = t.getQuestTmpl(req,actSpec,actType,modNr,exNr,lastExNr,questNr,actSpec['lastQuestNr'],
                  __file__,inputfield=1)

    # create feedback template object
    feedbackTmpl = psp.PSP(req, filename='../include/feedback.tmpl',
			   vars = {'feedbacktext' : ''}) 


    # create main page template object
    mainPageTmpl = psp.PSP(req, filename='include/index.tmpl',
                           vars = {'Top' : topTmpl,
                                   'Question' : questionTmpl,
                                   'Feedback' : feedbackTmpl})                                   

    # result of this is an html page:
    req.content_type ='text/html'

    # save the session variable
    sess.save()

    if ajaxAct == 'updateQuestion': # handle AJAX request
	return ajax.updateQuestion(req,questionTmpl)

    # publish the template
    mainPageTmpl.run()
