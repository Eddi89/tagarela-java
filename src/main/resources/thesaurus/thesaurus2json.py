#!/usr/bin/env python

import json
import sys

# the list of synsets
synsets = []

# the dictionary of words -> synset ids
words2synsets = {}

for line in sys.stdin.readlines():
    if line.startswith("#"):
        continue
    words = line.strip().split(";")
    synset = []
    for word in words:
        if word not in synset:
            synset.append(word)
            if words2synsets.has_key(word):
                words2synsets[word].append(len(synsets))
            else:
                words2synsets[word] = [len(synsets)]
    synsets.append(synset)

print json.dumps(synsets) + "\n\n\n" + json.dumps(words2synsets)

