  S -> NP VP
  NP -> det noun
  NP -> det propNoun
  NP -> numeral noun
  NP -> poss noun
  Nbar -> poss noun
  NP -> poss Nbar
  Nbar -> poss Nbar
  NP -> noun AP
  NP -> AP noun
  Nbar -> noun AP
  Nbar -> AP noun
  Nbar -> Nbar AP
  Nbar -> AP Nbar
  NP -> Nbar AP
  NP -> AP Nbar
  NP -> det Nbar
  AP -> adv adj
  AdvP -> adv adv
  AdvP -> adv PP
  VP -> verb VP
  VP -> verb PP
  NP -> NP PP
  VP -> VP AdvP
  Vbar -> VP AdvP
  VP -> adv verb
  VP -> verb AP
  VP -> Vbar AP
  VP -> Vbar PP
  VP -> Vbar NP
  VP -> verb NP
  PP -> prep NP
  S -> NP Vbar
  NP -> NP S
  S -> conj S
  S -> adv S
  NPcoord -> conj NP
  NP -> NP NPcoord
  VPcoord -> conj VP
  VP -> VP VPcoord
  APcoord -> conj AP
  AP -> AP APcoord
  AdvPcoord -> conj AdvP
  AdvP -> AdvP Advcoord
  Vbar -> pronoun verb
  
# unary rules
VP -> verb
NP -> noun
NP -> numeral
NP -> pronoun
NP -> propNoun
AdvP -> adv
AP -> adj

