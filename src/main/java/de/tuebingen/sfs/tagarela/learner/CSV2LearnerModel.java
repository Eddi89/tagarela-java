package de.tuebingen.sfs.tagarela.learner;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * Utility that reads a column from a CSV file into a learner model.
 * @author rziai
 *
 */
public class CSV2LearnerModel {

	private static final String SEPCHAR = ",";
	private static final String QUOTE = "\"";
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		if (args.length < 3) {
			System.err.println("Need learner model db file, CSV table and column to read out!");
			return;
		}
		
		LearnerModel lm = new LearnerModel(args[0]);
		BufferedReader csv = new BufferedReader(new InputStreamReader(new FileInputStream(args[1])));
		int column = Integer.parseInt(args[2]);
		System.out.println("Reading column " + column + " of " + args[1] + " into learner model: " + args[0]);
		csv.readLine(); // discard first line that contains the header
		
		String line = null;
		
		while ((line = csv.readLine()) != null) {
			String[] data = line.split(SEPCHAR);
			if (data.length <= column || "".equals(data[0])) {
				continue;
			}
			String errorType = data[0].replaceAll(QUOTE, "");
			int value = Integer.parseInt(data[column].replaceAll(QUOTE, ""));
			System.out.println("Setting " + errorType + " to: " + value);
			lm.put(errorType, value);
		}
		
		csv.close();
		System.out.println("Done.");
	}

}
