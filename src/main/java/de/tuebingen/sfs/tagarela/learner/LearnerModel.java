package de.tuebingen.sfs.tagarela.learner;

import java.util.List;

import org.json.JSONObject;

import de.tuebingen.sfs.tagarela.util.BerkeleyDB;



/**
 * Learner model access class. Wraps a database instance that has the actual data.
 * @author rziai
 *
 */
public class LearnerModel {

	private BerkeleyDB db;
	
	public LearnerModel(String dbFileName) throws Exception {
		this.db = new BerkeleyDB(dbFileName);
	}

	public boolean put(String modelKey, JSONObject inputData) throws Exception {
		return db.put(modelKey, inputData);
	}
	
	public boolean put(String modelKey, int num) throws Exception {
		return db.put(modelKey, new Integer(num));
	}
	
	public JSONObject getObject(String modelKey) throws Exception {
		return db.getObject(modelKey);
	}
	
	public List<JSONObject> getObjects(String modelKey) throws Exception {
		return db.getObjects(modelKey);
	}
	
	public int getInt(String modelKey) throws Exception {
		return db.getInt(modelKey);
	}
}
