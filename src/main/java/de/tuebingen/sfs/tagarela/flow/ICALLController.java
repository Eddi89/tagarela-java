package de.tuebingen.sfs.tagarela.flow;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.flow.Flow;
import org.apache.uima.flow.JCasFlow_ImplBase;
import org.apache.uima.flow.Step;
import org.apache.uima.flow.impl.CapabilityLanguageFlowController;
import org.apache.uima.jcas.JCas;

public class ICALLController extends CapabilityLanguageFlowController {

	class ICALLFlow extends JCasFlow_ImplBase {

		
		public Step next() throws AnalysisEngineProcessException {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	
	
	
	public Flow computeFlow(JCas jcas) throws AnalysisEngineProcessException {
		ICALLFlow flow = new ICALLFlow();
		flow.setJCas(jcas);
		
		return flow;
	}




	/* (non-Javadoc)
	 * @see org.apache.uima.flow.impl.CapabilityLanguageFlowController#computeFlow(org.apache.uima.cas.CAS)
	 */
	@Override
	public Flow computeFlow(CAS acas) throws AnalysisEngineProcessException {
		try {
			return computeFlow(acas.getJCas());
		} catch (CASException e) {
			throw new AnalysisEngineProcessException(e);
		}
	}
	
	

}
