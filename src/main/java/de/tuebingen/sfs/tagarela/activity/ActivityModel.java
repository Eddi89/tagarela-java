package de.tuebingen.sfs.tagarela.activity;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import noNamespace.ActivityDocument;
import noNamespace.ActivityDocument.Activity;
import noNamespace.ImageDocument.Image;

import org.apache.xmlbeans.XmlException;



/**
 * Represents the activity model and wraps the underlying data source.
 * This ensures that the classes that use the activity model do not have
 * to know about how it is stored.
 */
public class ActivityModel {
	
	public class Presentation {
		private String l1Instructions;
		private String l2Instructions;
		private String audioFile;
		private Map<String, String> images;
		/**
		 * @param instructions
		 * @param instructions2
		 * @param audioFile
		 * @param images
		 */
		public Presentation(String l1Instructions, String l2Instructions,
				String audioFile, Map<String, String> images) {
			this.l1Instructions = l1Instructions;
			this.l2Instructions = l2Instructions;
			this.audioFile = audioFile;
			this.images = images;
		}
		/**
		 * @return the l1Instructions
		 */
		public String getL1Instructions() {
			return l1Instructions;
		}
		/**
		 * @return the l2Instructions
		 */
		public String getL2Instructions() {
			return l2Instructions;
		}
		/**
		 * @return the audioFile
		 */
		public String getAudioFile() {
			return audioFile;
		}
		/**
		 * @return the images
		 */
		public Map<String, String> getImages() {
			return images;
		}
	}

	public class Target {
		private String targetStr;
		private List<String> reqWords;

		/**
		 * @param targetStr
		 * @param reqWords
		 */
		public Target(String targetStr, List<String> reqWords) {
			this.targetStr = targetStr;
			this.reqWords = reqWords;
		}
		
		/**
		 * @return the targetStr
		 */
		public String getTargetStr() {
			return targetStr;
		}
		/**
		 * @return the reqWords
		 */
		public List<String> getReqWords() {
			return reqWords;
		}
		
	}

	public class Question {
		private List<Target> targets;
		private String questionStr;
		
		/**
		 * @param targets
		 * @param questionStr
		 */
		public Question(List<Target> targets, String questionStr) {
			this.targets = targets;
			this.questionStr = questionStr;
		}

		/**
		 * @return the targets
		 */
		public List<Target> getTargets() {
			return targets;
		}

		/**
		 * @return the questionStr
		 */
		public String getQuestionStr() {
			return questionStr;
		}
	}
	
	

	private Presentation presentation;
	private String procMode;
	private List<Question> questions;
	
	public ActivityModel(InputStream in) throws IOException {
		try {
			Activity xmlActivity = ActivityDocument.Factory.parse(in).getActivity();
			initPresentation(xmlActivity.getPresentation());
			procMode = xmlActivity.getProcMode();
			initQuestions(xmlActivity.getQuestionList());
		} catch (XmlException e) {
			throw new IOException(e.getMessage());
		}
	}
	
	private void initQuestions(List<noNamespace.QuestionDocument.Question> xmlQuests) {
		questions = new ArrayList<Question>();
		for (noNamespace.QuestionDocument.Question q : xmlQuests) {
			ArrayList<Target> targets = new ArrayList<Target>();
			for (noNamespace.TargetDocument.Target xmlT : q.getTargetList().getTargetList()) {
				ArrayList<String> reqWords = new ArrayList<String>();
				for (String word : xmlT.getReqWords().getWordList()) {
					reqWords.add(word);
				}
				targets.add(new Target(xmlT.getString(), reqWords));
			}
			questions.add(new Question(targets, q.getString()));
		}
	}
	
	private void initPresentation(noNamespace.PresentationDocument.Presentation xmlPres) {
		HashMap<String, String> images = new HashMap<String, String>();
		for (Image i : xmlPres.getImageList()) {
			images.put(i.getFilename(), i.getAlt());
		}
		presentation = new Presentation(xmlPres.getInstructions().getL1(),
				xmlPres.getInstructions().getL2(),
				xmlPres.getAudio() != null ? xmlPres.getAudio().getFilename() : null,
				images);
	}
	
	
	/*
	 * main for testing
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException {
		new ActivityModel(new FileInputStream(args[0]));
	}

	/**
	 * @return the presentation
	 */
	public Presentation getPresentation() {
		return presentation;
	}

	/**
	 * @return the procMode
	 */
	public String getProcMode() {
		return procMode;
	}

	/**
	 * @return the questions
	 */
	public List<Question> getQuestions() {
		return questions;
	}
}
