package de.tuebingen.sfs.tagarela.activity;

import java.util.List;

/**
 * This interface is to be implemented by classes that want to provide
 * strategies for computing the best answer from a set of target answers.
 * As there are multiple possible definitions of what the best answer is,
 * this is left to concrete subclasses to define.
 * @author rziai
 *
 */
public interface BestAnswerFinder {

	/**
	 * Computes the best answer from a set of given answers with respect
	 * to the learner input.
	 * @param answers The target answers
	 * @param learnerInput The learner input
	 * @return The best answer
	 */
	public String getBestAnswer(List<String> answers, String learnerInput);
}
