package de.tuebingen.sfs.tagarela.activity;

import java.util.List;

import de.linuxusers.levenshtein.simple.Levenshtein;

/**
 * Implementation of <code>BestAnswerFinder</code> that uses Levenshtein
 * distance to find the closest match among the target answers.
 * @author rziai
 *
 */
public class LevenshteinAnswerFinder implements BestAnswerFinder {

	/*
	 * (non-Javadoc)
	 * @see de.tuebingen.sfs.tagarela.activity.BestAnswerFinder#getBestAnswer(java.util.List, java.lang.String)
	 */
	public String getBestAnswer(List<String> answers, String learnerInput) {
		String bestAnswer = null;
		float bestScore = Float.MAX_VALUE;
		Levenshtein lev = new Levenshtein();
		
		for (String tAnswer : answers) {
			float score = lev.getStringDistance(tAnswer, learnerInput);
			if (score < bestScore) {
				bestAnswer = tAnswer;
				bestScore = score;
			}
		}
		return bestAnswer;
	}

}
