package de.tuebingen.sfs.tagarela.annotators;

import java.util.ArrayList;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

import de.tuebingen.sfs.tagarela.annotators.cam.CAMMatcher;
import de.tuebingen.sfs.tagarela.types.AnalysisResults;
import de.tuebingen.sfs.tagarela.types.CanonicMatchResult;
import de.tuebingen.sfs.tagarela.types.POSMatchResult;
import de.tuebingen.sfs.tagarela.types.Token;
import de.tuebingen.sfs.tagarela.util.Tools;

/**
 * POS Matcher implementation. Uses the POS of learner and
 * target answer to compare them token by token.
 * @author rziai
 *
 */
public class POSMatcher extends JCasAnnotator_ImplBase {

	private Logger logger;
	
	
	
	/* (non-Javadoc)
	 * @see org.apache.uima.analysis_component.AnalysisComponent_ImplBase#initialize(org.apache.uima.UimaContext)
	 */
	@Override
	public void initialize(UimaContext context)
			throws ResourceInitializationException {
		super.initialize(context);
		logger = context.getLogger();
	}
	
	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		try {
			// collect resulting tokens here
			ArrayList<Token> learnerTokens = new ArrayList<Token>();
			ArrayList<Token> answerTokens = new ArrayList<Token>();
			// ...and the POS here
			ArrayList<String> learnerPOS = new ArrayList<String>();
			ArrayList<String> answerPOS = new ArrayList<String>();
			// retrieve analysis results
			FSIterator arIter = jcas.getView("_InitialView").getAnnotationIndex(AnalysisResults.type).iterator();
			AnalysisResults ar = (AnalysisResults) arIter.get();
			
			// get the canonic match and go over it, looking for tokens that have not been matched
			CanonicMatchResult cmr = ar.getCanonicMatch();
			//... in the learner answer
			for (int i = 0; i < cmr.getUnmatchedStudentTokens().size(); i++) {
				Token t = cmr.getUnmatchedStudentTokens(i);
				learnerTokens.add(t);
				if (t.getCategory() == null) {
					learnerPOS.add("LEARNER_NO_POS-" + i);
				} else {
					learnerPOS.add(t.getCategory());
				}
			}
			
			// ...and in the target answer
			for (int i = 0; i < cmr.getUnmatchedAnswerTokens().size(); i++) {
				Token t = cmr.getUnmatchedAnswerTokens(i);
				answerTokens.add(t);
				if (t.getCategory() == null) {
					answerPOS.add("ANSWER_NO_POS-" + i);
				} else {
					answerPOS.add(t.getCategory());
				}
			}
			
			// construct a new CAMMatcher that does the real work
			CAMMatcher matcher = new CAMMatcher();
			// compute match
			matcher.computeMatch(learnerPOS, answerPOS);
			
			// create data structure for result
			POSMatchResult pmr = new POSMatchResult(ar.getCAS().getJCas());
			
			// set results
			pmr.setMatchedTokens(Tools.toMappingArray(learnerTokens, answerTokens, matcher.getMatchedFirst(), ar.getCAS().getJCas()));
			pmr.setUnmatchedStudentTokens(Tools.toArray(learnerTokens, matcher.getUnmatchedFirst(), ar.getCAS().getJCas()));
			pmr.setUnmatchedAnswerTokens(Tools.toArray(answerTokens, matcher.getUnmatchedSecond(), ar.getCAS().getJCas()));
			
			logger.log(Level.INFO, "Matched student POS: " + matcher.getMatchedFirst());
			logger.log(Level.INFO, "Unmatched student POS: " + matcher.getUnmatchedFirst());
			
			logger.log(Level.INFO, "Matched answer POS: " + matcher.getMatchedSecond());
			logger.log(Level.INFO, "Unmatched answer POS: " + matcher.getUnmatchedSecond());

			// put data structure into analysis results
			ar.setPosMatch(pmr);
		} catch (CASException e) {
			throw new AnalysisEngineProcessException(e);
		}
	}

}
