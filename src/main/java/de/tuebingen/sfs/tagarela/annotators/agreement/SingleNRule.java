package de.tuebingen.sfs.tagarela.annotators.agreement;

import org.apache.uima.cas.CASException;

import de.tuebingen.sfs.tagarela.types.Phrase;
import de.tuebingen.sfs.tagarela.types.Token;

/**
 * Percolates features up from single nouns in NPs.
 * @author rziai
 *
 */
public class SingleNRule extends Rule {

	private static final String headPOS = "noun";
	
	public SingleNRule() {
		super(new String[]{ "NP", "Nbar"});
	}

	/* (non-Javadoc)
	 * @see de.tuebingen.sfs.tagarela.annotators.agreement.Rule#apply(de.tuebingen.sfs.tagarela.types.Phrase)
	 */
	@Override
	public boolean apply(Phrase p, String agreementLabel) throws CASException {
		if (!anchorSymbols.contains(p.getCategory())) {
			return false;
		}
		if (p.getChildren().size() != 1) {
			return false;
		}
		Token child = (Token) p.getChildren(0);
		if (child.getLexDef().size() == 0) {
			return false;
		}
		
		p.setAgreement(computeAgreement(child, null, agreementLabel, headPOS));
		return true;
	}
	
	
}
