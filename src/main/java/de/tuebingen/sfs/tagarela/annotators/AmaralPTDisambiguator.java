package de.tuebingen.sfs.tagarela.annotators;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

import de.tuebingen.sfs.tagarela.annotators.disambiguator.Rule;
import de.tuebingen.sfs.tagarela.types.Token;
import de.tuebingen.sfs.tagarela.util.Tools;

/**
 * A flexible re-implementation of the linguistic intelligence in the TAGARELA
 * disambiguator. At some point, someone should re-implement this in
 * Constraint Grammar, where it belongs.
 * @author rziai
 *
 */
public class AmaralPTDisambiguator extends JCasAnnotator_ImplBase {
	
	private Logger logger;
	
	private ArrayList<Rule> rules;
	
	

	/* (non-Javadoc)
	 * @see org.apache.uima.analysis_component.AnalysisComponent_ImplBase#initialize(org.apache.uima.UimaContext)
	 */
	@Override
	public void initialize(UimaContext context)
			throws ResourceInitializationException {
		super.initialize(context);
		logger = context.getLogger();
		rules = new ArrayList<Rule>();
		
		// here's our linguistic intelligence for now...
		
		rules.add(new Rule(null, "=pronoun", "prep", new String[0]));
		rules.add(new Rule(null, "det", "prep", new String[0]));
		rules.add(new Rule("=verb", "poss", "prep", new String[0]));
		
		rules.add(new Rule("=pronoun", null, "verb", new String[0]));
		rules.add(new Rule("=propNoun", null, "verb", new String[0]));
		rules.add(new Rule("=noun", null, "verb", new String[0]));
		rules.add(new Rule(null, "=noun", "verb", new String[]{ "adj", "poss" }));
		
		rules.add(new Rule("=noun", null, "adj", new String[0]));
		rules.add(new Rule(null, "=noun", "adj", new String[0]));
		
		rules.add(new Rule(null, "=numeral,noun", "numeral", new String[0]));
		rules.add(new Rule(null, "=noun", "numeral", new String[0]));
		rules.add(new Rule("=numeral", null, "noun", new String[0]));
		
		rules.add(new Rule(null, "!verb", "conj", new String[]{ "adv", "verb" }));
	}



	@SuppressWarnings("unchecked")
	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		try {
			Iterator<JCas> viewIter = jcas.getViewIterator();
			
			while (viewIter.hasNext()) {
				JCas currentCas = viewIter.next();
				
				FSIterator tokenIter = currentCas.getAnnotationIndex(Token.type).iterator();
				
				// first collect all Tokens into a list because we need the deep structure
				ArrayList<Token> tokenList = new ArrayList<Token>();
				while (tokenIter.hasNext()) {
					Token t = (Token) tokenIter.next();
					if (t.getDeepForm() != null) {
						FSArray deepForm = t.getDeepForm();
						for (int i = 0; i < deepForm.size(); i++) {
							tokenList.add((Token) deepForm.get(i));
						}
					} else {
						tokenList.add(t);
					}
				}
				
				// loop through the Tokens
				Token previous = null, current = null, next = null;
				for (int i = 0; i < tokenList.size(); i++) {
					previous = i > 0 ? tokenList.get(i-1) : null;
					current = tokenList.get(i);
					next = i < (tokenList.size()-1) ? tokenList.get(i+1) : null;
		
					// do we have ambiguity? if not, move along!
					if (current.getLexDef().size() <= 1) {
						continue;
					}
					
					logger.log(Level.INFO, "Disambiguating token: " + current.getTokenString());
					
					// apply the rules, stop as soon as one applied
					String selected = null;
					for (Rule r : rules) {
						selected = r.apply(previous, current, next);
						if (selected != null) {
							logger.log(Level.INFO, "Applied rule: " + r + " ON prev: " +
									(previous != null ? previous.getTokenString() : "") +
									", cur: " + current.getTokenString() + ", next: " +
									(next != null ? next.getTokenString() : ""));
							break;
						}
					}
					// still no decision? use frequency
					if (selected == null) {
						selected = getBestFreqPOS(current);
						logger.log(Level.INFO, "Rules failed, used frequency to determine POS: " + selected
								+ " for token: " + current.getTokenString());
					}
					// eliminate other analyses
					Tools.eliminateOtherAnalyses(current, selected, currentCas);
				}
			}
		} catch (CASException e) {
			throw new AnalysisEngineProcessException(e);
		}
	}
	
	/*
	 * Returns the lexicon entry with the highest frequency for the given token.
	 */
	private String getBestFreqPOS(Token t) {
		int highest = Integer.MIN_VALUE;
		String pos = null;
		
		for (int i = 0; i < t.getLexDef().size(); i++) {
			int freq = t.getLexDef(i).getFrequency();
			if (freq > highest) {
				highest = freq;
				pos = t.getLexDef(i).getPos();
			}
		}
		return pos;
	}

}
