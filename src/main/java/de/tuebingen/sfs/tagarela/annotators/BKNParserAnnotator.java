package de.tuebingen.sfs.tagarela.annotators;

import java.io.IOException;
import java.util.Iterator;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

import bkn.BKN;
import bkn.Cell;
import bkn.Pair;
import bkn.Rule;
import bkn.Sentence;
import bkn.Table;
import bkn.Word;
import de.tuebingen.sfs.tagarela.types.Constituent;
import de.tuebingen.sfs.tagarela.types.Phrase;
import de.tuebingen.sfs.tagarela.types.Token;

/**
 * Parser annotator that uses BKN to parse the input and annotate Phrases.
 * @author rziai
 *
 */
public class BKNParserAnnotator extends JCasAnnotator_ImplBase {
	
	private Logger logger;
	private BKN parser;
	
	
	/* (non-Javadoc)
	 * @see org.apache.uima.analysis_component.AnalysisComponent_ImplBase#initialize(org.apache.uima.UimaContext)
	 */
	@Override
	public void initialize(UimaContext context)
			throws ResourceInitializationException {
		super.initialize(context);
		logger = context.getLogger();
		String grammarLoc = (String) context.getConfigParameterValue("grammarLoc");
		try {
			// load a new parser instance
			parser = new BKN(getClass().getResourceAsStream(grammarLoc));
		} catch (IOException e) {
			throw new ResourceInitializationException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		try {
			Iterator<JCas> viewIter = jcas.getViewIterator();

			// iterate over all views
			while (viewIter.hasNext()) {
				JCas currentCas = viewIter.next();
				FSIterator tokenIter = currentCas
						.getAnnotationIndex(Token.type).iterator();

				// first collect all Tokens into a list because we need the deep
				// structure
				Sentence s = new Sentence();
				boolean missingPOS = false;
				
				// iterate over tokens
				while (tokenIter.hasNext()) {
					Token t = (Token) tokenIter.next();
					if (t.getDeepForm() != null) {
						// tokens with deep form
						FSArray deepForm = t.getDeepForm();
						for (int i = 0; i < deepForm.size(); i++) {
							Token deepT = (Token) deepForm.get(i);
							Word w = new Word(deepT.getTokenString(), deepT.getCategory());
							w.setPayload(deepT);
							s.addWord(w);
						}
					} else {
						// tokens without deepform
						if (t.getCategory() == null) {
							// missing POS, abort
							missingPOS = true;
							break;
						}
						// construct new Word for BKN
						Word w = new Word(t.getTokenString(), t.getCategory());
						w.setPayload(t);
						s.addWord(w);
					}
				}
				
				if (missingPOS) {
					logger.log(Level.WARNING, "Missing POS tags, cannot parse sentence!");
				} else {
					logger.log(Level.INFO, "Parsing sentence: " + s);
					// parse sentence
					Table chart = parser.parse(s);
					logger.log(Level.INFO, "Chart is:\n" + chart);
					// annotate the resulting chart
					annotateTrees(chart, currentCas);
				}
			}
		} catch (Exception e) {
			throw new AnalysisEngineProcessException(e);
		}
	}
	
	/*
	 * Annotates the given chart in the given JCas.
	 */
	private void annotateTrees(Table chart, JCas jcas) {
		Sentence s = chart.getSentence();
		// go through the whole chart
		for (int y = chart.size() - 1; y >= 0; y--) {
			for (int x = 0; x < chart.size(); x++) {
				// get the cell at the current position
				Cell cell = chart.getCell(x+1,y+1);
				// loop through rules and build trees
	            for (Rule r : cell) {
	            	buildAndAnnotateTree(cell, r, jcas, s, null);
	            }
			}
		}
	}
	
	/*
	 * Recursive helper method that does the real annotation.
	 */
	private Constituent buildAndAnnotateTree(Cell c, Rule r,
			JCas jcas, Sentence s, Phrase parent) {
		// obtain reference cells
		Pair<Cell, Cell> refs = c.getRefCells(r);
		
		// null references? we're done here, this is a token
		if (refs == null
				|| refs.getFirst() == null) {
			return (Token) s.getWordAt(c.getX()).getPayload();
		}
		// create a new constituent and set label from rule
		Phrase phrase = new Phrase(jcas);
		phrase.setCategory(r.getName());
		phrase.setParent(parent);
		
		// get the tokens, we need them for the annotation positions
		Token start = (Token) s.getWordAt(c.getX()).getPayload();
		// indices of BKN are a bit... unusual, so we need to correct them
        Token end = (Token) s.getWordAt(c.getY()+c.getX()-1).getPayload();
        logger.log(Level.FINEST, "Constituent '" + phrase.getCategory() + "' from " +
        		start.getTokenString() + " to " + end.getTokenString());
		phrase.setBegin(start.getBegin());
		phrase.setEnd(end.getEnd());
		
		// follow left and right rules
		Rule leftRule = refs.getFirst().findRule(r.getLeft());
		Constituent leftChild = buildAndAnnotateTree(refs.getFirst(), leftRule, jcas, s, phrase);

		String rightPart = r.getRight();
		Constituent rightChild = null;
		// check whether rule is unary
		if (!rightPart.equals("")) {
			Rule rightRule = refs.getSecond().findRule(rightPart);
			rightChild = buildAndAnnotateTree(refs.getSecond(), rightRule, jcas, s, phrase);
		}
		FSArray children = null;
		// distinguish between unary and binary rules
		if (rightChild != null) {
			children = new FSArray(jcas, 2);
			children.set(1, rightChild);
		} else {
			children = new FSArray(jcas, 1);
		}
		// we always have a left child
		children.set(0, leftChild);
		
		// set children constituents
		phrase.setChildren(children);
		// only annotate if we don't already have this constituent in the index
		if (!containsConstituent(jcas, phrase)) {
			phrase.addToIndexes(jcas);
		}
		return phrase;
	}
	
	/*
	 * helper method that checks whether the given Phrase has already been annotated
	 */
	private boolean containsConstituent(JCas jcas, Phrase p) {
		AnnotationIndex index = jcas.getAnnotationIndex(Phrase.type);
		FSIterator iter = index.iterator();
		iter.moveTo(p);
		if (!iter.isValid()) {
			return false;
		}
		Phrase target = (Phrase) iter.get();
		
		while (p.getBegin() == target.getBegin()
				&& p.getEnd() == target.getEnd()) {
			if (target.getCategory().equals(p.getCategory())) {
				return true;
			}
			if (!iter.hasNext()) {
				return false;
			}
			target = (Phrase) iter.next();
		}
		return false;
	}
}
