package de.tuebingen.sfs.tagarela.annotators;

import java.util.Locale;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;

import de.tuebingen.sfs.tagarela.types.AnalysisResults;
import de.tuebingen.sfs.tagarela.util.StringMatchConstants;

/**
 * Annotator that does basic string matching between learner and target answer.
 * @author rziai
 *
 */
public class StringMatcher extends JCasAnnotator_ImplBase {
	
	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		try {
			
			// full string comparison
			String learnerInput = jcas.getView("_InitialView").getDocumentText();
			String bestAnswer = jcas.getView("bestAnswer").getDocumentText();
			
			// get analysis results
			AnalysisResults results = (AnalysisResults) jcas.getView("_InitialView").getAnnotationIndex(AnalysisResults.type).iterator().get();
			
			// test for exact match
			if (learnerInput.equals(bestAnswer)) {
				results.setStringMatch(StringMatchConstants.EXACT_MATCH);
				return;
			}
			Locale locale = new Locale("pt", "BR");
			String lcLearnerInput = learnerInput.toLowerCase(locale);
			String lcBestAnswer = bestAnswer.toLowerCase(locale);
			// lowercase match
			if (lcLearnerInput.equals(lcBestAnswer)) {
				results.setStringMatch(StringMatchConstants.LOWERCASE_MATCH);
				return;
			}
			
			// match without spaces
			String noSpaceLearner = learnerInput.replaceAll("\\s", "");
			String noSpaceAnswer = bestAnswer.replaceAll("\\s", "");
			if (noSpaceAnswer.equals(noSpaceLearner)) {
				results.setStringMatch(StringMatchConstants.SPACE_MATCH);
				return;
			}
			
			// match without punctuation
			String noPuncLearner = learnerInput.replaceAll("\\p{P}", "");
			String noPuncAnswer = bestAnswer.replaceAll("\\p{P}", "");
			if (noPuncAnswer.equals(noPuncLearner)) {
				results.setStringMatch(StringMatchConstants.NOPUNC_MATCH);
				return;
			}
			
			// lowercase match without punctuation
			if (noPuncAnswer.toLowerCase(locale).equals(noPuncLearner.toLowerCase(locale))) {
				results.setStringMatch(StringMatchConstants.LOWERCASE_NOPUNC_MATCH);
				return;
			}
			
			results.setStringMatch(StringMatchConstants.NO_MATCH);
			
		} catch (CASException e) {
			throw new AnalysisEngineProcessException(e);
		}
	}

}
