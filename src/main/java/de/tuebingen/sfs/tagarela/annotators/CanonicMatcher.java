package de.tuebingen.sfs.tagarela.annotators;

import java.util.ArrayList;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

import de.tuebingen.sfs.tagarela.annotators.cam.CAMMatcher;
import de.tuebingen.sfs.tagarela.types.AnalysisResults;
import de.tuebingen.sfs.tagarela.types.CanonicMatchResult;
import de.tuebingen.sfs.tagarela.types.Token;
import de.tuebingen.sfs.tagarela.types.TokenMatchResult;
import de.tuebingen.sfs.tagarela.util.Tools;

/**
 * Canonic Matcher implementation. Uses the canonic form of learner and
 * target answer to compare them token by token.
 * @author rziai
 *
 */
public class CanonicMatcher extends JCasAnnotator_ImplBase {

	private Logger logger;
	
	/* (non-Javadoc)
	 * @see org.apache.uima.analysis_component.AnalysisComponent_ImplBase#initialize(org.apache.uima.UimaContext)
	 */
	@Override
	public void initialize(UimaContext context)
			throws ResourceInitializationException {
		super.initialize(context);
		logger = context.getLogger();
	}
	
	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		try {
			// collect resulting tokens here
			ArrayList<Token> learnerTokens = new ArrayList<Token>();
			ArrayList<Token> answerTokens = new ArrayList<Token>();
			// ...and the canonic forms here
			ArrayList<String> learnerCanonic = new ArrayList<String>();
			ArrayList<String> answerCanonic = new ArrayList<String>();
			// retrieve analysis results
			FSIterator arIter = jcas.getView("_InitialView").getAnnotationIndex(AnalysisResults.type).iterator();
			AnalysisResults ar = (AnalysisResults) arIter.get();
			
			// get the token match and go over it, looking for tokens that have not been matched
			TokenMatchResult tmr = ar.getTokenMatch();
			
			//... in the learner answer
			for (int i = 0; i < tmr.getUnmatchedStudentTokens().size(); i++) {
				Token t = tmr.getUnmatchedStudentTokens(i);
				learnerTokens.add(t);
				if (t.getLexDef() == null || t.getLexDef().size() == 0
						|| t.getLexDef(0).getCanonic() == null) {
					learnerCanonic.add("LEARNER_NO_CANONIC-" + i);
				} else {
					learnerCanonic.add(t.getLexDef(0).getCanonic());
				}
			}
			
			// ...and in the target answer
			for (int i = 0; i < tmr.getUnmatchedAnswerTokens().size(); i++) {
				Token t = tmr.getUnmatchedAnswerTokens(i);
				answerTokens.add(t);
				if (t.getLexDef() == null || t.getLexDef().size() == 0
						|| t.getLexDef(0).getCanonic() == null) {
					answerCanonic.add("ANSWER_NO_CANONIC-" + i);
				} else {
					answerCanonic.add(t.getLexDef(0).getCanonic());
				}
			}
			
			// construct a new CAMMatcher that does the real work
			CAMMatcher matcher = new CAMMatcher();
			// compute match
			matcher.computeMatch(learnerCanonic, answerCanonic);
			
			// create data structure for result
			CanonicMatchResult cmr = new CanonicMatchResult(ar.getCAS().getJCas());
			
			// set results
			cmr.setMatchedTokens(Tools.toMappingArray(learnerTokens, answerTokens, matcher.getMatchedFirst(), ar.getCAS().getJCas()));
			cmr.setUnmatchedStudentTokens(Tools.toArray(learnerTokens, matcher.getUnmatchedFirst(), ar.getCAS().getJCas()));
			cmr.setUnmatchedAnswerTokens(Tools.toArray(answerTokens, matcher.getUnmatchedSecond(), ar.getCAS().getJCas()));
			
			logger.log(Level.INFO, "Matched student canonic: " + matcher.getMatchedFirst());
			logger.log(Level.INFO, "Unmatched student canonic: " + matcher.getUnmatchedFirst());
			
			logger.log(Level.INFO, "Matched answer canonic: " + matcher.getMatchedSecond());
			logger.log(Level.INFO, "Unmatched answer canonic: " + matcher.getUnmatchedSecond());

			// put data structure into analysis results
			ar.setCanonicMatch(cmr);
		} catch (CASException e) {
			throw new AnalysisEngineProcessException(e);
		}
	}

}
