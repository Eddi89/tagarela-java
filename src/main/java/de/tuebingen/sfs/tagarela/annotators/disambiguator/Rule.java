package de.tuebingen.sfs.tagarela.annotators.disambiguator;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import de.tuebingen.sfs.tagarela.types.Token;

/**
 * Represents a local disambiguation rule.
 * @author rziai
 *
 */
public class Rule {

	private String leftPOSConstraint;
	private String rightPOSConstraint;
	private String toBeSelected;
	
	private String[] negativeSelectionConstraints;
	
	/**
	 * @param leftPOSConstraint The left POS tag
	 * @param rightPOSConstraint The right POS tag
	 * @param toBeSelected The POS to be selected if contexts apply
	 * @param negativeSelectionConstraints A list of POS tags that are NOT allowed to be
	 * a possible analysis for the current token
	 */
	public Rule(String leftPOSConstraint, String rightPOSConstraint,
			String toBeSelected, String[] negativeSelectionConstraints) {
		this.leftPOSConstraint = leftPOSConstraint;
		this.rightPOSConstraint = rightPOSConstraint;
		this.toBeSelected = toBeSelected;
		this.negativeSelectionConstraints = negativeSelectionConstraints;
	}
	
	/**
	 * Disambiguates the Token according to the rule encoded by this instance.
	 * @param leftToken The left context
	 * @param currentToken The token to be disambiguated
	 * @param rightToken The right context
	 * @param jcas The JCas the Tokens belong to
	 * @return The selected POS if the Rule applied, null otherwise
	 */
	public String apply(Token leftToken, Token currentToken, Token rightToken) {
		// check that the Token actually has the POS we want to select
		if (! hasPOS(currentToken, toBeSelected)) {
			return null;
		}
		
		// make sure the left context matches
		if (leftPOSConstraint != null && !evaluatePOSConstraint(leftToken, leftPOSConstraint)) {
			return null;
		}
		
		// make sure the right context matches
		if (rightPOSConstraint != null && !evaluatePOSConstraint(rightToken, rightPOSConstraint)) {
			return null;
		}
		
		// make sure none of the analyses in the current Token have one of the supplied
		// 'no-go' POS
		for (String negPOS : negativeSelectionConstraints) {
			if (hasPOS(currentToken, negPOS)) {
				return null;
			}
		}
		
		return toBeSelected;
	}
	
	/*
	 * "Parses" the constraint string and decides which helper
	 * method to call
	 */
	private boolean evaluatePOSConstraint(Token t, String constraint) {
		if (t == null && constraint != null) {
			return false;
		}
		
		if (constraint.startsWith("=")) {
			if (constraint.contains(",")) {
				return isPOSSet(t, new HashSet<String>(
						Arrays.asList(constraint.substring(1).split(","))));
			} else {
				return isPOS(t, constraint.substring(1));
			}
		} else {
			return constraint.startsWith("!") ?  !hasPOS(t, constraint.substring(1))
					: hasPOS(t, constraint);
		}
	}
	
	/*
	 * determine whether a given Token has an entry with the given POS tag
	 */
	private boolean hasPOS(Token t, String pos) {
		if (t.getLexDef() == null) {
			return false;
		}
		for (int i = 0; i < t.getLexDef().size(); i++) {
			String lexpos = t.getLexDef(i).getPos();
			if (lexpos != null && lexpos.equalsIgnoreCase(pos)) {
				return true;
			}
		}
		return false;
	}
	
	/*
	 * determines whether a Token unambiguously has the given POS tag
	 */
	private boolean isPOS(Token t, String pos) {
		String thepos = t.getCategory();
		return thepos != null ? thepos.equalsIgnoreCase(pos) : false;
	}
	
	/*
	 * determines whether a given Token has exactly the given analyses
	 */
	private boolean isPOSSet(Token t, Set<String> pos) {
		
		if (t.getLexDef() == null || t.getLexDef().size() != pos.size()) {
			return false;
		}
		
		for (int i = 0; i < t.getLexDef().size(); i++) {
			String lexpos = t.getLexDef(i).getPos();
			if (lexpos == null || !pos.contains(lexpos)) {
				return false;
			}
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String ret =  leftPOSConstraint + "\t" + toBeSelected  + "\t" + rightPOSConstraint + "\tand not: [ ";
		for (String s : negativeSelectionConstraints) {
			ret += s + ", ";
		}
		return ret + "]";
	}
	
	
}
