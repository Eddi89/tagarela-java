package de.tuebingen.sfs.tagarela.annotators.agreement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.NonEmptyFSList;

import de.drni.misc.uima.util.list.UimaListHelper;
import de.tuebingen.sfs.tagarela.types.Agreement;
import de.tuebingen.sfs.tagarela.types.AnalysisResults;
import de.tuebingen.sfs.tagarela.types.LexiconInfo;
import de.tuebingen.sfs.tagarela.types.Phrase;
import de.tuebingen.sfs.tagarela.types.Token;

/**
 * Abstract class that provides useful base implementations of
 * various methods for agreement computation.
 * @author rziai
 *
 */
public abstract class Rule {

	protected List<String> anchorSymbols;
	private static HashMap<String, ArrayList<Rule>> ruleRegistry = new HashMap<String, ArrayList<Rule>>();

	/**
	 * @param anchorSymbol
	 */
	public Rule(List<String> anchorSymbols) {
		this.anchorSymbols = anchorSymbols;
		for (String s: anchorSymbols) {
			registerRule(s, this);
		}
	}
	
	public Rule(String[] anchorSymbols) {
		this(Arrays.asList(anchorSymbols));
	}
	
	/**
	 * Applies this Rule to the given phrase. Subclasses are expected to pick
	 * the actual subconstituents of the phrase that are supposed to agree
	 * and call the helper methods of this class.
	 * @param p The phrase to apply the rule on
	 * @return <code>true</code> if the rule applied, <code>false</code> otherwise
	 */
	public abstract boolean apply(Phrase p, String agreementLabel) throws CASException;
	
	/*
	 * computes agreement between two tokens, this is implemented here so subclasses can just use it
	 */
	protected Agreement computeAgreement(Token first, Token second, String label,
			String headPOS) throws CASException {

		JCas jcas = first.getView().getJCas();
		Agreement agreement = new Agreement(jcas);
		LexiconInfo lexInfo1 = first.getLexDef(0);
		LexiconInfo lexInfo2 = second == null ? null : second
				.getLexDef(0);

		String person1 = lexInfo1.getPerson();
		String person2 = lexInfo2 == null ? null : lexInfo2.getPerson();

		String number1 = lexInfo1.getNumber();
		String number2 = lexInfo2 == null ? null : lexInfo2.getNumber();

		String gender1 = lexInfo1.getGender();
		String gender2 = lexInfo2 == null ? null : lexInfo2.getGender();

		String personResult = evaluateFeatureAgreement(person1, person2);
		String numberResult = evaluateFeatureAgreement(number1, number2);
		String genderResult = evaluateFeatureAgreement(gender1, gender2);

		agreement.setPerson(personResult);
		agreement.setNumber(numberResult);
		agreement.setGender(genderResult);
		agreement.setLabel(label);
		if ("error".equals(personResult) || "error".equals(numberResult)
				|| "error".equals(genderResult)) {
			FSArray offendingConstituents = new FSArray(jcas, 2);
			offendingConstituents.set(0, first);
			offendingConstituents.set(1, second);
			agreement.setOffendingConstituents(offendingConstituents);
		}
		return agreement;
	}
	
	/*
	 * computes agreement between two phrases
	 */
	protected Agreement computeAgreement(Phrase first, Phrase second, String label) throws CASException {
		
		Agreement agreementFirst = first.getAgreement();
		Agreement agreementSecond = second.getAgreement();
		JCas jcas = first.getCAS().getJCas();
		Agreement agreement =  evaluatePhraseAgreement(agreementFirst, agreementSecond);
			agreement.setLabel(label);
			if ("error".equals(agreement.getPerson()) || "error".equals(agreement.getNumber())
					|| "error".equals(agreement.getGender())) {
				FSArray offendingConstituents = new FSArray(jcas, 2);
				offendingConstituents.set(0, first);
				offendingConstituents.set(1, second);
				agreement.setOffendingConstituents(offendingConstituents);
			}
		
		return agreement;
	}
	
	protected Agreement evaluatePhraseAgreement(Agreement first, Agreement second) throws CASException {
		Agreement result = new Agreement(first.getCAS().getJCas());
		if (first == null && second != null) {
			result.setPerson(second.getPerson());
			result.setNumber(second.getNumber());
			result.setGender(second.getGender());
		} else if (first != null && second == null) {
			result.setPerson(first.getPerson());
			result.setNumber(first.getNumber());
			result.setGender(first.getGender());
		} else if (first == null && second == null) {
			return null;
		}
		
		result.setPerson(evaluateFeatureAgreement(first.getPerson(), second.getPerson()));
		result.setNumber(evaluateFeatureAgreement(first.getNumber(), second.getNumber()));
		result.setGender(evaluateFeatureAgreement(first.getGender(), second.getGender()));
		return result;
	}
	
	/*
	 * helper method to compare feature values
	 */
	protected String evaluateFeatureAgreement(String featVal1, String featVal2) {
		if (featVal1 == null) {
			return featVal2;
		}
		if (featVal2 == null) {
			return featVal1;
		}
		if (featVal1.equals(featVal2)) {
			return featVal1;
		}
		if (featVal1.contains(featVal2)) {
			return featVal2;
		}
		if (featVal2.contains(featVal1)) {
			return featVal1;
		}
		return "error";
	}
	
	protected static void registerRule(String symbol, Rule rule) {
		ArrayList<Rule> ruleList = ruleRegistry.get(symbol);
		if (ruleList == null) {
			ruleList = new ArrayList<Rule>();
			ruleRegistry.put(symbol, ruleList);
		}
		ruleList.add(rule);
	}
	
	protected Rule applyRules(String symbol, Phrase p, String agreementLabel) throws CASException {
		ArrayList<Rule> ruleList = ruleRegistry.get(symbol);
		if (ruleList == null) {
			return null;
		}
		for (Rule r : ruleList) {
			if (r.apply(p, agreementLabel)) {
				return r;
			}
		}
		return null;
	}
	
	protected Rule applyRules(String[] symbols, Phrase p, String agreementLabel) throws CASException {
		for (String s : symbols) {
			Rule r = applyRules(s, p, agreementLabel);
			if (r != null) {
				return r;
			}
		}
		return null;
	}
	
	protected void annotateGlobally(Agreement a, AnalysisResults ar) throws CASException {
		NonEmptyFSList agreementResults = ar.getAgreementResults();
		JCas jcas = ar.getView().getJCas();
		if (agreementResults == null) {
			agreementResults = new NonEmptyFSList(jcas);
		}
		agreementResults = UimaListHelper.addToFSList(jcas, agreementResults, a);
		ar.setAgreementResults(agreementResults);
	}
	
	protected void annotateGlobally(Agreement[] agreements, AnalysisResults ar) throws CASException {
		for (Agreement a : agreements) {
			annotateGlobally(a, ar);
		}
	}
	
}
