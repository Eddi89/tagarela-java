package de.tuebingen.sfs.tagarela.annotators;

import java.util.Iterator;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;

import de.tuebingen.sfs.tagarela.types.Token;
import andyr.jtokeniser.BreakIteratorTokeniser;

/*
 * Simple portuguese tokenizer class for testing. Inspired by the simple tokenizer
 * in the new WERTi system by Aleks Dimitrov
 */
public class NaivePTTokenizer extends JCasAnnotator_ImplBase {

	private static final Logger logger = Logger.getLogger("AnalysisServlet");
	
	@SuppressWarnings("unchecked")
	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		try {
			Iterator<JCas> viewIter = jcas.getViewIterator();
			
			while (viewIter.hasNext()) {
				JCas view = viewIter.next();
				if (view == jcas) {
					continue;
				}
				String text = view.getDocumentText();
				logger.info("Analyzing text: " + text);
				BreakIteratorTokeniser tokeniser = new BreakIteratorTokeniser(text, new Locale("pt", "BR"));
				
				int offset = 0;
				while (tokeniser.hasMoreTokens()) {
					String tok = tokeniser.nextToken();
					Token token = new Token(view);
					offset = text.indexOf(tok, offset);
					int start = offset;
					offset += tok.length();
					int end = offset;
					token.setBegin(start);
					token.setEnd(end);
					token.addToIndexes(view);
				}
			}
			
		} catch (CASException e) {
			throw new AnalysisEngineProcessException(e);
		}
		
	}

}
