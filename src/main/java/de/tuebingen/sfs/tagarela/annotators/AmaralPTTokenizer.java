package de.tuebingen.sfs.tagarela.annotators;

import java.util.Iterator;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.json.JSONArray;
import org.json.JSONException;

import de.tuebingen.sfs.tagarela.resources.PortugueseTokenization;
import de.tuebingen.sfs.tagarela.types.LexiconInfo;
import de.tuebingen.sfs.tagarela.types.Token;
import de.tuebingen.sfs.tagarela.util.Tools;

/**
 * Tokenizer implementation that loosely follows the TAGARELA implementation.
 * @author rziai
 *
 */
public class AmaralPTTokenizer extends JCasAnnotator_ImplBase {

	private static Pattern wordPattern = Pattern.compile("[0-9\\p{L}\\.-]+");
	private static Locale locale = new Locale("pt", "BR");
	
	private static final String ABBREV_TAG = "abrev";
	
	@SuppressWarnings("unchecked")
	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		try {
			Iterator<JCas> viewIter = jcas.getViewIterator();
			
			while (viewIter.hasNext()) {
				JCas currentCas = viewIter.next();
				String docText = currentCas.getDocumentText();
				Matcher m = wordPattern.matcher(docText);
				
				while (m.find()) {
					int start = m.start();
					int end = m.end();
					String match = m.group();
					Token t = new Token(currentCas);
					t.setBegin(start);
					
					if (PortugueseTokenization.abbreviations.has(
						match.toLowerCase(locale))) {
						// abbreviation - set pos tag
						t.setCategory(ABBREV_TAG);
					} else {
						// found a dot but no abbreviation, must be
						// end of sentence, not part of the token
						if (match.charAt(match.length()-1) == '.') {
							end = end-1;
							match = match.substring(0, match.length()-1);
						}
					}
					
					// we can set the end now
					t.setEnd(end);
					
					if (match.matches("[0-9]+")) {
						LexiconInfo lexInfo = new LexiconInfo(currentCas);
						lexInfo.setCanonic(match);
						lexInfo.setPos("numeral");
						lexInfo.setSource("tokn");
						t.setCategory("numeral");
						FSArray defs = new FSArray(currentCas,1);
						defs.set(0, lexInfo);
						t.setLexDef(defs);
					}
					
					if (match.contains("-")) {
						// encliticization - analyze particle and verb
						Iterator<String> keyIter = PortugueseTokenization.prons.keys();
						while (keyIter.hasNext()) {
							String ending = keyIter.next();
							if (match.endsWith(ending)) {
								FSArray deepForm = createDeepCliticAnalysis(match, currentCas);
								t.setDeepForm(deepForm);
								// set start and end for deep tokens
								int middle = match.indexOf('-');
								t.getDeepForm(0).setBegin(start);
								t.getDeepForm(0).setEnd(start + middle);
								t.getDeepForm(1).setBegin(start+middle+1);
								t.getDeepForm(1).setEnd(end);
								break;
							}
						}
					}
					
					if (PortugueseTokenization.prepositions.has(
							match.toLowerCase(locale))) {
						// contracted prepositions
						FSArray deepForm = createDeepPrepAnalysis(match.toLowerCase(locale), currentCas);
						t.setDeepForm(deepForm);
						// set start and end for deep tokens
						for (int i = 0; i < deepForm.size(); i++) {
							t.getDeepForm(i).setBegin(start);
							t.getDeepForm(i).setEnd(end);
						}
					}
					
					t.setTokenString(docText.substring(start, end));
					t.addToIndexes(currentCas);
				}
			}
		} catch (Exception e) {
			throw new AnalysisEngineProcessException(e);
		}
		
	}
	
	/*
	 * Create deep analysis data structure for tokens with encliticization
	 */
	private FSArray createDeepCliticAnalysis(String cliticMatch, JCas jcas) throws JSONException {
		// deep analysis will have two parts
		FSArray arr = new FSArray(jcas, 2);
		
		// take care of the verb part
		String verbPart = cliticMatch.substring(0, cliticMatch.indexOf('-'));
		String baseForm = toBaseForm(verbPart);
		Token verbToken = new Token(jcas);
		verbToken.setTokenString(baseForm);
		FSArray verbLexArray = new FSArray(jcas, 1);
		LexiconInfo verbInfo = new LexiconInfo(jcas);
		verbInfo.setPos("verb");
		verbInfo.setSource("tokn");
		verbLexArray.set(0, verbInfo);
		verbToken.setLexDef(verbLexArray);
		verbToken.setCategory("verb");
		
		// the pronoun part
		Token pronToken = new Token(jcas);
		String pronPart = cliticMatch.substring(cliticMatch.indexOf('-'));
		JSONArray entry = PortugueseTokenization.prons.getJSONArray(pronPart);
		pronToken.setTokenString(entry.getString(0));
		JSONArray lexForms = entry.getJSONArray(1);
		FSArray pronLexArray = new FSArray(jcas, lexForms.length());
		for (int i = 0; i < lexForms.length(); i++) {
			pronLexArray.set(i, Tools.toLexInfo(lexForms.getJSONObject(i), jcas, "tokn"));
		}
		pronToken.setLexDef(pronLexArray);
		if (pronLexArray.size() == 1) {
			pronToken.setCategory(((LexiconInfo)pronLexArray.get(0)).getPos());
		}
		arr.set(0, verbToken);
		arr.set(1, pronToken);
		return arr;
	}
	
	/*
	 * Create deep analysis for tokens with contractions
	 */
	private FSArray createDeepPrepAnalysis(String prepMatch, JCas jcas) throws JSONException {
		FSArray arr = new FSArray(jcas, 2);
		JSONArray entry = PortugueseTokenization.prepositions.getJSONArray(prepMatch);

		for (int a = 0; a < entry.length(); a++) {
			Token deepToken = new Token(jcas);
			JSONArray deepEntry = entry.getJSONArray(a);
			deepToken.setTokenString(deepEntry.getString(0));
			JSONArray deepLexForms = deepEntry.getJSONArray(1);
			FSArray deepLexArray = new FSArray(jcas, deepLexForms.length());
			for (int i = 0; i < deepLexForms.length(); i++) {
				deepLexArray.set(i, Tools.toLexInfo(deepLexForms
						.getJSONObject(i), jcas, "tokn"));
			}
			// unambiguous? set POS
			if (deepLexArray.size() == 1) {
				deepToken.setCategory(((LexiconInfo)deepLexArray.get(0)).getPos());
			}
			deepToken.setLexDef(deepLexArray);
			arr.set(a, deepToken);
		}
		
		return arr;
	}

	/*
	 * helper method that applies very rudimentary morphological analysis to derive verb baseforms
	 */
	private String toBaseForm(String verbPart) {
		char lastChar = verbPart.charAt(verbPart.length()-1);
		String result = verbPart.substring(0, verbPart.length()-1);
		
		if (lastChar == 'á') {
			result += "ar";
		} else if (lastChar == 'ê') {
			result += "er";
		} else if (lastChar == 'í') {
			result += "ir";
		} else {
			result = verbPart;
		}
		
		return result;
	}
}
