package de.tuebingen.sfs.tagarela.annotators;

import java.util.Iterator;
import java.util.List;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.StringArray;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

import com.stibocatalog.hunspell.Hunspell;
import com.stibocatalog.hunspell.Hunspell.Dictionary;

import de.tuebingen.sfs.tagarela.resources.NILCLexicon;
import de.tuebingen.sfs.tagarela.types.Spelling;
import de.tuebingen.sfs.tagarela.types.Token;

/**
 * Spell checker inplementation based on Hunspell. Uses the dictionary from the BrOffice project.
 * Additionally checks words against the NILC lexicon.
 * @author rziai
 *
 */
public class HunSpellChecker extends JCasAnnotator_ImplBase {

	private Logger logger;
	
	private String dictionaryBase;
	private Dictionary dict;
	
	

	/* (non-Javadoc)
	 * @see org.apache.uima.analysis_component.AnalysisComponent_ImplBase#initialize(org.apache.uima.UimaContext)
	 */
	@Override
	public void initialize(UimaContext context)
			throws ResourceInitializationException {
		super.initialize(context);
		logger = context.getLogger();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void process(JCas cas) throws AnalysisEngineProcessException {
		// dictionary base directory
		dictionaryBase = (String) getContext().getConfigParameterValue("dictionaryBase");
		try {
			// create spell checker instance
			dict = Hunspell.getInstance().getDictionary(dictionaryBase);
		} catch (Exception e) {
			throw new AnalysisEngineProcessException(e);
		}
		
		try {
			Iterator<JCas> viewIter = cas.getViewIterator();
			// iterate over views
			while (viewIter.hasNext()) {
				JCas view = viewIter.next();
				AnnotationIndex index = view.getAnnotationIndex(Token.type);
				FSIterator iter = index.iterator();
				// iterate over tokens
				while (iter.hasNext()) {
					Token token = (Token) iter.next();
					String tok = token.getCoveredText();
					// check spelling
					boolean misspelled = misspelled(tok);
					// spelling analysis data structure
					Spelling spelling = new Spelling(view);
					spelling.setMisspelled(misspelled);
					if (misspelled) {
						// get suggestions
						List<String> suggestList = dict.suggest(tok);
						logger.log(Level.INFO, "Suggestions found for " + tok + ": " + suggestList.toString());
						StringArray suggestArray = new StringArray(view, suggestList.size());
						int i = 0;
						for (String suggestion : suggestList) {
							suggestArray.set(i, suggestion);
							i++;
						}
						//... and store them
						spelling.setSuggestions(suggestArray);
					}
					// store spelling analysis
					token.setSpelling(spelling);
				}
			}
		} catch (Exception e) {
			throw new AnalysisEngineProcessException(e);
		}
		
		
	}

	@Override
	public void destroy() {
		super.destroy();
		// make sure our Hunspell instance finds the way to the trash
		// as it's really a C library, it doesn't get garbage-collected
		try {
			Hunspell.getInstance().destroyDictionary(dictionaryBase);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	 * helper that asks spell checker AND lexicon
	 */
	private boolean misspelled(String tok) throws Exception {
		return dict.misspelled(tok) && NILCLexicon.getInstance().get(tok).isEmpty();
	}
	

}
