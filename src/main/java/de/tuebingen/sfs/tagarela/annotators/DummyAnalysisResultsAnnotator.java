package de.tuebingen.sfs.tagarela.annotators;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;

import de.tuebingen.sfs.tagarela.types.AnalysisResults;

/**
 * Shortest annotator ever. Its only task is to create an instance of
 * <code>AnalysisResults</code> that we can fill in subsequent processing.
 * @author rziai
 *
 */
public class DummyAnalysisResultsAnnotator extends JCasAnnotator_ImplBase {

	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		AnalysisResults ar = new AnalysisResults(jcas);
		ar.addToIndexes(jcas);
	}

}
