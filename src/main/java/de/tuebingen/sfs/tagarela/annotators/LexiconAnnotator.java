package de.tuebingen.sfs.tagarela.annotators;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.json.JSONObject;

import de.tuebingen.sfs.tagarela.resources.NILCLexicon;
import de.tuebingen.sfs.tagarela.types.LexiconInfo;
import de.tuebingen.sfs.tagarela.types.Token;
import de.tuebingen.sfs.tagarela.util.Tools;

/**
 * Lexicon lookup implementation. Uses the NILC lexicon for full-form
 * lookup of tokens.
 * @author rziai
 *
 */
public class LexiconAnnotator extends JCasAnnotator_ImplBase {

	private Logger logger;
	private Locale locale = new Locale("pt", "BR");
	
	
	
	/* (non-Javadoc)
	 * @see org.apache.uima.analysis_component.AnalysisComponent_ImplBase#initialize(org.apache.uima.UimaContext)
	 */
	@Override
	public void initialize(UimaContext context)
			throws ResourceInitializationException {
		super.initialize(context);
		logger = context.getLogger();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		
		try {
			Iterator<JCas> casIter = jcas.getViewIterator();
			// iterate over views
			while (casIter.hasNext()) {
				JCas currentjcas = casIter.next();
				
				FSIterator iter = currentjcas.getAnnotationIndex(Token.type)
						.iterator();
				
				// iterate over tokens
				while (iter.hasNext()) {
					Token t = (Token) iter.next();
					String tag = t.getCategory();

					// if POS is set
					if (tag != null) {
						// annotate corresponding entry
						annotateKnownPOS(t, currentjcas);
					} else {
						// if not, annotate all
						annotateUnknownPOS(t, currentjcas);
					}

				}
			}
		} catch (Exception e) {
			throw new AnalysisEngineProcessException(e);
		}
	}
	
	/*
	 * annotates tokens with known POS
	 */
	private void annotateKnownPOS(Token t, JCas jcas) throws Exception {
		// already got lexicon information? job's done
		if (t.getLexDef() != null) {
			return;
		}
		NILCLexicon lexicon = NILCLexicon.getInstance();
		List<JSONObject> defs = lexicon.get(t.getCoveredText().toLowerCase(locale));
		String tag = t.getCategory();
		JSONObject correctDef = null;
		
		for (JSONObject def : defs) {
			if (def.getString("pos").equals(tag)) {
				correctDef = def;
				break;
			}
		}
		
		// didn't find matching definition? annotate all
		if (correctDef == null) {
			annotateUnknownPOS(t, jcas);
			return;
		}
		
		LexiconInfo lexInfo = Tools.toLexInfo(correctDef, jcas, "lexicon");
		FSArray lexArray = new FSArray(jcas, 1);
		lexArray.set(0, lexInfo);
		t.setLexDef(lexArray);
	}
	
	/*
	 * annotates tokens with unknown POS
	 */
	private void annotateUnknownPOS(Token t, JCas jcas) throws Exception {
		logger.log(Level.INFO, "Unknown POS for token '" + t.getCoveredText() + "', asking lexicon");
		NILCLexicon lexicon = NILCLexicon.getInstance();
		List<JSONObject> defs = lexicon.get(t.getCoveredText().toLowerCase(locale));
		FSArray arr = new FSArray(jcas, defs.size());
		int i = 0;
		
		for (JSONObject def : defs) {
			LexiconInfo lexInfo = Tools.toLexInfo(def, jcas, "lexicon");
			arr.set(i, lexInfo);
			i++;
		}
		t.setLexDef(arr);
		
		// only one definition? then we have no ambiguity!
		if (arr.size() == 1) {
			t.setCategory(((LexiconInfo) arr.get(0)).getPos());
		}
	}

}
