package de.tuebingen.sfs.tagarela.annotators.cam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Generic content assessment module implementation. Compares the given
 * lists of strings and computes mappings between individual items.
 * @author rziai
 *
 */
public class CAMMatcher {
	
	/**
	 * Represents a mapping from a source string into a target string.
	 * @author rziai
	 *
	 */
	public class Mapping {
		private String item;
		private int firstPos;
		private int secondPos;
		/**
		 * @param item
		 * @param firstPos
		 * @param secondPos
		 */
		public Mapping(String item, int firstPos, int secondPos) {
			this.item = item;
			this.firstPos = firstPos;
			this.secondPos = secondPos;
		}
		/**
		 * @return the item
		 */
		public String getItem() {
			return item;
		}
		/**
		 * @return the firstPos
		 */
		public int getFirstPos() {
			return firstPos;
		}
		/**
		 * @return the secondPos
		 */
		public int getSecondPos() {
			return secondPos;
		}
		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + firstPos;
			result = prime * result + ((item == null) ? 0 : item.hashCode());
			result = prime * result + secondPos;
			return result;
		}
		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof Mapping))
				return false;
			Mapping other = (Mapping) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (firstPos != other.firstPos)
				return false;
			if (item == null) {
				if (other.item != null)
					return false;
			} else if (!item.equals(other.item))
				return false;
			if (secondPos != other.secondPos)
				return false;
			return true;
		}
		private CAMMatcher getOuterType() {
			return CAMMatcher.this;
		}
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return item + ": " + firstPos + " -> " + secondPos;
		}
		
		
	}

	private List<Mapping> matchedFirst;
	private List<Mapping> unmatchedFirst;
	private List<Mapping> matchedSecond;
	private List<Mapping> unmatchedSecond;
	
	
	
	/*
	 * computes the preferred mapping from a list of mappings
	 */
	private Mapping getBestMapping(List<Mapping> candidates, List<Mapping> alreadyMatched) {
		// we keep the positions already that were already mapped to in the second string here
		ArrayList<Integer> secondStrPos = new ArrayList<Integer>();
		for (Mapping m : alreadyMatched) {
			secondStrPos.add(m.getSecondPos());
		}
		// base case: 2 candidates
		if (candidates.size() == 2) {
			Mapping cand1 = candidates.get(0);
			Mapping cand2 = candidates.get(1);
			int diff1 = Math.abs(cand1.getFirstPos() - cand1.getSecondPos());
			int diff2 = Math.abs(cand2.getFirstPos() - cand2.getSecondPos());
			
			// first criterion: distance between soruce and target positions
			Mapping chosenByDistance = null;
			if (diff1 <= diff2) {
				chosenByDistance = cand1;
			} else {
				chosenByDistance = cand2;
			}
			
			// second criterion: exclude mappings already impossible because of
			// other existing mappings
			Mapping chosenByRedundancy = null;
			if (secondStrPos.contains(cand1.getSecondPos())) {
				chosenByRedundancy = cand2;
			} else if (secondStrPos.contains(cand2.getSecondPos())) {
				chosenByRedundancy = cand1;
			}
			
			// prefer the redundancy choice over the distance choice
			if (chosenByRedundancy == null) {
				return chosenByDistance;
			} else {
				return chosenByRedundancy;
			}
		} else {
			// more than 2, recursive case
			Mapping intermediate = getBestMapping(Arrays.asList(new Mapping[]{ candidates.get(candidates.size()-2), candidates.get(candidates.size()-1)}),
					alreadyMatched);
			ArrayList<Mapping> newCandidates = new ArrayList<Mapping>(candidates.subList(0, candidates.size()-2));
			newCandidates.add(intermediate);
			return getBestMapping(newCandidates, alreadyMatched);
		}
	}
	
	public void computeMatch(List<String> firstSeq, List<String> secondSeq) {
		
		matchedFirst = new ArrayList<Mapping>();
		unmatchedFirst = new ArrayList<Mapping>();
		// from first sequence into second sequence
		for (int i = 0; i < firstSeq.size(); i++) {
			ArrayList<Mapping> candidates = new ArrayList<Mapping>();
			for (int j = 0; j < secondSeq.size(); j++) {
				if (firstSeq.get(i).equals(secondSeq.get(j))) {
					candidates.add(new Mapping(firstSeq.get(i), i, j));
				}
			}
			if (candidates.size() == 1) {
				matchedFirst.add(candidates.get(0));
			} else if(candidates.size() > 1) {
				matchedFirst.add(getBestMapping(candidates, matchedFirst));
			} else {
				unmatchedFirst.add(new Mapping(firstSeq.get(i), i, -1));
			}
		}
		
		matchedSecond = new ArrayList<Mapping>();
		unmatchedSecond = new ArrayList<Mapping>();
		// from second sequence into first sequence
		for (int i = 0; i < secondSeq.size(); i++) {
			ArrayList<Mapping> candidates = new ArrayList<Mapping>();
			for (int j = 0; j < firstSeq.size(); j++) {
				if (secondSeq.get(i).equals(firstSeq.get(j))) {
					candidates.add(new Mapping(secondSeq.get(i), i, j));
				}
			}
			if (candidates.size() == 1) {
				matchedSecond.add(candidates.get(0));
			} else if(candidates.size() > 1) {
				matchedSecond.add(getBestMapping(candidates, matchedSecond));
			} else {
				unmatchedSecond.add(new Mapping(secondSeq.get(i), i, -1));
			}
		}
		
		
		
	}

	/**
	 * @return the matchedFirst
	 */
	public List<Mapping> getMatchedFirst() {
		return matchedFirst;
	}

	/**
	 * @return the unmatchedFirst
	 */
	public List<Mapping> getUnmatchedFirst() {
		return unmatchedFirst;
	}

	/**
	 * @return the matchedSecond
	 */
	public List<Mapping> getMatchedSecond() {
		return matchedSecond;
	}

	/**
	 * @return the unmatchedSecond
	 */
	public List<Mapping> getUnmatchedSecond() {
		return unmatchedSecond;
	}
}
