package de.tuebingen.sfs.tagarela.annotators.agreement;

import org.apache.uima.cas.CASException;

import de.tuebingen.sfs.tagarela.types.Agreement;
import de.tuebingen.sfs.tagarela.types.Phrase;
import de.tuebingen.sfs.tagarela.util.Tools;

/**
 * Checks agreement on coordinated NPs.
 * @author rziai
 *
 */
public class CoordNPRule extends Rule {
	
	public CoordNPRule() {
		super(new String[]{ "NP" });
	}

	@Override
	public boolean apply(Phrase p, String agreementLabel) throws CASException {
		if (!anchorSymbols.contains(p.getCategory())) {
			return false;
		}
		if (p.getChildren().size() != 2) {
			return false;
		}
		
		if (!(p.getChildren(1) instanceof Phrase)) {
			return false;
		}
		
		Phrase right = (Phrase) p.getChildren(1);
		if (!"NPCoord".equals(right.getCategory())) {
			return false;
		}
		Phrase left = (Phrase) p.getChildren(0);
		Phrase otherNP = (Phrase) right.getChildren(1);
		
		Rule leftRule = applyRules("NP", left, "NPcoordLeft-" + agreementLabel);
		Rule rightRule = applyRules("NP", otherNP, "NPcoordRight-" + agreementLabel);
		
		if (leftRule == null || rightRule == null) {
			return false;
		}
		
		Agreement a = computeAgreement(left, otherNP, agreementLabel);
		p.setAgreement(a);
		annotateGlobally(a, Tools.getAnalysisResults(p.getCAS().getJCas()));
		return true;
	}

}
