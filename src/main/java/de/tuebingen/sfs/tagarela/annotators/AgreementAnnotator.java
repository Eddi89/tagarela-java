package de.tuebingen.sfs.tagarela.annotators;

import java.util.ArrayList;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

import de.tuebingen.sfs.tagarela.annotators.agreement.ComplexNPRule;
import de.tuebingen.sfs.tagarela.annotators.agreement.CoordNPRule;
import de.tuebingen.sfs.tagarela.annotators.agreement.Rule;
import de.tuebingen.sfs.tagarela.annotators.agreement.SimpleNPRule;
import de.tuebingen.sfs.tagarela.annotators.agreement.SingleNRule;
import de.tuebingen.sfs.tagarela.annotators.agreement.GeneralSRule;
import de.tuebingen.sfs.tagarela.types.Phrase;

/**
 * Agreement checker implementation. Uses different subclasses of <code>Rule</code> to
 * check constraints on parse trees.
 * @author rziai
 *
 */
public class AgreementAnnotator extends JCasAnnotator_ImplBase {

	private Logger logger;
	private ArrayList<Rule> rules;
	private String[][] labelPriorities;
	
	/* (non-Javadoc)
	 * @see org.apache.uima.analysis_component.AnalysisComponent_ImplBase#initialize(org.apache.uima.UimaContext)
	 */
	@Override
	public void initialize(UimaContext context)
			throws ResourceInitializationException {
		super.initialize(context);
		logger = context.getLogger();
		labelPriorities = new String[][]{ {"S", "subj-verb"}, {"VP", "vp-only"}, {"NP", "np-only"} };
		rules = new ArrayList<Rule>();
		rules.add(new SingleNRule());
		rules.add(new SimpleNPRule());
		rules.add(new ComplexNPRule());
		rules.add(new CoordNPRule());
		rules.add(new GeneralSRule());
	}

	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		try {
				JCas currentCas = jcas.getView("_InitialView");
				// get an iterator over Phrases
				FSIterator constIter = currentCas.getAnnotationIndex(Phrase.type).iterator();
				// record indices so that we only consider the biggest subtrees
				int begin = -1, end = -1;
				// collect candidates here
				ArrayList<Phrase> longestPhrases = new ArrayList<Phrase>();
				
				// iterate over the Phrases
				while (constIter.hasNext()) {
					Phrase p = (Phrase) constIter.next();
					if (begin == -1 || end == -1) {
						// first Phrase
						begin = p.getBegin();
						end = p.getEnd();
					} else if (p.getBegin() != begin || p.getEnd() != end) {
						// last Phrase, we're done
						break;
					}
					longestPhrases.add(p);
				}
				// only got one? go annotate it
				if (longestPhrases.size() == 1) {
					annotateAgreement(longestPhrases.get(0));
				// more than one? choose the one we want according to the priority list
				} else if (longestPhrases.size() > 0){
					annotateAgreement(chooseBest(longestPhrases));
				}
		} catch (Exception e) {
			throw new AnalysisEngineProcessException(e);
		}
	}
	
	/*
	 * Chooses the best tree for annotation according to the priority list
	 */
	private Phrase chooseBest(ArrayList<Phrase> longestPhrases) {
		for (String[] entry : labelPriorities) {
			Phrase found = null;
			for (Phrase p : longestPhrases) {
				if (entry[0].equals(p.getCategory())) {
					found = p;
					break;
				}
			}
			if (found != null) {
				return found;
			}
		}
		return longestPhrases.get(0);
	}
	
	/*
	 * Real agreement annotation starts here
	 */
	private void annotateAgreement(Phrase p) throws CASException {
		String label = null;
		for (String[] entry : labelPriorities) {
			if (entry[0].equals(p.getCategory())) {
				label = entry[1];
				break;
			}
		}
		logger.log(Level.INFO, "Applying rules for " + p.getCategory());
		for (Rule r : rules) {
			// one a rule applied, we stop
			if (r.apply(p, label)) {
				break;
			}
		}
	}

}
