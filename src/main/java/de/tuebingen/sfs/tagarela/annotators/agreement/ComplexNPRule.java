package de.tuebingen.sfs.tagarela.annotators.agreement;

import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.cas.FSArray;

import de.tuebingen.sfs.tagarela.types.Agreement;
import de.tuebingen.sfs.tagarela.types.Phrase;
import de.tuebingen.sfs.tagarela.types.Token;
import de.tuebingen.sfs.tagarela.util.Tools;

/**
 * Checks agreement on complex NPs, e.g. NPs that have a depth > 1.
 * @author rziai
 *
 */
public class ComplexNPRule extends Rule {

	public ComplexNPRule() {
		super(new String[]{ "NP", "Nbar"});
	}
	
	@Override
	public boolean apply(Phrase p, String agreementLabel) throws CASException {
		if (!anchorSymbols.contains(p.getCategory())) {
			return false;
		}
		
		if (p.getChildren().size() != 2) {
			return false;
		}
		
		if (!(p.getChildren(1) instanceof Phrase)) {
			return false;
		}
		Token left;
		if (p.getChildren(0) instanceof Token) {
			left = (Token) p.getChildren(0);
		} else if (p.getChildren(0) instanceof Phrase
				&& ((Phrase)p.getChildren(0)).getChildren().size() == 1) {
			Phrase leftP = (Phrase) p.getChildren(0);
			left = (Token) leftP.getChildren(0);
		} else {
			return false;
		}
		
		Phrase right = (Phrase) p.getChildren(1);
		if (!anchorSymbols.contains(right.getCategory())) {
			// exception for single APs
			if ("AP".equals(right.getCategory()) && right.getChildren().size() == 1) {
				Token adj = (Token) right.getChildren(0);
				Agreement agr = new Agreement(p.getCAS().getJCas());
				agr.setLabel("adj-NP");
				agr.setNumber(evaluateFeatureAgreement(adj.getLexDef(0).getNumber(),
						left.getLexDef(0).getNumber()));
				agr.setGender(evaluateFeatureAgreement(adj.getLexDef(0).getGender(),
						left.getLexDef(0).getGender()));
				if ("error".equals(agr.getNumber()) || "error".equals(agr.getGender())) {
					FSArray offendingConstituents = new FSArray(p.getCAS().getJCas(), 2);
					offendingConstituents.set(0, left);
					offendingConstituents.set(1, adj);
					agr.setOffendingConstituents(offendingConstituents);
				}
				p.setAgreement(agr);
				annotateGlobally(agr, Tools.getAnalysisResults(p.getCAS().getJCas()));
				return true;
			} else {
				return false;
			}
		} else {
			Rule r = applyRules(anchorSymbols.toArray(new String[anchorSymbols.size()]), right, "sub-" + agreementLabel);
			Agreement rightAgr = right.getAgreement();
			if (rightAgr != null) {
				Agreement mainAgr = new Agreement(p.getCAS().getJCas());
				mainAgr.setLabel("complex-NP");
				mainAgr.setNumber(evaluateFeatureAgreement(left.getLexDef(0).getNumber(),
						rightAgr.getNumber()));
				mainAgr.setGender(evaluateFeatureAgreement(left.getLexDef(0).getGender(),
						rightAgr.getGender()));
				if ("error".equals(mainAgr.getNumber()) || "error".equals(mainAgr.getGender())) {
					FSArray offendingConstituents = new FSArray(p.getCAS().getJCas(), 2);
					offendingConstituents.set(0, left);
					offendingConstituents.set(1, right);
					mainAgr.setOffendingConstituents(offendingConstituents);
				}
				p.setAgreement(mainAgr);
				annotateGlobally(mainAgr, Tools.getAnalysisResults(p.getCAS().getJCas()));
				return true;
			} else {
				return false;
			}
		}
	}

}
