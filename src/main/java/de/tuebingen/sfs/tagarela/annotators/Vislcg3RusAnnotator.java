package de.tuebingen.sfs.tagarela.annotators;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;

import de.tuebingen.sfs.tagarela.types.CGReading;
import de.tuebingen.sfs.tagarela.types.CGToken;
import de.tuebingen.sfs.tagarela.types.Token;

/**
 * Annotate a text using the external tools - hfst-based morph. analyser and vislcg3 
 * shallow syntactic parser. The locations of vislcg3 and the grammar are 
 * provided by the activity.
 * 
 * @author Niels Ott?
 * @author Adriane Boyd
 * @author Heli Uibo
 * @author Eduard Schaf
 *
 */
public class Vislcg3RusAnnotator extends JCasAnnotator_ImplBase {

	private static final Logger log =
			Logger.getLogger(Vislcg3RusAnnotator.class);

	//private final String CGSentenceBoundaryToken = ".";
	private String vislcg3Loc;
	private String vislcg3DisGrammarLoc;
	private String vislcg3SyntGrammarLoc; // currently not available

	//local paths:

	//	private final String preprocessLoc = "/usr/bin/perl" + "./rus_resources/preprocess"; // deactivated
	//	private final String hfstOptLookupLoc = "/usr/local/bin/hfst-optimized-lookup";
	//	private final String lookupFlags = "-q"; // "-q" = do not print output // not possible for the jar
	//	private final String lookup2cgLoc = "/usr/bin/perl " + "./rus_resources/lookup2cg"; // replaced by cg-conv
	private final String cgConvLoc = "cg-conv";
	private final String loadJar = "java -jar";
	private final String jarOptLookupLoc = "./rus_resources/hfst-ol.jar";
	private final String optHfstLoc = "./rus_resources/analyser-gt-desc.ohfst";
	
	private int answerPosition = 0;

	@Override
	public void initialize(UimaContext context)
			throws ResourceInitializationException {
		super.initialize(context);
		vislcg3Loc = (String) context.getConfigParameterValue("vislcg3Loc");
		vislcg3DisGrammarLoc = (String) context.getConfigParameterValue("vislcg3DisGrammarLoc");
		vislcg3SyntGrammarLoc = (String) context.getConfigParameterValue("vislcg3SyntGrammarLoc");
	}

	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		log.debug("Starting vislcg3 processing");

		final long startTime = System.currentTimeMillis();



//		 collect original tokens here
		ArrayList<Token> originalTokens = new ArrayList<Token>();
		FSIterator tokenIter = jcas.getAnnotationIndex(Token.type).iterator();
		while (tokenIter.hasNext()) {
			originalTokens.add((Token) tokenIter.next());
		}



		try {
			JCas fullAnswerView = jcas.getView("fullAnswer");

			// collect original tokens here
			String[] fullAnswerTokens = fullAnswerView.getDocumentText().split("\\s");
			
			JCas answerPositionView = jcas.getView("answerPosition");
			
			answerPosition = Integer.parseInt(answerPositionView.getDocumentText());
			
			log.info("answerPosition=" + answerPosition);
			
			// don't process input when no answer was provided
			if(fullAnswerTokens[answerPosition-1].isEmpty()){
				return;
			}

			// convert token list to cg input
			String cg3input = toCG3Input(fullAnswerTokens);
			// run vislcg3
			log.info("running vislcg3");
			String cg3output = runFST_CG(cg3input);  // was: runVislCG3(cg3input)

			log.info("parsing CG output");
			List<CGToken> newTokens = parseCGOutput(cg3output, jcas);

			//			if (newTokens.size() == 0) {
			//				throw new IllegalArgumentException("CG3 output is empty!"); 
			//			}

			// assert that we got as many tokens back as we provided
			if (newTokens.size() != originalTokens.size()) {
				throw new IllegalArgumentException("Token list size mismatch: " +
						"Original tokens: " + originalTokens.size() + ", After CG3: " + newTokens.size()); 
			}

			log.info("Number of original tokens:"+originalTokens.size());

			log.info("Number of new tokens:"+newTokens.size());

			// complete new tokens with information from old ones
			for (int i = 0; i < originalTokens.size(); i++) {
				Token origT = originalTokens.get(i);
				CGToken newT = newTokens.get(i); 
				log.info("Current surface form=" + origT.getCoveredText());
				log.info("Number of readings= " + newT.getReadings().size());
				for(int j = 0; j < newT.getReadings().size(); j++){
					CGReading currentReading = newT.getReadings(j);
					log.info("Reading " + (j+1) + ":");
					log.info("Lemma=" + currentReading.getLemma());
					log.info("Tag=" + currentReading.getTag());
				}
				//				String reading = "";
				//				if(newT.getReadings().size() > 0){
				//					reading = newT.getReadings().get(0).toString();
				//				}
				//				if(i > 1000 && i < 2001){
				//					log.info("Original Token: "+origT.getCoveredText() + " At index =" + i + 
				//					"\tNew Token: "+origT.getCoveredText() + "\t CGToken: " + reading);
				//				}
				//log.info("Token: "+origT.getCoveredText()+" CGToken:"+reading); // testing
				copy(origT, newT);
				//log.info("new token begins at: " + newT.getBegin()); // testing
				// update CAS
				jcas.removeFsFromIndexes(origT);
				jcas.addFsToIndexes(newT);
			}
		} catch (IOException e) {
			throw new AnalysisEngineProcessException(e);
		} catch (IllegalArgumentException e) {
			throw new AnalysisEngineProcessException(e);
		} catch (InterruptedException e) {
			throw new AnalysisEngineProcessException(e);
		} catch (CASException e) {
			// TODO Auto-generated catch block
			throw new AnalysisEngineProcessException(e);
		}

		log.info("Finished visclg3 processing");

		final long endTime = System.currentTimeMillis();

		log.info("Total execution time: " + (endTime - startTime)*0.001 + " seconds." );
	}

	/*
	 * helper for copying over information from Token to CGToken
	 */
	private void copy(Token source, CGToken target) {
		target.setBegin(source.getBegin());
		target.setEnd(source.getEnd());
		// TODO temporary for testing purposes
		target.setSpelling(source.getSpelling());
		target.setDeepForm(source.getDeepForm());
		target.setLexDef(source.getLexDef());
		target.setTokenString(source.getTokenString());
	}

//	/*
//	 * helper for converting Token annotations to a String for vislcg3
//	 */
//	private String toCG3Input(List<Token> tokenList) {
//		StringBuilder result = new StringBuilder();
//
//		for (Token t : tokenList) {
//			String coveredText = t.getCoveredText();
//			result.append(coveredText);
//			result.append("\n"); // each token on a separate line
//			log.info("coveredText=" + coveredText);
//		}
//		//log.info("text to be parsed: "+result.toString());
//		return result.toString();
//	}
	
	/*
	 * helper for converting Token annotations to a String for vislcg3
	 */
	private String toCG3Input(String[] fullAnswerTokens) {
		StringBuilder result = new StringBuilder();

		for (String t : fullAnswerTokens) {
			String coveredText = t;
			result.append(coveredText);
			result.append("\n"); // each token on a separate line
			log.info("coveredText=" + coveredText);
		}
		//log.info("text to be parsed: "+result.toString());
		return result.toString();
	}

	/*
	 * helper for running the pipeline consisting of external tools for morphological analysis 
	 * (FST) + morph. disambiguation + shallow syntactic analysis (CG). 
	 * The preprocessing (tokenisation) is done by OpenNlpTokenizer.
	 */
	private String runFST_CG(String input) throws IOException,InterruptedException {

		// get timestamp in milliseconds and use it in the names of the temporary files 
		// in order to avoid conflicts between simultaneous users                                                                                            
		long timestamp = System.currentTimeMillis();

		String inputfileLoc = "./rus_output/cg3AnalyserInputFiles/cg3AnalyserInput"+timestamp+".tmp";
		String outputfileLoc = "./rus_output/cg3AnalyserOutputFiles/cg3AnalyserOutput"+timestamp+".tmp";

		//create temporary files for saving cg3 input and output                                                   
		File inputfile = new File(inputfileLoc);
		inputfile.createNewFile();
		File outputfile = new File(outputfileLoc);
		outputfile.createNewFile();


		//create an input file object and write input (text to be analyzed) to the file cg3inputXXXXX.tmp
		Writer cg3inputfile = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(inputfileLoc), "UTF-8"));

		try {
			// TODO cg-conv has a bug when no Russian words are included so to be save
			// we include a Russian word when analysing and ignore it later
//			input = ".\n" + input;
			cg3inputfile.write(input);
		}
		finally {
			cg3inputfile.close();
		}

		// compose text analysis pipeline and run a process

		// this was the previous version which required to install hfst on the computer (but it is faster)

		// when reading CG input from a file and writing CG output to another file:
		//        String[] textAnalysisPipeline = {
		//        		"/bin/sh", 
		//        		"-c", 
		//        		"/bin/cat " + inputfileLoc + 
		//        		//" | " + preprocessLoc + // disabled because it's causing miss alignment (removes original tokens)
		//        		" | " + hfstOptLookupLoc + " " + lookupFlags + " " + optHfstLoc + 
		//        		" | " + cgConvLoc + 
		//        		" | " + vislcg3Loc + " -g " + vislcg3DisGrammarLoc +  
		//        		" > " + outputfileLoc};


		// the newer version is using hfst-ol.jar to load the .ohfst files (ol = optimized lookup) 
		// (but it is slower, due to command line use of the jar)
		// this can be improved when you manage to load it within java, using the interface

		// when reading CG input from a file and writing CG output to another file:
		String[] textAnalysisPipeline = {
				"/bin/sh", 
				"-c", 
				"/bin/cat " + inputfileLoc + 
				//" | " + preprocessLoc + // disabled because it's causing miss alignment (removes original tokens)
				" | " + loadJar + " " + jarOptLookupLoc + " " + optHfstLoc +
				" | " + "tail -n+5" + // get rid of the header that hfst-ol.jar produces
				" | " + "cut -f1-2"+ // get rid of the weight
				//" | " + lookup2cgLoc + 
				" | " + cgConvLoc + 
        		" | " + vislcg3Loc + " -g " + vislcg3DisGrammarLoc +  
				" > " + outputfileLoc};


		// There was a problem with the syntactic rules, therefore using only disambiguation rules 
		// right now. Otherwise, the following should be added to the pipeline: 
		// " | " + vislcg3Loc + " -g " + vislcg3SyntGrammarLoc +

		// String[] textAnalysisPipeline = {"/bin/sh", "-c", "/bin/echo \""+ input + "\" | " + 
		// lookupLoc + " "+ lookupFlags + fstLoc + lookup2cgLoc + vislcg3Loc + " -g " + vislcg3GrammarLoc};
		log.info("Text analysis pipeline: "+textAnalysisPipeline[2]);
		Process process = Runtime.getRuntime().exec(textAnalysisPipeline);
		process.waitFor();

		byte[] encoded = Files.readAllBytes(Paths.get(outputfileLoc));

		String result = new String(encoded, StandardCharsets.UTF_8);		

		// delete temporary files:
		inputfile.delete();
		outputfile.delete();

		//        // always keep the latest two input/output files, delete the oldest files 
		//        
		//        File inputFileDir = new File("./rus_output/cg3AnalyserInputFiles/");
		//		File[] inputFiles = inputFileDir.listFiles();
		//		List<File> inputFilesList = new ArrayList<File>(Arrays.asList(inputFiles));
		//		// sort according to last modified date
		//		inputFilesList.sort(new FileComparator());
		//		if(inputFilesList.size() == 3){
		//			// delete the oldest file, this is always the first element in the list
		//			inputFilesList.get(0).delete();
		//		}
		//		
		//		File outputFileDir = new File("./rus_output/cg3AnalyserOutputFiles/");
		//		File[] outputFiles = outputFileDir.listFiles();
		//		List<File> outputFilesList = new ArrayList<File>(Arrays.asList(outputFiles));
		//		// sort according to last modified date
		//		outputFilesList.sort(new FileComparator());
		//		if(outputFilesList.size() == 3){
		//			// delete the oldest file, this is always the first element in the list
		//			outputFilesList.get(0).delete();
		//		}

		return result;
	}

	/*
	 * helper for parsing output from vislcg3 back into our CGTokens
	 */
	private List<CGToken> parseCGOutput(String cgOutput, JCas jcas) throws CASException {
		
		int tokenCount = 0;
		
		ArrayList<CGToken> result = new ArrayList<CGToken>();

		// current token and its readings
		CGToken current = null;
		ArrayList<CGReading> currentReadings = new ArrayList<CGReading>();
		// read output line by line, eat multiple newlines
		String[] cgOutputLines = cgOutput.split("\n+");
		// TODO cg-conv has a bug when no Russian words are included so to be save
		// we include a Russian word when analysing and ignore it here, thats why lineCount = 1
		for (int lineCount = 0; lineCount < cgOutputLines.length; lineCount++) {
			String line = cgOutputLines[lineCount];

			// case 1: new cohort
			if (line.startsWith("\"<")) {
				tokenCount++;
				if(tokenCount == answerPosition){
					if (current != null) {
						// save previous token
						current.setReadings(new FSArray(jcas, currentReadings.size()));
						int i = 0;
						for (CGReading cgr : currentReadings) {
							current.setReadings(i, cgr);
							i++;
						}
						result.add(current);
					}
					// create new token
					current = new CGToken(jcas);
					currentReadings = new ArrayList<CGReading>();
				}
				// case 2: a reading in the current cohort
			} else {
				if(tokenCount == answerPosition){
					CGReading reading = new CGReading(jcas);
					// split reading line into tags
					String[] temp = line.split("\\s+");

					reading.setLemma(temp[1]);

					String tag = "";
					for(int i = 2; i < temp.length; i++){
						tag += "+" + temp[i];
					}

					reading.setTag(tag);

					// add the reading
					currentReadings.add(reading);
				}				
			}
		}
		if (current != null) {
			// save last token
			current.setReadings(new FSArray(jcas, currentReadings.size()));
			int i = 0;
			for (CGReading cgr : currentReadings) {
				current.setReadings(i, cgr);
				i++;
			}
			result.add(current);			
		}
		return result;
	}
}