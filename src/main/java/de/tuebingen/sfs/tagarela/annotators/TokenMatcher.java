package de.tuebingen.sfs.tagarela.annotators;

import java.util.ArrayList;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

import de.tuebingen.sfs.tagarela.annotators.cam.CAMMatcher;
import de.tuebingen.sfs.tagarela.types.AnalysisResults;
import de.tuebingen.sfs.tagarela.types.Token;
import de.tuebingen.sfs.tagarela.types.TokenMatchResult;
import de.tuebingen.sfs.tagarela.util.Tools;

/**
 * Token Matcher implementation. Uses the surface form of learner and
 * target answer to compare them token by token.
 * @author rziai
 *
 */
public class TokenMatcher extends JCasAnnotator_ImplBase {

	private Logger logger;
	
	
	
	/* (non-Javadoc)
	 * @see org.apache.uima.analysis_component.AnalysisComponent_ImplBase#initialize(org.apache.uima.UimaContext)
	 */
	@Override
	public void initialize(UimaContext context)
			throws ResourceInitializationException {
		super.initialize(context);
		logger = context.getLogger();
	}

	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		
		try {
			// collect resulting tokens here
			ArrayList<Token> learnerTokens = new ArrayList<Token>();
			ArrayList<Token> answerTokens = new ArrayList<Token>();
			// ...and the canonic forms here
			ArrayList<String> learnerTokenStrings = new ArrayList<String>();
			ArrayList<String> answerTokenStrings = new ArrayList<String>();
			
			// get a token iterator and go over tokens
			FSIterator tokenIter = jcas.getView("_InitialView").getAnnotationIndex(Token.type).iterator();
			while (tokenIter.hasNext()) {
				Token t = (Token) tokenIter.next();
				learnerTokens.add(t);
				learnerTokenStrings.add(t.getTokenString().toLowerCase());
			}
			
			// same for target answer
			tokenIter = jcas.getView("bestAnswer").getAnnotationIndex(Token.type).iterator();
			while (tokenIter.hasNext()) {
				Token t = (Token) tokenIter.next();
				answerTokens.add(t);
				answerTokenStrings.add(t.getTokenString().toLowerCase());
			}
			
			// construct a CAM matcher
			CAMMatcher matcher = new CAMMatcher();
			// compute the match
			matcher.computeMatch(learnerTokenStrings, answerTokenStrings);
			
			// retrieve analysis results
			AnalysisResults ar = Tools.getAnalysisResults(jcas);
			TokenMatchResult tmr = new TokenMatchResult(ar.getCAS().getJCas());
			
			// set results
			tmr.setMatchedTokens(Tools.toMappingArray(learnerTokens, answerTokens, matcher.getMatchedFirst(), ar.getCAS().getJCas()));
			tmr.setUnmatchedStudentTokens(Tools.toArray(learnerTokens, matcher.getUnmatchedFirst(), ar.getCAS().getJCas()));
			tmr.setUnmatchedAnswerTokens(Tools.toArray(answerTokens, matcher.getUnmatchedSecond(), ar.getCAS().getJCas()));
			
			logger.log(Level.INFO, "Matched student tokens: " + matcher.getMatchedFirst());
			logger.log(Level.INFO, "Unmatched student tokens: " + matcher.getUnmatchedFirst());
			
			logger.log(Level.INFO, "Matched answer tokens: " + matcher.getMatchedSecond());
			logger.log(Level.INFO, "Unmatched answer tokens: " + matcher.getUnmatchedSecond());

			// store in analysis results
			ar.setTokenMatch(tmr);
		} catch (CASException e) {
			throw new AnalysisEngineProcessException(e);
		}
		
	}
	
	

}
