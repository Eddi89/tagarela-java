package de.tuebingen.sfs.tagarela.annotators.agreement;

import org.apache.uima.cas.CASException;

import de.tuebingen.sfs.tagarela.types.Agreement;
import de.tuebingen.sfs.tagarela.types.Constituent;
import de.tuebingen.sfs.tagarela.types.Phrase;
import de.tuebingen.sfs.tagarela.types.Token;
import de.tuebingen.sfs.tagarela.util.Tools;

/**
 * Checks agreement on simple NPs.
 * @author rziai
 *
 */
public class SimpleNPRule extends Rule {

	
	public SimpleNPRule() {
		super(new String[]{ "NP", "Nbar"});
	}

	@Override
	public boolean apply(Phrase p, String agreementLabel) throws CASException {
		if (!anchorSymbols.contains(p.getCategory())) {
			return false;
		}
		if (p.getChildren().size() != 2) {
			return false;
		}
		Constituent firstC = p.getChildren(0);
		Constituent secondC = p.getChildren(1);
		
		if (!(firstC instanceof Token) || !(secondC instanceof Token)) {
			return false;
		}
		Token first = (Token) firstC;
		Token second = (Token) secondC;
		
		Agreement a = computeAgreement(first, second, agreementLabel, "noun");
		p.setAgreement(a);
		annotateGlobally(a, Tools.getAnalysisResults(p.getCAS().getJCas()));
		
		return true;
	}

}
