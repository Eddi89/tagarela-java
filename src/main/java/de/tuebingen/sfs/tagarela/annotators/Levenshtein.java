package de.tuebingen.sfs.tagarela.annotators;

public class Levenshtein {
	
	private String hintAnswer;
	private String correctedAnswer;
	private int editDistance;

	public Levenshtein(String hintAnswer, String correctedAnswer, int editDistance) {
		this.hintAnswer = hintAnswer;
		this.correctedAnswer = correctedAnswer;
		this.editDistance = editDistance;
	}
	
	public Levenshtein() {
		this.hintAnswer = "";
		this.correctedAnswer = "";
		this.editDistance = 0;
	}

	public String getHintAnswer() {
		return hintAnswer;
	}



	public void setHintAnswer(String hintAnswer) {
		this.hintAnswer = hintAnswer;
	}



	public String getCorrectedAnswer() {
		return correctedAnswer;
	}



	public void setCorrectedAnswer(String correctedAnswer) {
		this.correctedAnswer = correctedAnswer;
	}


	public int getEditDistance() {
		return editDistance;
	}

	public void setEditDistance(int editDistance) {
		this.editDistance = editDistance;
	}
	
	

	@Override
	public String toString() {
		return "Levenshtein [hintAnswer=" + hintAnswer + ", correctedAnswer="
				+ correctedAnswer + ", editDistance=" + editDistance + "]";
	}

	/**
	 * Construct the levenshtein matrix.
	 * @param studentAnswer the student's answer.
	 * @param correctAnswer the correct answer.
	 */
	public void createLevMatrix(String studentAnswer, String correctAnswer){
		String[][] levArray = new String[studentAnswer.length() + 1][correctAnswer.length() + 1];;
		
		// increment along the first column of each row
		for(int row = 0; row <= studentAnswer.length(); row++)
		{
			levArray[row][0] = row+"l"; // left edge
		}

		// increment each column in the first row
		for(int column = 0; column <= correctAnswer.length(); column++)
		{
			levArray[0][column] = column + "t"; // top edge
		}

		// Fill in the rest of the matrix
		for(int row = 1; row <= studentAnswer.length(); row++)
		{
			for(int column = 1; column <= correctAnswer.length(); column++)
			{
				// store the cell entries from left, above and above left
				String cellLeft = levArray[row][column - 1];
				String cellAbove = levArray[row - 1][column];
				String cellAboveLeft = levArray[row - 1][column - 1];

				// store the numbers of the cell entries from left, above and above left
				int numLeft = Integer.parseInt(cellLeft.substring(0, cellLeft.length()-1));
				int numAbove = Integer.parseInt(cellAbove.substring(0, cellAbove.length()-1));
				int numAboveLeft = Integer.parseInt(cellAboveLeft.substring(0, cellAboveLeft.length()-1));

				// if the character of the student's answer is equal to 
				// the one of the correct answer, assign the cell entry directly
				if(studentAnswer.charAt(row-1) == correctAnswer.charAt(column-1)){
					levArray[row][column] = numAboveLeft + "e"; // equal
				}
				// otherwise compute the minimum of the remaining options and
				// assign the smallest number to the cell entry
				// note: +1 because edit cost increases by 1
				else {
					int minimum = Math.min(Math.min(numAboveLeft + 1, // substitution
							numLeft + 1), // insertion
							numAbove + 1); // deletion
					if(minimum == numAboveLeft + 1 ){
						levArray[row][column] = minimum + "s"; // substitute
					}
					else if(minimum == numAbove + 1){
						levArray[row][column] = minimum + "d"; // delete 
					}
					else if(minimum == numLeft + 1){
						levArray[row][column] = minimum + "i"; // insert
					}
				}
			}
		}
		// compute the simplest path to the correct answer and update the matrix
		simplestPath(levArray);

		// compute the instructions to get to the correct answer using the simplest path
		instructMe(studentAnswer, correctAnswer, levArray);
		
		// print the table
//		printTable(studentAnswer, correctAnswer, levArray);
		
		// last cell of the lev matrix (bottom right)
		String lastCell = levArray[studentAnswer.length()][correctAnswer.length()];
		
		// store the edit distance
		editDistance = Integer.parseInt(lastCell.substring(0, lastCell.length()-1));
	}
	
	/**
	 * Create the simplest path of changes in the table.
	 * @param levArray The table with the results and changes of the computing.
	 */
	public void simplestPath(String[][] levArray){

		// start at the the last cell (bottom right) to compute the simplest path
		for (int row = levArray.length-1; row >= 0; row--)
		{
			for (int column = levArray[row].length-1; column >= 0; column--)
			{
				// cells of interest become capitalized in the levenshtein matrix
				String currentCell = levArray[row][column].toUpperCase();

				// the first cell and last path entry
				if(currentCell.equals("0T")){
					levArray[row][column] = currentCell;
				}
				// if the path enters the left edge, it means that the student's answer contains
				// redundant character(s) at the begin of the answer, thats why the cell right to 
				// this cell becomes the instruction "D" to delete this character
				// move to the cell above
				else if(currentCell.endsWith("L")){
					levArray[row][column+1] = levArray[row][column+1].toUpperCase().replace("E", "D").replace("S", "D").replace("I", "D");
					row--; 
					column++; // since the column will decrease by 1
				}
				// if the path enters the top edge, it means that the student's answer is missing
				// character(s) at the begin of the answer, thats why the cell below this cell
				// becomes the instruction "I" to insert the missing character
				// move to the cell to the left
				else if(currentCell.endsWith("T")){
					levArray[row+1][column] = levArray[row+1][column].toUpperCase().replace("E", "I").replace("S", "I").replace("D", "I");
				}
				// assign the current cell and move to the cell above left
				else if(currentCell.endsWith("E")||
						currentCell.endsWith("S")){
					levArray[row][column] = currentCell;
					row--;
				}
				// assign the current cell and move to the cell above
				else if(currentCell.endsWith("D")){
					levArray[row][column] = currentCell;
					row--;
					column++; // since column will decrease by 1
				}
				// assign the current cell and move to the cell to the left
				else if(currentCell.endsWith("I")){
					levArray[row][column] = currentCell;
				}
			}
		}
	}
	
	
	/**
	 * Tell the student how to get to the correct answer.
	 * @param student The student's answer.
	 * @param correct The correct answer.
	 * @param levArray The levenshtein matrix.
	 */
	public void instructMe(String student, String correct, String[][] levArray){

		for (int row = 1; row < levArray.length; row++){
			for(int column = 1; column < levArray[row].length; column++){
				String cell = levArray[row][column];
				// only consider cells with upper case letter at the end
				if(Character.isUpperCase(cell.charAt(cell.length()-1))){
					String studentChar = student.charAt(row-1)+"";
					String correctChar = correct.charAt(column-1)+"";
					// do nothing, the given character was correct
					if(cell.endsWith("E")){
						hintAnswer += studentChar;
						correctedAnswer += studentChar;
					}
					// substitute the student's character with the correct character
					// the student's character is marked blue in the html document
					else if(cell.endsWith("S")){
						hintAnswer += "<span name=\""+ "S" + "\" style=\"color:blue\">" + studentChar +"</span>";
						correctedAnswer += correctChar;
					}
					// insert the correct character
					// the place of the correct character is marked with "_" in html
					else if(cell.endsWith("I")){
						hintAnswer += "<span name=\""+ "I" + "\" style=\"background-color:#F4A460\">" + "_" +"</span>";
						correctedAnswer += correctChar;
					}
					// delete the student's character
					// the character to delete appears in read in the html document
					else if(cell.endsWith("D")){
						hintAnswer += "<span name=\""+ "D" + "\" style=\"color:red\">" + studentChar +"</span>";
					}
				}
			}
		}
	}

	/**
     * Print the table with the results of the computing.
     * @param student First word.
     * @param correct second word.
     * @param table The table with the results of the computing.
     * @param chgTable The table with the results and changes of the computing.
     */
    public void printTable(String student, String correct, String[][] chgTable)
    {
        // the words need to be fixed, so that they will be displayed correctly
        String fixedS1 = " " + student;
        String fixedS2 = " " + correct;
        
        System.out.print("    ");
        
        // the second word which is printed above of the table
        for (int i = 0; i < fixedS2.length(); i++)
        {
        	System.out.print(" ");
            System.out.print(fixedS2.charAt(i) + "   ");
        }
        
        System.out.println();
        
        for (int row = 0; row < chgTable.length; row++)
        {
            // the characters of the first word are printed at the beginning
            System.out.print(fixedS1.charAt(row) + " ");
            // then the actual table
            for (int column = 0; column < chgTable[row].length; column++)
            {
            	if(chgTable[row][column].length() < 3){
            		System.out.print(" ");
            	}
                System.out.print("  " + chgTable[row][column]);
            }
            System.out.println();
        }
        
        String result = "The distance between " + student + " and " + correct + " is " +
        		chgTable[student.length()][correct.length()].split("(?<=\\d)(?=\\D)")[0];
        
        System.out.println("\n" + result);
        
    }
}
