package de.tuebingen.sfs.tagarela.annotators.agreement;

import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;

import de.tuebingen.sfs.tagarela.types.Agreement;
import de.tuebingen.sfs.tagarela.types.AnalysisResults;
import de.tuebingen.sfs.tagarela.types.Phrase;
import de.tuebingen.sfs.tagarela.types.Token;
import de.tuebingen.sfs.tagarela.util.Tools;

/**
 * Checks agreement on sentences.
 * @author rziai
 *
 */
public class GeneralSRule extends Rule {

	public GeneralSRule() {
		super(new String[]{ "S" });
	}
	
	@Override
	public boolean apply(Phrase p, String agreementLabel) throws CASException {
		if (!anchorSymbols.contains(p.getCategory())) {
			return false;
		}
		if (p.getChildren().size() != 2
				|| !"NP".equals(p.getChildren(0).getCategory())
				|| !"VP".equals(p.getChildren(1).getCategory())) {
			return false;
		}
		Phrase np = (Phrase) p.getChildren(0);
		Phrase vp = (Phrase) p.getChildren(1);
		
		// get the verb
		Token verb = (Token) Tools.findConstituent("verb", vp);
		
		// make sure the NP is annotated
		Rule r = applyRules(new String[]{"NP", "Nbar"}, np, "np-subj");
		
		if (r == null) {
			return false;
		}
		
		JCas jcas = p.getView().getJCas();
		AnalysisResults ar = Tools.getAnalysisResults(jcas);
		//annotateGlobally(np.getAgreement(), ar);
		
		// subject-verb agreement
		String result = computeSubjVerbAgr(np, verb);
		if (result != null) {
			Agreement subjVerbAgr = new Agreement(jcas);
			subjVerbAgr.setPerson(result);
			subjVerbAgr.setLabel(agreementLabel);
			if ("error".equals(result)) {
				FSArray offendingConstituents = new FSArray(jcas, 2);
				offendingConstituents.set(0, np);
				offendingConstituents.set(1, verb);
				subjVerbAgr.setOffendingConstituents(offendingConstituents);
			}
			
			annotateGlobally(subjVerbAgr, ar);
		}
		
		
		// predicative stuff
		if (vp.getChildren().size() > 1 && (vp.getChildren(1) instanceof Phrase)) {
			Phrase vpCompl = (Phrase) vp.getChildren(1);
			String verbCanonic = verb.getLexDef(0).getCanonic();
			if ("ser".equals(verbCanonic) || "estar".equals(verbCanonic)) {
				if ("NP".equals(vpCompl.getCategory())) {
					Rule npRule = applyRules("NP", vpCompl, "predicative-NP");
					if (npRule != null) {
						// check if VP complement has correct agreement
						// only check for subj-pred agreement if this one is fine
						Agreement vpComplAgr = vpCompl.getAgreement();
						if (Tools.getError(vpComplAgr) != null) {
							annotateGlobally(vpComplAgr, ar);
						} else {
							Agreement agr = computeAgreement(np, vpCompl,
								"subj-pred");
							annotateGlobally(agr, ar);
						}
					}
				} else if ("AP".equals(vpCompl.getCategory())) {
					Token adj = (Token) Tools.findConstituent("adj", vpCompl);
					Agreement agr = new Agreement(jcas);
					agr.setLabel("subj-pred");
					agr.setNumber(evaluateFeatureAgreement(adj.getLexDef(0).getNumber(),
							np.getAgreement().getNumber()));
					agr.setGender(evaluateFeatureAgreement(adj.getLexDef(0).getGender(),
							np.getAgreement().getGender()));
					if ("error".equals(agr.getNumber()) || "error".equals(agr.getGender())) {
						FSArray offendingConstituents = new FSArray(jcas, 2);
						offendingConstituents.set(0, np);
						offendingConstituents.set(1, adj);
						agr.setOffendingConstituents(offendingConstituents);
					}
					annotateGlobally(agr, ar);
				}
			} else if ("NP".equals(vpCompl.getCategory())) {
				applyRules("NP", vpCompl, "NP-in-VP");
			} else if (vpCompl.getChildren().size() == 2 &&
					"NP".equals(vpCompl.getChildren(1).getCategory())) {
				Phrase subNp = (Phrase) vpCompl.getChildren(1);
				applyRules("NP", subNp, "NP-in-VP");
			}
		}
		return true;
	}
	
	private String computeSubjVerbAgr(Phrase np, Token verb) {
		Agreement a = np.getAgreement();
		if (a == null || a.getPerson() == null) {
			return null;
		}
		if (verb.getLexDef().size() == 0
				|| verb.getLexDef(0).getForm() == null) {
			return null;
		}
		if (verb.getLexDef(0).getForm().contains(np.getAgreement().getPerson())) {
			return np.getAgreement().getPerson();
		} else {
			return "error";
		}
	}

}
