package de.tuebingen.sfs.tagarela.annotators.disambiguator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper class that can read disambiguation rules from a file.
 * @author rziai
 *
 */
public class RuleReader {

	/**
	 * Read a list of rules from the given input stream.
	 * @param stream The stream to read from
	 * @return A list of rules
	 * @throws IOException
	 */
	public static List<Rule> readRules(InputStream stream) throws IOException {
		ArrayList<Rule> rules = new ArrayList<Rule>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		
		String line = null;
		while ((line = reader.readLine()) != null) {
			String[] parts = line.split("\t", 4);
			String leftPOSConstraint = parts[0].equals("0") ? null : parts[0];
			String rightPOSConstraint = parts[1].equals("0") ? null : parts[1];
			String toBeSelected = parts[2].equals("0") ? null : parts[2];
			String[] negativeSelectionConstraints = parts[3].equals("0") ?
					new String[0] : parts[3].split(",");
			rules.add(new Rule(leftPOSConstraint, rightPOSConstraint,
					toBeSelected, negativeSelectionConstraints));
		}
		reader.close();
		return rules;
	}
}
