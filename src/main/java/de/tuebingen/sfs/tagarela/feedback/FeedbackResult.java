package de.tuebingen.sfs.tagarela.feedback;

/**
 * Wrapper class that represents particular instances of errors.
 * Contains a reference to the feedback message and the arguments for it.
 * @author rziai
 *
 */
public class FeedbackResult {

	private FeedbackMessage message;
	private String[] args;
	/**
	 * @param message
	 * @param args
	 */
	public FeedbackResult(FeedbackMessage message, String[] args) {
		this.message = message;
		this.args = args;
	}
	
	public String messageString() {
		return message.message(args);
	}

	/**
	 * @return the message
	 */
	public FeedbackMessage getMessage() {
		return message;
	}
	
	public String toString() {
		String res = "";
		res += "Arguments: ";

        if (args != null) {
            for (String s : args ) {
                res += s + " ";
            }
		    res = res.trim();
        }

		res += ", Error Type:" + message.getError();
		return res;
	}
}
