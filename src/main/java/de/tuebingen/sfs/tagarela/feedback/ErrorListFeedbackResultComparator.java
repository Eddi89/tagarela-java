package de.tuebingen.sfs.tagarela.feedback;

import java.util.Comparator;
import java.util.List;

/**
 * Implementation of a comparison strategy for feedback results. Compares
 * errors according to the error lists from activity model and learner model,
 * see thesis (ch. 4).
 * @author rziai
 *
 */
public class ErrorListFeedbackResultComparator implements Comparator<FeedbackResult> {

	private List<Error> actList;
	private List<Error> learnerList;
	
	public ErrorListFeedbackResultComparator(List<Error> actList,
			List<Error> learnerList) {
		this.actList = actList;
		this.learnerList = learnerList;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(FeedbackResult o1, FeedbackResult o2) {
		Error e1 = o1.getMessage().getError();
		Error e2 = o2.getMessage().getError();
		
		// no error: positive feedback
		if (e1 == null) {
			return 1;
		} else if (e2 == null) {
			return -1;
		}
		
		// find the errors preferred by both models
		int firstAct = findFirst(e1, e2, actList);
		int firstLearn = findFirst(e1, e2, learnerList);
		
		// didn't find any error type in any list: fallback to default strategy
		if (firstAct == 0 && firstLearn == 0) {
			if (e1 instanceof MeaningError) {
				return 1;
			} else if (e2 instanceof MeaningError) {
				return -1;
			} else {
				return 0;
			}
		}
		
		// "agreement" between both lists: perfect
		if (firstAct == firstLearn) {
			return firstAct;
		} else {
			// different error types prioritized
			// first go for activity
			if (firstAct != 0) {
				return firstAct;
			} else {
				// then for learner error
				if (firstLearn != 0) {
					return firstLearn;
				}
			}
		}
		return 0;
	}
	
	/*
	 * helper method that returns -1 or 1 depending on whether
	 * e1 is before e2 or after e2 in the given list. Returns 0 if
	 * neither error is found in the list.
	 */
	private int findFirst(Error e1, Error e2, List<Error> list) {
		for (Error e : list) {
			if (e.equals(e1)) {
				return 1;
			} else if (e.equals(e2)) {
				return -1;
			}
		}
		return 0;
	}

	
}
