package de.tuebingen.sfs.tagarela.feedback.diagnosis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.uima.analysis_engine.TypeOrFeature;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;

import de.drni.misc.uima.util.annotation.TypeSystemUtils;
import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.feedback.Error;
import de.tuebingen.sfs.tagarela.feedback.FeedbackMessage;
import de.tuebingen.sfs.tagarela.feedback.FeedbackResult;
import de.tuebingen.sfs.tagarela.feedback.MeaningError;
import de.tuebingen.sfs.tagarela.learner.LearnerModel;
import de.tuebingen.sfs.tagarela.types.AnalysisResults;
import de.tuebingen.sfs.tagarela.types.POSMatchResult;
import de.tuebingen.sfs.tagarela.types.Token;
import de.tuebingen.sfs.tagarela.types.TokenMapping;
import de.tuebingen.sfs.tagarela.util.Tools;

/**
 * Diagnoses lexical choice errors.
 * @author rziai
 *
 */
public class POSMatchDiagnosis implements DiagnosisModule {

	private static final HashMap<String, String> pos2realNames =
		new HashMap<String, String>();
	private static final HashMap<String, FeedbackMessage> pos2message =
		new HashMap<String, FeedbackMessage>();
	static {
		pos2realNames.put("verb", "verb");
		pos2realNames.put("noun", "noun");
		pos2realNames.put("prep", "preposition");
		pos2realNames.put("propNoun", "proper noun");
		pos2realNames.put("adj", "adjective");
		pos2realNames.put("adv", "adverb");
		pos2realNames.put("pronoun", "pronoun");
		pos2realNames.put("det", "determiner");
		
		pos2message.put("verb", FeedbackMessage.pos1);
		pos2message.put("noun", FeedbackMessage.pos1);
		pos2message.put("prep", FeedbackMessage.pos2);
		pos2message.put("propNoun", FeedbackMessage.pos3);
		pos2message.put("adj", FeedbackMessage.pos1);
		pos2message.put("adv", FeedbackMessage.pos1);
		pos2message.put("pronoun", FeedbackMessage.pos4);
		pos2message.put("det", FeedbackMessage.pos1);
	}
	
	public FeedbackResult getDiagnosis(JCas jcas, ActivityModel actModel,
			LearnerModel learnerModel) throws CASException {
		FeedbackResult result = null;
		AnalysisResults ar = Tools.getAnalysisResults(jcas);
		
		POSMatchResult pmr = ar.getPosMatch();
		
		if (pmr.getMatchedTokens().size() > 0) {
			for (int i = 0; i < pmr.getMatchedTokens().size(); i++) {
				TokenMapping tm = pmr.getMatchedTokens(i);
				Token first = tm.getFirst();
				Token second = tm.getSecond();
				String pos = first.getCategory();
				
				String firstCanonic = first.getLexDef(0).getCanonic();
				String secondCanonic = second.getLexDef(0).getCanonic();
				
				String realName = pos2realNames.get(pos);
				FeedbackMessage message = pos2message.get(pos);
				
				if (realName != null && message != null) {
					if(FeedbackMessage.pos2.equals(message)) {
						result = new FeedbackResult(message, new String[]{ realName, secondCanonic, firstCanonic });
					} else {
						result = new FeedbackResult(message, new String[]{ realName, firstCanonic, secondCanonic });
					}
				} else {
					result = new FeedbackResult(FeedbackMessage.pos5, new String[]{ firstCanonic, secondCanonic });
				}
				break;
			}
		}
		
		return result;
	}

	public List<TypeOrFeature> getAnalysisRequirements() {
		ArrayList<TypeOrFeature> reqs = new ArrayList<TypeOrFeature>();
		reqs.add(TypeSystemUtils.createFeature(AnalysisResults.class, "posMatch"));
		return reqs;
	}

	public Error[] getDetectableErrors() {
		return new Error[]{ MeaningError.WRONG_POS_INSTANCE };
	}

}
