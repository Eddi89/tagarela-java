package de.tuebingen.sfs.tagarela.feedback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.ResultSpecification;
import org.apache.uima.analysis_engine.TypeOrFeature;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.jcas.JCas;

import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.feedback.diagnosis.DiagnosisModule;
import de.tuebingen.sfs.tagarela.feedback.diagnosis.RusSpellingDiagnosis;
import de.tuebingen.sfs.tagarela.feedback.diagnosis.RusWordChoiceDiagnosis;
import de.tuebingen.sfs.tagarela.feedback.diagnosis.RusWordFormDiagnosis;
import de.tuebingen.sfs.tagarela.feedback.diagnosis.StringMatchDiagnosis;
import de.tuebingen.sfs.tagarela.learner.LearnerModel;

/**
 * Adaptive diagnosis manager implementation. This is the new diagnosis mechanism
 * described in the thesis. Uses information from the activity model and the
 * learner model to select a particular error.
 * @author rziai
 *
 */
public class RusAdaptiveDiagnosisManager implements DiagnosisManager {
	
	private static final Logger logger = Logger.getLogger("RusAdaptiveDiagnosisManager");

	private ArrayList<DiagnosisModule> diagnosisModules;
	private HashMap<Error, DiagnosisModule> errors2modules;
	
	public RusAdaptiveDiagnosisManager() {
		diagnosisModules = new ArrayList<DiagnosisModule>();
		errors2modules = new HashMap<Error, DiagnosisModule>();
		
		// add all our diagnosis modules
		diagnosisModules.add(new StringMatchDiagnosis());
		diagnosisModules.add(new RusSpellingDiagnosis());
		diagnosisModules.add(new RusWordChoiceDiagnosis());
		diagnosisModules.add(new RusWordFormDiagnosis());
		
		for (DiagnosisModule dm : diagnosisModules) {
			for (Error e : dm.getDetectableErrors()) {
				errors2modules.put(e, dm);
			}
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see de.tuebingen.sfs.tagarela.feedback.DiagnosisManager#feedback(org.apache.uima.jcas.JCas, de.tuebingen.sfs.tagarela.activity.ActivityModel, de.tuebingen.sfs.tagarela.learner.LearnerModel)
	 */
	public FeedbackResult feedback(JCas cas, ActivityModel actModel,
			LearnerModel learnerModel) throws CASException {
		// get error type list from the activity model
		List<Error> preferredByActivity = getErrorList(actModel);
		
		// abort if we don't have activity info
		if (preferredByActivity == null) {
			return new FeedbackResult(FeedbackMessage.noActModel, null);
		}
		
		ArrayList<FeedbackResult> results = new ArrayList<FeedbackResult>();
		
		// get all diagnoses
		for (DiagnosisModule dm : diagnosisModules) {
			FeedbackResult fr = dm.getDiagnosis(cas, actModel, learnerModel);
			if (fr != null) {
				results.add(fr);
			}
		}
		
		// update counts in the learner model
		for (FeedbackResult fr : results) {
			Error error = fr.getMessage().getError();
			if (error != null) {
				int prevCount;
				try {
					prevCount = learnerModel.getInt(error.toString());
				} catch (Exception e) {
					prevCount = 0;
				}
				try {
					logger.info("prevCount="+prevCount);
					learnerModel.put(error.toString(), prevCount+1);
				} catch (Exception e) {}
				
				// sub type of error
				int prevCountSubType;
				String subType = fr.getMessage().name();
				try {
					prevCountSubType = learnerModel.getInt(subType);
				} catch (Exception e) {
					prevCountSubType = 0;
				}
				try {
					logger.info("prevCountSubType="+prevCountSubType);
					learnerModel.put(subType, prevCountSubType+1);
				} catch (Exception e) {}
			}
			else{// count correct answers as well
				int prevCountCorrect;
				String correct = "CORRECT";
				try {
					prevCountCorrect = learnerModel.getInt(correct);
				} catch (Exception e) {
					prevCountCorrect = 0;
				}
				try {
					logger.info("prevCountCorrect="+prevCountCorrect);
					learnerModel.put(correct, prevCountCorrect+1);
				} catch (Exception e) {}
				
				// sub type of correct
				int prevCountSubType;
				String subType = fr.getMessage().name();
				try {
					prevCountSubType = learnerModel.getInt(subType);
				} catch (Exception e) {
					prevCountSubType = 0;
				}
				try {
					logger.info("prevCountSubType="+prevCountSubType);
					learnerModel.put(subType, prevCountSubType+1);
				} catch (Exception e) {}
			}
		}
		
		// get error type preferred by learner model
		List<Error> preferredByLearner = getErrorList(learnerModel);
		
		// get the result
		if (results.isEmpty()) {
			return new FeedbackResult(FeedbackMessage.noMessage, null);
		} else if (results.size() == 1) {
			return results.get(0);
		} else {
			// now prioritize feedback on preferred errors
			logger.info("Feedback results before sorting: " + results);
			Collections.sort(results, new ErrorListFeedbackResultComparator(
					preferredByActivity, preferredByLearner));
			Collections.reverse(results);
			logger.info("Feedback results after sorting: " + results);

			return results.get(0);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see de.tuebingen.sfs.tagarela.feedback.DiagnosisManager#getResultSpecification(de.tuebingen.sfs.tagarela.activity.ActivityModel, org.apache.uima.cas.TypeSystem)
	 */
	public ResultSpecification getResultSpecification(ActivityModel actModel,
			TypeSystem typeSystem) {
		ResultSpecification spec = UIMAFramework.getResourceSpecifierFactory().createResultSpecification();
		spec.setTypeSystem(typeSystem);
		List<Error> errorList = getErrorList(actModel);
		if (errorList.isEmpty()) {
			// no preferred error: fallback to default strategy, run everything
			for (DiagnosisModule dm : diagnosisModules) {
				for (TypeOrFeature tof : dm.getAnalysisRequirements()) {
					spec.addResultTypeOrFeature(tof);
				}
			}
			return spec;
		} else {
			// got preferred errors: create result specification accordingly
			for (Error e : errorList) {
				DiagnosisModule preferredModule = errors2modules.get(e);
				for (TypeOrFeature tof : preferredModule.getAnalysisRequirements()) {
					spec.addResultTypeOrFeature(tof);
				}
			}
			return spec;
		}
	}

	/*
	 * helper method to get the preferred error out of the activity model
	 */
	private Error getPreferredError(ActivityModel actModel) {
		String procMode = actModel.getProcMode();
		return toError(procMode);
	}

	private Error getPreferredError(LearnerModel learnerModel) {
		int maxCount = Integer.MIN_VALUE;
		Error preferred = null;
		ArrayList<Error> errorTypes = new ArrayList<Error>();
		errorTypes.addAll(Arrays.asList(FormError.values()));
		errorTypes.addAll(Arrays.asList(MeaningError.values()));
		
		for (Error e : errorTypes) {
			try {
				int count = learnerModel.getInt(e.toString());
				if (count > maxCount) {
					maxCount = count;
					preferred = e;
				}
			} catch (Exception e1) {}
		}
		
		return preferred;
	}
	
	/*
	 * gets the error list from the learner model, sorted by error count
	 */
	private List<Error> getErrorList(final LearnerModel learnerModel) {
		ArrayList<Error> errorTypes = new ArrayList<Error>();
		errorTypes.addAll(Arrays.asList(FormError.values()));
		errorTypes.addAll(Arrays.asList(MeaningError.values()));
		
		Collections.sort(errorTypes, Collections.reverseOrder(new Comparator<Error>() {
		
			public int compare(Error o1, Error o2) {
				Integer count1 = Integer.MIN_VALUE;
				Integer count2 = Integer.MIN_VALUE;
				try {
					count1 = learnerModel.getInt(o1.toString());
					count2 = learnerModel.getInt(o2.toString());
					return count1.compareTo(count2);
				} catch (Exception e) {
					if (count1 == Integer.MIN_VALUE && count2 == Integer.MIN_VALUE) {
						return 0;
					} else if (count1 != Integer.MIN_VALUE) {
						return 1;
					} else {
						return -1;
					}
				}
			}
		}));
		return errorTypes;
	}
	
	/*
	 * converts an error type string into its Error instance
	 */
	private Error toError(String errorString) {
		Error error = null;
		try {
			error = MeaningError.valueOf(errorString);
		} catch (IllegalArgumentException e) {
			try {
				error = FormError.valueOf(errorString);
			} catch (IllegalArgumentException e2) {
				return null;
			}
			
		}
		return error;
	}
	
	/*
	 * Gets the error type list from the activity model
	 */
	private List<Error> getErrorList(ActivityModel actModel) {
		ArrayList<Error> list = new ArrayList<Error>();
		String procMode = actModel.getProcMode();
		if ("FormError".equals(procMode)) {
			list.addAll(Arrays.asList(FormError.values()));
			return list;
		} else if ("MeaningError".equals(procMode)) {
			list.addAll(Arrays.asList(MeaningError.values()));
			return list;
		}
		String[] errorStrings = procMode.split(",");
		
		for (String e : errorStrings) {
			Error error = toError(e);
			if (error != null) {
				list.add(error);
			}
		}
		
		return list;
	}
}
