package de.tuebingen.sfs.tagarela.feedback.diagnosis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.uima.analysis_engine.TypeOrFeature;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.jcas.JCas;

import de.drni.misc.uima.util.annotation.TypeSystemUtils;
import de.drni.misc.uima.util.list.FSListIterable;
import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.feedback.Error;
import de.tuebingen.sfs.tagarela.feedback.FeedbackMessage;
import de.tuebingen.sfs.tagarela.feedback.FeedbackResult;
import de.tuebingen.sfs.tagarela.feedback.FormError;
import de.tuebingen.sfs.tagarela.learner.LearnerModel;
import de.tuebingen.sfs.tagarela.types.Agreement;
import de.tuebingen.sfs.tagarela.types.AnalysisResults;
import de.tuebingen.sfs.tagarela.types.CanonicMatchResult;
import de.tuebingen.sfs.tagarela.types.Constituent;
import de.tuebingen.sfs.tagarela.util.Tools;

/**
 * Diagnosis for agreement and word form errors.
 * @author rziai
 *
 */
public class CanonicMatchAgreementDiagnosis implements DiagnosisModule {

	private static final HashMap<String, String> symbols2displayNames = new HashMap<String, String>();
	static {
		symbols2displayNames.put("det", "determiner");
		symbols2displayNames.put("Nbar", "noun phrase");
		symbols2displayNames.put("NP", "noun phrase");
		symbols2displayNames.put("VP", "verb phrase");
		symbols2displayNames.put("prep", "preposition");
		symbols2displayNames.put("propNoun", "proper noun");
	}
	
	private static String getDisplayName(String symbol) {
		String ret = symbols2displayNames.get(symbol);
		return ret != null ? ret : symbol;
	}
	
	public FeedbackResult getDiagnosis(JCas jcas, ActivityModel actModel,
			LearnerModel learnerModel) throws CASException {
		AnalysisResults ar = Tools.getAnalysisResults(jcas);
		FeedbackResult result = null;
		result = checkAgreement(ar, jcas);
		if (result != null) {
			return result;
		}
		CanonicMatchResult cmr = ar.getCanonicMatch();
		
		if (cmr.getMatchedTokens().size() > 0) {
			if (cmr.getMatchedTokens().size() == 1) {
				result = new FeedbackResult(FeedbackMessage.CanoCompl1,
						new String[]{ cmr.getMatchedTokens(0).getFirst().getTokenString() });
			} else if (cmr.getMatchedTokens().size() > 1) {
				
				String temp = "";
				for (int i = 0; i < cmr.getMatchedTokens().size(); i++) {
					temp += "<b><i>" + cmr.getMatchedTokens(i).getFirst().getTokenString() + "</i></b>";
					if (i != cmr.getMatchedTokens().size()-1) {
						temp += ", ";
					}
				}
				result = new FeedbackResult(FeedbackMessage.CanoCompl2, new String[]{ temp });
			}
		}
		return result;
	}
	
	private FeedbackResult checkAgreement(AnalysisResults ar, JCas jcas) {
		FSListIterable iterable = new FSListIterable(ar.getAgreementResults());
		ArrayList<Agreement> erroneous = new ArrayList<Agreement>();
		
		for (FeatureStructure fs : iterable) {
			Agreement a = (Agreement) fs;
			if ("error".equals(a.getPerson())
					|| "error".equals(a.getNumber())
					|| "error".equals(a.getGender())) {
				erroneous.add(a);
			}
		}
		if (erroneous.size() == 1) {
			return new FeedbackResult(FeedbackMessage.agr1, getAgrArgs(jcas, erroneous.get(0)));
		} else if (erroneous.size() > 1) {
			String temp = "";
			for (int i = 0; i < erroneous.size(); i++) {
				temp += toString(jcas, erroneous.get(i), i+1);
			}
			return new FeedbackResult(FeedbackMessage.agr2, new String[]{ temp });
		}
		return null;
	}
	
	private String toString(JCas jcas, Agreement a, int num) {
		String result = "<p>" + num + ") agreement error in <i><b>";
		result += Tools.getError(a);
		result += "</b></i> between the <i><b>";
		Constituent left = a.getOffendingConstituents(0);
		Constituent right = a.getOffendingConstituents(1);
		result += getDisplayName(left.getCategory()) + "</b></i> and the <i><b>" + getDisplayName(right.getCategory());
		result += "</b></i> in the sequence <i><b>" +
		jcas.getDocumentText().substring(left.getBegin(), right.getEnd()) + "</b></i></p>";
		return result;
	}
	
	private String[] getAgrArgs(JCas jcas, Agreement a) {
		String feat = null;
		if ("error".equals(a.getPerson())) {
			feat = "person";
		} else if ("error".equals(a.getNumber())) {
			feat = "number";
		} else if ("error".equals(a.getGender())) {
			feat = "gender";
		}
		Constituent left = a.getOffendingConstituents(0);
		Constituent right = a.getOffendingConstituents(1);
		String leftC = getDisplayName(left.getCategory());
		String rightC = getDisplayName(right.getCategory());
		String sequence = jcas.getDocumentText().substring(left.getBegin(), right.getEnd());
		return new String[]{ feat, leftC, rightC, sequence};
	}

	/*
	 * (non-Javadoc)
	 * @see de.tuebingen.sfs.tagarela.feedback.diagnosis.DiagnosisModule#getAnalysisRequirements()
	 */
	public List<TypeOrFeature> getAnalysisRequirements() {
		ArrayList<TypeOrFeature> reqs = new ArrayList<TypeOrFeature>();
		reqs.add(TypeSystemUtils.createFeature(AnalysisResults.class, "canonicMatch"));
		reqs.add(TypeSystemUtils.createFeature(AnalysisResults.class, "agreementResults"));
		return reqs;
	}

	/*
	 * (non-Javadoc)
	 * @see de.tuebingen.sfs.tagarela.feedback.diagnosis.DiagnosisModule#getDetectableErrors()
	 */
	public Error[] getDetectableErrors() {
		return new Error[]{ FormError.AGREEMENT, FormError.WORDFORM };
	}

}
