package de.tuebingen.sfs.tagarela.feedback.diagnosis;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.apache.log4j.Logger;
import org.apache.uima.analysis_engine.TypeOrFeature;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;

import de.drni.misc.uima.util.annotation.TypeSystemUtils;
import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.annotators.Levenshtein;
import de.tuebingen.sfs.tagarela.feedback.Error;
import de.tuebingen.sfs.tagarela.feedback.FeedbackMessage;
import de.tuebingen.sfs.tagarela.feedback.FeedbackResult;
import de.tuebingen.sfs.tagarela.feedback.FormError;
import de.tuebingen.sfs.tagarela.learner.LearnerModel;
import de.tuebingen.sfs.tagarela.types.CGToken;
import de.tuebingen.sfs.tagarela.types.Token;

/**
 * Diagnoses spelling errors.
 * @author rziai
 *
 */
public class RusSpellingDiagnosis implements DiagnosisModule {
	
	private static final Logger logger = Logger.getLogger("RusSpellingDiagnosis");

	public FeedbackResult getDiagnosis(JCas jcas, ActivityModel actModel,
			LearnerModel learnerModel) throws CASException {
		
		
		FeedbackResult result = null;
		
		JCas bestAnswerView = jcas.getView("bestAnswer");
		
		String bestAnswer = bestAnswerView.getDocumentText();
		logger.info("bestAnswer="+bestAnswer);
		
		JCas answerWithStressView = jcas.getView("answerWithStress");
					
		String answerWithStress = answerWithStressView.getDocumentText();
		logger.info("answerWithStress="+answerWithStress);
		
		Pattern unstressedO = Pattern.compile("о");
	    Pattern unstressedE = Pattern.compile("е");
		String stressMarker = "\u0301";
		//Pattern stressMarkerPattern=Pattern.compile(primaryStressMarker);
		
		Matcher unstressedOMatcher = unstressedO.matcher(answerWithStress);
		Matcher unstressedEMatcher = unstressedE.matcher(answerWithStress);
		//Matcher stressMarkerMatcher = stressMarkerPattern.matcher(answerWithStress);
				
		// get tokens from the jcas
		FSIterator<Annotation> cgTokenIter = jcas.getAnnotationIndex(CGToken.type).iterator();
		
		
		// iterate over all tokens
		while (cgTokenIter.hasNext()) {
			
			CGToken cgt = (CGToken) cgTokenIter.next();
			
			String surfaceForm = cgt.getCoveredText();
			logger.info("surfaceForm="+surfaceForm);
			
			Levenshtein lev = new Levenshtein();
			
			lev.createLevMatrix(surfaceForm, bestAnswer);
			
			int levDistance = lev.getEditDistance();
			logger.info("levDistance="+levDistance);
			
			if(levDistance > 0){	
				// answerWithStress
				String hintAnswer = lev.getHintAnswer();
				logger.info("hintAnswer="+hintAnswer);
				
				if(answerWithStress.length()==(surfaceForm.length()+1))
				{
					result = new FeedbackResult(FeedbackMessage.wrongSpelling, new String[]{surfaceForm, hintAnswer});		
					int stressedMarkerPosition = answerWithStress.indexOf(stressMarker);
					
					String unstressedRule = "The stress in Russian influences the pronunciation of vowels. The stressed vowels represent the same sound, as it is written: e.g. Саша [са́ша], кот [ко́т], дверь [две́рь].  But the unstressed vowels are pronounced differenly: their pronunciation doesn&#39;t correspond to their spelling. For instance, a word &#39;дома́&#39; - &#39;houses&#39; sounds like [дама́] with [a] in the first syllable, but this unstressed vowel is spelled with &#39;o&#39;. Why? The spelling of unstressed vowels in the root may be checked by altering the word. Try you find the word form or another word with the same root, where the unstressed vowel becomes stressed. If such a word exists, then the vowel should be spelled as it sounds in the stressed position.&lt;br&gt;&lt;br&gt; Example: we have a word &#39;кота́&#39;, where the vowel &#39;o&#39; is unstressed. It sounds as [a]. But we know, that there is a word form &#39;кот&#39;, where &#39;o&#39; is stressed. Since such a word exists, the vowel in the stem must be spelled as &#39;o&#39; in every word form.&lt;br&gt;&lt;br&gt; Keep in mind, that this rule does not apply for all words. For some of them we have to memorize the spelling or check it in the dictionary.";
					unstressedRule = "&lt;span&gt;"+unstressedRule+"&lt;/span&gt";
					unstressedRule = "<span style=\"color:blue\" rule=\""+  unstressedRule + "\" onclick=\"showFixedTooltip(this, 'rule')\">Rule</span>";
					
					while(unstressedOMatcher.find())
					{
						int positionUnstressedO = unstressedOMatcher.start();
						
						
						// if a vowel is not followed by a stress marker, it is unstressed. 
						//Further conditions for unstressed vowels can be applied
						if(stressedMarkerPosition != (positionUnstressedO+1) && positionUnstressedO != answerWithStress.length()-1)
						{
							if(surfaceForm.charAt(positionUnstressedO)=='а')
								result = new FeedbackResult(FeedbackMessage.wrongSpellingUnstressedO, new String[]{hintAnswer, unstressedRule});
						} 
					}
					while(unstressedEMatcher.find())
					{
						int positionUnstressedE = unstressedEMatcher.start();
						if(stressedMarkerPosition != (positionUnstressedE+1) && positionUnstressedE != answerWithStress.length()-1)
						{
							if(surfaceForm.charAt(positionUnstressedE)=='и')
								result = new FeedbackResult(FeedbackMessage.wrongSpellingUnstressedE, new String[]{hintAnswer, unstressedRule});
						}
					}										
				} else 
					result = new FeedbackResult(FeedbackMessage.wrongSpelling, new String[]{surfaceForm, hintAnswer});				
			}
		}
		return result;
	}

	public List<TypeOrFeature> getAnalysisRequirements() {
		ArrayList<TypeOrFeature> reqs = new ArrayList<TypeOrFeature>();
		reqs.add(TypeSystemUtils.createFeature(Token.class, "spellingAnalysis"));
		return reqs;
	}

	public Error[] getDetectableErrors() {
		return new Error[]{ FormError.SPELLING };
	}

}
