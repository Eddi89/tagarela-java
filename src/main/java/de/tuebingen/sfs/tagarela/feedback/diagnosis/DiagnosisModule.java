/**
 * 
 */
package de.tuebingen.sfs.tagarela.feedback.diagnosis;

import java.util.List;

import org.apache.uima.analysis_engine.TypeOrFeature;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;

import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.feedback.Error;
import de.tuebingen.sfs.tagarela.feedback.FeedbackResult;
import de.tuebingen.sfs.tagarela.learner.LearnerModel;

/**
 * @author rziai
 *
 */
public interface DiagnosisModule {
	
	public static final String TYPE_PREFIX = "de.tuebingen.sfs.tagarela.types.";

	/**
	 * Returns a diagnosis, given the arguments.
	 * @param jcas The annotated input
	 * @param actModel The activity model
	 * @param learnerModel The learner model
	 * @return The feedback result, i.e. the diagnosis
	 * @throws CASException
	 */
	public FeedbackResult getDiagnosis(JCas jcas, ActivityModel actModel, LearnerModel learnerModel) throws CASException;
	
	/**
	 * Returns a list of analysis requirements this module needs for diagnosis.
	 * @return
	 */
	public List<TypeOrFeature> getAnalysisRequirements();
	
	/**
	 * Returns the error types this module can detect.
	 * @return
	 */
	public Error[] getDetectableErrors();
}
