package de.tuebingen.sfs.tagarela.feedback;

import java.util.Formatter;

/**
 * A feedback message in the TAGARELA system. Contains a template string
 * that is filled with the arguments given to the <code>message()</code> method
 * to produce the final message string.
 * @author rziai
 *
 */
public enum FeedbackMessage {
	
	/**
	 * 1 word wrong
	 */
	spell1("I don't know the word <b><i>%s</i></b>.<p>Please replace it before I continue my analysis.</p>", FormError.SPELLING),
	/**
	 * 1 word, 1 suggestion
	 */
    spell2("I cannot find the word <b><i>%s</i></b> in my dictionary.<p>Are you sure you don't mean <b><i>%s</i></b>?</p>", FormError.SPELLING),
    /**
     * 1 word, several suggestions
     */
    spell3("I think there is a problem with the spelling of <b><i>%s</i></b>.<p>See if you can replace it with one of the following words: %s.</p>", FormError.SPELLING),
    /**
     * Several wrong words with suggestions
     */
    spell4("I found some spelling errors in your input. Look at the suggestions I have for them.%s", FormError.SPELLING),
    /**
     * Everything's fine
     */
    strM1("Very Good! Keep going!", null),
    /**
     * Correct answer with capitalization errors
     */
    strM2("Your answer is correct, but review it for capitalization errors.", FormError.CAPITALIZATION),
    /**
     * Correct but missing final punctuation mark
     */
    strM3("Good! But you forgot the final punctuation mark.", FormError.PUNCTUATION),
    /**
     * Correct but missing final punctuation mark and capitalization erros
     */
    strM4("Your answer is fine, but you did not put the final puctuation mark and you should check it for capitalization errors.", FormError.CAPITALIZATION),
    /**
     * Everything's fine
     */
    strM5("Excellent!", null),
    /**
     * Everything's fine
     */
    strM6("You got it!", null),
    /**
     * Everything's fine
     */
    strM7("Very Good!", null),
    /**
     * Everything's fine
     */
    strM8("That's it!", null),
    /**
     * Everything's fine
     */
    strM9("Good Job!", null),
    /**
     * Everything's fine
     */
    strM10("That's right!", null),
    /**
     * Everything's fine
     */
    strM11("Perfect!", null),
    /**
     * Everything's fine
     */
    strM12("All right! Keep going!", null),
    /**
     * Correct but unnecessary spaces or punctuation
     */
    strM13("Your answer is correct, but review it for unnecessary spaces or punctuation.", FormError.PUNCTUATION),
    /**
     * Final punctuation issue
     */
    strM14("There seems to be an issue with the final punctuation in your sentence.", FormError.PUNCTUATION),
    /**
     * Missing key word in answer to picture
     */
    reqW1D("You have not included this key word in your answer: %s.<p>Please use all the words required to describe the picture.</p>", MeaningError.MISSING_CONCEPTS),
    /**
     * Several missing key words
     */
    reqW2D("There are some key words missing in your answer.<p>Look at the list of required words and make sure you use all of them.</p>", MeaningError.MISSING_CONCEPTS),
    /**
     * Complete answer but word order issue or punctuation
     */
    descWO1("I know your answer is complete, but I can't match it to any possible answers in my database.<p>Try changing the position of the words in your sentence, or checking for wrong punctuation.</p>", FormError.WORD_ORDER),
    /**
     * Missing punctuation
     */
    missingPunct("You have provided all the necessary words but review your answer for missing or unnecessary punctuation. <p>See if you are not missing the final dot.</p>", FormError.PUNCTUATION),
    /**
     * Extra punctuation
     */
    extraPunct("I think there are some punctuation marks in your answer that are not necessary.", FormError.PUNCTUATION),
    /**
     * One agreement error
     */
    agr1("There is an agreement error in <b>%s</b> between the <b>%s</b> and the <b>%s</b> in the sequence <b><i>%s</i></b> from your answer.", FormError.AGREEMENT),
    /**
     * Several agreement errors
     */
    agr2("There are some agreement errors in your sentence. Look at the explanations below to fix them. %s", FormError.AGREEMENT),
    /**
     * One word form wrong
     */
    CanoCompl1("You are almost there. Change the form of the word <b>\"%s\"</b>, and try again.", FormError.WORDFORM),
    // NEW RUSSIAN CODE //
    /**
     * Wrong word choice
     */
    wrongWordChoice("You have selected the <span style=\"color:red\">wrong word</span>.<br><br>Hint: <br>%s", MeaningError.WORDCHOICE),
    /**
     * One word form wrong, i.e. wrong case
     */
    wrongCase("Please check the <span style=\"color:red\">word ending</span> in <b>\"%s<span style=\"color:red\">%s</span>\"</b>. You entered the <b>\"%s\"</b> form, but the preposition requires <span style=\"color:green\">locative</span>.<br> Hints: <u>%s</u>,  <u>%s</u>", FormError.WORDFORM),
    /**
     * One word form wrong, i.e. wrong number
     */
    wrongNumber("How do we say that in singular? Please check the <span style=\"color:red\">word ending</span> in <b>\"%s<span style=\"color:red\">%s</span>\"</b>. You entered the <b>\"%s\"</b> form, but there is only <span style=\"color:green\">one object</span> in the picture.<br> Hints: <u>%s</u>,  <u>%s</u>", FormError.WORDFORM),
    /**
     * Spelling error in the answer
     */
    wrongSpelling("I don't know the word <b><i>%s</i></b>.<p>Please correct it using my hint: <b><i>%s</i></b>.</p>", FormError.SPELLING),
    /**
     * Spelling error in the answer
     */
    wrongSpellingUnstressedO("I found an incorrectly spelled vowel here: <b><i>%s</i></b>.<p> Remember, <span style=\"color:green\">unstressed</span> 'o' may sound like 'а'.</p><br> Hint: <u>%s</u>", FormError.SPELLING),
    // The word should be spelled  with 'o',  if you can find a word form, where the 'o' is stressed
    /**
     * Spelling error in the answer
     */
    wrongSpellingUnstressedE("I found an incorrectly spelled vowel here: <b><i>%s</i></b>.<p> Remember, <span style=\"color:green\">unstressed</span> 'e' may sound like 'и'.</p><br> Hint: <u>%s</u>", FormError.SPELLING),
    // The word should be spelled  as 'e', if you can find a word form, where the 'e' is stressed
    /**
     * Several word forms wrong
     */
    wrongCaseAndNumber("Sorry, you typed the word in <b>\"%s\"</b> and <b>\"%s\"</b>. I was expecting <span style=\"color:green\">singular locative</span>. Could you rewrite the form again?<br> Hints: <u>%s</u>,  <u>%s</u>", FormError.WORDFORM),
    // NEW RUSSIAN CODE //   
    /**
     * Several word forms wrong
     */
    CanoCompl2("You are using different forms of some of the words in the answer. Change the forms of the words below so that they can match the ones I am expecting.<p>%s</p>", FormError.WORDFORM),
    /**
     * Wrong verb, noun, adjective, adverb or determiner
     */
    pos1("I am not expecting the %s <b>%s</b> for this answer. Try using <b>%s</b> instead.", MeaningError.WRONG_POS_INSTANCE),
    /**
     * Wrong preposition
     */
    pos2("There is a problem with the %s you have chosen. Try using <b>%s</b> instead of <b>%s</b>.", MeaningError.WRONG_POS_INSTANCE),
    /**
     * Wrong proper noun
     */
    pos3("I think there is a problem with the %s you have chosen. <p>Are you sure you want to use <b>%s</b> instead of <b>%s</b>?</p>", MeaningError.WRONG_POS_INSTANCE),
    /**
     * Wrong pronoun
     */
    pos4("Please check if the %s <b>%s</b> is appropriate for this answer.<p> You may try using <b>%s</b> instead. </p>", MeaningError.WRONG_POS_INSTANCE),
    /**
     * Some other wrong word
     */
    pos5("Is the word <b>%s</b> appropriate for this answer?<p> Try using <b>%s</b> instead. </p>", MeaningError.WRONG_POS_INSTANCE),
    /**
     * Missing elements, Reading
     */
    missingR("There are too many elements missing in your answer for me to evaluate it. <p>Please go back to the text and try to find the necessary information to answer the question.</p>", MeaningError.MISSING_CONCEPTS),
    /**
     * Missing elements, Listening
     */
    missingL("There are too many elements missing in your answer for me to evaluate it. <p>Read the question carefully and listen to the passage one more time.</p>", MeaningError.MISSING_CONCEPTS),
    /**
     * Missing elements, Description
     */
    missingD("It is impossible for me to evaluate your answer. There are too many necessary words missing. <p>Make sure you use all the required words to describe the picture.</p>", MeaningError.MISSING_CONCEPTS),
    /**
     * Missing elements, Rephrasing
     */
    missingRP("I cannot evaluate your answer because it lacks several necessary words required to complete the task.<p>Read the instructions again and use the words presented by the exercise.</p>", MeaningError.MISSING_CONCEPTS),
    /**
     * Missing elements, Vocabulary
     */
    missingV("I cannot evaluate your answer because there are too many required words missing. Read the instructions, and use all the necessary vocabulary items required by the exercise.", MeaningError.MISSING_CONCEPTS),
    /**
     * Testing message
     */
    check("Checking: %s, %s", null),
    /**
     * Extra determiner before proper noun
     */
    det_propNoun("Proper nouns usually do not have to be preceded by determiners.<p>Try re-writing your sentence without <b>\"%s\"</b>.</p>", FormError.EXTRA_DET),
    /**
     * One extra element
     */
    exta_element1("You have included all the necessary elements to answer the question, but I found one extra word. <p>Re-write your sentence without <b>\"%s\"</b>.</p>", MeaningError.EXTRA_CONCEPTS),
    /**
     * Several extra elements
     */
    exta_element2("Although all necessary words to answer the question are present in your sentence, I believe there are also some unnecessary ones. Re-write your sentence without the following words:<p>%s</p>", MeaningError.EXTRA_CONCEPTS),
    /**
     * Missing pos
     */
    missing1("Your answer is close, but there is a <b>%s</b> missing in your sentence.", MeaningError.MISSING_CONCEPTS),
    /**
     * Three missing words
     */
    missing2("You are going in the right direction, but there are still three words missing in your answer.", MeaningError.MISSING_CONCEPTS),
    /**
     * One missing word
     */
    missing3("There is one important word missing in your answer.", MeaningError.MISSING_CONCEPTS),
    /**
     * Three missing words and maybe some unnecessary ones
     */
    missing4("There are three necessary words missing in your answer. <p>You may also be using some words that are not necessary to answer the question.</p>", MeaningError.MISSING_AND_EXTRA),
    /**
     * All words present but one missing
     */
    missing5("You have written all the words necessary to answer the question, except for <b>%s</b>.", MeaningError.MISSING_CONCEPTS),
    /**
     * One extra word
     */
    extra1("Your answer has one extra word.", MeaningError.EXTRA_CONCEPTS),
    /**
     * Several extra words
     */
    extra2("Your answer has several extra words.", MeaningError.EXTRA_CONCEPTS),
    /**
     * After 'gostar' you have to use 'de'
     */
    gostar_de("REMEMBER! <p>You have to use the preposition <b>de</b> after the verb gostar.</p>", FormError.MISSING_PREP),
    /**
     * Missing 'de'
     */
    no_de("The preposition <b>de</b> is missing.", FormError.MISSING_PREP),
    /**
     * 2 words missing
     */
    missing_2_words("You have to include the words <b>%s</b> and <b>%s</b> in your answer.", MeaningError.MISSING_CONCEPTS),
    /**
     * 2 words missing and possibly unnecessary ones
     */
    missing_2_words2("The words <b>%s</b> and <b>%s</b> are missing in your answer. <p>Also review it for unnecessary words.</p>", MeaningError.MISSING_AND_EXTRA),
    /**
     * Missing pos and possibly unnecessary material
     */
    missing6("There is an important <b>%s</b> missing in your sentence. <p>Also review it for unnecessary words.</p>", MeaningError.MISSING_AND_EXTRA),
    /**
     * Possibly unecessary material but start adding one word
     */
    missing7("There may be unnecessary words in your answer, but begin by adding the word <b>%s</b>.", MeaningError.MISSING_AND_EXTRA),
    // FIB is a special case
    /**
     * Report 1 blank
     */
    fibsFeedback1("Blank 1: %s", null),
    /**
     * Report 2 blanks
     */
    fibsFeedback2("Blank 1: %s<p></p>Blank 2: %s", null),
    /**
     * Report 3 blanks
     */
    fibsFeedback3("Blank 1: %s<p></p>Blank 2: %s<p></p>Blank 3:: %s", null),
    /**
     * Testing message
     */
    test("testing messages: %s", null),
    /**
     * Activity problem
     */
    noActModel("There is a problem with this activity. <p>Please click on the 'Report Errors' button and send this error message.</p> ", null),
    /**
     * We don't know what's going on
     */
    noMessage("I don't know what to say to you. <p>Please click on the 'Report Errors' button, and send the sentence you typed in.</p>", null);
	
	private Error error;
	private String format;
	
	FeedbackMessage(String format, Error error) {
		this.format = format;
		this.error = error;
	}

	public Error getError() {
		return error;
	}
	
	/**
	 * Returns the actual message, formatted with the given parameters.
	 * @param args The parameters for the message
	 * @return The formatted string
	 */
	public String message(Object[] args) {
		return new Formatter().format(format, args).toString();
	}
}
