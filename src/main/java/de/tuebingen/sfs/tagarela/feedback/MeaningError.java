package de.tuebingen.sfs.tagarela.feedback;

/**
 * Meaning-based errors in the system.
 * @author rziai
 *
 */
public enum MeaningError implements Error {
	WRONG_POS_INSTANCE ("Wrong lexical choice"),
	MISSING_CONCEPTS ("Missing concepts"),
	EXTRA_CONCEPTS ("Extra concepts"),
	MISSING_AND_EXTRA ("Missing and extra concepts"),
	WORDCHOICE ("Wrong word choice");
	
	private String displayName;
	
	MeaningError(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}
}
