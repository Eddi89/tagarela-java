package de.tuebingen.sfs.tagarela.feedback.diagnosis;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.apache.uima.analysis_engine.TypeOrFeature;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.UnsafeInput;

import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.feedback.Error;
import de.tuebingen.sfs.tagarela.feedback.FeedbackMessage;
import de.tuebingen.sfs.tagarela.feedback.FeedbackResult;
import de.tuebingen.sfs.tagarela.feedback.FormError;
import de.tuebingen.sfs.tagarela.learner.LearnerModel;
import de.tuebingen.sfs.tagarela.types.CGReading;
import de.tuebingen.sfs.tagarela.types.CGToken;

/**
 * Diagnosis on word form errors.
 * 
 * @author eduard
 *
 */
public class RusWordFormDiagnosis implements DiagnosisModule{
	
	private static final Logger logger = Logger.getLogger("RusWordFormDiagnosis");
	
	private static final HashMap<String, String> tag2realNames =
			new HashMap<String, String>();
		static {
			tag2realNames.put("Nom", "Nominative");
			tag2realNames.put("Acc", "Accusative");
			tag2realNames.put("Gen", "Genitive");
			tag2realNames.put("Dat", "Dative");
			tag2realNames.put("Ins", "Instrumental");
			tag2realNames.put("Pl", "Plural");
		}
	
	public FeedbackResult getDiagnosis(JCas jcas, ActivityModel actModel,
			LearnerModel learnerModel) throws CASException {
		
		FeedbackResult result = null;
		
		JCas bestAnswerLemmaView = jcas.getView("bestAnswerLemma");
		
		String bestAnswerLemma = bestAnswerLemmaView.getDocumentText();
		
		logger.info("bestAnswerLemma=" + bestAnswerLemmaView.getDocumentText());
		
		Pattern wrongCasePattern = Pattern.compile("Nom|Acc|Gen|Dat|Ins|Loc2|Gen2|Voc");
		
		Pattern wrongNumberPattern = Pattern.compile("Pl");
		
		// get tokens from the jcas
		FSIterator<Annotation> cgTokenIter = jcas.getAnnotationIndex(CGToken.type).iterator();
		
		Map<String, String> lemmaToExemplarMap = loadMapKryo("lemmaToExemplarMap");
		
		// iterate over all tokens
		while (cgTokenIter.hasNext()) {
			
			CGToken cgt = (CGToken) cgTokenIter.next();
			
			String surfaceForm = cgt.getCoveredText();
			logger.info("surfaceForm="+surfaceForm);
			
			String lemma, tag, aCase, number, translation, table, tooltip = "";
			
			// iterate over all readings
			for(int i = 0; i < cgt.getReadings().size(); i++){
				CGReading currentReading = cgt.getReadings(i);
				lemma = currentReading.getLemma();
				lemma = lemma.replaceAll("\"", "");
				logger.info("lemma="+lemma);
				// check for lemma first, don't proceed otherwise
				if(bestAnswerLemma.equals(lemma)){
					tag = currentReading.getTag();
					logger.info("tag="+tag);
					Matcher wrongCaseMatcher = wrongCasePattern.matcher(tag);
					Matcher wrongNumberMatcher = wrongNumberPattern.matcher(tag);	
					String ending = "";
					if(surfaceForm.length() > lemma.length()){
						ending = surfaceForm.substring(lemma.length());						
					}
					logger.info("ending=" + ending);		
					
					String[] exemplar = lemmaToExemplarMap.get(lemma).split("\t");
					translation = exemplar[0];
					logger.info("translation=" +translation);
					table = exemplar[2];
					
					// construct tooltip for the table and translation
					translation = "&lt;span&gt;"+translation+"&lt;/span&gt";
					translation = "<span style=\"color:blue\" translation=\""+ translation + "\" onmouseover=\"showTooltip(this, 'translation')\">Translation</span>";
					table = "<span style=\"color:blue\" exemplarTable=\""+ table + "\" onmouseover=\"showTooltip(this, 'exemplarTable')\">Exemplar</span>";
					
					// wrong case
					if(wrongCaseMatcher.find()){
						aCase = wrongCaseMatcher.group();
						aCase = tag2realNames.get(aCase);
						result = new FeedbackResult(FeedbackMessage.wrongCase, new String[]{ lemma, ending, aCase, translation, table });
						// both number and case are incorrect
						if(wrongNumberMatcher.find()){
							aCase = wrongCaseMatcher.group();
							number = wrongNumberMatcher.group();
							result = new FeedbackResult(FeedbackMessage.wrongCaseAndNumber, new String[]{ number, aCase, translation, table});
						}
					}
					
					// wrong number
					if(wrongNumberMatcher.find()){
						number = wrongNumberMatcher.group();
						number = tag2realNames.get(number);
						result = new FeedbackResult(FeedbackMessage.wrongNumber, new String[]{ lemma, ending, number, translation, table });
					}
				}
			}
		}
		return result;
	}
	/*
	 * (non-Javadoc)
	 * @see de.tuebingen.sfs.tagarela.feedback.diagnosis.DiagnosisModule#getAnalysisRequirements()
	 */
	public List<TypeOrFeature> getAnalysisRequirements() {
		ArrayList<TypeOrFeature> reqs = new ArrayList<TypeOrFeature>();
		return reqs;
	}

	/*
	 * (non-Javadoc)
	 * @see de.tuebingen.sfs.tagarela.feedback.diagnosis.DiagnosisModule#getDetectableErrors()
	 */
	public Error[] getDetectableErrors() {
		return new Error[]{ FormError.WORDFORM };
	}
	

	
	/**
	 * Deserialize a map on the hard drive.
	 * @param mapName the name of the map
	 * @return the map stored on the hard drive
	 */
	private <T1, T2> Map<T1, T2> loadMapKryo(String mapName){
		Kryo kryo = new Kryo();
		kryo.register(HashMap.class, 0);
		Map<T1, T2> map = new HashMap<T1, T2>();
		try
		{
			UnsafeInput input = new UnsafeInput(new FileInputStream("rus_resources/"+mapName+".ser"));
			map = kryo.readObject(input, HashMap.class);
			input.close();
		}catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
		logger.info(mapName+" loaded");
		return map;
	}
	

}
