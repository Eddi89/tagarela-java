package de.tuebingen.sfs.tagarela.feedback;

import java.util.Comparator;

/**
 * This comparator sorts <code>FeedbackResult</code>s according to their relevance
 * using information from the activity model and the learner model.
 * @author rziai
 *
 */
public class SingleErrorFeedbackResultComparator implements Comparator<FeedbackResult> {

	private Error preferredByActivity;
	private Error preferredByLearner;
	
	public SingleErrorFeedbackResultComparator(Error preferredByActivity,
			Error preferredByLearner) {
		this.preferredByActivity = preferredByActivity;
		this.preferredByLearner = preferredByLearner;
	}
	
	public int compare(FeedbackResult o1, FeedbackResult o2) {
		Error e1 = o1.getMessage().getError();
		Error e2 = o2.getMessage().getError();
		
		// no error: positive feedback
		if (e1 == null) {
			return 1;
		} else if (e2 == null) {
			return -1;
		}
		
		// some error: decide which one is more important
		// learner-specific takes priority over activity-specific
		// default strategy: meaning-based more important than form-based
		if (preferredByLearner.equals(e1)) {
			return 1;
		} else if (preferredByLearner.equals(e2)) {
			return -1;
		} else if (preferredByActivity.equals(e1)) {
			return 1;
		} else if (preferredByActivity.equals(e2)) {
			return -1;
		} else if (e1 instanceof MeaningError) {
			return 1;
		} else if (e2 instanceof MeaningError) {
			return -1;
		}
		return 0;
	}

}
