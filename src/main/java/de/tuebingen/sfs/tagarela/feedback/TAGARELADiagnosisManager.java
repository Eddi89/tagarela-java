package de.tuebingen.sfs.tagarela.feedback;

import java.util.ArrayList;

import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.ResultSpecification;
import org.apache.uima.analysis_engine.TypeOrFeature;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.jcas.JCas;

import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.feedback.diagnosis.CanonicMatchAgreementDiagnosis;
import de.tuebingen.sfs.tagarela.feedback.diagnosis.DiagnosisModule;
import de.tuebingen.sfs.tagarela.feedback.diagnosis.ExtraAndMissingDiagnosis;
import de.tuebingen.sfs.tagarela.feedback.diagnosis.POSMatchDiagnosis;
import de.tuebingen.sfs.tagarela.feedback.diagnosis.SpellingDiagnosis;
import de.tuebingen.sfs.tagarela.feedback.diagnosis.StringMatchDiagnosis;
import de.tuebingen.sfs.tagarela.feedback.diagnosis.TokenMatchDiagnosis;
import de.tuebingen.sfs.tagarela.learner.LearnerModel;

/**
 * Diagnosis manager that implements TAGARELA's default strategy.
 * @author rziai
 *
 */
public class TAGARELADiagnosisManager implements DiagnosisManager {

	private ArrayList<DiagnosisModule> submodules;
	
	public TAGARELADiagnosisManager() {
		// our list of submodules
		submodules = new ArrayList<DiagnosisModule>();
		// ordering of modules determines feedback priority
		submodules.add(new StringMatchDiagnosis());
		submodules.add(new SpellingDiagnosis());
		submodules.add(new TokenMatchDiagnosis());
		submodules.add(new CanonicMatchAgreementDiagnosis());
		submodules.add(new POSMatchDiagnosis());
		submodules.add(new ExtraAndMissingDiagnosis());
	}
	
	public FeedbackResult feedback(JCas cas, ActivityModel actModel,
			LearnerModel learnerModel) throws CASException {
		// loop through all modules
		for (DiagnosisModule module : submodules) {
			FeedbackResult result = module.getDiagnosis(cas, actModel, learnerModel);
			
			// stop at the first one that said something
			if (result != null) {
				return result;
			}
		}
		// return some positive message
		return new FeedbackResult(FeedbackMessage.noMessage, null);
	}

	public ResultSpecification getResultSpecification(ActivityModel actModel, TypeSystem typeSystem) {
		ResultSpecification spec = UIMAFramework.getResourceSpecifierFactory().createResultSpecification();
		spec.setTypeSystem(typeSystem);
		
		for (DiagnosisModule dm : submodules) {
			for (TypeOrFeature tof : dm.getAnalysisRequirements()) {
				spec.addResultTypeOrFeature(tof);
			}
		}
		return spec;
	}

}
