package de.tuebingen.sfs.tagarela.feedback.diagnosis;

import java.util.ArrayList;
import java.util.List;

import org.apache.uima.analysis_engine.TypeOrFeature;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;

import de.drni.misc.uima.util.annotation.TypeSystemUtils;
import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.feedback.Error;
import de.tuebingen.sfs.tagarela.feedback.FeedbackMessage;
import de.tuebingen.sfs.tagarela.feedback.FeedbackResult;
import de.tuebingen.sfs.tagarela.feedback.FormError;
import de.tuebingen.sfs.tagarela.feedback.MeaningError;
import de.tuebingen.sfs.tagarela.learner.LearnerModel;
import de.tuebingen.sfs.tagarela.types.AnalysisResults;
import de.tuebingen.sfs.tagarela.types.CanonicMatchResult;
import de.tuebingen.sfs.tagarela.types.Token;
import de.tuebingen.sfs.tagarela.util.Tools;

/**
 * Diagnoses extra and missing concepts.
 * @author rziai
 *
 */
public class ExtraAndMissingDiagnosis implements DiagnosisModule {
	
	public FeedbackResult getDiagnosis(JCas jcas, ActivityModel actModel,
			LearnerModel learnerModel) throws CASException {
		FeedbackResult result = null;
		AnalysisResults ar = Tools.getAnalysisResults(jcas);
		CanonicMatchResult cmr = ar.getCanonicMatch();
		// we need these all the time, so only compute them once
		int unmatched_student = 0;
		for (int i = 0; i < cmr.getUnmatchedStudentTokens().size(); i++) {
			Token t = cmr.getUnmatchedStudentTokens(i);
			if (!t.getSpelling().getMisspelled()) {
				unmatched_student++;
			}
		}
		int unmatched_answer = cmr.getUnmatchedAnswerTokens().size();
		// no unmatched found? return
		if (unmatched_student == 0 && unmatched_answer == 0) {
			return null;
		}
		
		
		// several concepts missing
		if (unmatched_answer > 3) {
			return new FeedbackResult(FeedbackMessage.missingL, null);
		}
		
		// special cases of extra concepts
		result = extraConcepts(jcas, cmr, unmatched_student, unmatched_answer);
		if (result != null) {
			return result;
		}
		
		// missing concepts
		result = missingConcepts(jcas, cmr, unmatched_student, unmatched_answer);
		if (result != null) {
			return result;
		}
		
		// mixture of missing and extra concepts
		result = missingAndExtra(jcas, cmr, unmatched_student, unmatched_answer);
		
		return result;
	}
	
	private FeedbackResult missingAndExtra(JCas jcas, CanonicMatchResult cmr, int unmatched_student, int unmatched_answer) {
		if (unmatched_answer > 0) {
			if (unmatched_answer == 1 && unmatched_student > 0) {
				Token unmatchedA = cmr.getUnmatchedAnswerTokens(0);
				if ("verb".equals(unmatchedA.getCategory())
						|| "noun".equals(unmatchedA.getCategory())) {
					return new FeedbackResult(FeedbackMessage.missing6,
							new String[]{ unmatchedA.getCategory() });
				} else {
					return new FeedbackResult(FeedbackMessage.missing7,
							new String[]{ unmatchedA.getTokenString() });
				}
			} else if (unmatched_answer == 2) {
				return new FeedbackResult(FeedbackMessage.missing_2_words2,
						new String[]{ cmr.getUnmatchedAnswerTokens(0).getTokenString(),
						cmr.getUnmatchedAnswerTokens(1).getTokenString()});
			} else {
				return new FeedbackResult(FeedbackMessage.missing4,
						null);
			}
		}
		return null;
	}
	
	private FeedbackResult missingConcepts(JCas jcas, CanonicMatchResult cmr, int unmatched_student, int unmatched_answer) {
		if (unmatched_student == 0) {
			if (unmatched_answer > 0) {
				if (unmatched_answer == 1) {
					// check for gostar-de error
					Token unmatched = cmr.getUnmatchedAnswerTokens(0);
					if ("de".equals(unmatched.getTokenString())) {
						FSIterator iter = jcas.getAnnotationIndex(Token.type).iterator();
						iter.moveTo(unmatched);
						iter.next();
						if (!iter.hasNext()) {
							return null;
						}
						Token next = (Token) iter.next();
						if ("gostar".equals(next.getLexDef(0).getCanonic())) {
							return new FeedbackResult(FeedbackMessage.gostar_de,
									null);
						} else {
							return new FeedbackResult(FeedbackMessage.no_de,
									null);
						}
					} else {
						if ("verb".equals(unmatched.getCategory())
								|| "noun".equals(unmatched.getCategory())) {
							return new FeedbackResult(FeedbackMessage.missing1,
									new String[]{ unmatched.getCategory() });
						} else {
							return new FeedbackResult(FeedbackMessage.missing5,
									new String[]{ unmatched.getTokenString() });
						}
					}
				} else if (unmatched_answer == 2) {
					return new FeedbackResult(FeedbackMessage.missing_2_words,
							new String[]{ cmr.getUnmatchedAnswerTokens(0).getTokenString(),
							cmr.getUnmatchedAnswerTokens(1).getTokenString()});
				} else {
					return new FeedbackResult(FeedbackMessage.missing2,
							null);
				}
			}
		}
		return null;
	}
	
	private FeedbackResult extraConcepts(JCas jcas, CanonicMatchResult cmr, int unmatched_student, int unmatched_answer) {
		if (unmatched_answer == 0) {
			if (unmatched_student > 0) {
				// look for determiner + proper noun
				for (int i = 0; i < unmatched_answer; i++) {
					Token unmatched = cmr.getUnmatchedAnswerTokens(i);
					if ("det".equals(unmatched.getCategory())) {
						FSIterator iter = jcas.getAnnotationIndex(Token.type).iterator();
						iter.moveTo(unmatched);
						iter.next();
						if (!iter.hasNext()) {
							continue;
						}
						Token next = (Token) iter.next();
						if ("propNoun".equals(next.getCategory())) {
							return new FeedbackResult(FeedbackMessage.det_propNoun,
									new String[]{ unmatched.getCategory() });
						}
					}
				}
			}
			// one extra concept
			if (unmatched_student == 1) {
				return new FeedbackResult(FeedbackMessage.exta_element1,
						new String[]{ cmr.getUnmatchedStudentTokens(0).getTokenString() });
			// multiple extra concepts
			} else if (unmatched_student > 1) {
				String temp = "";
				for (int i = 0; i < unmatched_student; i++) {
					temp += "<b>" + cmr.getUnmatchedStudentTokens(i).getTokenString() + "</b>";
					if (i < unmatched_student-1) {
						temp += ", ";
					} else {
						temp += ".";
					}
				}
				return new FeedbackResult(FeedbackMessage.exta_element2, new String[]{ temp });
			}
		}
		return null;
	}

	public List<TypeOrFeature> getAnalysisRequirements() {
		ArrayList<TypeOrFeature> reqs = new ArrayList<TypeOrFeature>();
		reqs.add(TypeSystemUtils.createFeature(AnalysisResults.class, "tokenMatch"));
		reqs.add(TypeSystemUtils.createFeature(AnalysisResults.class, "canonicMatch"));
		return reqs;
	}

	public Error[] getDetectableErrors() {
		return new Error[]{ MeaningError.EXTRA_CONCEPTS, MeaningError.MISSING_CONCEPTS,
				MeaningError.MISSING_AND_EXTRA, FormError.MISSING_PREP, FormError.EXTRA_DET};
	}

}
