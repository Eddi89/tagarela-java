package de.tuebingen.sfs.tagarela.feedback.diagnosis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.uima.analysis_engine.TypeOrFeature;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;

import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.feedback.Error;
import de.tuebingen.sfs.tagarela.feedback.FeedbackMessage;
import de.tuebingen.sfs.tagarela.feedback.FeedbackResult;
import de.tuebingen.sfs.tagarela.feedback.MeaningError;
import de.tuebingen.sfs.tagarela.learner.LearnerModel;
import de.tuebingen.sfs.tagarela.types.CGReading;
import de.tuebingen.sfs.tagarela.types.CGToken;

/**
 * Diagnosis on word form errors.
 * 
 * @author eduard
 *
 */
public class RusWordChoiceDiagnosis implements DiagnosisModule{
	
	private static final Logger logger = Logger.getLogger("RusWordFormDiagnosis");
	
	public FeedbackResult getDiagnosis(JCas jcas, ActivityModel actModel,
			LearnerModel learnerModel) throws CASException {
		
		FeedbackResult result = null;
		
		JCas bestAnswerLemmaView = jcas.getView("bestAnswerLemma");
		
		String bestAnswerLemma = bestAnswerLemmaView.getDocumentText();
		
		logger.info("bestAnswerLemma=" + bestAnswerLemmaView.getDocumentText());		
		
		// get tokens from the jcas
		FSIterator<Annotation> cgTokenIter = jcas.getAnnotationIndex(CGToken.type).iterator();
		
		// iterate over all tokens
		while (cgTokenIter.hasNext()) {
			
			CGToken cgt = (CGToken) cgTokenIter.next();
			
			String surfaceForm = cgt.getCoveredText();
			logger.info("surfaceForm="+surfaceForm);
			
			String lemma = "";
			
			// iterate over all readings
			for(int i = 0; i < cgt.getReadings().size(); i++){
				CGReading currentReading = cgt.getReadings(i);
				lemma = currentReading.getLemma();
				lemma = lemma.replaceAll("\"", "");
				logger.info("lemma="+lemma);
				// wrong word choice
				if(!bestAnswerLemma.equals(lemma)){
					JCas distractorsView = jcas.getView("distractors");
					
					String distractors = distractorsView.getDocumentText();
					
					logger.info("distractors=" + distractors);
					
					String[] distractorsArr = distractors.split("\t");
					
					List<String> distractorsList = Arrays.asList(distractorsArr);
					
					Collections.shuffle(distractorsList);
					
					List<String> choiceList = new ArrayList<String>(3);
					  
					String choiceSpanStart = "<input type=\"radio\" style=\"cursor: pointer;\" onclick=\"incorrect(this)\"><span>";
					String choiceSpanEnd = "</span><br>";
					
					String choice1 = distractorsList.get(0);
					String choice2 = distractorsList.get(1);
					
					choiceList.add(choiceSpanStart+choice1+choiceSpanEnd);
					logger.info("choice1="+choice1);
					choiceList.add(choiceSpanStart+choice2+choiceSpanEnd);
					logger.info("choice2="+choice2);
					
					choiceSpanStart = choiceSpanStart.replace("incorrect", "correct");
					choiceList.add(choiceSpanStart+bestAnswerLemma+choiceSpanEnd);
					
					Collections.shuffle(choiceList);
					
					String hintButton = "<button type=\"button\" onclick=\"showHint(this)\">What is in the picture?</button>";
					
					String hint = "<span style=\"display: none;\">";
					
					for(String choiceOption: choiceList){
						hint += choiceOption;
					}
					
					hint += "</span>";
					
					hint = hintButton + hint;
					
					result = new FeedbackResult(FeedbackMessage.wrongWordChoice, new String[]{hint});
				}				
			}
		}
		return result;
	}
	/*
	 * (non-Javadoc)
	 * @see de.tuebingen.sfs.tagarela.feedback.diagnosis.DiagnosisModule#getAnalysisRequirements()
	 */
	public List<TypeOrFeature> getAnalysisRequirements() {
		ArrayList<TypeOrFeature> reqs = new ArrayList<TypeOrFeature>();
		return reqs;
	}

	/*
	 * (non-Javadoc)
	 * @see de.tuebingen.sfs.tagarela.feedback.diagnosis.DiagnosisModule#getDetectableErrors()
	 */
	public Error[] getDetectableErrors() {
		return new Error[]{ MeaningError.WORDCHOICE };
	}

}
