package de.tuebingen.sfs.tagarela.feedback.diagnosis;

import java.util.ArrayList;
import java.util.List;

import org.apache.uima.analysis_engine.TypeOrFeature;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;

import de.drni.misc.uima.util.ArrayTools;
import de.drni.misc.uima.util.annotation.TypeSystemUtils;
import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.feedback.Error;
import de.tuebingen.sfs.tagarela.feedback.FeedbackMessage;
import de.tuebingen.sfs.tagarela.feedback.FeedbackResult;
import de.tuebingen.sfs.tagarela.feedback.FormError;
import de.tuebingen.sfs.tagarela.learner.LearnerModel;
import de.tuebingen.sfs.tagarela.types.Token;

/**
 * Diagnoses spelling errors.
 * @author rziai
 *
 */
public class SpellingDiagnosis implements DiagnosisModule {

	public FeedbackResult getDiagnosis(JCas cas, ActivityModel actModel,
			LearnerModel learnerModel) {
		ArrayList<Token> wrongOnes = new ArrayList<Token>();
		
		JCas learnerCAS = cas;
		FSIterator iter = learnerCAS.getAnnotationIndex(Token.type).iterator();
		
		while (iter.hasNext()) {
			Token t = (Token) iter.next();
			if (t.getSpelling().getMisspelled()) {
				wrongOnes.add(t);
			}
		}
		
		FeedbackResult result = null;
		if (!wrongOnes.isEmpty()) {
			//result += "You seem to have at least one spelling error:";
			String temp = "";
			for (Token t : wrongOnes) {
				temp += "<p><b><i>" + t.getCoveredText() + "</i></b>: " +
				ArrayTools.toString(t.getSpelling().getSuggestions()) + "</p>";
			}
			result = new FeedbackResult(FeedbackMessage.spell4, new String[]{temp});
		}
		return result;
	}

	public List<TypeOrFeature> getAnalysisRequirements() {
		ArrayList<TypeOrFeature> reqs = new ArrayList<TypeOrFeature>();
		reqs.add(TypeSystemUtils.createFeature(Token.class, "spellingAnalysis"));
		return reqs;
	}

	public Error[] getDetectableErrors() {
		return new Error[]{ FormError.SPELLING };
	}

}
