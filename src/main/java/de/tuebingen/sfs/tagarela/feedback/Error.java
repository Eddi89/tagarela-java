package de.tuebingen.sfs.tagarela.feedback;

/**
 * An error type in the ILTS. Currently there are <code>MeaningError</code>
 * and <code>FormError</code> that both implement this interface.
 * @author rziai
 *
 */
public interface Error {
	
	/**
	 * Returns a name for the error suitable for humans.
	 * @return
	 */
	public String getDisplayName();
}
