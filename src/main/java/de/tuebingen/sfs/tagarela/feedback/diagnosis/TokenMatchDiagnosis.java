package de.tuebingen.sfs.tagarela.feedback.diagnosis;

import java.util.ArrayList;
import java.util.List;

import org.apache.uima.analysis_engine.TypeOrFeature;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;

import de.drni.misc.uima.util.annotation.TypeSystemUtils;
import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.feedback.Error;
import de.tuebingen.sfs.tagarela.feedback.FeedbackMessage;
import de.tuebingen.sfs.tagarela.feedback.FeedbackResult;
import de.tuebingen.sfs.tagarela.feedback.FormError;
import de.tuebingen.sfs.tagarela.learner.LearnerModel;
import de.tuebingen.sfs.tagarela.types.AnalysisResults;
import de.tuebingen.sfs.tagarela.types.TokenMatchResult;
import de.tuebingen.sfs.tagarela.util.StringMatchConstants;
import de.tuebingen.sfs.tagarela.util.Tools;

/**
 * Diagnoses word order errors.
 * @author rziai
 *
 */
public class TokenMatchDiagnosis implements DiagnosisModule {

	public FeedbackResult getDiagnosis(JCas jcas, ActivityModel actModel,
			LearnerModel learnerModel) throws CASException {
		
		AnalysisResults ar = Tools.getAnalysisResults(jcas);
		TokenMatchResult tmr = ar.getTokenMatch();
		FeedbackResult result = null;
		
		/* FIXME this is highly naive... need to find a better heuristic
		 *
		 */
		if (StringMatchConstants.NO_MATCH.equals(ar.getStringMatch()) && tmr.getUnmatchedStudentTokens().size() == 0
				&& tmr.getUnmatchedAnswerTokens().size() == 0) {
			result = new FeedbackResult(FeedbackMessage.descWO1, null);
			// TODO integrate check for extra punctuation!
		}
		
		return result;
	}

	public List<TypeOrFeature> getAnalysisRequirements() {
		ArrayList<TypeOrFeature> reqs = new ArrayList<TypeOrFeature>();
		reqs.add(TypeSystemUtils.createFeature(AnalysisResults.class, "tokenMatch"));
		reqs.add(TypeSystemUtils.createFeature(AnalysisResults.class, "stringMatch"));
		return reqs;
	}

	public Error[] getDetectableErrors() {
		return new Error[]{ FormError.WORD_ORDER };
	}

}
