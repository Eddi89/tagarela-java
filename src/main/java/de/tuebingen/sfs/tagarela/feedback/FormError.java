package de.tuebingen.sfs.tagarela.feedback;

/**
 * Form-based errors in the system.
 * @author rziai
 *
 */
public enum FormError implements Error {
	PUNCTUATION ("Punctuation error"),
	CAPITALIZATION ("Capitalization error"),
	SPELLING ("Spelling error"),
	WORD_ORDER ("Wrong word order"),
	AGREEMENT ("Agreement error"),
	WORDFORM ("Wrong word form"),
	MISSING_PREP ("Missing preposition"),
	EXTRA_DET ("Extra determiner");
	
	private String displayName;
	
	FormError(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}
}
