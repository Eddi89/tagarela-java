package de.tuebingen.sfs.tagarela.feedback.diagnosis;

import java.util.ArrayList;
import java.util.List;

import org.apache.uima.analysis_engine.TypeOrFeature;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;

import de.drni.misc.uima.util.annotation.TypeSystemUtils;
import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.feedback.Error;
import de.tuebingen.sfs.tagarela.feedback.FeedbackMessage;
import de.tuebingen.sfs.tagarela.feedback.FeedbackResult;
import de.tuebingen.sfs.tagarela.feedback.FormError;
import de.tuebingen.sfs.tagarela.feedback.MeaningError;
import de.tuebingen.sfs.tagarela.learner.LearnerModel;
import de.tuebingen.sfs.tagarela.types.AnalysisResults;
import de.tuebingen.sfs.tagarela.util.StringMatchConstants;
import de.tuebingen.sfs.tagarela.util.Tools;

/**
 * Diagnoses punctuation, capitalization, and superfluous spaces.
 * @author rziai
 *
 */
public class StringMatchDiagnosis implements DiagnosisModule {

	public FeedbackResult getDiagnosis(JCas jcas, ActivityModel actModel,
			LearnerModel learnerModel) throws CASException {
		AnalysisResults ar = Tools.getAnalysisResults(jcas);
		String matchResult = ar.getStringMatch();
		
		FeedbackResult result = null;
		
		if (StringMatchConstants.NO_INPUT.equals(matchResult)) {
			result = new FeedbackResult(FeedbackMessage.missing3, null);
		} else if (StringMatchConstants.EXACT_MATCH.equals(matchResult)) {
			result = new FeedbackResult(FeedbackMessage.strM8, null);
		} else if (StringMatchConstants.LOWERCASE_MATCH.equals(matchResult)) {
			result = new FeedbackResult(FeedbackMessage.strM2, null);
		} else if (StringMatchConstants.SPACE_MATCH.equals(matchResult)) {
			result = new FeedbackResult(FeedbackMessage.strM13, null);
		} else if (StringMatchConstants.NOPUNC_MATCH.equals(matchResult)) {
			result = new FeedbackResult(FeedbackMessage.strM3, null);
		} else if (StringMatchConstants.LOWERCASE_NOPUNC_MATCH.equals(matchResult)) {
			result = new FeedbackResult(FeedbackMessage.strM4, null);
		}
		
		return result;
	}

	public List<TypeOrFeature> getAnalysisRequirements() {
		ArrayList<TypeOrFeature> reqs = new ArrayList<TypeOrFeature>();
		reqs.add(TypeSystemUtils.createFeature(AnalysisResults.class, "stringMatch"));
		return reqs;
	}

	public Error[] getDetectableErrors() {
		return new Error[]{ FormError.CAPITALIZATION, FormError.PUNCTUATION, MeaningError.MISSING_CONCEPTS};
	}

	
}
