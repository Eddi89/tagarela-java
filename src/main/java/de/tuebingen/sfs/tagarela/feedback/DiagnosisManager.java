package de.tuebingen.sfs.tagarela.feedback;

import org.apache.uima.analysis_engine.ResultSpecification;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.jcas.JCas;

import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.learner.LearnerModel;

/**
 * General diagnosis manager interface.
 * @author rziai
 *
 */
public interface DiagnosisManager {

	/**
	 * Returns a feedback message according to the annotated input, activity model 
	 * and learner model passed as parameters.
	 * @param cas The annotated input
	 * @param actModel The activity model
	 * @param learnerModel The learner model
	 * @return
	 * @throws CASException
	 */
	public FeedbackResult feedback(JCas cas, ActivityModel actModel,
			LearnerModel learnerModel) throws CASException;
	
	/**
	 * Returns a <code>ResultSpecification</code>, i.e. a description of what
	 * analysis types are required according to the given activity model and type system. 
	 * @param actModel The activity model
	 * @param typeSystem The type system to use.
	 * @return
	 */
	public ResultSpecification getResultSpecification(ActivityModel actModel, TypeSystem typeSystem);

}