package de.tuebingen.sfs.tagarela.test;

import java.io.FileNotFoundException;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import com.sleepycat.db.Database;
import com.sleepycat.db.DatabaseEntry;
import com.sleepycat.db.DatabaseException;

public class DBTest {

	/**
	 * @param args
	 * @throws DatabaseException 
	 * @throws FileNotFoundException 
	 * @throws JSONException 
	 */
	public static void main(String[] args) throws FileNotFoundException, DatabaseException, JSONException {
		Database myDatabase = new Database(args[0], null, null);
		DatabaseEntry key = new DatabaseEntry("current".getBytes());
		DatabaseEntry value = new DatabaseEntry();
		myDatabase.get(null, key, value, null);
		myDatabase.close();
		JSONObject object = new JSONObject(new String(value.getData()));
		
		System.out.println(object.getJSONObject("actSpec")
				.getJSONObject("presentation")
				.getJSONObject("instructions")
				.getString("L2"));
		Iterator<String> iter = object.keys();
	
		while (iter.hasNext()) {
			String keyStr = iter.next();
			System.out.println("Key: " + keyStr + "\tValue: " + object.getString(keyStr));
		}
	}

}
