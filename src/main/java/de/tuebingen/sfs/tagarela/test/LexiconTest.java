package de.tuebingen.sfs.tagarela.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.uutuc.factory.AnalysisEngineFactory;

import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.activity.ActivityModel.Question;
import de.tuebingen.sfs.tagarela.activity.ActivityModel.Target;
import de.tuebingen.sfs.tagarela.types.Token;

public class LexiconTest {

	private static final String[] dirs = new String[]{
		"WebContent/Description",
		"WebContent/FIB",
		"WebContent/Listening",
		"WebContent/Reading",
		"WebContent/Rephrasing",
		"WebContent/Vocabulary"};
	
	/**
	 * @param args
	 * @throws IOException 
	 * @throws UIMAException 
	 */
	public static void main(String[] args) throws UIMAException, IOException {
		HashMap<String, Integer> knownWordList = new HashMap<String, Integer>();
		HashMap<String, Integer> unknownWordList = new HashMap<String, Integer>();
		
		AnalysisEngine ae = AnalysisEngineFactory.createAnalysisEngineFromPath("desc/LexiconAggregate.xml", new Object[0]);
		
		for (String dirName : dirs) {
			System.out.println("Testing " + dirName);
			File dir = new File(dirName);
			for (File mod : dir.listFiles()) {
				if (!mod.getName().startsWith("Module")) {
					continue;
				}
				System.out.println("\t" + mod.getName());
				for (File nr : mod.listFiles()) {
					if (nr.getName().startsWith(".")) {
						continue;
					}
					System.out.println("\t\tExercise " + nr.getName());
					ActivityModel actModel = new ActivityModel(
							new FileInputStream(nr.getAbsolutePath() +
									File.separator + "act.xml"));
					int i = 1;
					for (Question q : actModel.getQuestions()) {
						System.out.println("\t\t\tQuestion " + i);
						for (Target t : q.getTargets()) {
							String tStr = t.getTargetStr();
							//assertNotNull(tStr);
							System.out.println("Testing sentence: " + tStr);
							JCas jcas = ae.newJCas();
							jcas.setDocumentText(tStr);
							ae.process(jcas);
							
							FSIterator iter = jcas.getAnnotationIndex(Token.type).iterator();
							while (iter.hasNext()) {
								Token tok = (Token) iter.next();
								String lower = tok.getTokenString().toLowerCase();
								// unknown words
								if (tok.getLexDef() == null || tok.getLexDef().size() == 0) {
									Integer count = unknownWordList.get(lower);
									if (count == null) {
										unknownWordList.put(lower, 1);
									} else {
										unknownWordList.put(lower, count+1);
									}
								// known words
								} else {
									Integer count = knownWordList.get(lower);
									if (count == null) {
										knownWordList.put(lower, 1);
									} else {
										knownWordList.put(lower, count+1);
									}
								}
							}
							
							
						}
						i++;
					}
					
				}
				
			}
		}
		
		int totalKnownTypes = knownWordList.size();
		int totalKnownWords = 0;
		for (String w : knownWordList.keySet()) {
			totalKnownWords += knownWordList.get(w);
		}
		int totalUnKnownTypes = unknownWordList.size();
		int totalUnKnownWords = 0;
		for (String w : unknownWordList.keySet()) {
			totalUnKnownWords += unknownWordList.get(w);
		}
		
		System.out.println("Total type count: " + (totalKnownTypes+totalUnKnownTypes));
		System.out.println("Known types: " + totalKnownTypes);
		System.out.println("Unknown types: " + totalUnKnownTypes);
		
		System.out.println("Total word count: " + (totalKnownWords+totalUnKnownWords));
		System.out.println("Known words: " + totalKnownWords);
		System.out.println("Unknown words: " + totalUnKnownWords);
		System.out.println();
		
		System.out.println("Unknown instances:");
		for (String w : unknownWordList.keySet()) {
			System.out.println(w);
		}
	}

}
