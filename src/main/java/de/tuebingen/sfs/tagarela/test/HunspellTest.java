package de.tuebingen.sfs.tagarela.test;

import java.io.FileNotFoundException;
import com.stibocatalog.hunspell.Hunspell;
import com.stibocatalog.hunspell.Hunspell.Dictionary;

public class HunspellTest {

	/**
	 * @param args
	 * @throws  
	 * @throws UnsupportedEncodingException 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws Exception {
		Dictionary dict = Hunspell.getInstance().getDictionary("resources/dict/pt_BR");
		if (dict.misspelled(args[0])) {
			System.out.println("Input " + args[0] + " looks wrong, here are some suggestions:");
			System.out.println(dict.suggest(args[0]));
		} else {
			System.out.println("Input is fine!");
		}
	}

}
