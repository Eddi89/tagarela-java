package de.tuebingen.sfs.tagarela.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.uutuc.factory.AnalysisEngineFactory;
import org.uutuc.util.AnnotationRetrieval;

import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.activity.ActivityModel.Question;
import de.tuebingen.sfs.tagarela.activity.ActivityModel.Target;
import de.tuebingen.sfs.tagarela.types.Phrase;
import de.tuebingen.sfs.tagarela.types.Token;

public class BKNParserTest {

	private static final String[] dirs = new String[]{
		"WebContent/Description",
		//"WebContent/FIB",
		"WebContent/Listening",
		"WebContent/Reading",
		"WebContent/Rephrasing",
		"WebContent/Vocabulary"};
	
	
	
	private AnalysisEngine ae;
	
	public ArrayList<String> successes;
	public ArrayList<String> failedPOS;
	public ArrayList<String> failedGrammar;
	
	
	public BKNParserTest() throws Exception {
		ae = AnalysisEngineFactory.createAnalysisEngineFromPath("desc/ParserAggregate.xml", new Object[0]);
		ae.getLogger().setLevel(Level.OFF);
		successes = new ArrayList<String>();
		failedPOS = new ArrayList<String>();
		failedGrammar = new ArrayList<String>();
	}

	public void testParse() throws FileNotFoundException, IOException, ResourceInitializationException, AnalysisEngineProcessException {
		for (String dirName : dirs) {
			System.out.println("Testing " + dirName);
			File dir = new File(dirName);
			for (File mod : dir.listFiles()) {
				if (!mod.getName().startsWith("Module")) {
					continue;
				}
				System.out.println("\t" + mod.getName());
				for (File nr : mod.listFiles()) {
					if (nr.getName().startsWith(".")) {
						continue;
					}
					System.out.println("\t\tExercise " + nr.getName());
					ActivityModel actModel = new ActivityModel(
							new FileInputStream(nr.getAbsolutePath() +
									File.separator + "act.xml"));
					int i = 1;
					for (Question q : actModel.getQuestions()) {
						System.out.println("\t\t\tQuestion " + i);
					TLOOP:
						for (Target t : q.getTargets()) {
							String tStr = t.getTargetStr();
							//assertNotNull(tStr);
							System.out.println("Testing sentence: " + tStr);
							JCas jcas = ae.newJCas();
							jcas.setDocumentText(tStr);
							ae.process(jcas);
							
							Token last = AnnotationRetrieval.get(jcas, Token.class, -1);
							FSIterator iter = jcas.getAnnotationIndex(Token.type).iterator();
							while (iter.hasNext()) {
								Token tok = (Token) iter.next();
								if (tok.getCategory() == null) {
									failedPOS.add(tStr);
									continue TLOOP;
								}
							}
							
							Phrase p = AnnotationRetrieval.get(jcas, Phrase.class, 0);
							if (p == null) {
								failedGrammar.add(tStr);
								continue;
							}
							if (0 != p.getBegin()) {
								failedGrammar.add(tStr);
								continue;
							}
							if (last.getEnd() != p.getEnd()) {
								failedGrammar.add(tStr);
								continue;
							}
							successes.add(tStr);
						}
						i++;
					}
					
				}
				
			}
		}
	}

	public static void main(String[] args) throws Exception {
		BKNParserTest test = new BKNParserTest();
		test.testParse();
		System.out.println();
		System.out.println("Successful parses: " + test.successes.size());
		System.out.println("Failed parses: " + (test.failedPOS.size()+test.failedGrammar.size()));
		System.out.println("Total: " + (test.successes.size()+test.failedPOS.size()+test.failedGrammar.size()));
		/*
		System.out.println("Failed sentences:");
		for (String s : test.failures) {
			System.out.println(s);
		}
		*/
		System.out.println();
		System.out.println("Failed due to missing POS info: " + test.failedPOS.size());
		for (String s : test.failedPOS) {
			System.out.println(s);
		}
		
		System.out.println();
		System.out.println("Failed due to lacking grammar coverage: " + test.failedGrammar.size());
		for (String s : test.failedGrammar) {
			System.out.println(s);
		}

	}
}
