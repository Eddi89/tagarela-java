
/* First created by JCasGen Thu Jun 11 12:31:13 CEST 2015 */
package de.tuebingen.sfs.tagarela.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.cas.TOP_Type;

/** A mapping of two tokens.
 * Updated by JCasGen Tue Jun 16 03:18:14 CEST 2015
 * @generated */
public class TokenMapping_Type extends TOP_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (TokenMapping_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = TokenMapping_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new TokenMapping(addr, TokenMapping_Type.this);
  			   TokenMapping_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new TokenMapping(addr, TokenMapping_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = TokenMapping.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("de.tuebingen.sfs.tagarela.types.TokenMapping");
 
  /** @generated */
  final Feature casFeat_first;
  /** @generated */
  final int     casFeatCode_first;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getFirst(int addr) {
        if (featOkTst && casFeat_first == null)
      jcas.throwFeatMissing("first", "de.tuebingen.sfs.tagarela.types.TokenMapping");
    return ll_cas.ll_getRefValue(addr, casFeatCode_first);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setFirst(int addr, int v) {
        if (featOkTst && casFeat_first == null)
      jcas.throwFeatMissing("first", "de.tuebingen.sfs.tagarela.types.TokenMapping");
    ll_cas.ll_setRefValue(addr, casFeatCode_first, v);}
    
  
 
  /** @generated */
  final Feature casFeat_second;
  /** @generated */
  final int     casFeatCode_second;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getSecond(int addr) {
        if (featOkTst && casFeat_second == null)
      jcas.throwFeatMissing("second", "de.tuebingen.sfs.tagarela.types.TokenMapping");
    return ll_cas.ll_getRefValue(addr, casFeatCode_second);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setSecond(int addr, int v) {
        if (featOkTst && casFeat_second == null)
      jcas.throwFeatMissing("second", "de.tuebingen.sfs.tagarela.types.TokenMapping");
    ll_cas.ll_setRefValue(addr, casFeatCode_second, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public TokenMapping_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_first = jcas.getRequiredFeatureDE(casType, "first", "de.tuebingen.sfs.tagarela.types.Token", featOkTst);
    casFeatCode_first  = (null == casFeat_first) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_first).getCode();

 
    casFeat_second = jcas.getRequiredFeatureDE(casType, "second", "de.tuebingen.sfs.tagarela.types.Token", featOkTst);
    casFeatCode_second  = (null == casFeat_second) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_second).getCode();

  }
}



    