
/* First created by JCasGen Thu Jun 11 12:31:13 CEST 2015 */
package de.tuebingen.sfs.tagarela.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.cas.TOP_Type;

/** Spell checker analysis of a given token.
 * Updated by JCasGen Tue Jun 16 03:18:14 CEST 2015
 * @generated */
public class Spelling_Type extends TOP_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Spelling_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Spelling_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Spelling(addr, Spelling_Type.this);
  			   Spelling_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Spelling(addr, Spelling_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Spelling.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("de.tuebingen.sfs.tagarela.types.Spelling");
 
  /** @generated */
  final Feature casFeat_misspelled;
  /** @generated */
  final int     casFeatCode_misspelled;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public boolean getMisspelled(int addr) {
        if (featOkTst && casFeat_misspelled == null)
      jcas.throwFeatMissing("misspelled", "de.tuebingen.sfs.tagarela.types.Spelling");
    return ll_cas.ll_getBooleanValue(addr, casFeatCode_misspelled);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setMisspelled(int addr, boolean v) {
        if (featOkTst && casFeat_misspelled == null)
      jcas.throwFeatMissing("misspelled", "de.tuebingen.sfs.tagarela.types.Spelling");
    ll_cas.ll_setBooleanValue(addr, casFeatCode_misspelled, v);}
    
  
 
  /** @generated */
  final Feature casFeat_suggestions;
  /** @generated */
  final int     casFeatCode_suggestions;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getSuggestions(int addr) {
        if (featOkTst && casFeat_suggestions == null)
      jcas.throwFeatMissing("suggestions", "de.tuebingen.sfs.tagarela.types.Spelling");
    return ll_cas.ll_getRefValue(addr, casFeatCode_suggestions);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setSuggestions(int addr, int v) {
        if (featOkTst && casFeat_suggestions == null)
      jcas.throwFeatMissing("suggestions", "de.tuebingen.sfs.tagarela.types.Spelling");
    ll_cas.ll_setRefValue(addr, casFeatCode_suggestions, v);}
    
   /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @return value at index i in the array 
   */
  public String getSuggestions(int addr, int i) {
        if (featOkTst && casFeat_suggestions == null)
      jcas.throwFeatMissing("suggestions", "de.tuebingen.sfs.tagarela.types.Spelling");
    if (lowLevelTypeChecks)
      return ll_cas.ll_getStringArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_suggestions), i, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_suggestions), i);
  return ll_cas.ll_getStringArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_suggestions), i);
  }
   
  /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @param v value to set
   */ 
  public void setSuggestions(int addr, int i, String v) {
        if (featOkTst && casFeat_suggestions == null)
      jcas.throwFeatMissing("suggestions", "de.tuebingen.sfs.tagarela.types.Spelling");
    if (lowLevelTypeChecks)
      ll_cas.ll_setStringArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_suggestions), i, v, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_suggestions), i);
    ll_cas.ll_setStringArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_suggestions), i, v);
  }
 



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public Spelling_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_misspelled = jcas.getRequiredFeatureDE(casType, "misspelled", "uima.cas.Boolean", featOkTst);
    casFeatCode_misspelled  = (null == casFeat_misspelled) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_misspelled).getCode();

 
    casFeat_suggestions = jcas.getRequiredFeatureDE(casType, "suggestions", "uima.cas.StringArray", featOkTst);
    casFeatCode_suggestions  = (null == casFeat_suggestions) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_suggestions).getCode();

  }
}



    