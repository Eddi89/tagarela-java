

/* First created by JCasGen Thu Jun 11 12:31:13 CEST 2015 */
package de.tuebingen.sfs.tagarela.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.FSArray;


/** A token in running text.
 * Updated by JCasGen Tue Jun 16 03:18:14 CEST 2015
 * XML source: /home/eduard/workspace3/tagarela-java/desc/basicNlpTypes.xml
 * @generated */
public class Token extends Constituent {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Token.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Token() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public Token(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public Token(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public Token(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: spelling

  /** getter for spelling - gets Contains spell checking information for this token.
   * @generated
   * @return value of the feature 
   */
  public Spelling getSpelling() {
    if (Token_Type.featOkTst && ((Token_Type)jcasType).casFeat_spelling == null)
      jcasType.jcas.throwFeatMissing("spelling", "de.tuebingen.sfs.tagarela.types.Token");
    return (Spelling)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Token_Type)jcasType).casFeatCode_spelling)));}
    
  /** setter for spelling - sets Contains spell checking information for this token. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setSpelling(Spelling v) {
    if (Token_Type.featOkTst && ((Token_Type)jcasType).casFeat_spelling == null)
      jcasType.jcas.throwFeatMissing("spelling", "de.tuebingen.sfs.tagarela.types.Token");
    jcasType.ll_cas.ll_setRefValue(addr, ((Token_Type)jcasType).casFeatCode_spelling, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: deepForm

  /** getter for deepForm - gets The underlying form of the token which is itself a list of tokens. This is needed for contractions and other cases where tokens need to be analyzed on the basis of their underlying form.
   * @generated
   * @return value of the feature 
   */
  public FSArray getDeepForm() {
    if (Token_Type.featOkTst && ((Token_Type)jcasType).casFeat_deepForm == null)
      jcasType.jcas.throwFeatMissing("deepForm", "de.tuebingen.sfs.tagarela.types.Token");
    return (FSArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Token_Type)jcasType).casFeatCode_deepForm)));}
    
  /** setter for deepForm - sets The underlying form of the token which is itself a list of tokens. This is needed for contractions and other cases where tokens need to be analyzed on the basis of their underlying form. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setDeepForm(FSArray v) {
    if (Token_Type.featOkTst && ((Token_Type)jcasType).casFeat_deepForm == null)
      jcasType.jcas.throwFeatMissing("deepForm", "de.tuebingen.sfs.tagarela.types.Token");
    jcasType.ll_cas.ll_setRefValue(addr, ((Token_Type)jcasType).casFeatCode_deepForm, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for deepForm - gets an indexed value - The underlying form of the token which is itself a list of tokens. This is needed for contractions and other cases where tokens need to be analyzed on the basis of their underlying form.
   * @generated
   * @param i index in the array to get
   * @return value of the element at index i 
   */
  public Token getDeepForm(int i) {
    if (Token_Type.featOkTst && ((Token_Type)jcasType).casFeat_deepForm == null)
      jcasType.jcas.throwFeatMissing("deepForm", "de.tuebingen.sfs.tagarela.types.Token");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((Token_Type)jcasType).casFeatCode_deepForm), i);
    return (Token)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((Token_Type)jcasType).casFeatCode_deepForm), i)));}

  /** indexed setter for deepForm - sets an indexed value - The underlying form of the token which is itself a list of tokens. This is needed for contractions and other cases where tokens need to be analyzed on the basis of their underlying form.
   * @generated
   * @param i index in the array to set
   * @param v value to set into the array 
   */
  public void setDeepForm(int i, Token v) { 
    if (Token_Type.featOkTst && ((Token_Type)jcasType).casFeat_deepForm == null)
      jcasType.jcas.throwFeatMissing("deepForm", "de.tuebingen.sfs.tagarela.types.Token");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((Token_Type)jcasType).casFeatCode_deepForm), i);
    jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((Token_Type)jcasType).casFeatCode_deepForm), i, jcasType.ll_cas.ll_getFSRef(v));}
   
    
  //*--------------*
  //* Feature: lexDef

  /** getter for lexDef - gets A list of possible lexicon info bundles for a given token.
   * @generated
   * @return value of the feature 
   */
  public FSArray getLexDef() {
    if (Token_Type.featOkTst && ((Token_Type)jcasType).casFeat_lexDef == null)
      jcasType.jcas.throwFeatMissing("lexDef", "de.tuebingen.sfs.tagarela.types.Token");
    return (FSArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Token_Type)jcasType).casFeatCode_lexDef)));}
    
  /** setter for lexDef - sets A list of possible lexicon info bundles for a given token. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setLexDef(FSArray v) {
    if (Token_Type.featOkTst && ((Token_Type)jcasType).casFeat_lexDef == null)
      jcasType.jcas.throwFeatMissing("lexDef", "de.tuebingen.sfs.tagarela.types.Token");
    jcasType.ll_cas.ll_setRefValue(addr, ((Token_Type)jcasType).casFeatCode_lexDef, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for lexDef - gets an indexed value - A list of possible lexicon info bundles for a given token.
   * @generated
   * @param i index in the array to get
   * @return value of the element at index i 
   */
  public LexiconInfo getLexDef(int i) {
    if (Token_Type.featOkTst && ((Token_Type)jcasType).casFeat_lexDef == null)
      jcasType.jcas.throwFeatMissing("lexDef", "de.tuebingen.sfs.tagarela.types.Token");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((Token_Type)jcasType).casFeatCode_lexDef), i);
    return (LexiconInfo)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((Token_Type)jcasType).casFeatCode_lexDef), i)));}

  /** indexed setter for lexDef - sets an indexed value - A list of possible lexicon info bundles for a given token.
   * @generated
   * @param i index in the array to set
   * @param v value to set into the array 
   */
  public void setLexDef(int i, LexiconInfo v) { 
    if (Token_Type.featOkTst && ((Token_Type)jcasType).casFeat_lexDef == null)
      jcasType.jcas.throwFeatMissing("lexDef", "de.tuebingen.sfs.tagarela.types.Token");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((Token_Type)jcasType).casFeatCode_lexDef), i);
    jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((Token_Type)jcasType).casFeatCode_lexDef), i, jcasType.ll_cas.ll_getFSRef(v));}
   
    
  //*--------------*
  //* Feature: tokenString

  /** getter for tokenString - gets the token string itself - needed so that we can process deep forms the same way as normal tokens
   * @generated
   * @return value of the feature 
   */
  public String getTokenString() {
    if (Token_Type.featOkTst && ((Token_Type)jcasType).casFeat_tokenString == null)
      jcasType.jcas.throwFeatMissing("tokenString", "de.tuebingen.sfs.tagarela.types.Token");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Token_Type)jcasType).casFeatCode_tokenString);}
    
  /** setter for tokenString - sets the token string itself - needed so that we can process deep forms the same way as normal tokens 
   * @generated
   * @param v value to set into the feature 
   */
  public void setTokenString(String v) {
    if (Token_Type.featOkTst && ((Token_Type)jcasType).casFeat_tokenString == null)
      jcasType.jcas.throwFeatMissing("tokenString", "de.tuebingen.sfs.tagarela.types.Token");
    jcasType.ll_cas.ll_setStringValue(addr, ((Token_Type)jcasType).casFeatCode_tokenString, v);}    
  }

    