

/* First created by JCasGen Thu Jun 11 12:31:13 CEST 2015 */
package de.tuebingen.sfs.tagarela.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.jcas.cas.NonEmptyFSList;


/** Analysis results that hold for the whole string in question.
 * Updated by JCasGen Tue Jun 16 03:18:14 CEST 2015
 * XML source: /home/eduard/workspace3/tagarela-java/desc/basicNlpTypes.xml
 * @generated */
public class AnalysisResults extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(AnalysisResults.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected AnalysisResults() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public AnalysisResults(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public AnalysisResults(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public AnalysisResults(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: stringMatch

  /** getter for stringMatch - gets Indicates whether we have a string match and if so, under which circumstances (full string or tokenized)
   * @generated
   * @return value of the feature 
   */
  public String getStringMatch() {
    if (AnalysisResults_Type.featOkTst && ((AnalysisResults_Type)jcasType).casFeat_stringMatch == null)
      jcasType.jcas.throwFeatMissing("stringMatch", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    return jcasType.ll_cas.ll_getStringValue(addr, ((AnalysisResults_Type)jcasType).casFeatCode_stringMatch);}
    
  /** setter for stringMatch - sets Indicates whether we have a string match and if so, under which circumstances (full string or tokenized) 
   * @generated
   * @param v value to set into the feature 
   */
  public void setStringMatch(String v) {
    if (AnalysisResults_Type.featOkTst && ((AnalysisResults_Type)jcasType).casFeat_stringMatch == null)
      jcasType.jcas.throwFeatMissing("stringMatch", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    jcasType.ll_cas.ll_setStringValue(addr, ((AnalysisResults_Type)jcasType).casFeatCode_stringMatch, v);}    
   
    
  //*--------------*
  //* Feature: agreementResults

  /** getter for agreementResults - gets holds information about agreement within the input
   * @generated
   * @return value of the feature 
   */
  public NonEmptyFSList getAgreementResults() {
    if (AnalysisResults_Type.featOkTst && ((AnalysisResults_Type)jcasType).casFeat_agreementResults == null)
      jcasType.jcas.throwFeatMissing("agreementResults", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    return (NonEmptyFSList)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((AnalysisResults_Type)jcasType).casFeatCode_agreementResults)));}
    
  /** setter for agreementResults - sets holds information about agreement within the input 
   * @generated
   * @param v value to set into the feature 
   */
  public void setAgreementResults(NonEmptyFSList v) {
    if (AnalysisResults_Type.featOkTst && ((AnalysisResults_Type)jcasType).casFeat_agreementResults == null)
      jcasType.jcas.throwFeatMissing("agreementResults", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    jcasType.ll_cas.ll_setRefValue(addr, ((AnalysisResults_Type)jcasType).casFeatCode_agreementResults, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: tokenMatch

  /** getter for tokenMatch - gets token match result
   * @generated
   * @return value of the feature 
   */
  public TokenMatchResult getTokenMatch() {
    if (AnalysisResults_Type.featOkTst && ((AnalysisResults_Type)jcasType).casFeat_tokenMatch == null)
      jcasType.jcas.throwFeatMissing("tokenMatch", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    return (TokenMatchResult)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((AnalysisResults_Type)jcasType).casFeatCode_tokenMatch)));}
    
  /** setter for tokenMatch - sets token match result 
   * @generated
   * @param v value to set into the feature 
   */
  public void setTokenMatch(TokenMatchResult v) {
    if (AnalysisResults_Type.featOkTst && ((AnalysisResults_Type)jcasType).casFeat_tokenMatch == null)
      jcasType.jcas.throwFeatMissing("tokenMatch", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    jcasType.ll_cas.ll_setRefValue(addr, ((AnalysisResults_Type)jcasType).casFeatCode_tokenMatch, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: canonicMatch

  /** getter for canonicMatch - gets canonic match result
   * @generated
   * @return value of the feature 
   */
  public CanonicMatchResult getCanonicMatch() {
    if (AnalysisResults_Type.featOkTst && ((AnalysisResults_Type)jcasType).casFeat_canonicMatch == null)
      jcasType.jcas.throwFeatMissing("canonicMatch", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    return (CanonicMatchResult)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((AnalysisResults_Type)jcasType).casFeatCode_canonicMatch)));}
    
  /** setter for canonicMatch - sets canonic match result 
   * @generated
   * @param v value to set into the feature 
   */
  public void setCanonicMatch(CanonicMatchResult v) {
    if (AnalysisResults_Type.featOkTst && ((AnalysisResults_Type)jcasType).casFeat_canonicMatch == null)
      jcasType.jcas.throwFeatMissing("canonicMatch", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    jcasType.ll_cas.ll_setRefValue(addr, ((AnalysisResults_Type)jcasType).casFeatCode_canonicMatch, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: posMatch

  /** getter for posMatch - gets pos match result
   * @generated
   * @return value of the feature 
   */
  public POSMatchResult getPosMatch() {
    if (AnalysisResults_Type.featOkTst && ((AnalysisResults_Type)jcasType).casFeat_posMatch == null)
      jcasType.jcas.throwFeatMissing("posMatch", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    return (POSMatchResult)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((AnalysisResults_Type)jcasType).casFeatCode_posMatch)));}
    
  /** setter for posMatch - sets pos match result 
   * @generated
   * @param v value to set into the feature 
   */
  public void setPosMatch(POSMatchResult v) {
    if (AnalysisResults_Type.featOkTst && ((AnalysisResults_Type)jcasType).casFeat_posMatch == null)
      jcasType.jcas.throwFeatMissing("posMatch", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    jcasType.ll_cas.ll_setRefValue(addr, ((AnalysisResults_Type)jcasType).casFeatCode_posMatch, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    