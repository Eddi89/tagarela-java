
/* First created by JCasGen Thu Jun 11 12:31:13 CEST 2015 */
package de.tuebingen.sfs.tagarela.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** A token in running text.
 * Updated by JCasGen Tue Jun 16 03:18:14 CEST 2015
 * @generated */
public class Token_Type extends Constituent_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Token_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Token_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Token(addr, Token_Type.this);
  			   Token_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Token(addr, Token_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Token.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("de.tuebingen.sfs.tagarela.types.Token");
 
  /** @generated */
  final Feature casFeat_spelling;
  /** @generated */
  final int     casFeatCode_spelling;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getSpelling(int addr) {
        if (featOkTst && casFeat_spelling == null)
      jcas.throwFeatMissing("spelling", "de.tuebingen.sfs.tagarela.types.Token");
    return ll_cas.ll_getRefValue(addr, casFeatCode_spelling);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setSpelling(int addr, int v) {
        if (featOkTst && casFeat_spelling == null)
      jcas.throwFeatMissing("spelling", "de.tuebingen.sfs.tagarela.types.Token");
    ll_cas.ll_setRefValue(addr, casFeatCode_spelling, v);}
    
  
 
  /** @generated */
  final Feature casFeat_deepForm;
  /** @generated */
  final int     casFeatCode_deepForm;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getDeepForm(int addr) {
        if (featOkTst && casFeat_deepForm == null)
      jcas.throwFeatMissing("deepForm", "de.tuebingen.sfs.tagarela.types.Token");
    return ll_cas.ll_getRefValue(addr, casFeatCode_deepForm);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setDeepForm(int addr, int v) {
        if (featOkTst && casFeat_deepForm == null)
      jcas.throwFeatMissing("deepForm", "de.tuebingen.sfs.tagarela.types.Token");
    ll_cas.ll_setRefValue(addr, casFeatCode_deepForm, v);}
    
   /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @return value at index i in the array 
   */
  public int getDeepForm(int addr, int i) {
        if (featOkTst && casFeat_deepForm == null)
      jcas.throwFeatMissing("deepForm", "de.tuebingen.sfs.tagarela.types.Token");
    if (lowLevelTypeChecks)
      return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_deepForm), i, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_deepForm), i);
  return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_deepForm), i);
  }
   
  /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @param v value to set
   */ 
  public void setDeepForm(int addr, int i, int v) {
        if (featOkTst && casFeat_deepForm == null)
      jcas.throwFeatMissing("deepForm", "de.tuebingen.sfs.tagarela.types.Token");
    if (lowLevelTypeChecks)
      ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_deepForm), i, v, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_deepForm), i);
    ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_deepForm), i, v);
  }
 
 
  /** @generated */
  final Feature casFeat_lexDef;
  /** @generated */
  final int     casFeatCode_lexDef;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getLexDef(int addr) {
        if (featOkTst && casFeat_lexDef == null)
      jcas.throwFeatMissing("lexDef", "de.tuebingen.sfs.tagarela.types.Token");
    return ll_cas.ll_getRefValue(addr, casFeatCode_lexDef);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setLexDef(int addr, int v) {
        if (featOkTst && casFeat_lexDef == null)
      jcas.throwFeatMissing("lexDef", "de.tuebingen.sfs.tagarela.types.Token");
    ll_cas.ll_setRefValue(addr, casFeatCode_lexDef, v);}
    
   /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @return value at index i in the array 
   */
  public int getLexDef(int addr, int i) {
        if (featOkTst && casFeat_lexDef == null)
      jcas.throwFeatMissing("lexDef", "de.tuebingen.sfs.tagarela.types.Token");
    if (lowLevelTypeChecks)
      return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_lexDef), i, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_lexDef), i);
  return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_lexDef), i);
  }
   
  /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @param v value to set
   */ 
  public void setLexDef(int addr, int i, int v) {
        if (featOkTst && casFeat_lexDef == null)
      jcas.throwFeatMissing("lexDef", "de.tuebingen.sfs.tagarela.types.Token");
    if (lowLevelTypeChecks)
      ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_lexDef), i, v, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_lexDef), i);
    ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_lexDef), i, v);
  }
 
 
  /** @generated */
  final Feature casFeat_tokenString;
  /** @generated */
  final int     casFeatCode_tokenString;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getTokenString(int addr) {
        if (featOkTst && casFeat_tokenString == null)
      jcas.throwFeatMissing("tokenString", "de.tuebingen.sfs.tagarela.types.Token");
    return ll_cas.ll_getStringValue(addr, casFeatCode_tokenString);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setTokenString(int addr, String v) {
        if (featOkTst && casFeat_tokenString == null)
      jcas.throwFeatMissing("tokenString", "de.tuebingen.sfs.tagarela.types.Token");
    ll_cas.ll_setStringValue(addr, casFeatCode_tokenString, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public Token_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_spelling = jcas.getRequiredFeatureDE(casType, "spelling", "de.tuebingen.sfs.tagarela.types.Spelling", featOkTst);
    casFeatCode_spelling  = (null == casFeat_spelling) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_spelling).getCode();

 
    casFeat_deepForm = jcas.getRequiredFeatureDE(casType, "deepForm", "uima.cas.FSArray", featOkTst);
    casFeatCode_deepForm  = (null == casFeat_deepForm) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_deepForm).getCode();

 
    casFeat_lexDef = jcas.getRequiredFeatureDE(casType, "lexDef", "uima.cas.FSArray", featOkTst);
    casFeatCode_lexDef  = (null == casFeat_lexDef) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_lexDef).getCode();

 
    casFeat_tokenString = jcas.getRequiredFeatureDE(casType, "tokenString", "uima.cas.String", featOkTst);
    casFeatCode_tokenString  = (null == casFeat_tokenString) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_tokenString).getCode();

  }
}



    