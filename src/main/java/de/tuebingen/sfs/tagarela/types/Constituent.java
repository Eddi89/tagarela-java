

/* First created by JCasGen Thu Jun 11 12:31:13 CEST 2015 */
package de.tuebingen.sfs.tagarela.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** A constituent within a sentence.
 * Updated by JCasGen Tue Jun 16 03:18:14 CEST 2015
 * XML source: /home/eduard/workspace3/tagarela-java/desc/basicNlpTypes.xml
 * @generated */
public class Constituent extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Constituent.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Constituent() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public Constituent(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public Constituent(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public Constituent(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: category

  /** getter for category - gets The constituent's label / the token's POS.
   * @generated
   * @return value of the feature 
   */
  public String getCategory() {
    if (Constituent_Type.featOkTst && ((Constituent_Type)jcasType).casFeat_category == null)
      jcasType.jcas.throwFeatMissing("category", "de.tuebingen.sfs.tagarela.types.Constituent");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Constituent_Type)jcasType).casFeatCode_category);}
    
  /** setter for category - sets The constituent's label / the token's POS. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setCategory(String v) {
    if (Constituent_Type.featOkTst && ((Constituent_Type)jcasType).casFeat_category == null)
      jcasType.jcas.throwFeatMissing("category", "de.tuebingen.sfs.tagarela.types.Constituent");
    jcasType.ll_cas.ll_setStringValue(addr, ((Constituent_Type)jcasType).casFeatCode_category, v);}    
  }

    