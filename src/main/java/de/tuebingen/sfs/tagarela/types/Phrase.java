

/* First created by JCasGen Thu Jun 11 12:31:13 CEST 2015 */
package de.tuebingen.sfs.tagarela.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.FSArray;


/** A phrase of one or more tokens.
 * Updated by JCasGen Tue Jun 16 03:18:14 CEST 2015
 * XML source: /home/eduard/workspace3/tagarela-java/desc/basicNlpTypes.xml
 * @generated */
public class Phrase extends Constituent {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Phrase.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Phrase() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public Phrase(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public Phrase(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public Phrase(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: children

  /** getter for children - gets The list of children in this phrase.
   * @generated
   * @return value of the feature 
   */
  public FSArray getChildren() {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_children == null)
      jcasType.jcas.throwFeatMissing("children", "de.tuebingen.sfs.tagarela.types.Phrase");
    return (FSArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Phrase_Type)jcasType).casFeatCode_children)));}
    
  /** setter for children - sets The list of children in this phrase. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setChildren(FSArray v) {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_children == null)
      jcasType.jcas.throwFeatMissing("children", "de.tuebingen.sfs.tagarela.types.Phrase");
    jcasType.ll_cas.ll_setRefValue(addr, ((Phrase_Type)jcasType).casFeatCode_children, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for children - gets an indexed value - The list of children in this phrase.
   * @generated
   * @param i index in the array to get
   * @return value of the element at index i 
   */
  public Constituent getChildren(int i) {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_children == null)
      jcasType.jcas.throwFeatMissing("children", "de.tuebingen.sfs.tagarela.types.Phrase");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((Phrase_Type)jcasType).casFeatCode_children), i);
    return (Constituent)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((Phrase_Type)jcasType).casFeatCode_children), i)));}

  /** indexed setter for children - sets an indexed value - The list of children in this phrase.
   * @generated
   * @param i index in the array to set
   * @param v value to set into the array 
   */
  public void setChildren(int i, Constituent v) { 
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_children == null)
      jcasType.jcas.throwFeatMissing("children", "de.tuebingen.sfs.tagarela.types.Phrase");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((Phrase_Type)jcasType).casFeatCode_children), i);
    jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((Phrase_Type)jcasType).casFeatCode_children), i, jcasType.ll_cas.ll_getFSRef(v));}
   
    
  //*--------------*
  //* Feature: parent

  /** getter for parent - gets The parent phrase.
   * @generated
   * @return value of the feature 
   */
  public Phrase getParent() {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_parent == null)
      jcasType.jcas.throwFeatMissing("parent", "de.tuebingen.sfs.tagarela.types.Phrase");
    return (Phrase)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Phrase_Type)jcasType).casFeatCode_parent)));}
    
  /** setter for parent - sets The parent phrase. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setParent(Phrase v) {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_parent == null)
      jcasType.jcas.throwFeatMissing("parent", "de.tuebingen.sfs.tagarela.types.Phrase");
    jcasType.ll_cas.ll_setRefValue(addr, ((Phrase_Type)jcasType).casFeatCode_parent, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: headPOS

  /** getter for headPOS - gets The pos of the head word.
   * @generated
   * @return value of the feature 
   */
  public String getHeadPOS() {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_headPOS == null)
      jcasType.jcas.throwFeatMissing("headPOS", "de.tuebingen.sfs.tagarela.types.Phrase");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Phrase_Type)jcasType).casFeatCode_headPOS);}
    
  /** setter for headPOS - sets The pos of the head word. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setHeadPOS(String v) {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_headPOS == null)
      jcasType.jcas.throwFeatMissing("headPOS", "de.tuebingen.sfs.tagarela.types.Phrase");
    jcasType.ll_cas.ll_setStringValue(addr, ((Phrase_Type)jcasType).casFeatCode_headPOS, v);}    
   
    
  //*--------------*
  //* Feature: agreement

  /** getter for agreement - gets information about agreement of daughter nodes
   * @generated
   * @return value of the feature 
   */
  public Agreement getAgreement() {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_agreement == null)
      jcasType.jcas.throwFeatMissing("agreement", "de.tuebingen.sfs.tagarela.types.Phrase");
    return (Agreement)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Phrase_Type)jcasType).casFeatCode_agreement)));}
    
  /** setter for agreement - sets information about agreement of daughter nodes 
   * @generated
   * @param v value to set into the feature 
   */
  public void setAgreement(Agreement v) {
    if (Phrase_Type.featOkTst && ((Phrase_Type)jcasType).casFeat_agreement == null)
      jcasType.jcas.throwFeatMissing("agreement", "de.tuebingen.sfs.tagarela.types.Phrase");
    jcasType.ll_cas.ll_setRefValue(addr, ((Phrase_Type)jcasType).casFeatCode_agreement, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    