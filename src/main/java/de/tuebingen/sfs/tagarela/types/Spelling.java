

/* First created by JCasGen Thu Jun 11 12:31:13 CEST 2015 */
package de.tuebingen.sfs.tagarela.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.TOP;
import org.apache.uima.jcas.cas.StringArray;


/** Spell checker analysis of a given token.
 * Updated by JCasGen Tue Jun 16 03:18:14 CEST 2015
 * XML source: /home/eduard/workspace3/tagarela-java/desc/basicNlpTypes.xml
 * @generated */
public class Spelling extends TOP {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Spelling.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Spelling() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public Spelling(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public Spelling(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: misspelled

  /** getter for misspelled - gets Flag indicating whether a word is misspelled or not.
   * @generated
   * @return value of the feature 
   */
  public boolean getMisspelled() {
    if (Spelling_Type.featOkTst && ((Spelling_Type)jcasType).casFeat_misspelled == null)
      jcasType.jcas.throwFeatMissing("misspelled", "de.tuebingen.sfs.tagarela.types.Spelling");
    return jcasType.ll_cas.ll_getBooleanValue(addr, ((Spelling_Type)jcasType).casFeatCode_misspelled);}
    
  /** setter for misspelled - sets Flag indicating whether a word is misspelled or not. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setMisspelled(boolean v) {
    if (Spelling_Type.featOkTst && ((Spelling_Type)jcasType).casFeat_misspelled == null)
      jcasType.jcas.throwFeatMissing("misspelled", "de.tuebingen.sfs.tagarela.types.Spelling");
    jcasType.ll_cas.ll_setBooleanValue(addr, ((Spelling_Type)jcasType).casFeatCode_misspelled, v);}    
   
    
  //*--------------*
  //* Feature: suggestions

  /** getter for suggestions - gets A list of suggestions from the spell checker.
   * @generated
   * @return value of the feature 
   */
  public StringArray getSuggestions() {
    if (Spelling_Type.featOkTst && ((Spelling_Type)jcasType).casFeat_suggestions == null)
      jcasType.jcas.throwFeatMissing("suggestions", "de.tuebingen.sfs.tagarela.types.Spelling");
    return (StringArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Spelling_Type)jcasType).casFeatCode_suggestions)));}
    
  /** setter for suggestions - sets A list of suggestions from the spell checker. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setSuggestions(StringArray v) {
    if (Spelling_Type.featOkTst && ((Spelling_Type)jcasType).casFeat_suggestions == null)
      jcasType.jcas.throwFeatMissing("suggestions", "de.tuebingen.sfs.tagarela.types.Spelling");
    jcasType.ll_cas.ll_setRefValue(addr, ((Spelling_Type)jcasType).casFeatCode_suggestions, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for suggestions - gets an indexed value - A list of suggestions from the spell checker.
   * @generated
   * @param i index in the array to get
   * @return value of the element at index i 
   */
  public String getSuggestions(int i) {
    if (Spelling_Type.featOkTst && ((Spelling_Type)jcasType).casFeat_suggestions == null)
      jcasType.jcas.throwFeatMissing("suggestions", "de.tuebingen.sfs.tagarela.types.Spelling");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((Spelling_Type)jcasType).casFeatCode_suggestions), i);
    return jcasType.ll_cas.ll_getStringArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((Spelling_Type)jcasType).casFeatCode_suggestions), i);}

  /** indexed setter for suggestions - sets an indexed value - A list of suggestions from the spell checker.
   * @generated
   * @param i index in the array to set
   * @param v value to set into the array 
   */
  public void setSuggestions(int i, String v) { 
    if (Spelling_Type.featOkTst && ((Spelling_Type)jcasType).casFeat_suggestions == null)
      jcasType.jcas.throwFeatMissing("suggestions", "de.tuebingen.sfs.tagarela.types.Spelling");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((Spelling_Type)jcasType).casFeatCode_suggestions), i);
    jcasType.ll_cas.ll_setStringArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((Spelling_Type)jcasType).casFeatCode_suggestions), i, v);}
  }

    