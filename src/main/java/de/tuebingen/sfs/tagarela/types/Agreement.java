

/* First created by JCasGen Thu Jun 11 12:31:13 CEST 2015 */
package de.tuebingen.sfs.tagarela.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.TOP;


/** Contains information about agreement of sub-constituents.
 * Updated by JCasGen Tue Jun 16 03:18:14 CEST 2015
 * XML source: /home/eduard/workspace3/tagarela-java/desc/basicNlpTypes.xml
 * @generated */
public class Agreement extends TOP {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Agreement.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Agreement() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public Agreement(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public Agreement(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: offendingConstituents

  /** getter for offendingConstituents - gets A list of constituents that do not agree.
   * @generated
   * @return value of the feature 
   */
  public FSArray getOffendingConstituents() {
    if (Agreement_Type.featOkTst && ((Agreement_Type)jcasType).casFeat_offendingConstituents == null)
      jcasType.jcas.throwFeatMissing("offendingConstituents", "de.tuebingen.sfs.tagarela.types.Agreement");
    return (FSArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((Agreement_Type)jcasType).casFeatCode_offendingConstituents)));}
    
  /** setter for offendingConstituents - sets A list of constituents that do not agree. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setOffendingConstituents(FSArray v) {
    if (Agreement_Type.featOkTst && ((Agreement_Type)jcasType).casFeat_offendingConstituents == null)
      jcasType.jcas.throwFeatMissing("offendingConstituents", "de.tuebingen.sfs.tagarela.types.Agreement");
    jcasType.ll_cas.ll_setRefValue(addr, ((Agreement_Type)jcasType).casFeatCode_offendingConstituents, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for offendingConstituents - gets an indexed value - A list of constituents that do not agree.
   * @generated
   * @param i index in the array to get
   * @return value of the element at index i 
   */
  public Constituent getOffendingConstituents(int i) {
    if (Agreement_Type.featOkTst && ((Agreement_Type)jcasType).casFeat_offendingConstituents == null)
      jcasType.jcas.throwFeatMissing("offendingConstituents", "de.tuebingen.sfs.tagarela.types.Agreement");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((Agreement_Type)jcasType).casFeatCode_offendingConstituents), i);
    return (Constituent)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((Agreement_Type)jcasType).casFeatCode_offendingConstituents), i)));}

  /** indexed setter for offendingConstituents - sets an indexed value - A list of constituents that do not agree.
   * @generated
   * @param i index in the array to set
   * @param v value to set into the array 
   */
  public void setOffendingConstituents(int i, Constituent v) { 
    if (Agreement_Type.featOkTst && ((Agreement_Type)jcasType).casFeat_offendingConstituents == null)
      jcasType.jcas.throwFeatMissing("offendingConstituents", "de.tuebingen.sfs.tagarela.types.Agreement");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((Agreement_Type)jcasType).casFeatCode_offendingConstituents), i);
    jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((Agreement_Type)jcasType).casFeatCode_offendingConstituents), i, jcasType.ll_cas.ll_getFSRef(v));}
   
    
  //*--------------*
  //* Feature: label

  /** getter for label - gets A name for the agreement, such as 'subject-verb' or 'NP-VP'.
   * @generated
   * @return value of the feature 
   */
  public String getLabel() {
    if (Agreement_Type.featOkTst && ((Agreement_Type)jcasType).casFeat_label == null)
      jcasType.jcas.throwFeatMissing("label", "de.tuebingen.sfs.tagarela.types.Agreement");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Agreement_Type)jcasType).casFeatCode_label);}
    
  /** setter for label - sets A name for the agreement, such as 'subject-verb' or 'NP-VP'. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setLabel(String v) {
    if (Agreement_Type.featOkTst && ((Agreement_Type)jcasType).casFeat_label == null)
      jcasType.jcas.throwFeatMissing("label", "de.tuebingen.sfs.tagarela.types.Agreement");
    jcasType.ll_cas.ll_setStringValue(addr, ((Agreement_Type)jcasType).casFeatCode_label, v);}    
   
    
  //*--------------*
  //* Feature: person

  /** getter for person - gets the person if agreement is correct, or 'error' if not
   * @generated
   * @return value of the feature 
   */
  public String getPerson() {
    if (Agreement_Type.featOkTst && ((Agreement_Type)jcasType).casFeat_person == null)
      jcasType.jcas.throwFeatMissing("person", "de.tuebingen.sfs.tagarela.types.Agreement");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Agreement_Type)jcasType).casFeatCode_person);}
    
  /** setter for person - sets the person if agreement is correct, or 'error' if not 
   * @generated
   * @param v value to set into the feature 
   */
  public void setPerson(String v) {
    if (Agreement_Type.featOkTst && ((Agreement_Type)jcasType).casFeat_person == null)
      jcasType.jcas.throwFeatMissing("person", "de.tuebingen.sfs.tagarela.types.Agreement");
    jcasType.ll_cas.ll_setStringValue(addr, ((Agreement_Type)jcasType).casFeatCode_person, v);}    
   
    
  //*--------------*
  //* Feature: number

  /** getter for number - gets the number if agreement is correct, or 'error' if not
   * @generated
   * @return value of the feature 
   */
  public String getNumber() {
    if (Agreement_Type.featOkTst && ((Agreement_Type)jcasType).casFeat_number == null)
      jcasType.jcas.throwFeatMissing("number", "de.tuebingen.sfs.tagarela.types.Agreement");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Agreement_Type)jcasType).casFeatCode_number);}
    
  /** setter for number - sets the number if agreement is correct, or 'error' if not 
   * @generated
   * @param v value to set into the feature 
   */
  public void setNumber(String v) {
    if (Agreement_Type.featOkTst && ((Agreement_Type)jcasType).casFeat_number == null)
      jcasType.jcas.throwFeatMissing("number", "de.tuebingen.sfs.tagarela.types.Agreement");
    jcasType.ll_cas.ll_setStringValue(addr, ((Agreement_Type)jcasType).casFeatCode_number, v);}    
   
    
  //*--------------*
  //* Feature: gender

  /** getter for gender - gets the gender if agreement is correct, or 'error' if not
   * @generated
   * @return value of the feature 
   */
  public String getGender() {
    if (Agreement_Type.featOkTst && ((Agreement_Type)jcasType).casFeat_gender == null)
      jcasType.jcas.throwFeatMissing("gender", "de.tuebingen.sfs.tagarela.types.Agreement");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Agreement_Type)jcasType).casFeatCode_gender);}
    
  /** setter for gender - sets the gender if agreement is correct, or 'error' if not 
   * @generated
   * @param v value to set into the feature 
   */
  public void setGender(String v) {
    if (Agreement_Type.featOkTst && ((Agreement_Type)jcasType).casFeat_gender == null)
      jcasType.jcas.throwFeatMissing("gender", "de.tuebingen.sfs.tagarela.types.Agreement");
    jcasType.ll_cas.ll_setStringValue(addr, ((Agreement_Type)jcasType).casFeatCode_gender, v);}    
  }

    