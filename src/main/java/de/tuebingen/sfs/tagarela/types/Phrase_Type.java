
/* First created by JCasGen Thu Jun 11 12:31:13 CEST 2015 */
package de.tuebingen.sfs.tagarela.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;

/** A phrase of one or more tokens.
 * Updated by JCasGen Tue Jun 16 03:18:14 CEST 2015
 * @generated */
public class Phrase_Type extends Constituent_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Phrase_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Phrase_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Phrase(addr, Phrase_Type.this);
  			   Phrase_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Phrase(addr, Phrase_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Phrase.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("de.tuebingen.sfs.tagarela.types.Phrase");
 
  /** @generated */
  final Feature casFeat_children;
  /** @generated */
  final int     casFeatCode_children;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getChildren(int addr) {
        if (featOkTst && casFeat_children == null)
      jcas.throwFeatMissing("children", "de.tuebingen.sfs.tagarela.types.Phrase");
    return ll_cas.ll_getRefValue(addr, casFeatCode_children);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setChildren(int addr, int v) {
        if (featOkTst && casFeat_children == null)
      jcas.throwFeatMissing("children", "de.tuebingen.sfs.tagarela.types.Phrase");
    ll_cas.ll_setRefValue(addr, casFeatCode_children, v);}
    
   /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @return value at index i in the array 
   */
  public int getChildren(int addr, int i) {
        if (featOkTst && casFeat_children == null)
      jcas.throwFeatMissing("children", "de.tuebingen.sfs.tagarela.types.Phrase");
    if (lowLevelTypeChecks)
      return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_children), i, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_children), i);
  return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_children), i);
  }
   
  /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @param v value to set
   */ 
  public void setChildren(int addr, int i, int v) {
        if (featOkTst && casFeat_children == null)
      jcas.throwFeatMissing("children", "de.tuebingen.sfs.tagarela.types.Phrase");
    if (lowLevelTypeChecks)
      ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_children), i, v, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_children), i);
    ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_children), i, v);
  }
 
 
  /** @generated */
  final Feature casFeat_parent;
  /** @generated */
  final int     casFeatCode_parent;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getParent(int addr) {
        if (featOkTst && casFeat_parent == null)
      jcas.throwFeatMissing("parent", "de.tuebingen.sfs.tagarela.types.Phrase");
    return ll_cas.ll_getRefValue(addr, casFeatCode_parent);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setParent(int addr, int v) {
        if (featOkTst && casFeat_parent == null)
      jcas.throwFeatMissing("parent", "de.tuebingen.sfs.tagarela.types.Phrase");
    ll_cas.ll_setRefValue(addr, casFeatCode_parent, v);}
    
  
 
  /** @generated */
  final Feature casFeat_headPOS;
  /** @generated */
  final int     casFeatCode_headPOS;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getHeadPOS(int addr) {
        if (featOkTst && casFeat_headPOS == null)
      jcas.throwFeatMissing("headPOS", "de.tuebingen.sfs.tagarela.types.Phrase");
    return ll_cas.ll_getStringValue(addr, casFeatCode_headPOS);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setHeadPOS(int addr, String v) {
        if (featOkTst && casFeat_headPOS == null)
      jcas.throwFeatMissing("headPOS", "de.tuebingen.sfs.tagarela.types.Phrase");
    ll_cas.ll_setStringValue(addr, casFeatCode_headPOS, v);}
    
  
 
  /** @generated */
  final Feature casFeat_agreement;
  /** @generated */
  final int     casFeatCode_agreement;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getAgreement(int addr) {
        if (featOkTst && casFeat_agreement == null)
      jcas.throwFeatMissing("agreement", "de.tuebingen.sfs.tagarela.types.Phrase");
    return ll_cas.ll_getRefValue(addr, casFeatCode_agreement);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setAgreement(int addr, int v) {
        if (featOkTst && casFeat_agreement == null)
      jcas.throwFeatMissing("agreement", "de.tuebingen.sfs.tagarela.types.Phrase");
    ll_cas.ll_setRefValue(addr, casFeatCode_agreement, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public Phrase_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_children = jcas.getRequiredFeatureDE(casType, "children", "uima.cas.FSArray", featOkTst);
    casFeatCode_children  = (null == casFeat_children) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_children).getCode();

 
    casFeat_parent = jcas.getRequiredFeatureDE(casType, "parent", "de.tuebingen.sfs.tagarela.types.Phrase", featOkTst);
    casFeatCode_parent  = (null == casFeat_parent) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_parent).getCode();

 
    casFeat_headPOS = jcas.getRequiredFeatureDE(casType, "headPOS", "uima.cas.String", featOkTst);
    casFeatCode_headPOS  = (null == casFeat_headPOS) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_headPOS).getCode();

 
    casFeat_agreement = jcas.getRequiredFeatureDE(casType, "agreement", "de.tuebingen.sfs.tagarela.types.Agreement", featOkTst);
    casFeatCode_agreement  = (null == casFeat_agreement) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_agreement).getCode();

  }
}



    