
/* First created by JCasGen Thu Jun 11 12:31:13 CEST 2015 */
package de.tuebingen.sfs.tagarela.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.cas.TOP_Type;

/** holds results of token matching
 * Updated by JCasGen Tue Jun 16 03:18:14 CEST 2015
 * @generated */
public class MatchResult_Type extends TOP_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (MatchResult_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = MatchResult_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new MatchResult(addr, MatchResult_Type.this);
  			   MatchResult_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new MatchResult(addr, MatchResult_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = MatchResult.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("de.tuebingen.sfs.tagarela.types.MatchResult");
 
  /** @generated */
  final Feature casFeat_matchedTokens;
  /** @generated */
  final int     casFeatCode_matchedTokens;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getMatchedTokens(int addr) {
        if (featOkTst && casFeat_matchedTokens == null)
      jcas.throwFeatMissing("matchedTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    return ll_cas.ll_getRefValue(addr, casFeatCode_matchedTokens);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setMatchedTokens(int addr, int v) {
        if (featOkTst && casFeat_matchedTokens == null)
      jcas.throwFeatMissing("matchedTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    ll_cas.ll_setRefValue(addr, casFeatCode_matchedTokens, v);}
    
   /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @return value at index i in the array 
   */
  public int getMatchedTokens(int addr, int i) {
        if (featOkTst && casFeat_matchedTokens == null)
      jcas.throwFeatMissing("matchedTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    if (lowLevelTypeChecks)
      return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_matchedTokens), i, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_matchedTokens), i);
  return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_matchedTokens), i);
  }
   
  /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @param v value to set
   */ 
  public void setMatchedTokens(int addr, int i, int v) {
        if (featOkTst && casFeat_matchedTokens == null)
      jcas.throwFeatMissing("matchedTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    if (lowLevelTypeChecks)
      ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_matchedTokens), i, v, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_matchedTokens), i);
    ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_matchedTokens), i, v);
  }
 
 
  /** @generated */
  final Feature casFeat_unmatchedStudentTokens;
  /** @generated */
  final int     casFeatCode_unmatchedStudentTokens;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getUnmatchedStudentTokens(int addr) {
        if (featOkTst && casFeat_unmatchedStudentTokens == null)
      jcas.throwFeatMissing("unmatchedStudentTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    return ll_cas.ll_getRefValue(addr, casFeatCode_unmatchedStudentTokens);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setUnmatchedStudentTokens(int addr, int v) {
        if (featOkTst && casFeat_unmatchedStudentTokens == null)
      jcas.throwFeatMissing("unmatchedStudentTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    ll_cas.ll_setRefValue(addr, casFeatCode_unmatchedStudentTokens, v);}
    
   /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @return value at index i in the array 
   */
  public int getUnmatchedStudentTokens(int addr, int i) {
        if (featOkTst && casFeat_unmatchedStudentTokens == null)
      jcas.throwFeatMissing("unmatchedStudentTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    if (lowLevelTypeChecks)
      return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_unmatchedStudentTokens), i, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_unmatchedStudentTokens), i);
  return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_unmatchedStudentTokens), i);
  }
   
  /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @param v value to set
   */ 
  public void setUnmatchedStudentTokens(int addr, int i, int v) {
        if (featOkTst && casFeat_unmatchedStudentTokens == null)
      jcas.throwFeatMissing("unmatchedStudentTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    if (lowLevelTypeChecks)
      ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_unmatchedStudentTokens), i, v, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_unmatchedStudentTokens), i);
    ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_unmatchedStudentTokens), i, v);
  }
 
 
  /** @generated */
  final Feature casFeat_unmatchedAnswerTokens;
  /** @generated */
  final int     casFeatCode_unmatchedAnswerTokens;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getUnmatchedAnswerTokens(int addr) {
        if (featOkTst && casFeat_unmatchedAnswerTokens == null)
      jcas.throwFeatMissing("unmatchedAnswerTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    return ll_cas.ll_getRefValue(addr, casFeatCode_unmatchedAnswerTokens);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setUnmatchedAnswerTokens(int addr, int v) {
        if (featOkTst && casFeat_unmatchedAnswerTokens == null)
      jcas.throwFeatMissing("unmatchedAnswerTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    ll_cas.ll_setRefValue(addr, casFeatCode_unmatchedAnswerTokens, v);}
    
   /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @return value at index i in the array 
   */
  public int getUnmatchedAnswerTokens(int addr, int i) {
        if (featOkTst && casFeat_unmatchedAnswerTokens == null)
      jcas.throwFeatMissing("unmatchedAnswerTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    if (lowLevelTypeChecks)
      return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_unmatchedAnswerTokens), i, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_unmatchedAnswerTokens), i);
  return ll_cas.ll_getRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_unmatchedAnswerTokens), i);
  }
   
  /** @generated
   * @param addr low level Feature Structure reference
   * @param i index of item in the array
   * @param v value to set
   */ 
  public void setUnmatchedAnswerTokens(int addr, int i, int v) {
        if (featOkTst && casFeat_unmatchedAnswerTokens == null)
      jcas.throwFeatMissing("unmatchedAnswerTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    if (lowLevelTypeChecks)
      ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_unmatchedAnswerTokens), i, v, true);
    jcas.checkArrayBounds(ll_cas.ll_getRefValue(addr, casFeatCode_unmatchedAnswerTokens), i);
    ll_cas.ll_setRefArrayValue(ll_cas.ll_getRefValue(addr, casFeatCode_unmatchedAnswerTokens), i, v);
  }
 



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public MatchResult_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_matchedTokens = jcas.getRequiredFeatureDE(casType, "matchedTokens", "uima.cas.FSArray", featOkTst);
    casFeatCode_matchedTokens  = (null == casFeat_matchedTokens) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_matchedTokens).getCode();

 
    casFeat_unmatchedStudentTokens = jcas.getRequiredFeatureDE(casType, "unmatchedStudentTokens", "uima.cas.FSArray", featOkTst);
    casFeatCode_unmatchedStudentTokens  = (null == casFeat_unmatchedStudentTokens) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_unmatchedStudentTokens).getCode();

 
    casFeat_unmatchedAnswerTokens = jcas.getRequiredFeatureDE(casType, "unmatchedAnswerTokens", "uima.cas.FSArray", featOkTst);
    casFeatCode_unmatchedAnswerTokens  = (null == casFeat_unmatchedAnswerTokens) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_unmatchedAnswerTokens).getCode();

  }
}



    