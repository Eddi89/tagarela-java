

/* First created by JCasGen Mon Jun 15 23:23:22 CEST 2015 */
package de.tuebingen.sfs.tagarela.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.NonEmptyStringList;


/** A reading in a constraint grammar cohort.
 * Updated by JCasGen Tue Jun 16 03:18:14 CEST 2015
 * XML source: /home/eduard/workspace3/tagarela-java/desc/basicNlpTypes.xml
 * @generated */
public class CGReading extends NonEmptyStringList {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(CGReading.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected CGReading() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public CGReading(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public CGReading(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
  //*--------------*
  //* Feature: tag

  /** getter for tag - gets The tag-sequence of this reading.
   * @generated
   * @return value of the feature 
   */
  public String getTag() {
    if (CGReading_Type.featOkTst && ((CGReading_Type)jcasType).casFeat_tag == null)
      jcasType.jcas.throwFeatMissing("tag", "de.tuebingen.sfs.tagarela.types.CGReading");
    return jcasType.ll_cas.ll_getStringValue(addr, ((CGReading_Type)jcasType).casFeatCode_tag);}
    
  /** setter for tag - sets The tag-sequence of this reading. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setTag(String v) {
    if (CGReading_Type.featOkTst && ((CGReading_Type)jcasType).casFeat_tag == null)
      jcasType.jcas.throwFeatMissing("tag", "de.tuebingen.sfs.tagarela.types.CGReading");
    jcasType.ll_cas.ll_setStringValue(addr, ((CGReading_Type)jcasType).casFeatCode_tag, v);}    
   
    
  //*--------------*
  //* Feature: lemma

  /** getter for lemma - gets The lemma of the word in this reading. May remain empty if there is no lemmatizer around.
   * @generated
   * @return value of the feature 
   */
  public String getLemma() {
    if (CGReading_Type.featOkTst && ((CGReading_Type)jcasType).casFeat_lemma == null)
      jcasType.jcas.throwFeatMissing("lemma", "de.tuebingen.sfs.tagarela.types.CGReading");
    return jcasType.ll_cas.ll_getStringValue(addr, ((CGReading_Type)jcasType).casFeatCode_lemma);}
    
  /** setter for lemma - sets The lemma of the word in this reading. May remain empty if there is no lemmatizer around. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setLemma(String v) {
    if (CGReading_Type.featOkTst && ((CGReading_Type)jcasType).casFeat_lemma == null)
      jcasType.jcas.throwFeatMissing("lemma", "de.tuebingen.sfs.tagarela.types.CGReading");
    jcasType.ll_cas.ll_setStringValue(addr, ((CGReading_Type)jcasType).casFeatCode_lemma, v);}    
  }

    