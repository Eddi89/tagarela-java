

/* First created by JCasGen Thu Jun 11 12:31:13 CEST 2015 */
package de.tuebingen.sfs.tagarela.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.cas.TOP;


/** holds results of token matching
 * Updated by JCasGen Tue Jun 16 03:18:14 CEST 2015
 * XML source: /home/eduard/workspace3/tagarela-java/desc/basicNlpTypes.xml
 * @generated */
public class MatchResult extends TOP {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(MatchResult.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected MatchResult() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public MatchResult(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public MatchResult(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: matchedTokens

  /** getter for matchedTokens - gets a list of matched elements
   * @generated
   * @return value of the feature 
   */
  public FSArray getMatchedTokens() {
    if (MatchResult_Type.featOkTst && ((MatchResult_Type)jcasType).casFeat_matchedTokens == null)
      jcasType.jcas.throwFeatMissing("matchedTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    return (FSArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_matchedTokens)));}
    
  /** setter for matchedTokens - sets a list of matched elements 
   * @generated
   * @param v value to set into the feature 
   */
  public void setMatchedTokens(FSArray v) {
    if (MatchResult_Type.featOkTst && ((MatchResult_Type)jcasType).casFeat_matchedTokens == null)
      jcasType.jcas.throwFeatMissing("matchedTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    jcasType.ll_cas.ll_setRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_matchedTokens, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for matchedTokens - gets an indexed value - a list of matched elements
   * @generated
   * @param i index in the array to get
   * @return value of the element at index i 
   */
  public TokenMapping getMatchedTokens(int i) {
    if (MatchResult_Type.featOkTst && ((MatchResult_Type)jcasType).casFeat_matchedTokens == null)
      jcasType.jcas.throwFeatMissing("matchedTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_matchedTokens), i);
    return (TokenMapping)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_matchedTokens), i)));}

  /** indexed setter for matchedTokens - sets an indexed value - a list of matched elements
   * @generated
   * @param i index in the array to set
   * @param v value to set into the array 
   */
  public void setMatchedTokens(int i, TokenMapping v) { 
    if (MatchResult_Type.featOkTst && ((MatchResult_Type)jcasType).casFeat_matchedTokens == null)
      jcasType.jcas.throwFeatMissing("matchedTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_matchedTokens), i);
    jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_matchedTokens), i, jcasType.ll_cas.ll_getFSRef(v));}
   
    
  //*--------------*
  //* Feature: unmatchedStudentTokens

  /** getter for unmatchedStudentTokens - gets a list of unmatched student input elements
   * @generated
   * @return value of the feature 
   */
  public FSArray getUnmatchedStudentTokens() {
    if (MatchResult_Type.featOkTst && ((MatchResult_Type)jcasType).casFeat_unmatchedStudentTokens == null)
      jcasType.jcas.throwFeatMissing("unmatchedStudentTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    return (FSArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_unmatchedStudentTokens)));}
    
  /** setter for unmatchedStudentTokens - sets a list of unmatched student input elements 
   * @generated
   * @param v value to set into the feature 
   */
  public void setUnmatchedStudentTokens(FSArray v) {
    if (MatchResult_Type.featOkTst && ((MatchResult_Type)jcasType).casFeat_unmatchedStudentTokens == null)
      jcasType.jcas.throwFeatMissing("unmatchedStudentTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    jcasType.ll_cas.ll_setRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_unmatchedStudentTokens, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for unmatchedStudentTokens - gets an indexed value - a list of unmatched student input elements
   * @generated
   * @param i index in the array to get
   * @return value of the element at index i 
   */
  public Token getUnmatchedStudentTokens(int i) {
    if (MatchResult_Type.featOkTst && ((MatchResult_Type)jcasType).casFeat_unmatchedStudentTokens == null)
      jcasType.jcas.throwFeatMissing("unmatchedStudentTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_unmatchedStudentTokens), i);
    return (Token)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_unmatchedStudentTokens), i)));}

  /** indexed setter for unmatchedStudentTokens - sets an indexed value - a list of unmatched student input elements
   * @generated
   * @param i index in the array to set
   * @param v value to set into the array 
   */
  public void setUnmatchedStudentTokens(int i, Token v) { 
    if (MatchResult_Type.featOkTst && ((MatchResult_Type)jcasType).casFeat_unmatchedStudentTokens == null)
      jcasType.jcas.throwFeatMissing("unmatchedStudentTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_unmatchedStudentTokens), i);
    jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_unmatchedStudentTokens), i, jcasType.ll_cas.ll_getFSRef(v));}
   
    
  //*--------------*
  //* Feature: unmatchedAnswerTokens

  /** getter for unmatchedAnswerTokens - gets a list of unmatched target answer elements
   * @generated
   * @return value of the feature 
   */
  public FSArray getUnmatchedAnswerTokens() {
    if (MatchResult_Type.featOkTst && ((MatchResult_Type)jcasType).casFeat_unmatchedAnswerTokens == null)
      jcasType.jcas.throwFeatMissing("unmatchedAnswerTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    return (FSArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_unmatchedAnswerTokens)));}
    
  /** setter for unmatchedAnswerTokens - sets a list of unmatched target answer elements 
   * @generated
   * @param v value to set into the feature 
   */
  public void setUnmatchedAnswerTokens(FSArray v) {
    if (MatchResult_Type.featOkTst && ((MatchResult_Type)jcasType).casFeat_unmatchedAnswerTokens == null)
      jcasType.jcas.throwFeatMissing("unmatchedAnswerTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    jcasType.ll_cas.ll_setRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_unmatchedAnswerTokens, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for unmatchedAnswerTokens - gets an indexed value - a list of unmatched target answer elements
   * @generated
   * @param i index in the array to get
   * @return value of the element at index i 
   */
  public Token getUnmatchedAnswerTokens(int i) {
    if (MatchResult_Type.featOkTst && ((MatchResult_Type)jcasType).casFeat_unmatchedAnswerTokens == null)
      jcasType.jcas.throwFeatMissing("unmatchedAnswerTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_unmatchedAnswerTokens), i);
    return (Token)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_unmatchedAnswerTokens), i)));}

  /** indexed setter for unmatchedAnswerTokens - sets an indexed value - a list of unmatched target answer elements
   * @generated
   * @param i index in the array to set
   * @param v value to set into the array 
   */
  public void setUnmatchedAnswerTokens(int i, Token v) { 
    if (MatchResult_Type.featOkTst && ((MatchResult_Type)jcasType).casFeat_unmatchedAnswerTokens == null)
      jcasType.jcas.throwFeatMissing("unmatchedAnswerTokens", "de.tuebingen.sfs.tagarela.types.MatchResult");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_unmatchedAnswerTokens), i);
    jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((MatchResult_Type)jcasType).casFeatCode_unmatchedAnswerTokens), i, jcasType.ll_cas.ll_getFSRef(v));}
  }

    