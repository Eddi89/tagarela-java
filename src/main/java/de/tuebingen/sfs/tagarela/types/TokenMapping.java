

/* First created by JCasGen Thu Jun 11 12:31:13 CEST 2015 */
package de.tuebingen.sfs.tagarela.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.TOP;


/** A mapping of two tokens.
 * Updated by JCasGen Tue Jun 16 03:18:14 CEST 2015
 * XML source: /home/eduard/workspace3/tagarela-java/desc/basicNlpTypes.xml
 * @generated */
public class TokenMapping extends TOP {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(TokenMapping.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected TokenMapping() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public TokenMapping(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public TokenMapping(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: first

  /** getter for first - gets The first token
   * @generated
   * @return value of the feature 
   */
  public Token getFirst() {
    if (TokenMapping_Type.featOkTst && ((TokenMapping_Type)jcasType).casFeat_first == null)
      jcasType.jcas.throwFeatMissing("first", "de.tuebingen.sfs.tagarela.types.TokenMapping");
    return (Token)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((TokenMapping_Type)jcasType).casFeatCode_first)));}
    
  /** setter for first - sets The first token 
   * @generated
   * @param v value to set into the feature 
   */
  public void setFirst(Token v) {
    if (TokenMapping_Type.featOkTst && ((TokenMapping_Type)jcasType).casFeat_first == null)
      jcasType.jcas.throwFeatMissing("first", "de.tuebingen.sfs.tagarela.types.TokenMapping");
    jcasType.ll_cas.ll_setRefValue(addr, ((TokenMapping_Type)jcasType).casFeatCode_first, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: second

  /** getter for second - gets The second Token
   * @generated
   * @return value of the feature 
   */
  public Token getSecond() {
    if (TokenMapping_Type.featOkTst && ((TokenMapping_Type)jcasType).casFeat_second == null)
      jcasType.jcas.throwFeatMissing("second", "de.tuebingen.sfs.tagarela.types.TokenMapping");
    return (Token)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((TokenMapping_Type)jcasType).casFeatCode_second)));}
    
  /** setter for second - sets The second Token 
   * @generated
   * @param v value to set into the feature 
   */
  public void setSecond(Token v) {
    if (TokenMapping_Type.featOkTst && ((TokenMapping_Type)jcasType).casFeat_second == null)
      jcasType.jcas.throwFeatMissing("second", "de.tuebingen.sfs.tagarela.types.TokenMapping");
    jcasType.ll_cas.ll_setRefValue(addr, ((TokenMapping_Type)jcasType).casFeatCode_second, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    