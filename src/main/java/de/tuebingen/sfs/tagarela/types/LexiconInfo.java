

/* First created by JCasGen Thu Jun 11 12:31:13 CEST 2015 */
package de.tuebingen.sfs.tagarela.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.cas.TOP;


/** A lexicon definition for a given token.
 * Updated by JCasGen Tue Jun 16 03:18:14 CEST 2015
 * XML source: /home/eduard/workspace3/tagarela-java/desc/basicNlpTypes.xml
 * @generated */
public class LexiconInfo extends TOP {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(LexiconInfo.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected LexiconInfo() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public LexiconInfo(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public LexiconInfo(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: person

  /** getter for person - gets 
   * @generated
   * @return value of the feature 
   */
  public String getPerson() {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_person == null)
      jcasType.jcas.throwFeatMissing("person", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    return jcasType.ll_cas.ll_getStringValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_person);}
    
  /** setter for person - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setPerson(String v) {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_person == null)
      jcasType.jcas.throwFeatMissing("person", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    jcasType.ll_cas.ll_setStringValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_person, v);}    
   
    
  //*--------------*
  //* Feature: number

  /** getter for number - gets 
   * @generated
   * @return value of the feature 
   */
  public String getNumber() {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_number == null)
      jcasType.jcas.throwFeatMissing("number", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    return jcasType.ll_cas.ll_getStringValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_number);}
    
  /** setter for number - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setNumber(String v) {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_number == null)
      jcasType.jcas.throwFeatMissing("number", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    jcasType.ll_cas.ll_setStringValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_number, v);}    
   
    
  //*--------------*
  //* Feature: gender

  /** getter for gender - gets 
   * @generated
   * @return value of the feature 
   */
  public String getGender() {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_gender == null)
      jcasType.jcas.throwFeatMissing("gender", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    return jcasType.ll_cas.ll_getStringValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_gender);}
    
  /** setter for gender - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setGender(String v) {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_gender == null)
      jcasType.jcas.throwFeatMissing("gender", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    jcasType.ll_cas.ll_setStringValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_gender, v);}    
   
    
  //*--------------*
  //* Feature: pos

  /** getter for pos - gets 
   * @generated
   * @return value of the feature 
   */
  public String getPos() {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_pos == null)
      jcasType.jcas.throwFeatMissing("pos", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    return jcasType.ll_cas.ll_getStringValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_pos);}
    
  /** setter for pos - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setPos(String v) {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_pos == null)
      jcasType.jcas.throwFeatMissing("pos", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    jcasType.ll_cas.ll_setStringValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_pos, v);}    
   
    
  //*--------------*
  //* Feature: canonic

  /** getter for canonic - gets The canonic form of the token (lemma).
   * @generated
   * @return value of the feature 
   */
  public String getCanonic() {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_canonic == null)
      jcasType.jcas.throwFeatMissing("canonic", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    return jcasType.ll_cas.ll_getStringValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_canonic);}
    
  /** setter for canonic - sets The canonic form of the token (lemma). 
   * @generated
   * @param v value to set into the feature 
   */
  public void setCanonic(String v) {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_canonic == null)
      jcasType.jcas.throwFeatMissing("canonic", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    jcasType.ll_cas.ll_setStringValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_canonic, v);}    
   
    
  //*--------------*
  //* Feature: frequency

  /** getter for frequency - gets The relative frequency qof the token with regard to a reference corpus.
   * @generated
   * @return value of the feature 
   */
  public int getFrequency() {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_frequency == null)
      jcasType.jcas.throwFeatMissing("frequency", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    return jcasType.ll_cas.ll_getIntValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_frequency);}
    
  /** setter for frequency - sets The relative frequency qof the token with regard to a reference corpus. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setFrequency(int v) {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_frequency == null)
      jcasType.jcas.throwFeatMissing("frequency", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    jcasType.ll_cas.ll_setIntValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_frequency, v);}    
   
    
  //*--------------*
  //* Feature: source

  /** getter for source - gets Indicates where the information in this feature bundle came from.
   * @generated
   * @return value of the feature 
   */
  public String getSource() {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_source == null)
      jcasType.jcas.throwFeatMissing("source", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    return jcasType.ll_cas.ll_getStringValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_source);}
    
  /** setter for source - sets Indicates where the information in this feature bundle came from. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setSource(String v) {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_source == null)
      jcasType.jcas.throwFeatMissing("source", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    jcasType.ll_cas.ll_setStringValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_source, v);}    
   
    
  //*--------------*
  //* Feature: case

  /** getter for case - gets 
   * @generated
   * @return value of the feature 
   */
  public String getCase() {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_case == null)
      jcasType.jcas.throwFeatMissing("case", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    return jcasType.ll_cas.ll_getStringValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_case);}
    
  /** setter for case - sets  
   * @generated
   * @param v value to set into the feature 
   */
  public void setCase(String v) {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_case == null)
      jcasType.jcas.throwFeatMissing("case", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    jcasType.ll_cas.ll_setStringValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_case, v);}    
   
    
  //*--------------*
  //* Feature: form

  /** getter for form - gets The form string, an enumeration of possible configurations for this token.
   * @generated
   * @return value of the feature 
   */
  public String getForm() {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_form == null)
      jcasType.jcas.throwFeatMissing("form", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    return jcasType.ll_cas.ll_getStringValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_form);}
    
  /** setter for form - sets The form string, an enumeration of possible configurations for this token. 
   * @generated
   * @param v value to set into the feature 
   */
  public void setForm(String v) {
    if (LexiconInfo_Type.featOkTst && ((LexiconInfo_Type)jcasType).casFeat_form == null)
      jcasType.jcas.throwFeatMissing("form", "de.tuebingen.sfs.tagarela.types.LexiconInfo");
    jcasType.ll_cas.ll_setStringValue(addr, ((LexiconInfo_Type)jcasType).casFeatCode_form, v);}    
  }

    