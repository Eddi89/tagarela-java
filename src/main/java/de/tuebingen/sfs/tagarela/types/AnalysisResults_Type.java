
/* First created by JCasGen Thu Jun 11 12:31:13 CEST 2015 */
package de.tuebingen.sfs.tagarela.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** Analysis results that hold for the whole string in question.
 * Updated by JCasGen Tue Jun 16 03:18:14 CEST 2015
 * @generated */
public class AnalysisResults_Type extends Annotation_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (AnalysisResults_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = AnalysisResults_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new AnalysisResults(addr, AnalysisResults_Type.this);
  			   AnalysisResults_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new AnalysisResults(addr, AnalysisResults_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = AnalysisResults.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("de.tuebingen.sfs.tagarela.types.AnalysisResults");
 
  /** @generated */
  final Feature casFeat_stringMatch;
  /** @generated */
  final int     casFeatCode_stringMatch;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getStringMatch(int addr) {
        if (featOkTst && casFeat_stringMatch == null)
      jcas.throwFeatMissing("stringMatch", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    return ll_cas.ll_getStringValue(addr, casFeatCode_stringMatch);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setStringMatch(int addr, String v) {
        if (featOkTst && casFeat_stringMatch == null)
      jcas.throwFeatMissing("stringMatch", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    ll_cas.ll_setStringValue(addr, casFeatCode_stringMatch, v);}
    
  
 
  /** @generated */
  final Feature casFeat_agreementResults;
  /** @generated */
  final int     casFeatCode_agreementResults;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getAgreementResults(int addr) {
        if (featOkTst && casFeat_agreementResults == null)
      jcas.throwFeatMissing("agreementResults", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    return ll_cas.ll_getRefValue(addr, casFeatCode_agreementResults);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setAgreementResults(int addr, int v) {
        if (featOkTst && casFeat_agreementResults == null)
      jcas.throwFeatMissing("agreementResults", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    ll_cas.ll_setRefValue(addr, casFeatCode_agreementResults, v);}
    
  
 
  /** @generated */
  final Feature casFeat_tokenMatch;
  /** @generated */
  final int     casFeatCode_tokenMatch;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getTokenMatch(int addr) {
        if (featOkTst && casFeat_tokenMatch == null)
      jcas.throwFeatMissing("tokenMatch", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    return ll_cas.ll_getRefValue(addr, casFeatCode_tokenMatch);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setTokenMatch(int addr, int v) {
        if (featOkTst && casFeat_tokenMatch == null)
      jcas.throwFeatMissing("tokenMatch", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    ll_cas.ll_setRefValue(addr, casFeatCode_tokenMatch, v);}
    
  
 
  /** @generated */
  final Feature casFeat_canonicMatch;
  /** @generated */
  final int     casFeatCode_canonicMatch;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getCanonicMatch(int addr) {
        if (featOkTst && casFeat_canonicMatch == null)
      jcas.throwFeatMissing("canonicMatch", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    return ll_cas.ll_getRefValue(addr, casFeatCode_canonicMatch);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setCanonicMatch(int addr, int v) {
        if (featOkTst && casFeat_canonicMatch == null)
      jcas.throwFeatMissing("canonicMatch", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    ll_cas.ll_setRefValue(addr, casFeatCode_canonicMatch, v);}
    
  
 
  /** @generated */
  final Feature casFeat_posMatch;
  /** @generated */
  final int     casFeatCode_posMatch;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getPosMatch(int addr) {
        if (featOkTst && casFeat_posMatch == null)
      jcas.throwFeatMissing("posMatch", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    return ll_cas.ll_getRefValue(addr, casFeatCode_posMatch);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setPosMatch(int addr, int v) {
        if (featOkTst && casFeat_posMatch == null)
      jcas.throwFeatMissing("posMatch", "de.tuebingen.sfs.tagarela.types.AnalysisResults");
    ll_cas.ll_setRefValue(addr, casFeatCode_posMatch, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public AnalysisResults_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_stringMatch = jcas.getRequiredFeatureDE(casType, "stringMatch", "uima.cas.String", featOkTst);
    casFeatCode_stringMatch  = (null == casFeat_stringMatch) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_stringMatch).getCode();

 
    casFeat_agreementResults = jcas.getRequiredFeatureDE(casType, "agreementResults", "uima.cas.NonEmptyFSList", featOkTst);
    casFeatCode_agreementResults  = (null == casFeat_agreementResults) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_agreementResults).getCode();

 
    casFeat_tokenMatch = jcas.getRequiredFeatureDE(casType, "tokenMatch", "de.tuebingen.sfs.tagarela.types.TokenMatchResult", featOkTst);
    casFeatCode_tokenMatch  = (null == casFeat_tokenMatch) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_tokenMatch).getCode();

 
    casFeat_canonicMatch = jcas.getRequiredFeatureDE(casType, "canonicMatch", "de.tuebingen.sfs.tagarela.types.CanonicMatchResult", featOkTst);
    casFeatCode_canonicMatch  = (null == casFeat_canonicMatch) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_canonicMatch).getCode();

 
    casFeat_posMatch = jcas.getRequiredFeatureDE(casType, "posMatch", "de.tuebingen.sfs.tagarela.types.POSMatchResult", featOkTst);
    casFeatCode_posMatch  = (null == casFeat_posMatch) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_posMatch).getCode();

  }
}



    