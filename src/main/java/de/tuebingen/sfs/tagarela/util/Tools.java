package de.tuebingen.sfs.tagarela.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.uima.cas.CASException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.tuebingen.sfs.tagarela.annotators.cam.CAMMatcher.Mapping;
import de.tuebingen.sfs.tagarela.types.Agreement;
import de.tuebingen.sfs.tagarela.types.AnalysisResults;
import de.tuebingen.sfs.tagarela.types.Constituent;
import de.tuebingen.sfs.tagarela.types.LexiconInfo;
import de.tuebingen.sfs.tagarela.types.Phrase;
import de.tuebingen.sfs.tagarela.types.Token;
import de.tuebingen.sfs.tagarela.types.TokenMapping;

/**
 * Various utility methods for different analysis and diagnosis modules.
 * @author rziai
 *
 */
public class Tools {

	@SuppressWarnings("unchecked")
	public static Map<String, Object> toMap(JSONObject jo) throws JSONException {
		HashMap<String, Object> result = new HashMap<String, Object>();
		Iterator<String> keyIter = jo.keys();
		
		while (keyIter.hasNext()) {
			String key = keyIter.next();
			result.put(key, jo.get(key));
		}
		return result;
	}
	
	public static JSONObject toJSON(Map<String, Object> map) {
		return new JSONObject(map);
	}
	
	public static List<Object> toList(JSONArray ja) throws JSONException {
		ArrayList<Object> result = new ArrayList<Object>();
		
		for (int i = 0; i < ja.length(); i++) {
			result.add(ja.get(i));
		}
		return result;
	}
	
	public static JSONArray toJSON(List<Object> list) {
		return new JSONArray(list);
	}
	
	public static LexiconInfo toLexInfo(JSONObject o, JCas jcas, String source) throws JSONException {
		LexiconInfo lexInfo = new LexiconInfo(jcas);
		
		String form = o.has("form") ? o.getString("form") : null;
		String formPers = null, formNum = null;
		if (form != null) {
			lexInfo.setForm(form);
			Matcher m = Pattern.compile("[^;]+;([1-3][sp]);").matcher(form);
			if (m.matches()) {
				formPers = m.group(1);
				formNum = makeNum(formPers);
			}
		}
		
		if (o.has("canonic")) lexInfo.setCanonic(o.getString("canonic"));
		if (o.has("frequency")) lexInfo.setFrequency(o.getInt("frequency"));
		if (o.has("pos")) lexInfo.setPos(o.getString("pos"));
		// person sometimes has 'n' as value which means null... argh
		if (o.has("person") && !o.getString("person").equalsIgnoreCase("n")) {
			lexInfo.setPerson(o.getString("person").toLowerCase());
		} else if (formPers != null) {
			lexInfo.setPerson(formPers);
		}
		if (o.has("number")) {
			lexInfo.setNumber(o.getString("number").toLowerCase());
		} else if (lexInfo.getPerson() != null) {
			lexInfo.setNumber(makeNum(lexInfo.getPerson()));
		} else if (formNum != null) {
			lexInfo.setNumber(formNum);
		}
		
		if (o.has("gender")) lexInfo.setGender(o.getString("gender").toLowerCase());
		if (o.has("spos")) lexInfo.setCase(o.getString("spos").toLowerCase());
		
		lexInfo.setSource(source);
		return lexInfo;
	}
	
	private static String makeNum(String pers) {
		switch (pers.charAt(1)) {
		case 's':
			return "si";
		case 'p':
			return "pl";
		default:
			return null;
		}
	}
	
	/*
	 * removes all analyses from a Token that do not correspond to the given one
	 */
	public static void eliminateOtherAnalyses(Token t, String selectedPos, JCas jcas) {
		FSArray unambiguous = new FSArray(jcas, 1);
		LexiconInfo target = null;
		for (int i = 0; i < t.getLexDef().size(); i++) {
			String lexpos = t.getLexDef(i).getPos();
			if (lexpos != null && lexpos.equalsIgnoreCase(selectedPos)) {
				target = t.getLexDef(i);
				break;
			}
		}
		if (target != null) {
			unambiguous.set(0, target);
			t.setLexDef(unambiguous);
			t.setCategory(selectedPos);
		}
	}
	
	/*
	 * standard breadth-first search algorithm for finding a sub-constituent
	 */
	public static Constituent findConstituent(String symbol, Phrase start) {
		Queue<Constituent> q = new LinkedList<Constituent>();
		q.add(start);
		
		while (!q.isEmpty()) {
			Constituent c = q.remove();
			if (c.getCategory() != null && c.getCategory().equals(symbol)) {
				return c;
			}
			if (c instanceof Phrase) {
				Phrase p = (Phrase) c;
				for (int i = 0; i < p.getChildren().size(); i++) {
					q.add(p.getChildren(i));
				}
			}
		}
		return null;
	}
	
	public static AnalysisResults getAnalysisResults(JCas jcas) throws CASException {
		JCas learnerView = jcas.getView("_InitialView");
		FSIterator iter = learnerView.getAnnotationIndex(AnalysisResults.type).iterator();
		if (iter.hasNext()) {
			return (AnalysisResults) iter.next();
		} else {
			return null;
		}
	}
	
	public static FSArray toArray(List<Token> tokens,  List<Mapping> mappings, JCas jcas) {
		FSArray arr = new FSArray(jcas, mappings.size());
		int i = 0;
		for (Mapping m : mappings) {
			Token t = tokens.get(m.getFirstPos());
			arr.set(i, t);
			i++;
		}
		return arr;
	}
	
	public static FSArray toMappingArray(List<Token> firstTokens, List<Token> secondTokens,
			List<Mapping> mappings, JCas jcas) {
		FSArray arr = new FSArray(jcas, mappings.size());
		int i = 0;
		for (Mapping m : mappings) {
			Token first = firstTokens.get(m.getFirstPos());
			Token second = secondTokens.get(m.getSecondPos());
			TokenMapping mapping = new TokenMapping(jcas);
			mapping.setFirst(first);
			mapping.setSecond(second);
			arr.set(i, mapping);
			i++;
		}
		return arr;
	}
	
	public static String getError(Agreement a) {
		if ("error".equals(a.getPerson())) {
			return "person";
		} else if ("error".equals(a.getNumber())) {
			return "number";
		} else if ("error".equals(a.getGender())) {
			return "gender";
		} else {
			return null;
		}
	}
}
