package de.tuebingen.sfs.tagarela.util;

/**
 * Holds constants for string matching so that we don't have magic
 * strings in the code.
 * @author rziai
 *
 */
public interface StringMatchConstants {

	public static final String EXACT_MATCH = "ok";
	public static final String LOWERCASE_MATCH = "ok-low";
	public static final String SPACE_MATCH = "ok-space";
	public static final String NOPUNC_MATCH = "ok-noP";
	public static final String LOWERCASE_NOPUNC_MATCH = "ok-low-noP";
	public static final String NO_MATCH = "no-match";
	// NEW RUSSIAN CODE
	public static final String NO_INPUT = "no-input";
}
