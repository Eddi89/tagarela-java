package de.tuebingen.sfs.tagarela.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.sleepycat.db.Cursor;
import com.sleepycat.db.Database;
import com.sleepycat.db.DatabaseConfig;
import com.sleepycat.db.DatabaseEntry;
import com.sleepycat.db.DatabaseException;
import com.sleepycat.db.Environment;
import com.sleepycat.db.EnvironmentConfig;
import com.sleepycat.db.LockMode;
import com.sleepycat.db.OperationStatus;
import com.sleepycat.db.Sequence;
import com.sleepycat.db.SequenceConfig;

/**
 * Convenience class for accessing a Berkeley DB.
 * @author rziai
 *
 */
public class BerkeleyDB {
	
	private static final Logger logger = Logger.getLogger("BerkeleyDB");

	protected Database db;

	public BerkeleyDB(String dbFileName) throws FileNotFoundException, DatabaseException {	 
		this.db = new Database(dbFileName, null, null);
		
	}

	public JSONObject getObject(String key) throws Exception {
		DatabaseEntry valueEntry = getValueEntry(key);
		return valueEntry != null ? new JSONObject(new String(valueEntry.getData(), "UTF-8"))
								: null;
	}
	
	public List<JSONObject> getObjects(String key) throws Exception {
		List<JSONObject> jsonObjectList = new ArrayList<JSONObject>();
		DatabaseEntry valueEntry = getValueEntry(key);
		if(valueEntry != null){
			String valueString = new String(valueEntry.getData(), "UTF-8");
			String[] valueEntries = valueString.split("\t");
			for(String entry: valueEntries){
				jsonObjectList.add(new JSONObject(entry));
			}
		}
		return jsonObjectList;
	}

	public String getString(String key) throws Exception {
		DatabaseEntry valueEntry = getValueEntry(key);
		return valueEntry != null ? new String(valueEntry.getData(), "UTF-8") : null;
	}

	public int getInt(String key) throws Exception {
		DatabaseEntry valueEntry = getValueEntry(key);
		return valueEntry != null ? new Integer(new String(valueEntry.getData(), "UTF-8")) : 0;
	}
	
	public JSONArray getArray(String key) throws Exception {
		DatabaseEntry valueEntry = getValueEntry(key);
		return valueEntry != null ? new JSONArray(new String(valueEntry.getData(), "UTF-8"))
								: null;
	}
	
	private DatabaseEntry getValueEntry(String key) throws Exception {
		DatabaseEntry keyEntry = new DatabaseEntry(key.getBytes("UTF-8"));
		DatabaseEntry valueEntry = new DatabaseEntry();
		OperationStatus status = db.get(null, keyEntry, valueEntry, null);
		
		return status.equals(OperationStatus.SUCCESS) ? valueEntry : null;
	}

	public boolean put(String key, Object value) throws Exception {
		byte[] keyBytes = key.getBytes("UTF-8");
		byte[] valueBytes = value.toString().getBytes("UTF-8");
		DatabaseEntry prevValueEntry = null;
		
		// retrieve the previous data for this key, if any
	    DatabaseEntry theData = getValueEntry(key);
	    if(theData != null){
	        String dataString = new String(theData.getData());
	        logger.info("Previous Key | Data : " +  key + " | " + dataString + "");
	        //its a JSONObject
	        if(dataString.contains("{")){
		        prevValueEntry = new DatabaseEntry(theData.getData());		            	
	        }
	    }
	    
		DatabaseEntry keyEntry = new DatabaseEntry(keyBytes);
		DatabaseEntry valueEntry = new DatabaseEntry(valueBytes);
		if(prevValueEntry != null){

	        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
	        outputStream.write(prevValueEntry.getData());
	        outputStream.write("\t".getBytes("UTF-8"));
	        outputStream.write(valueBytes);

	        byte newValue[] = outputStream.toByteArray( );
			keyEntry = new DatabaseEntry(key.getBytes("UTF-8"));
			valueEntry = new DatabaseEntry(newValue);
		}
        String keyString = new String(keyEntry.getData());
        String dataString = new String(valueEntry.getData());
		logger.info("New Key | Data : " +  keyString + " | " + dataString + "");
		OperationStatus status =  db.put(null, keyEntry, valueEntry);
		if (status.equals(OperationStatus.SUCCESS)) {
			db.sync();
			return true;
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		db.close();
	}
}