package de.tuebingen.sfs.tagarela.servlet;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.ResultSpecification;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.util.XMLInputSource;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONWriter;

import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.activity.BestAnswerFinder;
import de.tuebingen.sfs.tagarela.activity.LevenshteinAnswerFinder;
import de.tuebingen.sfs.tagarela.activity.ActivityModel.Target;
import de.tuebingen.sfs.tagarela.feedback.AdaptiveDiagnosisManager;
import de.tuebingen.sfs.tagarela.feedback.DiagnosisManager;
import de.tuebingen.sfs.tagarela.learner.LearnerModel;

/**
 * Servlet implementation class AnalysisServlet. This is the entry
 * point of the ILTS.
 */
public class AnalysisServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = Logger.getLogger("AnalysisServlet");
	
	// you can configure the logger here :
	// tagarela-java/src/main/webapp/WEB-INF/classes/log4j.properties
	
	private static final String descriptorName = "fixedFlowPipeline.xml";
	private final HashMap<String, Object> aeConfig = new HashMap<String, Object>();
	private String dbRoot;
	

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	@Override
	public void init() throws ServletException {
		String dictionaryBase = getServletContext().getRealPath("/WEB-INF/classes/dict") + "/pt_BR";
		aeConfig.put("dictionaryBase", dictionaryBase);
		logger.info("dictionaryBase=" + aeConfig.get("dictionaryBase"));
		aeConfig.put("grammarLoc", "/parser/AmaralGrammar.txt");
		logger.info("grammarLoc=" + aeConfig.get("grammarLoc"));
		dbRoot = getServletContext().getRealPath("/databases");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// retrieve parameters from request
		String actType = request.getParameter("actType");
		int modNr = Integer.parseInt(request.getParameter("modNr"));
		int exNr = Integer.parseInt(request.getParameter("exNr"));
		int questNr = Integer.parseInt(request.getParameter("questNr"));
		String userID = request.getParameter("userID");
		
		// construct learner model database filename
		String lmName = dbRoot + File.separator + userID + ".bsddb";
		
		logger.info("Learner model database filename=" + lmName);
		
		logger.info("Servlet called with parameters: " + actType + ", " +
				modNr + ", " + exNr + ", " + questNr + ", " + userID);
		
		// get activity model
		URL activityURL = getServletContext().getResource("/" + actType + "/Module" +
				modNr + "/" + exNr + "/act.xml");
		logger.info("activityURL=" + activityURL);
		ActivityModel actModel = new ActivityModel(activityURL.openStream());
		logger.info("actModel=" + actModel.getPresentation().getL1Instructions());
		
		try {
			// construct learnermodel
			LearnerModel lm = new LearnerModel(lmName);
			// decode learner input data from JSON
			JSONObject inputData = new JSONObject(request.getParameter("ajaxArg"));
			logger.info("inputData=" + inputData);
			String modelKey = actType + '-' + modNr + '-' + exNr + '-' + questNr;
			logger.info("modelKey=" + modelKey);
			lm.put(modelKey, inputData);
			// load UIMA descriptor
			XMLInputSource desc =
				new XMLInputSource(getClass().getClassLoader().getResource(descriptorName));
			logger.info("desc=" + desc.getURL());
			final ResourceSpecifier desc_spec =
                UIMAFramework.getXMLParser().parseResourceSpecifier(desc);
			// construct analysis engine
			AnalysisEngine ae = UIMAFramework.produceAnalysisEngine(desc_spec);
			// set engine parameters
			configEngine(ae, aeConfig);
			// create the CAS from the info we have
			JCas jcas = createJCas(inputData, actModel, questNr, ae);
			//DiagnosisManager fbManager = new TAGARELADiagnosisManager();
			
			DiagnosisManager fbManager = new AdaptiveDiagnosisManager();
			ResultSpecification spec = fbManager.getResultSpecification(actModel, jcas.getTypeSystem());
			// PROCESSING
			ae.process(jcas, spec);
			
			// all preliminary, in the future we might want to use the learner model during
			// processing already
			
			// obtain feedback
			String feedbackText = feedbackText(jcas, actModel, lm, fbManager,
					actType, modNr, exNr, questNr);
			logger.info("feedbackText=" + feedbackText);
			// write response
			response.setHeader("X-AJAX-Update", "True");
			JSONObject feedback = new JSONObject();
			feedback.put("FeedbackText", feedbackText);
			response.setCharacterEncoding("UTF-8");
			new JSONWriter(response.getWriter()).object()
					.key("status").value(0)
					.key("contents").value(feedback)
					.endObject();
			
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/*
	 * helper for creating a CAS dynamically from the input we have
	 */
	@SuppressWarnings("unchecked")
	private JCas createJCas(JSONObject inputData, ActivityModel actModel, int questNr,
			AnalysisEngine ae) throws CASException, ResourceInitializationException, JSONException {
		JCas jcas = ae.newJCas();
		//JCas learnerView = jcas.createView("learnerInput");
		String learnerInput;
		
		
		if (inputData.length() > 1) {
			Iterator<String> keyIter = inputData.sortedKeys();
			learnerInput = "";
			// for each input item, append to learner input
			while (keyIter.hasNext()) {
				// 
				learnerInput += inputData.get(keyIter.next()).toString() + " ";
			}
			learnerInput = learnerInput.trim();
		} else {
			learnerInput = inputData.getString("learnerInput");
		}
		jcas.setDocumentText(learnerInput);
		logger.info("learnerInput=" + learnerInput);
		
		BestAnswerFinder baFinder = new LevenshteinAnswerFinder();
		ArrayList<String> answers = new ArrayList<String>();
		
		for (Target t : actModel.getQuestions().get(questNr-1).getTargets()) {
			answers.add(t.getTargetStr());
			
			// TODO required words: where do we put them? (DocumentAnnotation)
		}
		logger.info("answers=" + answers);
		
		String bestAnswer = baFinder.getBestAnswer(answers, learnerInput);
		logger.info("bestAnswer=" + bestAnswer);
		JCas newView = jcas.createView("bestAnswer");
		newView.setDocumentText(bestAnswer);
		
		// return the multi-view CAS
		return jcas;
	}
	
	/*
	 * taken from the new WERTi by A. Dimitrov, adapted
	 */
	private static void configEngine(AnalysisEngine ae, Map<String, Object> config) {
        if (config == null) {
            logger.warn("No configuration found for " + ae.getClass().getName());
            return;
        }

        logger.info("Configuring " + ae.getClass().getName());
        for (String s : config.keySet()) {
            logger.info("Configuring: " + s + " with " + config.get(s));
            ae.setConfigParameterValue(s, config.get(s));
        }
    }
	
	/*
	 * helper method that constructs the feedback text
	 */
	private String feedbackText(JCas jcas, ActivityModel actModel, LearnerModel lm,
			DiagnosisManager manager, String actType, int modNr, int exNr, int questNr) throws CASException {
		String result = "<div id='FeedbackTextUnit'>";
		String showSolution = new Formatter().format("<p><i>To see a possible answer, click <a href=\"#\" " +
				"onclick=\"postJSON('../ajax.py/showSolution?actType=%s&amp;modNr=%s&amp;exNr=%s&amp;questNr=%s', " +
				" {'showSolution':'%s'});return false;\">here</a>.</i></p>",
				new Object[]{ actType, modNr, exNr, questNr, jcas.getView("bestAnswer").getDocumentText()}).toString();
		result += "<i>Input:</i> <b>" + jcas.getDocumentText() + "</b>";
		// diagnosis manager is called here
		result += "<p>" +  manager.feedback(jcas, actModel, lm).messageString() + "</p>";
		result += showSolution;
		result += "</div";
		return result;
	}
	
}
