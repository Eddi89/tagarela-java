package de.tuebingen.sfs.tagarela.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.ResultSpecification;
import org.apache.uima.cas.CASException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.ResourceSpecifier;
import org.apache.uima.util.XMLInputSource;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONWriter;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.UnsafeInput;
import com.sleepycat.db.DatabaseEntry;

import de.tuebingen.sfs.tagarela.activity.ActivityModel;
import de.tuebingen.sfs.tagarela.activity.BestAnswerFinder;
import de.tuebingen.sfs.tagarela.activity.LevenshteinAnswerFinder;
import de.tuebingen.sfs.tagarela.activity.ActivityModel.Target;
import de.tuebingen.sfs.tagarela.feedback.DiagnosisManager;
import de.tuebingen.sfs.tagarela.feedback.FeedbackMessage;
import de.tuebingen.sfs.tagarela.feedback.FeedbackResult;
import de.tuebingen.sfs.tagarela.feedback.RusAdaptiveDiagnosisManager;
import de.tuebingen.sfs.tagarela.learner.LearnerModel;
import de.tuebingen.sfs.tagarela.types.CGToken;

/**
 * Servlet implementation class AnalysisServlet. This is the entry
 * point of the ILTS.
 */
public class RusAnalysisServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = Logger.getLogger("RusAnalysisServlet");
	
	// you can configure the logger here :
	// tagarela-java/src/main/webapp/WEB-INF/classes/log4j.properties
	
	private static final String descriptorName = "/rus_desc/rusFixedFlowPipeline.xml";
	private final HashMap<String, Object> aeConfig = new HashMap<String, Object>();
	private String dbRoot;
	

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	@Override
	public void init() throws ServletException {
		dbRoot = getServletContext().getRealPath("/databases");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// retrieve parameters from request
		String actType = request.getParameter("actType");
		int modNr = Integer.parseInt(request.getParameter("modNr"));
		int exNr = Integer.parseInt(request.getParameter("exNr"));
		int questNr = Integer.parseInt(request.getParameter("questNr"));
		String userID = request.getParameter("userID");
		
		// construct learner model database filename
		String lmName = dbRoot + File.separator + userID + ".bsddb";
		
		logger.info("Learner model database filename=" + lmName);
		
		logger.info("Servlet called with parameters: " + actType + ", " +
				modNr + ", " + exNr + ", " + questNr + ", " + userID);
		
		// get activity model
		URL activityURL = getServletContext().getResource("/" + actType + "/Module" +
				modNr + "/" + exNr + "/act.xml");
		logger.info("activityURL=" + activityURL);
		ActivityModel actModel = new ActivityModel(activityURL.openStream());
		logger.info("actModel=" + actModel.getPresentation().getL1Instructions());
		
		try {
			// construct learnermodel
			LearnerModel lm = new LearnerModel(lmName);
			// decode learner input data from JSON
			JSONObject inputData = new JSONObject(request.getParameter("ajaxArg"));
			logger.info("inputData=" + inputData);
//			String modelKey = actType + '-' + modNr + '-' + exNr + '-' + questNr;
//			logger.info("modelKey=" + modelKey);
//			lm.put(modelKey, inputData);
			// load UIMA descriptor
			XMLInputSource desc =
				new XMLInputSource(getClass().getClassLoader().getResource(descriptorName));
			final ResourceSpecifier desc_spec =
                UIMAFramework.getXMLParser().parseResourceSpecifier(desc);
			// construct analysis engine
			AnalysisEngine ae = UIMAFramework.produceAnalysisEngine(desc_spec);
			// set engine parameters
			configEngine(ae, aeConfig);
			// create the CAS from the info we have
			JCas jcas = createJCas(inputData, actModel, questNr, ae);
			//DiagnosisManager fbManager = new TAGARELADiagnosisManager();
			
			DiagnosisManager fbManager = new RusAdaptiveDiagnosisManager();
			ResultSpecification spec = fbManager.getResultSpecification(actModel, jcas.getTypeSystem());
			// PROCESSING
			ae.process(jcas, spec);
			
			// all preliminary, in the future we might want to use the learner model during
			// processing already
			
			// obtain feedback
			String feedbackText = feedbackText(jcas, actModel, lm, fbManager,
					actType, modNr, exNr, questNr, inputData);
			logger.info("feedbackText=" + feedbackText);
			// write response
			response.setHeader("X-AJAX-Update", "True");
			JSONObject feedback = new JSONObject();
			feedback.put("FeedbackText", feedbackText);
			response.setCharacterEncoding("UTF-8");
			new JSONWriter(response.getWriter()).object()
					.key("status").value(0)
					.key("contents").value(feedback)
					.endObject();
			
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

	/*
	 * helper for creating a CAS dynamically from the input we have
	 */
	@SuppressWarnings("unchecked")
	private JCas createJCas(JSONObject inputData, ActivityModel actModel, int questNr,
			AnalysisEngine ae) throws CASException, ResourceInitializationException, JSONException {
		JCas jcas = ae.newJCas();
		//JCas learnerView = jcas.createView("learnerInput");
		String learnerInput;
		
		
		if (inputData.length() > 1) {
			Iterator<String> keyIter = inputData.sortedKeys();
			learnerInput = "";
			// for each input item, append to learner input
			while (keyIter.hasNext()) {
				// 
				learnerInput += inputData.get(keyIter.next()).toString() + " ";
			}
			learnerInput = learnerInput.trim();
		} else {
			learnerInput = inputData.getString("learnerInput");
		}
		jcas.setDocumentText(learnerInput);
		logger.info("learnerInput=" + learnerInput);
		
		BestAnswerFinder baFinder = new LevenshteinAnswerFinder();
		ArrayList<String> answers = new ArrayList<String>();
		
		Map<String, String[]> targetInfoMap = new HashMap<String, String[]>();
				
		String distractors = "";
		
		int counter = 0;
		
		for (Target t : actModel.getQuestions().get(questNr-1).getTargets()) {
			String[] targetInfo = t.getTargetStr().split("#");
			String target = targetInfo[0].replaceAll("\u0301", ""); // remove stress
			targetInfoMap.put(target, targetInfo);
			answers.add(target); 
						
			if(counter == 0){
				for(String distractor: t.getReqWords()){
					if(distractors.isEmpty()){
						distractors += distractor;
					}
					else{
						distractors += "\t" + distractor;
					}				
				}
			}
			
			counter++;
			
			// TODO required words: where do we put them? (DocumentAnnotation)
		}
		
		logger.info("answers=" + answers);
		
		JCas distractorsView = jcas.createView("distractors");
		distractorsView.setDocumentText(distractors);
		logger.info("distractors=" + distractors);
				
		String bestAnswer = baFinder.getBestAnswer(answers, learnerInput);
				
		logger.info("bestAnswer=" + bestAnswer);
		JCas answerView = jcas.createView("bestAnswer");
		answerView.setDocumentText(bestAnswer);		

		String[] targetInfo = targetInfoMap.get(bestAnswer);
		
		String answerWithStress = targetInfo[0];
		String answerLemma = targetInfo[1];
		String answerContextLeft = targetInfo[2];
		String answerContextRight = targetInfo[3];
		String answerPosition = targetInfo[4];
		
		JCas answerWithStressView = jcas.createView("answerWithStress");
		answerWithStressView.setDocumentText(answerWithStress);
		logger.info("answerWithStress=" + answerWithStress);
		
		JCas answerLemmaView = jcas.createView("bestAnswerLemma");
		answerLemmaView.setDocumentText(answerLemma);
		logger.info("bestAnswerLemma=" + answerLemma);
		
		JCas fullAnswerView = jcas.createView("fullAnswer");
		String fullAnswer = answerContextLeft + learnerInput + answerContextRight;
		fullAnswerView.setDocumentText(fullAnswer);
		logger.info("fullAnswer=" + fullAnswer);
		
		JCas answerPositionView = jcas.createView("answerPosition");
		answerPositionView.setDocumentText(answerPosition);
		logger.info("answerPosition=" + answerPosition);
		
		// return the multi-view CAS
		return jcas;
	}
	
	/*
	 * taken from the new WERTi by A. Dimitrov, adapted
	 */
	private static void configEngine(AnalysisEngine ae, Map<String, Object> config) {
        if (config == null) {
            logger.warn("No configuration found for " + ae.getClass().getName());
            return;
        }

        logger.info("Configuring " + ae.getClass().getName());
        for (String s : config.keySet()) {
            logger.info("Configuring: " + s + " with " + config.get(s));
            ae.setConfigParameterValue(s, config.get(s));
        }
    }
	
	/*
	 * helper method that constructs the feedback text
	 */
	private String feedbackText(JCas jcas, ActivityModel actModel, LearnerModel lm,
			DiagnosisManager manager, String actType, int modNr, int exNr, int questNr, JSONObject inputData) throws Exception {
		
		String result = "<div id='FeedbackTextUnit'>";
		String showSolution = new Formatter().format("<p><i>To see a possible answer, click <a href=\"#\" " +
				"onclick=\"postJSON('../ajax.py/showSolution?actType=%s&amp;modNr=%s&amp;exNr=%s&amp;questNr=%s', " +
				" {'showSolution':'%s'});return false;\">here</a>.</i></p>",
				new Object[]{ actType, modNr, exNr, questNr, jcas.getView("bestAnswer").getDocumentText()}).toString();
		result += "<i>Input:</i> <b>" + jcas.getDocumentText() + "</b>";
		// diagnosis manager is called here
		FeedbackResult fbResult = manager.feedback(jcas, actModel, lm);
		FeedbackMessage fbMessage = fbResult.getMessage();
		if(fbResult.getMessage().getError() != null){ // there was an error
			String type = fbMessage.getError().toString();
			logger.info("feedbackType="+type);
			inputData.put("type", type);	
			String subType = fbMessage.name();
			logger.info("feedbackSubType="+subType);
			inputData.put("subType", subType);		
		}
		else{// correct answer
			String type = "CORRECT";
			logger.info("feedbackType="+type);
			inputData.put("type", type);	
			String subType = fbMessage.name();
			logger.info("feedbackSubType="+subType);
			inputData.put("subType", subType);
		}
		String modelKey = actType + '-' + modNr + '-' + exNr + '-' + questNr;
		logger.info("modelKey=" + modelKey);
		String learnerInputField = inputData.getString("learnerInput");
		if(learnerInputField.isEmpty()){
			inputData.remove("learnerInput");
		}
		logger.info("input Data="+ inputData);
		lm.put(modelKey, inputData);
		result += "<p>" +  fbResult.messageString() + "</p>";
		result += showSolution;		

		List<String> types = new ArrayList<String>();
		List<String> subTypes = new ArrayList<String>();
		
		types.add("WORDCHOICE");
//		subTypes.add("wrongWordChoice");
		
		types.add("WORDFORM");
		subTypes.add("wrongCase");
		subTypes.add("wrongNumber");
		subTypes.add("wrongCaseAndNumber");
		
		types.add("SPELLING");
		subTypes.add("wrongSpelling");
		subTypes.add("wrongSpellingUnstressedO");
		subTypes.add("wrongSpellingUnstressedE");
		
		types.add("MISSING_CONCEPTS");
//		subTypes.add("missing3");
		
		types.add("CORRECT");
//		subTypes.add("strM8");
		
		types.add("CAPITALIZATION");
//		subTypes.add("strM2");
		
		types.add("PUNCTUATION");
//		subTypes.add("strM3");
	    
	    String statistics = "<div id=\"statistics\" style=\"display: none\">";
	    Map<String, String> beautifiedStatisticsMap = beautifyStatistics();
	    
	    for(int l = 1; l <= 1; l++){ // 1 is hard coded
	    	int moduleNr = l;

		    modelKey = actType + '-' + moduleNr + '-' + exNr + '-' + questNr;
		    logger.info("module model key="+modelKey);

		    String moduleStatisticsButton = "<button id=\"mod"+moduleNr+"-btn\" class=\"show\" onclick=\"showStatistics(this)\">Show Statistics for Module "+moduleNr+"</button><br>";
		    
		    String moduleStatistics = moduleStatisticsButton+"<div style=\"display: none\" id=\"mod"+moduleNr+"\" class=\"moduleStatistics";
		    if(modNr == moduleNr){
		    	moduleStatistics += " current\" >";
		    }
		    else{
		    	moduleStatistics += "\" >";
		    }
			
			Map<String, Integer> moduleStatisticsMap = initializeStatistics();
//			
			
			String allExerciseStatistics = "";
	    
		    for(int k = 1; k <= 2; k++){ // 2 is hard coded
		    	int exerciseNr = k;
	
			    modelKey = actType + '-' + moduleNr + '-' + exerciseNr + '-' + questNr;
			    logger.info("exercise model key="+modelKey);
			    
			    String exerciseStatisticsButton = "<button id=\"ex"+exerciseNr+"-btn\" class=\"show\" onclick=\"showStatistics(this)\">Show Statistics for Exercise "+exerciseNr+"</button><br>";
			    
			    String exerciseStatistics = exerciseStatisticsButton+"<div style=\"display: none\" id=\"ex"+exerciseNr+"\" class=\"exerciseStatistics";
			    if(exNr == exerciseNr){
			    	exerciseStatistics += " current\" >";
			    }
			    else{
			    	exerciseStatistics += "\" >";
			    }
				
				Map<String, Integer> exerciseStatisticsMap = initializeStatistics();

				// get activity model
				URL activityURL = getServletContext().getResource("/" + actType + "/Module" +
						modNr + "/" + exerciseNr + "/act.xml");
				logger.info("activityURL=" + activityURL);
				actModel = new ActivityModel(activityURL.openStream());
				
				String allQuestionsStatistics = "";
			    
			    // generate statistics for all questions
			    for(int i = 1; i <= actModel.getQuestions().size(); i++){
			    	int questionNr = i;
	
				    modelKey = actType + '-' + moduleNr + '-' + exerciseNr + '-' + questionNr;
				    
				    String questionStatisticsButton = "<button id=\"quest"+questionNr+"-btn\" class=\"show\" onclick=\"showStatistics(this)\">Show Statistics for Question "+questionNr+"</button><br>";
				    
				    String questionStatistics = questionStatisticsButton+"<div style=\"display: none\" id=\"quest"+questionNr+"\" class=\"questionStatistics";
				    if(exNr == exerciseNr && questNr == questionNr){
				    	questionStatistics += " current\" >";
				    }
				    else{
				    	questionStatistics += "\" >";
				    }
					
					Map<String, Integer> questionStatisticsMap = initializeStatistics();
					
					// retrieve the previous data for this key, if any
				    List<JSONObject> jsonObjectList = lm.getObjects(modelKey);
				    for(int j = 0; j < jsonObjectList.size(); j++){
				    	JSONObject entry = jsonObjectList.get(j);
				    	String type = entry.getString("type");
				    	questionStatisticsMap.put(type, questionStatisticsMap.get(type)+1);
				    	exerciseStatisticsMap.put(type, exerciseStatisticsMap.get(type)+1);
				    	moduleStatisticsMap.put(type, moduleStatisticsMap.get(type)+1);
				    	String subType = entry.getString("subType");
				    	if(questionStatisticsMap.containsKey(subType)){
					    	questionStatisticsMap.put(subType, questionStatisticsMap.get(subType)+1);	 
					    	exerciseStatisticsMap.put(subType, exerciseStatisticsMap.get(subType)+1);   
					    	moduleStatisticsMap.put(subType, moduleStatisticsMap.get(subType)+1);		
				    	}
				    }
				    
				    for(Entry<String, Integer> entry: questionStatisticsMap.entrySet()){
				    	String key = entry.getKey();
				    	if(types.contains(key)){
				    		if(key.equals("WORDFORM")||key.equals("SPELLING")){
				    			questionStatistics += "<div class=\"type\">";
							    key = beautifiedStatisticsMap.get(key);
							    questionStatistics += key + ": " + entry.getValue()+"<br>";
				    		}
				    		else{
				    			questionStatistics += "<div class=\"type\">";
							    key = beautifiedStatisticsMap.get(key);
							    questionStatistics += key + ": " + entry.getValue()+"<br>";
							    questionStatistics += "</div>";
				    		}
				    	}
				    	if(subTypes.contains(key)){
				    		if(key.equals("wrongCase")||key.equals("wrongNumber")||
				    				key.equals("wrongSpelling")||key.equals("wrongSpellingUnstressedO")){
				    			questionStatistics += "<div class=\"subType\">";
							    key = beautifiedStatisticsMap.get(key);
							    questionStatistics += key + ": " + entry.getValue()+"<br>";
				    		}
				    		else{
				    			questionStatistics += "<div class=\"subType\">";
							    key = beautifiedStatisticsMap.get(key);
							    questionStatistics += key + ": " + entry.getValue()+"<br>";
							    questionStatistics += "</div>";
				    		}
						    questionStatistics += "</div>";
				    	}
				    }
				    questionStatistics += "</div>";
				    allQuestionsStatistics += questionStatistics;
			    }
			    
			    for(Entry<String, Integer> entry: exerciseStatisticsMap.entrySet()){
			    	String key = entry.getKey();
			    	if(types.contains(key)){
			    		if(key.equals("WORDFORM")||key.equals("SPELLING")){
			    			exerciseStatistics += "<div class=\"type\">";
						    key = beautifiedStatisticsMap.get(key);
						    exerciseStatistics += key + ": " + entry.getValue()+"<br>";
			    		}
			    		else{
			    			exerciseStatistics += "<div class=\"type\">";
						    key = beautifiedStatisticsMap.get(key);
						    exerciseStatistics += key + ": " + entry.getValue()+"<br>";
						    exerciseStatistics += "</div>";
			    		}
			    	}
			    	if(subTypes.contains(key)){
			    		if(key.equals("wrongCase")||key.equals("wrongNumber")||
			    				key.equals("wrongSpelling")||key.equals("wrongSpellingUnstressedO")){
			    			exerciseStatistics += "<div class=\"subType\">";
						    key = beautifiedStatisticsMap.get(key);
						    exerciseStatistics += key + ": " + entry.getValue()+"<br>";
			    		}
			    		else{
			    			exerciseStatistics += "<div class=\"subType\">";
						    key = beautifiedStatisticsMap.get(key);
						    exerciseStatistics += key + ": " + entry.getValue()+"<br>";
						    exerciseStatistics += "</div>";
			    		}
					    exerciseStatistics += "</div>";
			    	}
			    }
			    exerciseStatistics += allQuestionsStatistics;
			    exerciseStatistics += "</div>";
			    allExerciseStatistics += exerciseStatistics;
		    }
		    
		    for(Entry<String, Integer> entry: moduleStatisticsMap.entrySet()){
		    	String key = entry.getKey();
		    	if(types.contains(key)){
		    		if(key.equals("WORDFORM")||key.equals("SPELLING")){
		    			moduleStatistics += "<div class=\"type\">";
					    key = beautifiedStatisticsMap.get(key);
				    	moduleStatistics += key + ": " + entry.getValue()+"<br>";
		    		}
		    		else{
			    		moduleStatistics += "<div class=\"type\">";
					    key = beautifiedStatisticsMap.get(key);
				    	moduleStatistics += key + ": " + entry.getValue()+"<br>";
				    	moduleStatistics += "</div>";
		    		}
		    	}
		    	if(subTypes.contains(key)){
		    		if(key.equals("wrongCase")||key.equals("wrongNumber")||
		    				key.equals("wrongSpelling")||key.equals("wrongSpellingUnstressedO")){
			    		moduleStatistics += "<div class=\"subType\">";
					    key = beautifiedStatisticsMap.get(key);
				    	moduleStatistics += key + ": " + entry.getValue()+"<br>";
		    		}
		    		else{
		    			moduleStatistics += "<div class=\"subType\">";
					    key = beautifiedStatisticsMap.get(key);
				    	moduleStatistics += key + ": " + entry.getValue()+"<br>";
				    	moduleStatistics += "</div>";
		    		}
			    	moduleStatistics += "</div>";
		    	}
		    }
		    moduleStatistics += allExerciseStatistics;
		    moduleStatistics += "</div>";
		    statistics += moduleStatistics;
	    }	    

	    statistics += "</div>";
	    result += statistics;

		result += "</div";
		return result;
	}
	
	private Map<String, Integer> initializeStatistics(){
		Map<String, Integer> statisticsMap = new LinkedHashMap<String, Integer>();
		
//		WORDCHOICE
//		wrongWordChoice
//
//		WORDFORM
//			wrongCase
//			wrongNumber
//			wrongCaseAndNumber
//	
//		SPELLING
//			wrongSpelling
//			wrongSpellingUnstressedO
//			wrongSpellingUnstressedE
//	
//		MISSING_CONCEPTS
//			missing3
//	
//		CORRECT
//			strM8
//	
//		CAPITALIZATION
//			strM2
//	
//		PUNCTUATION
//			strM3
		statisticsMap.put("WORDCHOICE", 0);
//		statisticsMap.put("wrongWordChoice", 0);
		
		statisticsMap.put("WORDFORM", 0);
		statisticsMap.put("wrongCase", 0);
		statisticsMap.put("wrongNumber", 0);
		statisticsMap.put("wrongCaseAndNumber", 0);
		
		statisticsMap.put("SPELLING", 0);
		statisticsMap.put("wrongSpelling", 0);
		statisticsMap.put("wrongSpellingUnstressedO", 0);
		statisticsMap.put("wrongSpellingUnstressedE", 0);
		
		statisticsMap.put("MISSING_CONCEPTS", 0);
//		statisticsMap.put("missing3", 0);
		
		statisticsMap.put("CORRECT", 0);
//		statisticsMap.put("strM8", 0);
		
		statisticsMap.put("CAPITALIZATION", 0);
//		statisticsMap.put("strM2", 0);
		
		statisticsMap.put("PUNCTUATION", 0);
//		statisticsMap.put("strM3", 0);
		
		return statisticsMap;
	}
	
	private Map<String, String> beautifyStatistics(){
		Map<String, String> statisticsMap = new LinkedHashMap<String, String>();
		
		statisticsMap.put("WORDCHOICE", "Word choice errors");
		
		statisticsMap.put("WORDFORM", "Word form errors");
		statisticsMap.put("wrongCase", "Case");
		statisticsMap.put("wrongNumber", "Number");
		statisticsMap.put("wrongCaseAndNumber", "Case and number");
		
		statisticsMap.put("SPELLING", "Spelling errors");
		statisticsMap.put("wrongSpelling", "Wrong spelling");
		statisticsMap.put("wrongSpellingUnstressedO", "Unstressed o");
		statisticsMap.put("wrongSpellingUnstressedE", "Unstressed e");
		
		statisticsMap.put("MISSING_CONCEPTS", "Missing word errors");
		
		statisticsMap.put("CORRECT", "Correct answers");
		
		statisticsMap.put("CAPITALIZATION", "Capitalization errors");
		
		statisticsMap.put("PUNCTUATION", "Punctuation errors");
		
		return statisticsMap;
	}
	
}
