package de.tuebingen.sfs.tagarela.resources;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sleepycat.db.DatabaseException;

import de.tuebingen.sfs.tagarela.util.BerkeleyDB;

/**
 * Convenience class for accessing the NILC lexicon.
 * @author rziai
 *
 */
public class NILCLexicon {

	private static final String dbName = "/lexicon/lexiconNilc.bsddb";
	private static NILCLexicon instance;
	
	private BerkeleyDB db;
	
	private NILCLexicon() throws FileNotFoundException, DatabaseException {
		db = new BerkeleyDB(getClass().getResource(dbName).getFile());
	}
	
	public static NILCLexicon getInstance() throws Exception{
		if (instance == null) {
			instance = new NILCLexicon();
		}
		return instance;
	}
	
	/**
	 * Get the entries for the given key.
	 * @param key The string key
	 * @return The lexicon entries, as JSON objects
	 * @throws Exception
	 */
	public List<JSONObject> get(String key) throws Exception {
		ArrayList<JSONObject> result = new ArrayList<JSONObject>();
		JSONArray arr = db.getArray(key);
		if (arr == null) {
			return result;
		}
		for (int i = 0; i < arr.length(); i++) {
			result.add(arr.getJSONObject(i));
		}
		return result;
	}
	
	public static void main(String[] args) {
		try {
			NILCLexicon lexicon = getInstance();
			System.out.println(lexicon.get("abacate"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
