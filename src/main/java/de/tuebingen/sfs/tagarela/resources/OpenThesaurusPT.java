package de.tuebingen.sfs.tagarela.resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OpenThesaurusPT {
	
	public static class Synset implements Iterable<String> {
		private int id;
		private List<String> words;
		/**
		 * @param id
		 * @param words
		 */
		public Synset(int id, List<String> words) {
			this.id = id;
			this.words = words;
		}
		/* (non-Javadoc)
		 * @see java.lang.Iterable#iterator()
		 */
		public Iterator<String> iterator() {
			return words.iterator();
		}
		/**
		 * @return the id
		 */
		public int getId() {
			return id;
		}
		
		public String toString() {
			return id + ": " + words;
		}
		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + id;
			result = prime * result + ((words == null) ? 0 : words.hashCode());
			return result;
		}
		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof Synset))
				return false;
			Synset other = (Synset) obj;
			if (id != other.id)
				return false;
			if (words == null) {
				if (other.words != null)
					return false;
			} else if (!words.equals(other.words))
				return false;
			return true;
		}
		
		
	}

	private static JSONArray synsets;
	
	private static JSONObject words2synsets;
	
	static {
		BufferedReader in = new BufferedReader(new InputStreamReader(
				OpenThesaurusPT.class.getResourceAsStream("/thesaurus/thesaurus.json")));
		String line = null;
		String all = "";
		
		try {
			while ((line = in.readLine()) != null) {
				if (line.equals("")) {
					line = "\n";
				}
				all += line;
			}
			String[] parts = all.split("\n\n");
			synsets = new JSONArray(parts[0]);
			words2synsets = new JSONObject(parts[1]);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Looks up the synsets of a word and returns them in a list.
	 * @param word The word to get synset for
	 * @return A list of synsets, empty if none are found
	 */
	public static List<Synset> getSynsets(String word) {
		ArrayList<Synset> result = new ArrayList<Synset>();
		try {
			JSONArray synIDs = words2synsets.getJSONArray(word);
			
			for (int h = 0; h < synIDs.length(); h++) {
				JSONArray synsetList = synsets.getJSONArray(synIDs.getInt(h));
				ArrayList<String> synset = new ArrayList<String>();
				
				for (int i = 0; i < synsetList.length(); i++) {
					String syn = synsetList.getString(i);
					synset.add(syn);				
				}
				result.add(new Synset(synIDs.getInt(h), synset));
			}
			return result;
		} catch (JSONException e) {
			return result;
		}
	}
}
