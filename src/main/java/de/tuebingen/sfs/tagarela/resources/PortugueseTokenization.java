package de.tuebingen.sfs.tagarela.resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Wraps the specifics or Portuguese tokenization (contractions, encliticizations, abbreviations).
 * @author rziai
 *
 */
public class PortugueseTokenization {

	public static JSONObject prons;
	public static JSONObject prepositions;
	// abbreviations is really a set, but realized as a JSON object with all values being null
	public static JSONObject abbreviations;
	
	static {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(
				PortugueseTokenization.class.getResourceAsStream("portuguesetokenization.json"), "UTF-8"));
		String line = null;
		String all = "";
		
			while ((line = in.readLine()) != null) {
				if (line.equals("")) {
					line = "\n";
				}
				all += line;
			}
			String[] parts = all.split("\n\n");
			prons = new JSONObject(parts[0]);
			prepositions = new JSONObject(parts[1]);
			abbreviations = new JSONObject(parts[2]);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
